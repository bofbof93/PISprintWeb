<?php

namespace TunisiaBundle\StatistiqueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('TunisiaBundleStatistiqueBundle:Default:index.html.twig', array('name' => $name));
    }
}
