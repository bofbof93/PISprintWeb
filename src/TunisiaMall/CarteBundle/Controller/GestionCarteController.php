<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace TunisiaMall\CarteBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use FOS\UserBundle\FOSUserEvents;
    use FOS\UserBundle\Event\FormEvent;
    use FOS\UserBundle\Event\FilterUserResponseEvent;
    use FOS\UserBundle\Event\GetResponseUserEvent;
    use FOS\UserBundle\Model\UserInterface;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\RedirectResponse;
    use Symfony\Component\Security\Core\Exception\AccessDeniedException;
    use TunisiaMall\EntityBundle\Entity\Cartedefidelite;
/**
 * Description of GestionCarte
 *
 * @author DELL
 */
class GestionCarteController extends Controller {
    //put your code here
   public function IndexAction(){
        $em = $this->getDoctrine()->getManager();
        $user=$this->getUser();
        $Cartes = $em->getRepository('EntityBundle:Cartedefidelite')->findAll();
        return $this->render('TunisiaMallCarteBundle:Carte:index.html.twig', array("cartes" => $Cartes,'user'=>$user));

        
    }
    public function AjoutAction(){
       
        $Carte = new Cartedefidelite();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $em = $this->getDoctrine()->getManager();
            $numero = $request->get('numero');
            $nbrepts = $request->get('nbrepoint');
            $enseigne = $em->getRepository("EntityBundle:Enseigne")->findby(array('User'=>$this->getUser()->getId()));
            $use =$request->get('user');
            $u=$em->getRepository("EntityBundle:User")->find($use);
            $Carte->setNbrPtTotale($nbrepts);
            $Carte->setNumero($numero);
            $Carte->setUser($u);
            
           
            $em->persist($Carte);
            $em->flush();

         
       }
         $em = $this->getDoctrine()->getManager();
        $users=$em->getRepository("EntityBundle:User")->findby(array('flagRole'=>3));
        $user= $this->getUser();
        return $this->render('TunisiaMallCarteBundle:Carte:ajout.html.twig',array('users'=>$users,'user'=>$user));
        
        
    }
    
     public function AfficherAction($id){
        
//         $em = $this->getDoctrine()->getManager();
//        $Actualite = $em->getRepository("EntityBundle:Actualite")->find($id);
//
//        $request = $this->get('request');
//
//        if ($request->getMethod() == 'POST') {
//
//            $titre = $request->get('titre');
//            $description = $request->get('description');
//            $Actualite->setDescription($description);
//            $Actualite->setTitre($titre);
//
//            $em->persist($Actualite);
//            $em->flush();
//            return $this->redirectToRoute("tunisia_mall_actualite_List");
//        }
 $em = $this->getDoctrine()->getManager();
        $users=$em->getRepository("EntityBundle:User")->findby(array('flagRole'=>3));
        $user= $this->getUser();
        return $this->render('CarteBundle:Carte:aff.html.twig', array("users" => $users,"user"=>$user));
   
        
        
    }
     public function EditAction($id){
        
        
        
        
    }
      public function UpdateAction($id){
        
        
        
        
    }
    public function AddAction(){
        
    }
      public function DeleteAction(){
        
        
        $em = $this->getDoctrine()->getManager();
        $Actualite = $em->getRepository("EntityBundle:Actualite")->find($id);
        $em->remove($Actualite);
        $em->flush();
        return $this->redirectToRoute("tunisia_mall_actualite_List");
        
        
    }
}
