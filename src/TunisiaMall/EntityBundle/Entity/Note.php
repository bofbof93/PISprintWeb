<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace TunisiaMall\EntityBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 */
class Note {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private  $User ;
    /**
     * @ORM\ManyToOne(targetEntity="Produit")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private  $Produit ;
    
    /**
     * @ORM\Column( type="string", columnDefinition="enum('1','2','3','4','5')")
     */
    private $Note;
    
    function getId() {
        return $this->id;
    }

    function getUser() {
        return $this->User;
    }

    function getProduit() {
        return $this->Produit;
    }

    function getNote() {
        return $this->Note;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setUser($User) {
        $this->User = $User;
    }

    function setProduit($Produit) {
        $this->Produit = $Produit;
    }

    function setNote($Note) {
        $this->Note = $Note;
    }


}
