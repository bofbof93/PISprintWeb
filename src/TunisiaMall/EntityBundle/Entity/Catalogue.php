<?php

namespace TunisiaMall\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * 
 */
class Catalogue {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column( type="string", length=255)
     */
    private $Ref;
    
    /**
     * @ORM\Column( type="string", length=255)
     */
    private $Slogan;
    
    /**
     * @ORM\Column(type="date")
     */
    private $DateDebut;
    /**
     * @ORM\Column(type="date")
     */
    private $DateFin;
/**
     * @ORM\Column(type="text")
     */
    private $Descriptif;
    
    /**
     * @ORM\Column( type="string")
     */
    private $Type;
    /**
     * @ORM\Column(type="integer")
     */
    private $Active;    
     /**
     * @ORM\ManyToOne(targetEntity="Enseigne")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $Enseigne;
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $User;

   

    function getType() {
        return $this->Type;
    }
    
    function setType($Type) {
        $this->Type = $Type;
    }
    function getEnseigne() {
        return $this->Enseigne;
    }

    function setEnseigne($Enseigne) {
        $this->Enseigne = $Enseigne;
    }
    function getId() {
        return $this->id;
    }

    function getRef() {
        return $this->Ref;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setRef($Ref) {
        $this->Ref = $Ref;
    }
    function getDateDebut() {
        return $this->DateDebut;
    }

    function getDateFin() {
        return $this->DateFin;
    }

    function setDateDebut($DateDebut) {
        $this->DateDebut = $DateDebut;
    }

    function setDateFin($DateFin) {
        $this->DateFin = $DateFin;
    }
    function getSlogan() {
        return $this->Slogan;
    }

    function getDescriptif() {
        return $this->Descriptif;
    }

    function setSlogan($Slogan) {
        $this->Slogan = $Slogan;
    }

    function setDescriptif($Descriptif) {
        $this->Descriptif = $Descriptif;
    }

    function getUser() {
        return $this->User;
    }

    function setUser($User) {
        $this->User = $User;
    }

    function getActive() {
        return $this->Active;
    }

    function setActive($Active) {
        $this->Active = $Active;
    }





}
