<?php

namespace TunisiaMall\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * 
 */
class Stock {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $Quantiteproduit;

    /**
     * @ORM\Column(type="date")
     */
    private $DateAjout;

    /**
     * @ORM\ManyToOne(targetEntity="Enseigne")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="SET NULL")

     */
    private $Enseigne;

    /**
     * @ORM\ManyToOne(targetEntity="Produit")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="SET NULL")
     */
    private $Produit;

    function getIdStock() {
        return $this->idStock;
    }

    function getRef() {
        return $this->Ref;
    }

    function getQuantitéproduit() {
        return $this->Quantiteproduit;
    }

    function setRef($Ref) {
        $this->Ref = $Ref;
    }

    function setQuantitéproduit($Quantiteproduit) {
        $this->Quantiteproduit = $Quantiteproduit;
    }

    function getQuantiteproduit() {
        return $this->Quantiteproduit;
    }

    function getResponsableEnseigne() {
        return $this->ResponsableEnseigne;
    }

    function setQuantiteproduit($Quantiteproduit) {
        $this->Quantiteproduit = $Quantiteproduit;
    }

    function setResponsableEnseigne($ResponsableEnseigne) {
        $this->ResponsableEnseigne = $ResponsableEnseigne;
    }

    function getId() {
        return $this->id;
    }

    function getEnseigne() {
        return $this->Enseigne;
    }

    function getProduit() {
        return $this->Produit;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEnseigne($Enseigne) {
        $this->Enseigne = $Enseigne;
    }

    function setProduit($Produit) {
        $this->Produit = $Produit;
    }

    function getDateAjout() {
        return $this->DateAjout;
    }

    function setDateAjout($DateAjout) {
        $this->DateAjout = $DateAjout;
    }

}
