<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace TunisiaMall\EntityBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 */
class CarteBancaire {
  /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
     /**
     * @ORM\Column(type="integer")
     */
    private $Numero;
     /**
     * @ORM\Column(type="string")
     */
    private $MotDePasse;
      /**
     * @ORM\Column(type="float")
     */
    private $Somme;
     /**
     * @ORM\ManyToOne(targetEntity="User")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private  $User ;
    
    function getId() {
        return $this->id;
    }

    function getNumero() {
        return $this->Numero;
    }

    function getMotDePasse() {
        return $this->MotDePasse;
    }

    function getSomme() {
        return $this->Somme;
    }

    function getUser() {
        return $this->User;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNumero($Numero) {
        $this->Numero = $Numero;
    }

    function setMotDePasse($MotDePasse) {
        $this->MotDePasse = $MotDePasse;
    }

    function setSomme($Somme) {
        $this->Somme = $Somme;
    }

    function setUser($User) {
        $this->User = $User;
    }


}
