<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace TunisiaMall\EntityBundle\Entity;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM ;


/**
 * Description of User
 *
 * @author DELL
 */

/**
 * @ORM\Entity 
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser{
    //put your code here
    
    /**
     * @ORM\Id 
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    
   protected $id;
   /**
     *@ORM\Column(type="blob", nullable=true)
     */
   protected $image;
   /**
    * @ORM\Column(type="integer", nullable=true)
    */
       
    /** @ORM\Column(name="facebook_id", type="string", length=255, nullable=true) */
    protected $facebook_id;
    /** @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true) */
    protected $facebook_access_token;
    /** @ORM\Column(name="google_id", type="string", length=255, nullable=true) */
    protected $google_id;
    /** @ORM\Column(name="google_access_token", type="string", length=255, nullable=true) */
    protected $google_access_token;
     /**
    * @ORM\Column(type="integer", nullable=true)
    */
   protected $flagRole;
      /**
    * @ORM\Column(type="integer", nullable=true)
    */
   protected $flagAccepted;
   
   function getFacebookId() {
       return $this->facebook_id;
   }

   function getFacebook_access_token() {
       return $this->facebook_access_token;
   }

   function getGoogle_id() {
       return $this->google_id;
   }

   function getGoogle_access_token() {
       return $this->google_access_token;
   }

   function setFacebookId($facebook_id) {
       $this->facebook_id = $facebook_id;
   }

   function setFacebookAccessToken($facebook_access_token) {
       $this->facebook_access_token = $facebook_access_token;
   }

   function setGoogle_id($google_id) {
       $this->google_id = $google_id;
   }

   function setGoogle_access_token($google_access_token) {
       $this->google_access_token = $google_access_token;
   }

      function getId() {
       return $this->id;
   }
   function getFlagRole() {
       return $this->flagRole;
   }

   function getFlagAccepted() {
       return $this->flagAccepted;
   }

   function setFlagRole($flagRole) {
       $this->flagRole = $flagRole;
   }

   function setFlagAccepted($flagAccepted) {
       $this->flagAccepted = $flagAccepted;
   }

      function getImage() {
       return $this->image;
   }

  
   function setImage($image) {
       $this->image = $image;
   }

      public function __construct() {
       parent::__construct();
   }
}
