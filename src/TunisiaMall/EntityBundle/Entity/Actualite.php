<?php

namespace TunisiaMall\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * 
 */
class Actualite {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
    /**
     *@ORM\Column(type="blob", nullable=true)

     */
    private $Image;
    /**
     *@ORM\Column(type="string",length=255)

     */
    private $Titre;

    /**
     * @ORM\Column(type="text")
     */
    private $Description;
/**
     * @ORM\Column(type="datetime")
     */
    private $date;
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $User;

   
    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

        
    function setTitre($Titre) {
        $this->Titre = $Titre;
    }

        function getImage() {
        return $this->Image;
    }

    function getDescription() {
        return $this->Description;
    }

   

    function getUser() {
        return $this->User;
    }

    function setIdActualite($idActualite) {
        $this->idActualite = $idActualite;
    }

    function setImage($Image) {
        $this->Image = $Image;
    }

    function setDescription($Description) {
        $this->Description = $Description;
    }

    function setUser($User) {
        $this->User = $User;
    }
    function getTitre() {
        return $this->Titre;
    }
    function getDate() {
        return $this->date;
    }

    function setDate($date) {
        $this->date = $date;
    }

 public function __construct() {

        $this->date = new \DateTime('now');
    }


}
