<?php

namespace TunisiaMall\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="TunisiaMall\ReservationPackBundle\Repository\ReservationPackRepository")
 * 
 */
class PackPublicitaire {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string",length=255)
     */
    public $Libelle;

    /**
     * @ORM\Column(type="float")
     */
    public $Prix;

    /**
     * @ORM\Column(type="integer")
     */
    public $Zone;

    /**
     * @ORM\Column(type="string",length=255)
     * 
     */
    private $Description;
    /**
     * @ORM\Column(type="string",length=255)
     * 
     */
    private $Etat;
    function getEtat() {
        return $this->Etat;
    }

    function setEtat($Etat) {
        $this->Etat = $Etat;
    }

        function getId() {
        return $this->id;
    }

    function getLibelle() {
        return $this->Libelle;
    }

    function getDatedebut() {
        return $this->Datedebut;
    }

    function getDatefin() {
        return $this->Datefin;
    }

    function getPrix() {
        return $this->Prix;
    }

    function getZone() {
        return $this->Zone;
    }

    function getImage() {
        return $this->Image;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setLibelle($Libelle) {
        $this->Libelle = $Libelle;
    }

    function setDatedebut($Datedebut) {
        $this->Datedebut = $Datedebut;
    }

    function setDatefin($Datefin) {
        $this->Datefin = $Datefin;
    }

    function setPrix($Prix) {
        $this->Prix = $Prix;
    }

    function setZone($Zone) {
        $this->Zone = $Zone;
    }

    function setImage($Image) {
        $this->Image = $Image;
    }

    function getDescription() {
        return $this->Description;
    }

    function setDescription($Description) {
        $this->Description = $Description;
    }

    function getUser() {
        return $this->User;
    }

    function setUser($User) {
        $this->User = $User;
    }

}
