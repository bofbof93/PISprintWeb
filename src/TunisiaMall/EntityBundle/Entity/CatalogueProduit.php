<?php

namespace TunisiaMall\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * 
 */
class CatalogueProduit {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Catalogue")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private  $Catalogue ;
    /**
     * @ORM\ManyToOne(targetEntity="Stock")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private  $Stock ;
    
    function getCatalogue() {
        return $this->Catalogue;
    }

    
    function setCatalogue($Catalogue) {
        $this->Catalogue = $Catalogue;
    }

   
    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function getStock() {
        return $this->Stock;
    }

    function setStock($Stock) {
        $this->Stock = $Stock;
    }



}
