<?php

namespace TunisiaMall\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * 
 */
class Cartedefidelite {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
      
     * @ORM\Column(type="string",length=255)
     */
    private $Numero;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $NbrPtTotale;
    
    /**

     * @ORM\ManyToOne(targetEntity="Enseigne" )
 * @@ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="SET NULL")
     */
    private  $Enseigne ;
     /**
     * @ORM\ManyToOne(targetEntity="User")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $User;

    
    function getNumero() {
        return $this->Numero;
    }
    
    function getNbrPtTotale() {
        return $this->NbrPtTotale;
    }

    function getEnseigne() {
        return $this->Enseigne;
    }

    

    function setNbrPtTotale($NbrPtTotale) {
        $this->NbrPtTotale = $NbrPtTotale;
    }

    function setEnseigne($Enseigne) {
        $this->Enseigne = $Enseigne;
    }

    function setNumero($Numero) {
        $this->Numero = $Numero;
    }
    function getId() {
        return $this->id;
    }

    function getUser() {
        return $this->User;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setUser($User) {
        $this->User = $User;
    }

}
