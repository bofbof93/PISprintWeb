<?php

namespace TunisiaMall\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * 
 */
class Panier {

   /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string",length=255)
     */
    private $Ref;

    /**
     * @ORM\Column(type="float")
     */
    private $PrixTotal;

      /**
     * @ORM\ManyToOne(targetEntity="User")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $User;
    
    /**
     * @var ArrayCollection $departements
     *
     * @ORM\OneToMany(targetEntity="Achat", mappedBy="panier", cascade={"persist", "remove", "merge"})
     */
    private $achats;
    
    public function __construct() {
        $this->achats = new ArrayCollection();
        
    }
    
   

    function getRef() {
        return $this->Ref;
    }

    function getPrixTotal() {
        return $this->PrixTotal;
    }

   

    function setRef($Ref) {
        $this->Ref = $Ref;
    }

    function setPrixTotal($PrixTotal) {
        $this->PrixTotal = $PrixTotal;
    }
    function getId() {
        return $this->id;
    }

    function getUser() {
        return $this->User;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setUser($User) {
        $this->User = $User;
    }

    /**
     * Add achats
     *
     * @param \TunisiaMall\EntityBundle\Entity\Achat $achats
     * @return Panier
     */
    public function addAchat(\TunisiaMall\EntityBundle\Entity\Achat $achats)
    {
        $this->achats[] = $achats;

        return $this;
    }

    /**
     * Remove achats
     *
     * @param \TunisiaMall\EntityBundle\Entity\Achat $achats
     */
    public function removeAchat(\TunisiaMall\EntityBundle\Entity\Achat $achats)
    {
        $this->achats->removeElement($achats);
    }

    /**
     * Get achats
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAchats()
    {
        return $this->achats;
    }
}
