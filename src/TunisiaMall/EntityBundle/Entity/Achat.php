<?php

namespace TunisiaMall\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * 
 */
class Achat {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string",length=255)
     */
    private $RefC;

    /**
     * @ORM\Column(type="float")
     */
    private $MontantTotal;

    /**
     * @ORM\Column(type="float")
     */
    private $prixTotal;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantite;

    /**
     * @ORM\Column(type="float")
     */
    private $Prix;
    
    /**
     * @var produit $produit
     *
     * @ORM\ManyToOne(targetEntity="Produit", inversedBy="achat", cascade={"persist", "merge"})
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="produit_id", referencedColumnName="id")
     * })
     */
    private $produit;
    
    /**
     * @var produit $produit
     *
     * @ORM\ManyToOne(targetEntity="Panier", inversedBy="achat", cascade={"persist", "merge"})
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="panier_id", referencedColumnName="id")
     * })
     */
    private $panier;




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     * @return PanierProduit
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer 
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set Prix
     *
     * @param float $prix
     * @return PanierProduit
     */
    public function setPrix($prix)
    {
        $this->Prix = $prix;

        return $this;
    }

    /**
     * Get Prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->Prix;
    }

    /**
     * Set PrixTotal
     *
     * @param float $prixTotal
     * @return PanierProduit
     */
    public function setPrixTotal($prixTotal)
    {
        $this->prixTotal = $prixTotal;

        return $this;
    }

    /**
     * Get PrixTotal
     *
     * @return float 
     */
    public function getPrixTotal()
    {
        return $this->prixTotal;
    }

    /**
     * Set produit
     *
     * @param \TunisiaMall\EntityBundle\Entity\Produit $produit
     * @return PanierProduit
     */
    public function setProduit(\TunisiaMall\EntityBundle\Entity\Produit $produit = null)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit
     *
     * @return \TunisiaMall\EntityBundle\Entity\Produit 
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * Set panier
     *
     * @param \TunisiaMall\EntityBundle\Entity\Panier $panier
     * @return PanierProduit
     */
    public function setPanier(\TunisiaMall\EntityBundle\Entity\Panier $panier = null)
    {
        $this->panier = $panier;

        return $this;
    }

    /**
     * Get panier
     *
     * @return \TunisiaMall\EntityBundle\Entity\Panier 
     */
    public function getPanier()
    {
        return $this->panier;
    }

    /**
     * Set RefC
     *
     * @param string $refC
     * @return Achat
     */
    public function setRefC($refC)
    {
        $this->RefC = $refC;

        return $this;
    }

    /**
     * Get RefC
     *
     * @return string 
     */
    public function getRefC()
    {
        return $this->RefC;
    }

    /**
     * Set MontantTotal
     *
     * @param float $montantTotal
     * @return Achat
     */
    public function setMontantTotal($montantTotal)
    {
        $this->MontantTotal = $montantTotal;

        return $this;
    }

    /**
     * Get MontantTotal
     *
     * @return float 
     */
    public function getMontantTotal()
    {
        return $this->MontantTotal;
    }
}
