<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace TunisiaMall\EntityBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * 
 */

class PromotionProduit {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Promotion")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private  $Promotion ;
    /**
     * @ORM\ManyToOne(targetEntity="Produit")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private  $Produit ;
    
    function getId() {
        return $this->id;
    }

    function getPromotion() {
        return $this->Promotion;
    }

    function getProduit() {
        return $this->Produit;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setPromotion($Promotion) {
        $this->Promotion = $Promotion;
    }

    function setProduit($Produit) {
        $this->Produit = $Produit;
    }


}
