<?php

namespace TunisiaMall\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="TunisiaMall\PromotionBundle\Repository\PromotionRepository")
 * 
 */
class Promotion {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    public $DateDebut;

    /**
     * @ORM\Column(type="date")
     */
    public $DateFin;

    /**
     * @ORM\Column(type="float")
     */
    public $Tauxdereduction;

   /**
     *@ORM\Column(type="blob", nullable=true)

     */
    private $Image;
    /**
     * @ORM\Column(type="string",length=255)
     * 
     */
    private $Description;

    /**
     * @ORM\Column( type="string")
     */
    public $Etat;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $User;
    
    

    function getId() {
        return $this->id;
    }

    function getDateDebut() {
        return $this->DateDebut;
    }

    function getDateFin() {
        return $this->DateFin;
    }

    function getTauxdereduction() {
        return $this->Tauxdereduction;
    }

    function getImage() {
        return $this->Image;
    }

    function getDescription() {
        return $this->Description;
    }

    function getEtat() {
        return $this->Etat;
    }

   
    function setId($id) {
        $this->id = $id;
    }

    function setDateDebut($DateDebut) {
        $this->DateDebut = $DateDebut;
    }

    function setDateFin($DateFin) {
        $this->DateFin = $DateFin;
    }

    function setTauxdereduction($Tauxdereduction) {
        $this->Tauxdereduction = $Tauxdereduction;
    }

    function setImage($Image) {
        $this->Image = $Image;
    }

    function setDescription($Description) {
        $this->Description = $Description;
    }

    function setEtat($Etat) {
        $this->Etat = $Etat;
    }

    function getUser() {
        return $this->User;
    }

    function setUser($User) {
        $this->User = $User;
    }



}
