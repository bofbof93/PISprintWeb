<?php

namespace TunisiaMall\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity
 * 
 */
class Enseigne {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string",length=255)
     */
    private $Libelle;

     /**
     * @ORM\Column(type="text")
     */
    private $Description;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $Etage;

    /**
     * @ORM\Column( type="string")
     */
    private $Categorie;

   /**
     * @ORM\Column( type="string",length=255)
     */
    private $TimeOuverture;

    /**
     * @ORM\Column( type="string",length=255)
     */
    private $TimeFermeture;

    /**
     * @ORM\Column( type="string",length=255)
     */
    private $URL;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    private $Image;

    /**
     * @ORM\Column(type="integer")
     */
    private $NBJour;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $User;

    function getId() {
        return $this->id;
    }

    function getLibelle() {
        return $this->Libelle;
    }

    function getEtage() {
        return $this->Etage;
    }

    function getCategorie() {
        return $this->Categorie;
    }

    function getTimeOuverture() {
        return $this->TimeOuverture;
    }

    function getTimeFermeture() {
        return $this->TimeFermeture;
    }

    function getURL() {
        return $this->URL;
    }

    function getImage() {
        return $this->Image;
    }

    function getNBJour() {
        return $this->NBJour;
    }

    function getUser() {
        return $this->User;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setLibelle($Libelle) {
        $this->Libelle = $Libelle;
    }

    function setEtage($Etage) {
        $this->Etage = $Etage;
    }

    function setCategorie($Categorie) {
        $this->Categorie = $Categorie;
    }

    function setTimeOuverture($TimeOuverture) {
        $this->TimeOuverture = $TimeOuverture;
    }

    function setTimeFermeture($TimeFermeture) {
        $this->TimeFermeture = $TimeFermeture;
    }

    function setURL($URL) {
        $this->URL = $URL;
    }

    function setImage($Image) {
        $this->Image = $Image;
    }

    function setNBJour($NBJour) {
        $this->NBJour = $NBJour;
    }

    function setUser($User) {
        $this->User = $User;
    }
    function getDescription() {
        return $this->Description;
    }

    function setDescription($Description) {
        $this->Description = $Description;
    }



}
