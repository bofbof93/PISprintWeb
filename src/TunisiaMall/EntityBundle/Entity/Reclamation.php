<?php

namespace TunisiaMall\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * 
 */
class Reclamation {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string",length=255)
     */
    private $NomPrenom;

    /**
     * @ORM\Column(type="string",length=255)
     */
    private $Email;
     /**
     * @ORM\Column(type="text")
     */
    private $Descriptif;
    /**
     * @ORM\Column(type="text")
     */
    private $Rep;
     /**
     * @ORM\ManyToOne(targetEntity="User")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $UserSource;
      /**
     * @ORM\ManyToOne(targetEntity="User")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $UserDesti;
    
    /**
     * @ORM\Column( type="string", columnDefinition="enum('Resolue', 'Non Resolue')")
     */
    private $Etat; 
    
    function getIdReclamation() {
        return $this->idReclamation;
    }

    function getNomPrenom() {
        return $this->NomPrenom;
    }

    function getEmail() {
        return $this->Email;
    }

    function getDescriptif() {
        return $this->Descriptif;
    }


    function setNomPrenom($NomPrenom) {
        $this->NomPrenom = $NomPrenom;
    }

    function setEmail($Email) {
        $this->Email = $Email;
    }

    function setDescriptif($Descriptif) {
        $this->Descriptif = $Descriptif;
    }
    function getId() {
        return $this->id;
    }

    function getRep() {
        return $this->Rep;
    }

    function getUserSource() {
        return $this->UserSource;
    }

    function getUserDesti() {
        return $this->UserDesti;
    }

    function getEtat() {
        return $this->Etat;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setRep($Rep) {
        $this->Rep = $Rep;
    }

    function setUserSource($UserSource) {
        $this->UserSource = $UserSource;
    }

    function setUserDesti($UserDesti) {
        $this->UserDesti = $UserDesti;
    }

    function setEtat($Etat) {
        $this->Etat = $Etat;
    }
}
