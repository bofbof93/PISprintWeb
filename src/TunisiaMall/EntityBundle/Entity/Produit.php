<?php

namespace TunisiaMall\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * 
 */
class Produit {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string",length=255)
     */
    private $Reference;

    /**
     * @ORM\Column(type="string",length=255)
     */
    private $Libelle;

    /**
     * @ORM\Column(type="text")
     */
    private $Descriptif;

    /**
     * @ORM\Column(type="float")
     */
    private $Prix;

    /**
     * @ORM\Column( type="string")
     */
    private $Categorie;

   /**
     *@ORM\Column(type="blob", nullable=true)
     */
    private $Image;
 
    /**
     * @ORM\Column(type="integer")
     */
    private $NbPoint;
    
    /**
     * @ORM\ManyToOne(targetEntity="Panier")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private  $Panier ;

    function getReference() {
        return $this->Reference;
    }

    function getLibelle() {
        return $this->Libelle;
    }

    function getDescriptif() {
        return $this->Descriptif;
    }

    function getPrix() {
        return $this->Prix;
    }

    function getCategorie() {
        return $this->Categorie;
    }

    function getImage() {
        return $this->Image;
    }

    function getNote() {
        return $this->Note;
    }

    function getNbPoint() {
        return $this->NbPoint;
    }

   
    function setReference($Reference) {
        $this->Reference = $Reference;
    }

    function setLibelle($Libelle) {
        $this->Libelle = $Libelle;
    }

    function setDescriptif($Descriptif) {
        $this->Descriptif = $Descriptif;
    }

    function setPrix($Prix) {
        $this->Prix = $Prix;
    }

    function setCategorie($Categorie) {
        $this->Categorie = $Categorie;
    }

    function setImage($Image) {
        $this->Image = $Image;
    }

    function setNote($Note) {
        $this->Note = $Note;
    }

    function setNbPoint($NbPoint) {
        $this->NbPoint = $NbPoint;
    }
    function getEnseigne() {
        return $this->Enseigne;
    }

    function setEnseigne($Enseigne) {
        $this->Enseigne = $Enseigne;
    }

    function getCatalogue() {
        return $this->Catalogue;
    }

    function getPanier() {
        return $this->Panier;
    }

  
    function setCatalogue($Catalogue) {
        $this->Catalogue = $Catalogue;
    }

    function setPanier($Panier) {
        $this->Panier = $Panier;
    }

    function getId() {
        return $this->id;
    }

    function getPromotion() {
        return $this->Promotion;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setPromotion($Promotion) {
        $this->Promotion = $Promotion;
    }



}
