<?php

namespace TunisiaMall\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *@ORM\Entity(repositoryClass="TunisiaMall\ReservationPackBundle\Repository\ReservationPackRepository")
 * 
 */
class ReservationPack {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $Datedebut;

    /**
     * @ORM\Column(type="date")
     */
    private $DateFin;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    private $Image;

    /**
     * @ORM\Column(type="text")
     */
    private $Description;

    /**
     * @ORM\Column( type="integer")
     */
    private $Acceptation;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $User;

    /**

     * @ORM\ManyToOne(targetEntity="PackPublicitaire")
     * @@ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="SET NULL")
     */
    private $PackPublicitaire;

    function getEtat() {
        return $this->etat;
    }

    function setEtat($etat) {
        $this->etat = $etat;
    }

    function getIdReservation() {
        return $this->idReservation;
    }

    function getRef() {
        return $this->Ref;
    }

    function getDatedebut() {
        return $this->Datedebut;
    }

    function getDateFin() {
        return $this->DateFin;
    }

    function getPackPublicitaire() {
        return $this->PackPublicitaire;
    }

    function setPackPublicitaire($PackPublicitaire) {
        $this->PackPublicitaire = $PackPublicitaire;
    }

    function setRef($Ref) {
        $this->Ref = $Ref;
    }

    function setDatedebut($Datedebut) {
        $this->Datedebut = $Datedebut;
    }

    function setDateFin($DateFin) {
        $this->DateFin = $DateFin;
    }

    function getId() {
        return $this->id;
    }

    function getUser() {
        return $this->User;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setUser($User) {
        $this->User = $User;
    }

    function getCarteBancaire() {
        return $this->CarteBancaire;
    }

    function setCarteBancaire($CarteBancaire) {
        $this->CarteBancaire = $CarteBancaire;
    }

    function getImage() {
        return $this->Image;
    }

    function getDescription() {
        return $this->Description;
    }

    function getAcceptation() {
        return $this->Acceptation;
    }

    function setImage($Image) {
        $this->Image = $Image;
    }

    function setDescription($Description) {
        $this->Description = $Description;
    }

    function setAcceptation($Acceptation) {
        $this->Acceptation = $Acceptation;
    }

}
