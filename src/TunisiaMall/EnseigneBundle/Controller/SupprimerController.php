<?php

namespace TunisiaMall\EnseigneBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TunisiaMall\EntityBundle\Entity\Enseigne;
use TunisiaMall\EntityBundle\Entity\ResponsableEnseigne;

class SupprimerController extends Controller {

    public function SupprimerEnseigneAction($id) {
        $em = $this->getDoctrine()->getManager();
        $Enseigne = $em->getRepository('EntityBundle:Enseigne')->find($id);
        $em->remove($Enseigne);
        $em->flush();
        return ($this->redirectToRoute('rechercher_Enseigne'));
    }

}
