<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AfficherImageController
 *
 * @author Asus X550L
 */
namespace TunisiaMall\PromotionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TunisiaMall\EntityBundle\Entity\Promotion;
use Symfony\Component\HttpFoundation\StreamedResponse;

class AfficherImageController extends Controller{

    public function photoAction($id) {
        $em = $this->getDoctrine()->getManager();
        $image_obj = $em->getRepository('EntityBundle:Promotion')->find($id);
        $photo = $image_obj->getImage();
        $response = new StreamedResponse(function () use ($photo) {
            echo stream_get_contents($photo);
        });
        $response->headers->set('Content-Type','image/jpeg');
        return $response;
    }

}
