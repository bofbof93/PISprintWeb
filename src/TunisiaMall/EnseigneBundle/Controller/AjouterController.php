<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AjouterController
 *
 * @author Asus X550L
 */
namespace TunisiaMall\EnseigneBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TunisiaMall\EntityBundle\Entity\Enseigne;
use TunisiaMall\EntityBundle\Entity\ResponsableEnseigne;
use Symfony\Component\HttpFoundation\File\File;


class AjouterController  extends Controller{
       
    public function AjouterEnseigneAction(){
        $user=$this->getUser();
        $em= $this->getDoctrine()->getManager();
        $responsables=$em->getRepository('EntityBundle:ResponsableEnseigne')->findAll();
        
            $request=$this->get('request');
            $Enseigne=new Enseigne();//suite au clic sur le bouton ajouter     
            
            if($request->getMethod()=='POST')
            {
              
                
                $Enseigne->setLibelle($request->get("Libelle"));
                $Enseigne->setDescription($request->get("Description"));
                $Enseigne->setEtage($request->get("Etage"));
                $Enseigne->setCategorie($request->get("Categorie"));
                 $Enseigne->setTimeOuverture($request->get("TimeOuverture"));
                 $Enseigne->setTimeFermeture($request->get("TimeFermeture"));
                 $Enseigne->setURL($request->get("URL"));
                 $Enseigne->setNBJour($request->get("NBJour"));
                    $Enseigne->setImage($request->get("imaEns"));
                $Enseigne->setUser($user);
                
                $em= $this->getDoctrine()->getManager();
                $em->persist($Enseigne);//pou l'ajou d'un objet 
                $em->flush();//predre consideration le persiste
            }
            return ($this->render("TunisiaMallEnseigneBundle:Enseigne:AjouterEnseigne.html.twig", array('responsables'=>$responsables, 'user' => $user)));
        
    }
    
}
