<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModifierController
 *
 * @author Asus X550L
 */
namespace TunisiaMall\EnseigneBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;



class ModifierController extends Controller {
    
     public function ModifierEnseigneAction($id){
        $user=$this->getUser();
        $em= $this->getDoctrine()->getManager();
        $Enseigne=$em->getRepository('EntityBundle:Enseigne')->find($id);
        $request= $this->get('request');
      
         if($request->getMethod()=='POST')
         {     $libelle= $request->get('Libelle');
               $Description=$request->get('Description');
               $Etage=$request->get('Etage');
               $Categorie=$request->get('Categorie');
               $TimeOuverture=$request->get('TimeOuverture');
               $TimeFermeture=$request->get('TimeFermeture');
               $URL=$request->get('URL');
               $NBJour=$request->get('NBJour');
         
               
               
                $Enseigne->setLibelle($request->get("Libelle"));
                $Enseigne->setDescription($request->get("Description"));
                $Enseigne->setEtage($request->get("Etage"));
                $Enseigne->setCategorie($request->get("Categorie"));
                $Enseigne->setTimeOuverture($request->get("TimeOuverture"));
                $Enseigne->setTimeFermeture($request->get("TimeFermeture"));
                $Enseigne->setURL($request->get("URL"));
                $Enseigne->setNBJour($request->get("NBJour"));
   
                $Enseigne->setUser($user);
                
                $em= $this->getDoctrine()->getManager();
                $em->persist($Enseigne);//pou l'ajou d'un objet 
                $em->flush();//predre consideration le persiste
                   return $this->redirectToRoute("tunisia_mall_actualite_List");
            }
        
        return $this->render('TunisiaMallEnseigneBundle:Enseigne:ModifierEnseigne.html.twig',
        array('Enseigne'=>$Enseigne, 'user' => $user));
        
 }
}
//$em = $this->getDoctrine()->getManager();
//        $Actualite = $em->getRepository("EntityBundle:Actualite")->find($id);
//
//        $request = $this->get('request');
//
//        if ($request->getMethod() == 'POST') {
//
//            $description = $request->get('description');
//            $titre = $request->get('titre');
//            $description = $request->get('description');
//            $Actualite->setDescription($description);
//            $Actualite->setTitre($titre);
//
//            $em->persist($Actualite);
//            $em->flush();
//            return $this->redirectToRoute("tunisia_mall_actualite_List_Admin");
//        }
//
//        return $this->render('TunisiaMallActualiteBundle:Actualite:UpdateActualite.html.twig', array("Actualite" => $Actualite));
