<?php

namespace TunisiaMall\EnseigneBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('TunisiaMallEnseigneBundle:Default:index.html.twig', array('name' => $name));
    }
}
