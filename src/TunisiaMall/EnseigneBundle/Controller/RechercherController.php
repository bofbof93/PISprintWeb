<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RechercherController
 *
 * @author Asus X550L
 */
namespace TunisiaMall\EnseigneBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class RechercherController extends Controller{
                      
    public function rechercherAction(){
        $user=$this->getUser();
            $em = $this->getDoctrine()->getManager();
            $Enseignes= $em->getRepository("EntityBundle:Enseigne")->findBy(array("User"=>$user->getId()));
            $request=$this->get('request');
            if($request->getMethod()=="POST"){
                $search = $request->get('search');
                $Enseignes =$em->getRepository('EntityBundle:Enseigne')
                        ->findBy(array('Libelle'=> $search
                            ));

            }
        return $this->render('TunisiaMallEnseigneBundle:Enseigne:ListeDesEnseigne.html.twig',
    array('Enseignes'=>$Enseignes,'user'=>$user));
        }
        
}
