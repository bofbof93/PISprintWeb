<?php

namespace TunisiaMall\ActualiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('TunisiaMallActualiteBundle:Default:index.html.twig', array('name' => $name));
    }
}
