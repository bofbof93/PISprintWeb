<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace TunisiaMall\ActualiteBundle\Controller;

use \Symfony\Bundle\FrameworkBundle\Controller\Controller;
use \TunisiaMall\EntityBundle\Entity\Actualite;

class ActualiteController extends Controller {

    public function AjouterAction() {
         $user=$this->getUser();
        $Actualite = new Actualite();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $imgActu = $request->get('imgactu');
            $titre = $request->get('titre');
            $description = $request->get('description');
            $Actualite->setDescription($description);
            $Actualite->setTitre($titre);
            $Actualite->setDate(new \DateTime('now'));
            $Actualite->setImage($imgActu);
            $Actualite->setUser($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($Actualite);
            $em->flush();

            return $this->redirectToRoute("tunisia_mall_actualite_List");
        }

        return $this->render('TunisiaMallActualiteBundle:Actualite:AjouterActualite.html.twig', array('user' => $user));
    }

    public function MAJAction($id) {

        $em = $this->getDoctrine()->getManager();
        $Actualite = $em->getRepository("EntityBundle:Actualite")->find($id);

        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {

            $titre = $request->get('titre');
            $description = $request->get('description');
            $Actualite->setDescription($description);
            $Actualite->setTitre($titre);
            $em->persist($Actualite);
            $em->flush();
            return $this->redirectToRoute("tunisia_mall_actualite_List");
        }

        return $this->render('TunisiaMallActualiteBundle:Actualite:UpdateActualite.html.twig', array("Actualite" => $Actualite));
    }

    public function SupprimeAction($id) {

        $em = $this->getDoctrine()->getManager();
        $Actualite = $em->getRepository("EntityBundle:Actualite")->find($id);
        $em->remove($Actualite);
        $em->flush();
        return $this->redirectToRoute("tunisia_mall_actualite_List");
    }

    public function ListeActualitesAction() {
        $user=$this->getUser();
        
        $em = $this->getDoctrine()->getManager();
        $Actualites= $em->getRepository('EntityBundle:Actualite')->findBy(array("User"=>$user->getId()));
        return $this->render('TunisiaMallActualiteBundle:Actualite:ListActualite.html.twig', array("Actualites" => $Actualites,'user' => $user));
    }

    public function ListeActualitesClientAction() {
        $user=$this->getUser();
        $em = $this->getDoctrine()->getManager();
        $Actualites = $em->getRepository('EntityBundle:Actualite')->findAll();
        return $this->render('TunisiaMallActualiteBundle:Actualite:Actualites.html.twig', array("Actualites" => $Actualites,'user' => $user));
    }

}
