<?php

namespace TunisiaMall\ReservationPackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TunisiaMall\EntityBundle\Entity\ReservationPack;

class ReservationPackController extends Controller {

    public function ListeAction() {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
         $clients=$em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>3));
        $responsable= $em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>2));
        
        $clientsCount=count($clients);
        $responsablesCount=count($responsable);
        $packs = $em->getRepository('EntityBundle:PackPublicitaire')
                ->findListPackDQL();
        $request = $this->get('request');
        if ($request->getMethod() == "POST") {
            $search = $request->get('search');
            $packs = $em->getRepository('EntityBundle:PackPublicitaire')
                    ->findBy(array('id' => $search
            ));
        }

        return $this->render('ReservationPackBundle:ReservationPack:ListePack.html.twig', array('packs' => $packs, 'user' => $user,'clientsCount'=>$clientsCount,'responsablesCount'=>$responsablesCount));
    }

    public function ReserverAction($id) {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
         $clients=$em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>3));
        $responsable= $em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>2));
        
        $clientsCount=count($clients);
        $responsablesCount=count($responsable);
        $Packs = $em->getRepository('EntityBundle:PackPublicitaire')->findBy(array('id' => $id));
        $pac = $em->getRepository('EntityBundle:PackPublicitaire')->find($id);
        $ResPack = new ReservationPack();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $ResPack->setDatedebut(new \DateTime($request->get('date1')));
            $ResPack->setDateFin(new \DateTime($request->get('date2')));
            $ResPack->setImage($request->get('img'));
            $ResPack->setDescription($request->get("des"));
            $ResPack->setAcceptation(1); //0 demade 1 accepter 2 achat
            $ResPack->setUser($user);

            $ResPack->setPackPublicitaire($pac);
            $em->persist($ResPack); //pou l'ajou d'un objet 
            $em->flush(); //predre consideration le persiste
            return ($this->redirectToRoute("reservation_pack_pub_Liste"));
        }

        return $this->render('ReservationPackBundle:ReservationPack:ReservationForm.html.twig', array('Packs' => $Packs,
                    'user' => $user,'clientsCount'=>$clientsCount,'responsablesCount'=>$responsablesCount));
    }

    public function HistoriqueAction() {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
         $clients=$em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>3));
        $responsable= $em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>2));
        
        $clientsCount=count($clients);
        $responsablesCount=count($responsable);
        $packs = $em->getRepository('EntityBundle:ReservationPack')
                ->findHistoriqueReservationDQL();
        $request = $this->get('request');
        if ($request->getMethod() == "POST") {
            $search = $request->get('search');
            $packs = $em->getRepository('EntityBundle:ReservationPack')
                    ->findBy(array('id' => $search
            ));
        }
        return $this->render('ReservationPackBundle:ReservationPack:Historique.html.twig', array('packs' => $packs, 'user' => $user,'clientsCount'=>$clientsCount,'responsablesCount'=>$responsablesCount));
    }

    public function DemandeReservationAction() {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
         $clients=$em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>3));
        $responsable= $em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>2));
        
        $clientsCount=count($clients);
        $responsablesCount=count($responsable);
        $res = $em->getRepository('EntityBundle:ReservationPack')
                ->ListeDemandeDeResDQL();
        $request = $this->get('request');
        if ($request->getMethod() == "POST") {
            $search = $request->get('search');
            $res = $em->getRepository('EntityBundle:ReservationPack')
                    ->findBy(array('id' => $search
            ));
        }
        return $this->render('ReservationPackBundle:ReservationPack:ListeDemandePack.html.twig', array('res' => $res, 'user' => $user,'clientsCount'=>$clientsCount,'responsablesCount'=>$responsablesCount));
    }

    public function AccepterReservationAction($id) {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
         $clients=$em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>3));
        $responsable= $em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>2));
        
        $clientsCount=count($clients);
        $responsablesCount=count($responsable);

        $ReservationPack = $em->getRepository('EntityBundle:ReservationPack')->find($id);

        //   $ReservationPack = new ReservationPack();

        $ReservationPack->setAcceptation(2);

        $em->persist($ReservationPack); //pou l'ajou d'un objet 
        $em->flush(); //predre consideration le persiste
        $res = $em->getRepository('EntityBundle:ReservationPack')
                ->ListeDemandeDeResDQL();

        return $this->render('ReservationPackBundle:ReservationPack:ListeDemandePack.html.twig', array('res' => $res,
                    'user' => $user,'clientsCount'=>$clientsCount,'responsablesCount'=>$responsablesCount));
    }

    public function RefuserReservationAction($id) {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
 $clients=$em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>3));
        $responsable= $em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>2));
        
        $clientsCount=count($clients);
        $responsablesCount=count($responsable);
        $ReservationPack = $em->getRepository('EntityBundle:ReservationPack')->find($id);

        //   $ReservationPack = new ReservationPack();

        $ReservationPack->setAcceptation(0);

        $em->persist($ReservationPack); //pou l'ajou d'un objet 
        $em->flush(); //predre consideration le persiste
        $res = $em->getRepository('EntityBundle:ReservationPack')
                ->ListeDemandeDeResDQL();

        return $this->render('ReservationPackBundle:ReservationPack:ListeDemandePack.html.twig', array('res' => $res,
                    'user' => $user,'clientsCount'=>$clientsCount,'responsablesCount'=>$responsablesCount));
    }

//        public function ListeNotificationAction() {
//        $user = $this->getUser();
//        $em = $this->getDoctrine()->getManager();
//        $notes = $em->getRepository('EntityBundle:ReservationPack')
//                ->notificationDeResDQL();
//
//        return $this->render('AdminBundle::Layout.html.twig', array('notes' => $notes, 'user' => $user));
//    }

    public function ReservationAPayerAction() {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $res = $em->getRepository('EntityBundle:ReservationPack')
                ->ReservationAPayerDQL();
 $clients=$em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>3));
        $responsable= $em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>2));
        
        $clientsCount=count($clients);
        $responsablesCount=count($responsable);
        return $this->render('ReservationPackBundle:ReservationPack:ReservationAPayer.html.twig', array('res' => $res, 'user' => $user,'clientsCount'=>$clientsCount,'responsablesCount'=>$responsablesCount));
    }

    public function PaiementAction($id) {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $clients=$em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>3));
        $responsable= $em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>2));
        
        $clientsCount=count($clients);
        $responsablesCount=count($responsable);
        $res = $em->getRepository('EntityBundle:ReservationPack')->findBy(array('id' => $id));


        return $this->render('ReservationPackBundle:ReservationPack:Paiement.html.twig', array('res' => $res,
                    'user' => $user,'clientsCount'=>$clientsCount,'responsablesCount'=>$responsablesCount));
    }

    public function ConfirmAction($id) {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $clients=$em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>3));
        $responsable= $em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>2));
        
        $clientsCount=count($clients);
        $responsablesCount=count($responsable);
        $res = $em->getRepository('EntityBundle:ReservationPack')->find($id);
        $request = $this->get('request');
        if ($request->getMethod() == "POST") {
            $clients=$em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>3));
        $responsable= $em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>2));
        
        $clientsCount=count($clients);
        $responsablesCount=count($responsable);
            $num = $request->get('num');
            $mdp = $request->get('password');
            $carte = $em->getRepository('EntityBundle:CarteBancaire')->findBy(array('Numero' => $num, 'MotDePasse' => $mdp));
            foreach ($carte as $cartes) {
                $carte = $cartes;
                $somme = $carte->getSomme();

                 if (($num!=$carte->getNumero())||($mdp!=$carte->getMotDePasse())||($carte->getSomme()<=0)) {
                    $this->get('session')->getFlashBag()->add(
                            'error', 'votre solde est insufisant!'
                    );
                }else{
                     $this->get('session')->getFlashBag()->add(
                            'notice', 'Paiement effectué avec succés!'
                    );
                $carte->setSomme($somme - $res->getPackPublicitaire()->getPrix());
                $em->persist($carte);
                $res->setAcceptation(3);
                $em->flush();}
               
            }
        }
        return $this->render('ReservationPackBundle:ReservationPack:confirm.html.twig', array(
                    'user' => $user,'clientsCount'=>$clientsCount,'responsablesCount'=>$responsablesCount));
    }

}
