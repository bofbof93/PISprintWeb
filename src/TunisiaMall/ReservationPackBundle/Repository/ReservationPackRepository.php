<?php

namespace TunisiaMall\ReservationPackBundle\Repository;

use Doctrine\ORM\EntityRepository;
use TunisiaMall\EntityBundle\Entity\PackPublicitaire;

class ReservationPackRepository extends EntityRepository {

    public function findHistoriqueReservationDQL() {
        $query = $this->getEntityManager()
                ->createQuery("SELECT m from EntityBundle:ReservationPack m where m.Acceptation=3");
        return $query->getResult();
    }

    public function findListPackDQL() {
        $query = $this->getEntityManager()
                ->createQuery("SELECT m from EntityBundle:PackPublicitaire m where m.Etat='Non_active'");
        return $query->getResult();
    }

    public function ListeDemandeDeResDQL() {
        $query = $this->getEntityManager()
                ->createQuery("SELECT m from EntityBundle:ReservationPack m where m.Acceptation=1");
        return $query->getResult();
    }

    public function ReservationAPayerDQL() {
        $query = $this->getEntityManager()
                ->createQuery("SELECT m from EntityBundle:ReservationPack m where m.Acceptation=2");
        return $query->getResult();
    }

}
