<?php

namespace TunisiaMall\AdminBundle\Controller;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TunisiaMall\EntityBundle\Entity\User;

class AfficheController extends Controller{

    public function afficherAction()
    {   
        $user=$this->getUser();
        $em = $this->getDoctrine()->getManager();
        $clients=$em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>3));
        $responsable= $em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>2));
        
        $clientsCount=count($clients);
        $responsablesCount=count($responsable);
        
        if($user){
        if($user->getFlagRole()==1 ){
        return $this->render('AdminBundle:Default:admin.html.twig',array(
            'user' => $user,
            'clientsCount' => $clientsCount,
            'responsablesCount' => $responsablesCount
         
        ));
        }
        else if($user->getFlagRole()==2){
            return $this->render('AdminBundle:Default:admin.html.twig',array(
            'user' => $user,
            'clientsCount' => $clientsCount,
            'responsablesCount' => $responsablesCount
        ));
        }
        }
        $url = $this->generateUrl('fos_user_security_login');
                $response = new RedirectResponse($url);
            return $response;
    }
        public function formulaireAction()
    {
            $user=$this->getUser();
            if($user){
        if($user->getUserRole()==1){
        return $this->render('AdminBundle:Default:formulaire.html.twig',array(
            'user' => $user
        ));
            }}
        else {
            $url = $this->generateUrl('fos_user_security_login');
                $response = new RedirectResponse($url);
            return $response;
        }
       
    }
    
   
     public function GestionDesComptesAction(){
        $user=$this->getUser();
         $em = $this->getDoctrine()->getManager();
        $users=$em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>3));
         $clients=$em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>3));
        $responsables= $em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>2));
        $responsablesCount=0;
        
        $clientsCount=count($clients);
        
        $responsablesCount=count($responsables);
            if($user){
       
        return $this->render('ResponsableBundle:Default:GestionDesComptes.html.twig',array('user'=>$user,'users'=>$users,'clientsCount'=>$clientsCount,'responsablesCount'=>$responsablesCount));
            }
         $url = $this->generateUrl('fos_user_security_login');
                $response = new RedirectResponse($url);
            return $response;
    }
     public function GestionDesComptesEnseigneAction(){
        $user=$this->getUser();
         $em = $this->getDoctrine()->getManager();
        $users=$em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>2));
         $clients=$em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>3));
        $responsable= $em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>2));
        
        $clientsCount=count($clients);
        $responsablesCount=count($responsable);
            if($user){
      
        return $this->render('ResponsableBundle:Default:GestionDesComptes.html.twig',array('user'=>$user,'users'=>$users,'clientsCount'=>$clientsCount,'responsablesCount'=>$responsablesCount));
         }
         $url = $this->generateUrl('fos_user_security_login');
                $response = new RedirectResponse($url);
            return $response;
    }
    
    public function AccepterCompteAction($id){
        $em = $this->getDoctrine()->getManager();
        
        $user =$em->getRepository("EntityBundle:User")->find($id);
        
        
        $user->setFlagAccepted(1);
         $em->persist($user);//pour l'ajout d'un objet 
                $em->flush();
        $url = $this->generateUrl('tunisia_mall_responsable_GestionComptes');
        $response = new RedirectResponse($url);
        return $response;
       
    }
    
    
}
