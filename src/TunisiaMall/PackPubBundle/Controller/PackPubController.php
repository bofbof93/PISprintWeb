<?php

namespace TunisiaMall\PackPubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TunisiaMall\EntityBundle\Entity\PackPublicitaire;

class PackPubController extends Controller {

    public function AjouterPackAction() {
        $user = $this->getUser();
        $request = $this->get('request');
        $Pack = new Packpublicitaire();
         $em = $this->getDoctrine()->getManager();//suite au clic sur le bouton ajouter     
         $clients=$em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>3));
        $responsable= $em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>2));
        
        $clientsCount=count($clients);
        $responsablesCount=count($responsable);
        if ($request->getMethod() == 'POST') {
            $Pack->setLibelle($request->get("libelle"));
            $Pack->setPrix($request->get("prix"));
            $Pack->setZone($request->get("zone"));
            $Pack->setDescription($request->get("description"));
            $Pack->setEtat($request->get("etat"));

            $em = $this->getDoctrine()->getManager();
            $em->persist($Pack); //pour l'ajou d'un objet 
            $em->flush(); //predre consideration le persiste
            return $this->redirectToRoute("pack_pub_Liste");
        }
        return ($this->render("PackPubBundle:PackPub:AjouterPack.html.twig", array('user' => $user,'clientsCount'=>$clientsCount,'responsablesCount'=>$responsablesCount)));
    }

    public function ListeDesPacksAction() {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $clients=$em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>3));
        $responsable= $em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>2));
        
        $clientsCount=count($clients);
        $responsablesCount=count($responsable);
        $packs = $em->getRepository('EntityBundle:ReservationPack')
                ->findListPackDQL();
  
        
        $request = $this->get('request');
        if ($request->getMethod() == "POST") {
            $search = $request->get('search');
            $packs = $em->getRepository('EntityBundle:ReservationPack')
                    ->findBy(array('id' => $search
            ));
        }
        return $this->render('PackPubBundle:PackPub:ListePack.html.twig', array('packs' => $packs, 'user' => $user,'clientsCount'=>$clientsCount,'responsablesCount'=>$responsablesCount));
    }

    public function modifierPackAction($id) {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $packs = $em->getRepository('EntityBundle:PackPublicitaire')->findBy(array('id' => $id));
        $Pack = $em->getRepository('EntityBundle:PackPublicitaire')->find($id);
        $request = $this->get('request');
$clients=$em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>3));
        $responsable= $em->getRepository("EntityBundle:User")->findby(array('flagAccepted'=>0,'flagRole'=>2));
        
        $clientsCount=count($clients);
        $responsablesCount=count($responsable);
        if ($request->getMethod() == 'POST') {
            $Pack->setZone($request->get("zone"));
            $Pack->setLibelle($request->get("libelle"));
            $Pack->setPrix($request->get("prix"));
            $Pack->setDescription($request->get("description"));
            $Pack->setEtat($request->get("etat"));

            $em->persist($Pack); //pou l'ajou d'un objet 
            $em->flush(); //predre consideration le persiste
            return ($this->redirectToRoute("pack_pub_Liste"));
        }

        return $this->render('PackPubBundle:PackPub:ModifierPack.html.twig', array('packs' => $packs, 'user' => $user,'clientsCount'=>$clientsCount,'responsablesCount'=>$responsablesCount));
    }

    public function supprimerPackAction($id) {
        $em = $this->getDoctrine()->getManager();
        $pack = $em->getRepository('EntityBundle:PackPublicitaire')->find($id);
        $em->remove($pack);
        $em->flush();
        return ($this->redirectToRoute('pack_pub_Liste'));
    }

  
}
