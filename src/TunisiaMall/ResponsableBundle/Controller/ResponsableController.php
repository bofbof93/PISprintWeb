<?php

namespace TunisiaMall\ResponsableBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use FOS\UserBundle\FOSUserEvents;
    use FOS\UserBundle\Event\FormEvent;
    use FOS\UserBundle\Event\FilterUserResponseEvent;
    use FOS\UserBundle\Event\GetResponseUserEvent;
    use FOS\UserBundle\Model\UserInterface;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\RedirectResponse;
    use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ResponsableController
 *
 * @author ISLEM
 */
class ResponsableController extends Controller {
    //put your code here
     public function afficherAction()
    {    $user=$this->getUser();
        if($user->getFlagRole()==2)
        return $this->render('ResponsableBundle:Default:Acceuil.html.twig',array('user'=>$user));
        else 
            $url = $this->generateUrl('fos_user_security_login');
                $response = new RedirectResponse($url);
            return $response;
    }
   
    public function GestionDesProduitsAction(){
       
        $user=$this->getUser();
        if($user->getFlagRole()==2)
      return $this->render('ResponsableBundle:Default:GestionDesProduits.html.twig',array('user'=>$user));
        else 
            $url = $this->generateUrl('fos_user_security_login');
                $response = new RedirectResponse($url);
            return $response;
       
        
    }
    public function GestionDuStockAction(){
         $user=$this->getUser();
        if($user->getFlagRole()==2)
     return $this->render('ResponsableBundle:Default:GestionDuStock.html.twig',array('user'=>$user));
        else 
            $url = $this->generateUrl('fos_user_security_login');
                $response = new RedirectResponse($url);
            return $response;
  
        
    }
     public function GestionDesEnseignesAction(){
        
          $user=$this->getUser();
        if($user->getFlagRole()==2)
      return $this->render('ResponsableBundle:Default:GestionDesEnseignes.html.twig',array('user'=>$user));
        else 
            $url = $this->generateUrl('fos_user_security_login');
                $response = new RedirectResponse($url);
            return $response;
        
       
    }
     public function GestionDesPackPublicitaireAction(){
              $user=$this->getUser();
        if($user->getFlagRole()==2)
     return $this->render('ResponsableBundle:Default:GestionPackPub.html.twig',array('user'=>$user));
    else 
            $url = $this->generateUrl('fos_user_security_login');
                $response = new RedirectResponse($url);
            return $response;
        
       
           }
     public function GestionDesCataloguesAction(){
        $user=$this->getUser();
        if($user->getFlagRole()==2)
    return $this->render('ResponsableBundle:Default:GestionDesCatalogues.html.twig',array('user'=>$user));
    else 
            $url = $this->generateUrl('fos_user_security_login');
                $response = new RedirectResponse($url);
            return $response;
        
     
            }
         public function AjouterProduitAction()
    {
             $user=$this->getUser();
        if($user->getFlagRole()==2){
        return $this->render('AdminBundle:Default:AjouterProduit.html.twig',array(
            'user' => $user
        ));
        }
        else {
            $url = $this->generateUrl('fos_user_security_login');
                $response = new RedirectResponse($url);
            return $response;
        }
        
    }
    public function StatistiqueAction(){
        $user=$this->getUser();
        return $this->render('ResponsableBundle:Default:Statistiques.html.twig',array('user'=>$user));
    }
    
   
    
    
}
