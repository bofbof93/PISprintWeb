<?php

namespace TunisiaMall\StatBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ob\HighchartsBundle\Highcharts\Highchart;


class StatController extends Controller {

    public function chartLineAction() {
 $user=$this->getUser();
        // Chart

        $series = array(
            array("name" => "Nom du graphe", "data" => array(1, 2, 4, 5, 6, 3, 8))
        );

        $ob = new Highchart();

        $ob->chart->renderTo('linechart'); // #id du div où afficher le graphe

        $ob->title->text('Titre du graphique');
        $ob->xAxis->title(array('text' => "Titre axe horizontal"));

        $ob->yAxis->title(array('text' => "Titre axe vertical "));

        $ob->series($series);

        return $this->render('TunisiaMallStatBundle:Default:index.html.twig', array(
                    'chart' => $ob,'user' => $user
        ));
    }

}
