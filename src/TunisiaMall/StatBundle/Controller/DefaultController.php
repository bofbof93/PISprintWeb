<?php

namespace TunisiaMall\StatBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('TunisiaMallStatBundle:Default:index.html.twig', array('name' => $name));
    }
}
