<?php

namespace TunisiaMall\PromotionProduitBundle\Controller;
use TunisiaMall\EntityBundle\Entity\PromotionProduit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TunisiaMall\PromotionProduitBundle\Form\PromotionProduitForm;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PromotionProduitController
 *
 * @author ISLEM
 */
class PromotionProduitController extends Controller {
    
    public function AjouterpromprodAction()
    {
       $user=$this->getUser();
       $em = $this->getDoctrine()->getManager();
       $PromotionProduit = new PromotionProduit();
       $form = $this->createForm(new PromotionProduitForm(), $PromotionProduit);
       $request = $this->get('request');
       $form->handleRequest($request);
       if($form->isValid())
       {
           
           $em->persist($PromotionProduit);
           $em->flush();
           return $this->redirect($this->generateUrl('tunisia_mall_promprod_Listpromprod'));
       }
       return $this->render('PromotionProduitBundle:PromotionProduit:AjouterPromotionProduit.html.twig',
               array(
                   'form'=>$form->createView(),'user' => $user
               ));
            //return ($this->redirectToRoute("tunisia_mall_produit_ListProduit"));//bech thizek lil page ili feha affichage ta3 tableu kol mil BD
        }
       
    public function ListpromprodAction()
    {   $user=$this->getUser();
        $em=$this->getDoctrine()->getManager();
        $PromotionProduit=$em->getRepository('EntityBundle:PromotionProduit')->findAll();
        
        return $this->render('PromotionProduitBundle:PromotionProduit:ListPromotionsProduit.html.twig',array('PromotionProduit'=>$PromotionProduit,'user' => $user ));
    }
    public function SupprimerpromprodAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $PromotionProduit=$em->getRepository('EntityBundle:PromotionProduit')->find($id);
        $em->remove($PromotionProduit);
        $em->flush();
        return ($this->redirectToRoute("tunisia_mall_promprod_Listpromprod"));
    }
    public function ModifierpromprodAction($id)
    {
        $user=$this->getUser();
        $em=$this->getDoctrine()->getManager();
        $PromotionProduit=$em->getRepository('EntityBundle:PromotionProduit')->find($id);
        $form = $this->createForm(new PromotionProduitForm(),$PromotionProduit);
        $request=$this->get('request');
        $form->handleRequest($request);
          if($form->isValid())
          {
              $em = $this->getDoctrine()->getManager();
              $em->persist($PromotionProduit);
              $em->flush();
              
               return ($this->redirectToRoute("tunisia_mall_promprod_Listpromprod"));
          }
        
       return $this->render('PromotionProduitBundle:PromotionProduit:ModifierPromProd.html.twig',array('form'=>$form->createView(),'user' => $user));
    }
    
    public function ListpromprodClientAction()
    {   $user=$this->getUser();
        $em=$this->getDoctrine()->getManager();
        $PromotionProduit=$em->getRepository('EntityBundle:PromotionProduit')->findAll();
        
        return $this->render('PromotionProduitBundle:PromotionProduitClient:ListPromotionsProduit.html.twig',array('PromotionProduit'=>$PromotionProduit,'user' => $user ));
    }
    
    
}
