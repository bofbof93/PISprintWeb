<?php

namespace TunisiaMall\PromotionProduitBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PromotionProduitForm
 *
 * @author ISLEM
 */
class PromotionProduitForm extends AbstractType {
    
    public function buildForm(FormBuilderInterface $builder,
            array $options) {
        $builder
              
               
                
                ->add('Produit','entity',array('class'=>'EntityBundle:Produit','property'=>'Libelle' ,'attr' => array(
                        'class' => 'form-control'                            
                    ) ))
                ->add('Promotion','entity',array('class'=>'EntityBundle:Promotion','property'=>'tauxdereduction' ,'attr' => array(
                        'class' => 'form-control'                            
                    ) ))
                
                ->add('Ajouter','submit' , array(
                    'attr' => array(
                        'class' => 'btn btn-info pull-right'                            
                    )
                    
                ));
    

//put your code here
}

    public function getName() {
        return 'PromotionProduit';
    }
}
    