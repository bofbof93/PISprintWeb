<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AfficheController
 *
 * @author Asus X550L
 */
namespace TunisiaMall\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use \TunisiaMall\EntityBundle\Entity\Panier;
class AfficheController extends Controller{

public function afficherAction()
    {
         $user=$this->getUser();
       
         return $this->render('ClientBundle:Default:acceuil.html.twig',array(
            'user' => $user));
    
       
    }
public function catalogueAction(){
    $user=$this->getUser();
        return $this->render('ClientBundle:Default:catalogue.html.twig',array('user' => $user));
    }
public function gallerieAction(){
    $user=$this->getUser();
        return $this->render('ClientBundle:Default:gallerie.html.twig',array('user' => $user));
    }
public function fashionAction(){
    $user=$this->getUser();
        return $this->render('ClientBundle:Default:fashion.html.twig',array('user' => $user));
    }
public function boutiqueAction(){
    $user=$this->getUser();
        return $this->render('ClientBundle:Default:boutique.html.twig',array('user' => $user));
    }
public function contactAction(){
    $user=$this->getUser();
        return $this->render('ClientBundle:Default:contact.html.twig',array('user' => $user));
    }
public function panierAction(){
    $user=$this->getUser();
    $em = $this->getDoctrine()->getManager();
      
        $panier=$em->getRepository("EntityBundle:Panier")->findBy(array('User'=>$user));
        foreach($panier as $p){
            $panier=$p;
        }
       
        $produits = $em->getRepository("EntityBundle:Produit")->findBy(array('Panier'=>$panier));
        
    return $this->render('ClientBundle:Default:panier.html.twig',array('user' => $user,'produits'=>$produits,'panier'=>$panier));
    
    
} 
public function payementAction($id){
    $user=$this->getUser();
    $em = $this->getDoctrine()->getManager();
    $carte=$em->getRepository("EntityBundle:Cartedefidelite")->findBy(array('User'=>$user));
    foreach($carte as $c){
            $carte=$c;
        }
    
    $panier = $em->getRepository("EntityBundle:Panier")->find($id);
    
    
    if ($carte->getNbrPtTotale()> $panier->getPrixTotal()){
        $message="mrigueel";
        // return $this->render('ClientBundle:Default:payement.html.twig',array('user' => $user,'carte'=>$carte,'panier'=>$panier,'message'=>$message));
    
//$this->render('ClientBundle:Default:payement.html.twig',array('user' => $user,'carte'=>$carte,'panier'=>$panier));
    }else
    {
        $message="noon";
    }
    
    return $this->render('ClientBundle:Default:payement.html.twig',array('user' => $user,'carte'=>$carte,'panier'=>$panier,'message'=>$message));
    
}
public function confirmAction($idPanier,$idCarte){
    
    
}
    
}
