<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PanierController
 *
 * @author ESPRIT
 */
namespace TunisiaMall\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TunisiaMall\EntityBundle\Entity\Achat;


class PanierController extends Controller {
    
    
    public function  ajouterAction($id)
    {
        $user=$this->getUser();
        $em = $this->getDoctrine()->getManager();
        $panier = $em->getRepository("EntityBundle:Panier")->findOneBy(array('User'=>$user));
        $produit = $em->getRepository("EntityBundle:Produit")->findOneById($id);
        
        
        $achat = $em->getRepository("EntityBundle:Achat")->findBy(array('panier'=>$panier,'produit'=>$produit));
        
        if(sizeof($achat)){
            
        }
        else{
            $panierProduit = new Achat();
        
            $panierProduit->setPanier($panier);
            $panierProduit->setProduit($produit);
            $panierProduit->setPrix($produit->getPrix());
            $panierProduit->setPrixTotal($produit->getPrix());
            $panierProduit->setQuantite(1);
            $panier->addAchat($panierProduit);

            $em->persist($panierProduit);
            $em->flush();
        }
        
        
        
        return $this->redirect($this->generateUrl("tunisia_mall_client_panier"));
        
    }
    
    public function supprimerAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $panierproduit = $em->getRepository("EntityBundle:Achat")->find($id);
        if($em != null)
        {
            $em->remove($panierproduit);
            $em->flush(); 
        }
        
        //$user=$this->getUser();
        //$panier = $em->getRepository("EntityBundle:Panier")->findOneBy(array('User'=>$user));
        
        return $this->redirect($this->generateUrl("tunisia_mall_client_panier"));
                  
    }
    
    public function changerQuantiterAction(Request $request)
    {
        $id = $request->request->get('id');
        $somme = $request->request->get('somme');
        $new_quantite = $request->request->get('new_quantite');
        $sommepanier = $request->request->get('sommepanier');
        
        $em = $this->getDoctrine()->getManager();
        $achat = $em->getRepository("EntityBundle:Achat")->find($id);
        
        $achat->setQuantite($new_quantite);
        $achat->setPrixTotal($somme);
        
        
        $panier= $achat->getPanier();
        $panier->setPrixTotal($sommepanier);
        
        $em->flush();
        
        $response = new Response(json_encode(array('message' => 'enregistrement effectuer')));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
                  
    }
}
