<?php

namespace TunisiaMall\ProduitBundle\Form;

//use TunisiaMall\EntityBundle\Entity\Produit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

//use Symfony\Component\Form\Extension\Core\Type\FileType;
//use Symfony\Component\Form\Extension\Core\Type\ChoiceType ;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProduitForm
 *
 * @author ISLEM
 */
class ProduitForm extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('Reference', 'text', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                        )
                )
                ->add('Libelle', 'text', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('Descriptif', 'textarea', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('Prix', 'number', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('Categorie', 'choice', array(
                    'choices' => array(
                        'Fashion'=>'Fashion' ,
                        'Gastronomie'=>'Gastronomie',
                        'Sport'=>'Sport',
                        'Beauté'=>'Beauté',
                        'Bijoux & Accessoires'=>'Bijoux & Accessoires',
                        'Chaussures'=>'Chaussures',
                        'Culture'=>'Culture',
                        'Loisirs & Jeux'=>'Loisirs & Jeux',
                        'Maison & Décoration'=>'Maison & Décoration',
                        'Restaurations'=>'Restaurations',
                        'Services'=>'Services',
                        'Electronique & Hi-tech'=>'Electronique & Hi-tech',
                        'Voyages'=>'Voyages',
                        'Parfum & Bien Etre'=>'Parfum & Bien Etre'
                    ),
                    'required' => true,
                    'placeholder' => 'Choisir votre Categorie',
                    'empty_data' => null,
                    'attr' => array(
                        'class' => 'form-control'
                    )
                        )
                )
                ->add('Image', 'file', array(
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'data_class' => null
                ))
                ->add('NbPoint', 'number', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('Panier', 'entity', array('class' => 'EntityBundle:Panier', 'property' => 'id', 'attr' => array(
                        'class' => 'form-control'
            )))
                //->add('Medecin','entity',array(
                // 'class' =>'CalendrierMedecinsBundle:Medecin',
                //'property'=>'libelle'
                //))
                ->add('AjouterProduit', 'submit', array(
                    'attr' => array(
                        'class' => 'btn btn-info pull-right'
                    )
        ));
    }

    public function getName() {
        return 'Produit';
    }

}
