<?php
namespace TunisiaMall\ProduitBundle\Controller;
use TunisiaMall\EntityBundle\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TunisiaMall\ProduitBundle\Form\ProduitForm;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProduitController
 *
 * @author ISLEM
 */
class ProduitController extends Controller {
    //put your code here
     public function AjouterAction()
    {
       $em = $this->getDoctrine()->getManager();
       $user=$this->getUser();
       $Produit = new Produit();
       $form = $this->createForm(new ProduitForm(), $Produit);
       
       $request = $this->get('request');
       $form->handleRequest($request);
       if($form->isValid())
       {
          
//           $stream = fopen($Produit->getImage(), 'rb');
//           $Produit->setImage(stream_get_contents($stream));
           
           $em->persist($Produit);
           $em->flush();
           return $this->redirect($this->generateUrl('tunisia_mall_produit_ListProduit'));
       }
       return $this->render('TunisiaMallProduitBundle:Produit:AjouterProduit.html.twig',
               array(
                   'form'=>$form->createView() , 'user' => $user
               ));
            //return ($this->redirectToRoute("tunisia_mall_produit_ListProduit"));//bech thizek lil page ili feha affichage ta3 tableu kol mil BD
        }
       
    public function ListProduitAction()
    {   $user=$this->getUser();
        $em=$this->getDoctrine()->getManager();
        
        $Produit=$em->getRepository('EntityBundle:Produit')->findAll();
        $image=array();
        foreach ($Produit as $key => $entity)
        {
            $image[$key] = base64_encode(stream_get_contents($entity->getImage()));
                    
        }
        return $this->render('TunisiaMallProduitBundle:Produit:ListProduit.html.twig',array('Produit'=>$Produit,'user' => $user,'image'=>$image));
    }
    public function SupprimerAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $Produit=$em->getRepository('EntityBundle:Produit')->find($id);
        $em->remove($Produit);
        $em->flush();
        return ($this->redirectToRoute("tunisia_mall_produit_ListProduit"));
    }
    public function ModifierAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $user=$this->getUser();
        $Produit=$em->getRepository('EntityBundle:Produit')->find($id);
        $form = $this->createForm(new ProduitForm(),$Produit);
        
        $request=$this->get('request');
        $form->handleRequest($request);
          if($form->isValid())
          {
              $em = $this->getDoctrine()->getManager();
              $em->persist($Produit);
              $em->flush();
              
               return ($this->redirectToRoute("tunisia_mall_produit_ListProduit"));
          }
        
       return $this->render('TunisiaMallProduitBundle:Produit:ModifierProduit.html.twig',array('form'=>$form->createView(),'user' => $user));
    }
    public function rechercherAction()
        {
            $em = $this->getDoctrine()->getManager();
            $Produit = $em->getRepository('EntityBundle:Produit')->findall();
            $Request = $this->get('request');
            if($Request->getMethod()=="POST")
            {
                $Reference = $Request->get('Reference');
                $Produit = $em->getRepository('EntityBundle:Produit:Produit')->findBy(array('Reference'=>$Reference),array('Reference'=>'asc'));
            }
            return $this->render('TunisiaMallProduitBundle:Produit:RechercherProduit.html.twig',array('Produit'=>$Produit));
        }
}
