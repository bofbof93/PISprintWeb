<?php

namespace TunisiaMall\PromotionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AcceuilController extends Controller {

    public function acceuilPAction() {
        $user=$this->getUser();
        $em = $this->getDoctrine()->getManager();
        $promotions = $em->getRepository('EntityBundle:Promotion')->findAll();
        return $this->render('ClientBundle::layout.html.twig', array('promotions' => $promotions,'user' => $user));
    }

    public function afficheAction() {
        return $this->render('PromotionBundle:Promotion:Promotion.html.twig', array());
    }

}
