<?php

namespace TunisiaMall\PromotionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TunisiaMall\EntityBundle\Entity\Promotion;
use TunisiaMall\EntityBundle\Entity\PromotionProduit;

class PromotionController extends Controller {

    public function AjouterPromotionAction() {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $Promotion = new Promotion(); //suite au clic sur le bouton ajouter     

        if ($request->getMethod() == 'POST') {

            $Promotion->setDateDebut(new \DateTime($request->get("date1")));
            $Promotion->setDateFin(new \DateTime($request->get("date2")));
            $Promotion->setTauxdereduction($request->get("taux"));

//            var_dump($Promotion->getImage());
//            $stream = fopen($Promotion->getImage(), 'rb');
//            $Promotion->setImage(stream_get_contents($stream));
            $Promotion->setImage($request->get("img"));

            $Promotion->setDescription($request->get("des"));
            $Promotion->setEtat($request->get("etat"));
            $Promotion->setUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($Promotion); //pou l'ajou d'un objet 
            $em->flush(); //predre consideration le persiste
            return $this->redirectToRoute("rechercher_Promotion");
        }
        return ($this->render("PromotionBundle:Promotion:AjouterPromotion.html.twig", array('user' => $user)));
    }

    public function ModifierPromotionAction($id) {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $promotions = $em->getRepository('EntityBundle:Promotion')->findBy(array('id' => $id));
        $Promotion = $em->getRepository('EntityBundle:Promotion')->find($id);
        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {

            $Promotion->setDateFin(new \DateTime($request->get('date2')));
            $Promotion->setDateDebut(new \DateTime($request->get('date1')));
            $Promotion->setTauxdereduction($request->get('taux'));
            $Promotion->setImage($request->get('img'));
            $Promotion->setDescription($request->get("des"));
            $Promotion->setEtat($request->get('etat'));

            $em->persist($Promotion); //pou l'ajou d'un objet 
            $em->flush(); //predre consideration le persiste
            return ($this->redirectToRoute("rechercher_Promotion"));
        }

        return $this->render('PromotionBundle:Promotion:ModifierPromotion.html.twig', array('promotions' => $promotions,
                    'user' => $user));
    }

    public function rechercherAction() {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $promotions = $em->getRepository("EntityBundle:Promotion")->findBy(array("User" => $user->getId()));
        $request = $this->get('request');
        if ($request->getMethod() == "POST") {
            $search = $request->get('search');
            $promotions = $em->getRepository('EntityBundle:Promotion')
                    ->findBy(array('id' => $search
            ));
        }
        return $this->render('PromotionBundle:Promotion:ListeDesPromotion.html.twig', array('promotions' => $promotions, 'user' => $user));
    }

    public function SupprimerPromotionAction($id) {
        $em = $this->getDoctrine()->getManager();
        $promotion = $em->getRepository('EntityBundle:Promotion')->find($id);
        $em->remove($promotion);
        $em->flush();
        return ($this->redirectToRoute('rechercher_Promotion'));
    }

    public function AjoutPromotionProduitAction($id) {
        $em = $this->getDoctrine()->getManager();
        $promotion = $em->getRepository('EntityBundle:Promotion')->find($id);
        $em->remove($promotion);
        $em->flush();
        return ($this->redirectToRoute('rechercher_Promotion'));
    }

    public function ListeProduitPromotionAction($id) {

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $produits = $em->getRepository("EntityBundle:Produit")->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == "POST") {
            $search = $request->get('search');
            $produits = $em->getRepository('EntityBundle:Produit')
                    ->findBy(array('id' => $search
            ));
        }
        return $this->render('PromotionBundle:Promotion:ListeProduitPromotion.html.twig', array('produits' => $produits, 'idPromotion' => $id, 'user' => $user));
    }

    public function AccepterProduitPromotionAction($id, $idPromotion) {

        $em = $this->getDoctrine()->getManager();

        $idPromotionA = $em->getRepository('EntityBundle:Promotion')->find($idPromotion);
        $idProduit = $em->getRepository('EntityBundle:Produit')->find($id);
        $PromotionA = $em->getRepository('EntityBundle:PromotionProduit')->findBy(array("Promotion" => $idPromotion, "Produit" => $id));
 
        $PromotionP = new PromotionProduit();
        $PromotionP->setProduit($idProduit);
        $PromotionP->setPromotion($idPromotionA);

        $em->persist($PromotionP); //pou l'ajou d'un objet 
        $em->flush(); //predre consideration le persiste
        return $this->redirectToRoute("rechercher_Promotion");
    }
        public function PromotionAcceuilAction() {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $promotions = $em->getRepository('EntityBundle:Promotion')
                ->PromotionActiveDQL();
         
        return $this->render('PromotionBundle:Promotion:Promotion.html.twig', array('promotions' => $promotions, 'user' => $user));
    }
}
