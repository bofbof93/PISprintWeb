<?php

namespace TunisiaMall\PromotionBundle\Repository;

use Doctrine\ORM\EntityRepository;
use TunisiaMall\EntityBundle\Entity\Promotion;

class PromotionRepository extends EntityRepository {

    public function PromotionActiveDQL() {
        $query = $this->getEntityManager()
                ->createQuery("SELECT m from EntityBundle:Promotion m where m.Etat='Active'");
        return $query->getResult();
    }

   

}
