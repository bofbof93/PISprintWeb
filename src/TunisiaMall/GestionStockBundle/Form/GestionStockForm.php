<?php
namespace TunisiaMall\GestionStockBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GestionStockForm
 *
 * @author ISLEM
 */
class GestionStockForm extends AbstractType {
    //put your code here
     public function buildForm(FormBuilderInterface $builder,
            array $options) {
        $builder
              
               
                ->add('Quantiteproduit','integer' , array(
                    'attr' => array(
                        'class' => 'form-control'                            
                    )
                    
                ))
                ->add('DateAjout','date' ,  array(
                'placeholder' => array(
                'year' => 'Year', 'month' => 'Month', 'day' => 'Day'
                        
                    ),        
                    'attr' => array(
                        'class' => 'form-control'                            
                    )
                    
                ))
               
                
                ->add('Enseigne','entity',array('class'=>'EntityBundle:Enseigne','property'=>'Libelle' ,'attr' => array(
                        'class' => 'form-control'                            
                    ) ))
                
                ->add('Produit','entity',array('class'=>'EntityBundle:Produit','property'=>'Libelle' ,'attr' => array(
                        'class' => 'form-control'                            
                    ) ))
                
                ->add('AjouterStock','submit' , array(
                    'attr' => array(
                        'class' => 'btn btn-info pull-right'                            
                    )
                    
                ));
    }
    public function getName()
    {
        return 'Stock';
    }
}
