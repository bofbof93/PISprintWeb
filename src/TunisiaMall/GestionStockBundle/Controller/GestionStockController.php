<?php
namespace TunisiaMall\GestionStockBundle\Controller;
use TunisiaMall\EntityBundle\Entity\Stock;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TunisiaMall\GestionStockBundle\Form\GestionStockForm;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GestionStockController
 *
 * @author ISLEM
 */
class GestionStockController extends Controller{
    //put your code here
    public function AjouterAction()
    {
        $user=$this->getUser();
       $em = $this->getDoctrine()->getManager();
       $Stock = new Stock();
       $form = $this->createForm(new GestionStockForm(), $Stock);
       $request = $this->get('request');
       $form->handleRequest($request);
       if($form->isValid())
       {
           
           $em->persist($Stock);
           $em->flush();
           return $this->redirect($this->generateUrl('tunisia_mall_stock_ListStock'));
       }
       return $this->render('TunisiaMallGestionStockBundle:GestionStock:AjouterStock.html.twig',
               array(
                   'form'=>$form->createView(),'user' => $user
               ));
            //return ($this->redirectToRoute("tunisia_mall_produit_ListProduit"));//bech thizek lil page ili feha affichage ta3 tableu kol mil BD
        }
       
    public function ListAction()
    {   $user=$this->getUser();
        $em=$this->getDoctrine()->getManager();
        $Stock=$em->getRepository('EntityBundle:Stock')->findAll();
        
        return $this->render('TunisiaMallGestionStockBundle:GestionStock:ListStock.html.twig',array('Stock'=>$Stock,'user' => $user ));
    }
    public function SupprimerAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $Stock=$em->getRepository('EntityBundle:Stock')->find($id);
        $em->remove($Stock);
        $em->flush();
        return ($this->redirectToRoute("tunisia_mall_stock_ListStock"));
    }
    public function ModifierAction($id)
    {
        $user=$this->getUser();
        $em=$this->getDoctrine()->getManager();
        $Stock=$em->getRepository('EntityBundle:Stock')->find($id);
        $form = $this->createForm(new GestionStockForm(),$Stock);
        $request=$this->get('request');
        $form->handleRequest($request);
          if($form->isValid())
          {
              $em = $this->getDoctrine()->getManager();
              $em->persist($Stock);
              $em->flush();
              
               return ($this->redirectToRoute("tunisia_mall_stock_ListStock"));
          }
        
       return $this->render('TunisiaMallGestionStockBundle:GestionStock:ModifierStock.html.twig',array('form'=>$form->createView(),'user' => $user));
    }
    public function rechercherAction()
        {
            $em = $this->getDoctrine()->getManager();
            $Stock = $em->getRepository('EntityBundle:Stock')->findall();
            $Request = $this->get('request');
            if($Request->getMethod()=="POST")
            {
                $id = $Request->get('id');
                $Stock = $em->getRepository('EntityBundle:Stock:Stock')->findBy(array('id'=>$id),array('id'=>'asc'));
            }
            return $this->render('TunisiaMallGestionStockBundle:GestionStock:RechercherStock.html.twig',array('Stock'=>$Stock));
        }
}
