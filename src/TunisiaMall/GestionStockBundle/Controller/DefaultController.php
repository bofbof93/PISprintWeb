<?php

namespace TunisiaMall\GestionStockBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('TunisiaMallGestionStockBundle:Default:index.html.twig', array('name' => $name));
    }
}
