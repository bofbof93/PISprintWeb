<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new AppBundle\AppBundle(),
            new TunisiaMall\ClientBundle\ClientBundle(),
            new TunisiaMall\AdminBundle\AdminBundle(),
            new TunisiaMall\EntityBundle\EntityBundle(),
            new TunisiaMall\ResponsableBundle\ResponsableBundle(),
            new Users\UsersBundle\UsersUsersBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new TunisiaMall\ActualiteBundle\TunisiaMallActualiteBundle(),
            new Ob\HighchartsBundle\ObHighchartsBundle(),
            new TunisiaBundle\StatistiqueBundle\TunisiaBundleStatistiqueBundle(),
            new TunisiaMall\StatBundle\TunisiaMallStatBundle(),
            new TunisiaMall\EnseigneBundle\TunisiaMallEnseigneBundle(),
            new Nomaya\SocialBundle\NomayaSocialBundle(),
            new TunisiaMall\CarteBundle\TunisiaMallCarteBundle(),
            new TunisiaMall\GestionStockBundle\TunisiaMallGestionStockBundle(),
            new TunisiaMall\ProduitBundle\TunisiaMallProduitBundle(),
            new TunisiaMall\PromotionBundle\PromotionBundle(),
            new TunisiaMall\PackPubBundle\PackPubBundle(),
            
            new FOS\RestBundle\FOSRestBundle(),
            new FOS\CommentBundle\FOSCommentBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle($this),
            new HWI\Bundle\OAuthBundle\HWIOAuthBundle(),
            new TunisiaMall\ReservationPackBundle\ReservationPackBundle(),
            new TunisiaMall\PromotionProduitBundle\PromotionProduitBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Acme\DemoBundle\AcmeDemoBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
