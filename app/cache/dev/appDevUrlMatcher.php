<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/js/35a8e64')) {
            // _assetic_35a8e64
            if ($pathinfo === '/js/35a8e64.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '35a8e64',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_35a8e64',);
            }

            // _assetic_35a8e64_0
            if ($pathinfo === '/js/35a8e64_comments_1.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '35a8e64',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_35a8e64_0',);
            }

        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // promotion_produit_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'promotion_produit_homepage')), array (  '_controller' => 'TunisiaMall\\PromotionProduitBundle\\Controller\\DefaultController::indexAction',));
        }

        // tunisia_mall_promprod_Ajouterpromprod
        if ($pathinfo === '/Ajouterpromprod') {
            return array (  '_controller' => 'TunisiaMall\\PromotionProduitBundle\\Controller\\PromotionProduitController::AjouterpromprodAction',  '_route' => 'tunisia_mall_promprod_Ajouterpromprod',);
        }

        // tunisia_mall_promprod_Listpromprod
        if ($pathinfo === '/Listpromprod') {
            return array (  '_controller' => 'TunisiaMall\\PromotionProduitBundle\\Controller\\PromotionProduitController::ListpromprodAction',  '_route' => 'tunisia_mall_promprod_Listpromprod',);
        }

        // tunisia_mall_promprod_Supp
        if (0 === strpos($pathinfo, '/Supprimerpromprod') && preg_match('#^/Supprimerpromprod/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_promprod_Supp')), array (  '_controller' => 'TunisiaMall\\PromotionProduitBundle\\Controller\\PromotionProduitController::SupprimerpromprodAction',));
        }

        // tunisia_mall_promprod_Modif
        if (0 === strpos($pathinfo, '/Modifierpromprod') && preg_match('#^/Modifierpromprod/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_promprod_Modif')), array (  '_controller' => 'TunisiaMall\\PromotionProduitBundle\\Controller\\PromotionProduitController::ModifierpromprodAction',));
        }

        if (0 === strpos($pathinfo, '/List')) {
            // tunisia_mall_promprod_ListpromprodClient
            if ($pathinfo === '/ListpromprodClient') {
                return array (  '_controller' => 'TunisiaMall\\PromotionProduitBundle\\Controller\\PromotionProduitController::ListpromprodClientAction',  '_route' => 'tunisia_mall_promprod_ListpromprodClient',);
            }

            // reservation_pack_pub_Liste
            if ($pathinfo === '/ListeReservationPack') {
                return array (  '_controller' => 'TunisiaMall\\ReservationPackBundle\\Controller\\ReservationPackController::ListeAction',  '_route' => 'reservation_pack_pub_Liste',);
            }

        }

        // Reserver_pack
        if (0 === strpos($pathinfo, '/ReserverPack') && preg_match('#^/ReserverPack/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Reserver_pack')), array (  '_controller' => 'TunisiaMall\\ReservationPackBundle\\Controller\\ReservationPackController::ReserverAction',));
        }

        // historique
        if ($pathinfo === '/ListDeReservation') {
            return array (  '_controller' => 'TunisiaMall\\ReservationPackBundle\\Controller\\ReservationPackController::HistoriqueAction',  '_route' => 'historique',);
        }

        // Accepter_Demande
        if (0 === strpos($pathinfo, '/Accepter') && preg_match('#^/Accepter/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Accepter_Demande')), array (  '_controller' => 'TunisiaMall\\ReservationPackBundle\\Controller\\ReservationPackController::AccepterReservationAction',));
        }

        // Refuser_Demande
        if (0 === strpos($pathinfo, '/Refuser') && preg_match('#^/Refuser/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Refuser_Demande')), array (  '_controller' => 'TunisiaMall\\ReservationPackBundle\\Controller\\ReservationPackController::RefuserReservationAction',));
        }

        if (0 === strpos($pathinfo, '/List')) {
            // Demande_Reservation
            if ($pathinfo === '/ListeDemandeReservation') {
                return array (  '_controller' => 'TunisiaMall\\ReservationPackBundle\\Controller\\ReservationPackController::DemandeReservationAction',  '_route' => 'Demande_Reservation',);
            }

            // ReservationAPayer
            if ($pathinfo === '/ListResAPayer') {
                return array (  '_controller' => 'TunisiaMall\\ReservationPackBundle\\Controller\\ReservationPackController::ReservationAPayerAction',  '_route' => 'ReservationAPayer',);
            }

        }

        // Payer
        if (0 === strpos($pathinfo, '/Payer') && preg_match('#^/Payer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Payer')), array (  '_controller' => 'TunisiaMall\\ReservationPackBundle\\Controller\\ReservationPackController::PaiementAction',));
        }

        // Confirm_carte
        if (0 === strpos($pathinfo, '/Confirm') && preg_match('#^/Confirm/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Confirm_carte')), array (  '_controller' => 'TunisiaMall\\ReservationPackBundle\\Controller\\ReservationPackController::ConfirmAction',));
        }

        // pack_pub_ajouter
        if ($pathinfo === '/ajouterPack') {
            return array (  '_controller' => 'TunisiaMall\\PackPubBundle\\Controller\\PackPubController::AjouterPackAction',  '_route' => 'pack_pub_ajouter',);
        }

        // pack_pub_Liste
        if ($pathinfo === '/ListPack') {
            return array (  '_controller' => 'TunisiaMall\\PackPubBundle\\Controller\\PackPubController::ListeDesPacksAction',  '_route' => 'pack_pub_Liste',);
        }

        // Modifier_pack
        if (0 === strpos($pathinfo, '/ModifierPack') && preg_match('#^/ModifierPack/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Modifier_pack')), array (  '_controller' => 'TunisiaMall\\PackPubBundle\\Controller\\PackPubController::modifierPackAction',));
        }

        // Supprimer_pack
        if (0 === strpos($pathinfo, '/SupprimerPack') && preg_match('#^/SupprimerPack/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Supprimer_pack')), array (  '_controller' => 'TunisiaMall\\PackPubBundle\\Controller\\PackPubController::supprimerPackAction',));
        }

        // promotion_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'promotion_homepage')), array (  '_controller' => 'PromotionBundle:Default:index',));
        }

        // promotion_ajouter
        if ($pathinfo === '/AjouterPromotion') {
            return array (  '_controller' => 'TunisiaMall\\PromotionBundle\\Controller\\PromotionController::AjouterPromotionAction',  '_route' => 'promotion_ajouter',);
        }

        // Supprimer_promotion
        if (0 === strpos($pathinfo, '/SupprimerPromotion') && preg_match('#^/SupprimerPromotion/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Supprimer_promotion')), array (  '_controller' => 'TunisiaMall\\PromotionBundle\\Controller\\PromotionController::SupprimerPromotionAction',));
        }

        // Afficher_Modifier
        if (0 === strpos($pathinfo, '/ModifierPromotion') && preg_match('#^/ModifierPromotion/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Afficher_Modifier')), array (  '_controller' => 'TunisiaMall\\PromotionBundle\\Controller\\PromotionController::ModifierPromotionAction',));
        }

        // rechercher_Promotion
        if ($pathinfo === '/rechercherPromotion') {
            return array (  '_controller' => 'TunisiaMall\\PromotionBundle\\Controller\\PromotionController::rechercherAction',  '_route' => 'rechercher_Promotion',);
        }

        // image_route
        if (0 === strpos($pathinfo, '/images') && preg_match('#^/images/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'image_route')), array (  '_controller' => 'PromotionBundle:AfficherImage:photo',));
        }

        // acceuil_promo
        if ($pathinfo === '/acceuilP') {
            return array (  '_controller' => 'TunisiaMall\\PromotionBundle\\Controller\\AcceuilController::acceuilPAction',  '_route' => 'acceuil_promo',);
        }

        if (0 === strpos($pathinfo, '/Pro')) {
            // Comment
            if ($pathinfo === '/Promotion') {
                return array (  '_controller' => 'TunisiaMall\\PromotionBundle\\Controller\\PromotionController::PromotionAcceuilAction',  '_route' => 'Comment',);
            }

            // Liste_Produit
            if (0 === strpos($pathinfo, '/ProduitPromotion') && preg_match('#^/ProduitPromotion/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Liste_Produit')), array (  '_controller' => 'TunisiaMall\\PromotionBundle\\Controller\\PromotionController::ListeProduitPromotionAction',));
            }

        }

        // Accepter_Produit
        if (0 === strpos($pathinfo, '/AccepterProduitPromotion') && preg_match('#^/AccepterProduitPromotion/(?P<id>[^/]++)/(?P<idPromotion>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Accepter_Produit')), array (  '_controller' => 'TunisiaMall\\PromotionBundle\\Controller\\PromotionController::AccepterProduitPromotionAction',));
        }

        // tunisia_mall_produit_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_produit_homepage')), array (  '_controller' => 'TunisiaMall\\ProduitBundle\\Controller\\DefaultController::indexAction',));
        }

        // tunisia_mall_produit_AjouterProduit
        if ($pathinfo === '/AjouterProduit') {
            return array (  '_controller' => 'TunisiaMall\\ProduitBundle\\Controller\\ProduitController::AjouterAction',  '_route' => 'tunisia_mall_produit_AjouterProduit',);
        }

        // tunisia_mall_produit_ListProduit
        if ($pathinfo === '/ListProduit') {
            return array (  '_controller' => 'TunisiaMall\\ProduitBundle\\Controller\\ProduitController::ListProduitAction',  '_route' => 'tunisia_mall_produit_ListProduit',);
        }

        // tunisia_mall_produit_Supp
        if (0 === strpos($pathinfo, '/SupprimerProduit') && preg_match('#^/SupprimerProduit/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_produit_Supp')), array (  '_controller' => 'TunisiaMall\\ProduitBundle\\Controller\\ProduitController::SupprimerAction',));
        }

        // tunisia_mall_produit_Modif
        if (0 === strpos($pathinfo, '/ModifierProduit') && preg_match('#^/ModifierProduit/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_produit_Modif')), array (  '_controller' => 'TunisiaMall\\ProduitBundle\\Controller\\ProduitController::ModifierAction',));
        }

        // tunisia_mall_produit_Rech
        if ($pathinfo === '/rechercher') {
            return array (  '_controller' => 'TunisiaMall\\ProduitBundle\\Controller\\ProduitController::rechercherAction',  '_route' => 'tunisia_mall_produit_Rech',);
        }

        // tunisia_mall_gestion_stock_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_gestion_stock_homepage')), array (  '_controller' => 'TunisiaMall\\GestionStockBundle\\Controller\\DefaultController::indexAction',));
        }

        // tunisia_mall_stock_AjouterStock
        if ($pathinfo === '/AjouterStock') {
            return array (  '_controller' => 'TunisiaMall\\GestionStockBundle\\Controller\\GestionStockController::AjouterAction',  '_route' => 'tunisia_mall_stock_AjouterStock',);
        }

        // tunisia_mall_stock_ListStock
        if ($pathinfo === '/ListStock') {
            return array (  '_controller' => 'TunisiaMall\\GestionStockBundle\\Controller\\GestionStockController::ListAction',  '_route' => 'tunisia_mall_stock_ListStock',);
        }

        // tunisia_mall_stock_Supp
        if (0 === strpos($pathinfo, '/SupprimerStock') && preg_match('#^/SupprimerStock/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_stock_Supp')), array (  '_controller' => 'TunisiaMall\\GestionStockBundle\\Controller\\GestionStockController::SupprimerAction',));
        }

        // tunisia_mall_stock_Modif
        if (0 === strpos($pathinfo, '/ModifierStock') && preg_match('#^/ModifierStock/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_stock_Modif')), array (  '_controller' => 'TunisiaMall\\GestionStockBundle\\Controller\\GestionStockController::ModifierAction',));
        }

        // tunisia_mall_stock_Rech
        if ($pathinfo === '/rechercherStock') {
            return array (  '_controller' => 'TunisiaMall\\GestionStockBundle\\Controller\\GestionStockController::rechercherAction',  '_route' => 'tunisia_mall_stock_Rech',);
        }

        // tunisia_mall_carte_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_carte_homepage')), array (  '_controller' => 'TunisiaMall\\CarteBundle\\Controller\\DefaultController::indexAction',));
        }

        if (0 === strpos($pathinfo, '/Cartes')) {
            // tunisia_mall_responsable_Carte
            if ($pathinfo === '/Cartes') {
                return array (  '_controller' => 'TunisiaMall\\CarteBundle\\Controller\\GestionCarteController::IndexAction',  '_route' => 'tunisia_mall_responsable_Carte',);
            }

            // tunisia_mall_responsable_Carte_afficher
            if (preg_match('#^/Cartes/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_responsable_Carte_afficher')), array (  '_controller' => 'TunisiaMall\\CarteBundle\\Controller\\GestionCarteController::AfficherAction',));
            }

            // tunisia_mall_responsable_Carte_edit
            if (0 === strpos($pathinfo, '/Cartes/edit') && preg_match('#^/Cartes/edit/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_responsable_Carte_edit')), array (  '_controller' => 'TunisiaMall\\CarteBundle\\Controller\\GestionCarteController::EditAction',));
            }

            // tunisia_mall_responsable_Carte_delete
            if (0 === strpos($pathinfo, '/Cartes/delete') && preg_match('#^/Cartes/delete/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_responsable_Carte_delete')), array (  '_controller' => 'TunisiaMall\\CarteBundle\\Controller\\GestionCarteController::DeleteAction',));
            }

        }

        // tunisia_mall_responsable_Carte_ajout
        if ($pathinfo === '/ajouter') {
            return array (  '_controller' => 'TunisiaMall\\CarteBundle\\Controller\\GestionCarteController::AjoutAction',  '_route' => 'tunisia_mall_responsable_Carte_ajout',);
        }

        // enseigne_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'enseigne_homepage')), array (  '_controller' => 'EnseigneBundle:Default:index',));
        }

        // enseigne_ajouter
        if ($pathinfo === '/AjouterEnseigne') {
            return array (  '_controller' => 'TunisiaMall\\EnseigneBundle\\Controller\\AjouterController::AjouterEnseigneAction',  '_route' => 'enseigne_ajouter',);
        }

        // rechercher_Enseigne
        if ($pathinfo === '/rechercherEnseigne') {
            return array (  '_controller' => 'TunisiaMall\\EnseigneBundle\\Controller\\RechercherController::rechercherAction',  '_route' => 'rechercher_Enseigne',);
        }

        // Supprimer_Enseigne
        if (0 === strpos($pathinfo, '/SupprimerEnseigne') && preg_match('#^/SupprimerEnseigne/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Supprimer_Enseigne')), array (  '_controller' => 'TunisiaMall\\EnseigneBundle\\Controller\\SupprimerController::SupprimerEnseigneAction',));
        }

        // Enseigne_Modifier
        if (0 === strpos($pathinfo, '/ModifierEnseigne') && preg_match('#^/ModifierEnseigne/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Enseigne_Modifier')), array (  '_controller' => 'TunisiaMall\\EnseigneBundle\\Controller\\ModifierController::ModifierEnseigneAction',));
        }

        // tunisia_mall_stat_homepage
        if ($pathinfo === '/statistique') {
            return array (  '_controller' => 'TunisiaMall\\StatBundle\\Controller\\StatController::chartLineAction',  '_route' => 'tunisia_mall_stat_homepage',);
        }

        if (0 === strpos($pathinfo, '/hello')) {
            // tunisia_bundle_statistique_homepage
            if (preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_bundle_statistique_homepage')), array (  '_controller' => 'TunisiaBundle\\StatistiqueBundle\\Controller\\DefaultController::indexAction',));
            }

            // tunisia_mall_actualite_homepage
            if (preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_actualite_homepage')), array (  '_controller' => 'TunisiaMall\\ActualiteBundle\\Controller\\DefaultController::indexAction',));
            }

        }

        // tunisia_mall_actualite_Ajouter
        if ($pathinfo === '/AjouterActualite') {
            return array (  '_controller' => 'TunisiaMall\\ActualiteBundle\\Controller\\ActualiteController::AjouterAction',  '_route' => 'tunisia_mall_actualite_Ajouter',);
        }

        // tunisia_mall_actualite_List
        if ($pathinfo === '/ListActualites') {
            return array (  '_controller' => 'TunisiaMall\\ActualiteBundle\\Controller\\ActualiteController::ListeActualitesAction',  '_route' => 'tunisia_mall_actualite_List',);
        }

        // tunisia_mall_actualite_Supprimer
        if (0 === strpos($pathinfo, '/SupprimerActualites') && preg_match('#^/SupprimerActualites(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_actualite_Supprimer')), array (  '_controller' => 'TunisiaMall\\ActualiteBundle\\Controller\\ActualiteController::SupprimeAction',));
        }

        // tunisia_mall_actualite_MAJ
        if (0 === strpos($pathinfo, '/MAJ') && preg_match('#^/MAJ(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_actualite_MAJ')), array (  '_controller' => 'TunisiaMall\\ActualiteBundle\\Controller\\ActualiteController::MAJAction',));
        }

        // tunisia_mall_actualite_List_Client
        if ($pathinfo === '/Actualites') {
            return array (  '_controller' => 'TunisiaMall\\ActualiteBundle\\Controller\\ActualiteController::ListeActualitesClientAction',  '_route' => 'tunisia_mall_actualite_List_Client',);
        }

        if (0 === strpos($pathinfo, '/hello')) {
            // users_users_homepage
            if (preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'users_users_homepage')), array (  '_controller' => 'Users\\UsersBundle\\Controller\\DefaultController::indexAction',));
            }

            // tunisia_mall_responsable_homepage
            if (preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_responsable_homepage')), array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\DefaultController::indexAction',));
            }

        }

        // tunisia_mall_responsable_affichage
        if ($pathinfo === '/Responsable') {
            return array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\ResponsableController::afficherAction',  '_route' => 'tunisia_mall_responsable_affichage',);
        }

        if (0 === strpos($pathinfo, '/Comptes')) {
            // tunisia_mall_responsable_GestionComptes
            if ($pathinfo === '/Comptes') {
                return array (  '_controller' => 'TunisiaMall\\AdminBundle\\Controller\\AfficheController::GestionDesComptesAction',  '_route' => 'tunisia_mall_responsable_GestionComptes',);
            }

            // tunisia_mall_responsable_GestionComptesEnseigne
            if ($pathinfo === '/ComptesEnseigne') {
                return array (  '_controller' => 'TunisiaMall\\AdminBundle\\Controller\\AfficheController::GestionDesComptesEnseigneAction',  '_route' => 'tunisia_mall_responsable_GestionComptesEnseigne',);
            }

        }

        // tunisia_mall_responsable_GestionProduits
        if ($pathinfo === '/Produits') {
            return array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\ResponsableController::GestionDesProduitsAction',  '_route' => 'tunisia_mall_responsable_GestionProduits',);
        }

        // tunisia_mall_responsable_GestionStock
        if ($pathinfo === '/Stock') {
            return array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\ResponsableController::GestionDuStockAction',  '_route' => 'tunisia_mall_responsable_GestionStock',);
        }

        // tunisia_mall_responsable_GestionEnseignes
        if ($pathinfo === '/Enseignes') {
            return array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\ResponsableController::GestionDesEnseignesAction',  '_route' => 'tunisia_mall_responsable_GestionEnseignes',);
        }

        // tunisia_mall_responsable_GestionPub
        if ($pathinfo === '/Pub') {
            return array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\ResponsableController::GestionDesPackPublicitaireAction',  '_route' => 'tunisia_mall_responsable_GestionPub',);
        }

        // tunisia_mall_responsable_GestionCatalogues
        if ($pathinfo === '/Catalogues') {
            return array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\ResponsableController::GestionDesCataloguesAction',  '_route' => 'tunisia_mall_responsable_GestionCatalogues',);
        }

        // tunisia_mall_responsable_Statistique
        if ($pathinfo === '/Stat') {
            return array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\ResponsableController::StatistiqueAction',  '_route' => 'tunisia_mall_responsable_Statistique',);
        }

        if (0 === strpos($pathinfo, '/a')) {
            // tunisia_mall_Admin_affichage
            if ($pathinfo === '/admin') {
                return array (  '_controller' => 'TunisiaMall\\AdminBundle\\Controller\\AfficheController::afficherAction',  '_route' => 'tunisia_mall_Admin_affichage',);
            }

            // tunisia_mall_Admin_accept_compte
            if (0 === strpos($pathinfo, '/accept') && preg_match('#^/accept/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_Admin_accept_compte')), array (  '_controller' => 'TunisiaMall\\AdminBundle\\Controller\\AfficheController::AccepterCompteAction',));
            }

        }

        // tunisia_mall_Admin_AjouterProduit
        if ($pathinfo === '/AjouterProduit') {
            return array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\ResponsableController::AjouterProduitAction',  '_route' => 'tunisia_mall_Admin_AjouterProduit',);
        }

        // tunisia_mall_Admin_stock
        if ($pathinfo === '/formulaire') {
            return array (  '_controller' => 'TunisiaMall\\AdminBundle\\Controller\\AfficheController::formulaireAction',  '_route' => 'tunisia_mall_Admin_stock',);
        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }
                not_fos_user_security_login:

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_security_logout;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }
            not_fos_user_security_logout:

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_profile_edit;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }
            not_fos_user_profile_edit:

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/registration')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/registration') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_registration_register;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                }
                not_fos_user_registration_register:

                if (0 === strpos($pathinfo, '/registration/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/registration/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/registration/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/registration/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/registration/confirmed') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                // fos_user_resetting_request
                if ($pathinfo === '/resetting/request') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ($pathinfo === '/resetting/send-email') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ($pathinfo === '/resetting/check-email') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
                }
                not_fos_user_resetting_reset:

            }

        }

        // fos_user_change_password
        if ($pathinfo === '/change/change-password') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }

            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        // hwi_oauth_connect
        if (rtrim($pathinfo, '/') === '/auth') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'hwi_oauth_connect');
            }

            return array (  '_controller' => 'HWI\\Bundle\\OAuthBundle\\Controller\\ConnectController::connectAction',  '_route' => 'hwi_oauth_connect',);
        }

        if (0 === strpos($pathinfo, '/login')) {
            // hwi_oauth_connect_service
            if (0 === strpos($pathinfo, '/login/service') && preg_match('#^/login/service/(?P<service>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'hwi_oauth_connect_service')), array (  '_controller' => 'HWI\\Bundle\\OAuthBundle\\Controller\\ConnectController::connectServiceAction',));
            }

            // hwi_oauth_connect_registration
            if (0 === strpos($pathinfo, '/login/registration') && preg_match('#^/login/registration/(?P<key>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'hwi_oauth_connect_registration')), array (  '_controller' => 'HWI\\Bundle\\OAuthBundle\\Controller\\ConnectController::registrationAction',));
            }

            // hwi_oauth_service_redirect
            if (0 === strpos($pathinfo, '/loginRedirect') && preg_match('#^/loginRedirect/(?P<service>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'hwi_oauth_service_redirect')), array (  '_controller' => 'HWI\\Bundle\\OAuthBundle\\Controller\\ConnectController::redirectToServiceAction',));
            }

            if (0 === strpos($pathinfo, '/login/check-')) {
                // facebook_login
                if ($pathinfo === '/login/check-facebook') {
                    return array('_route' => 'facebook_login');
                }

                // google_login
                if ($pathinfo === '/login/check-google') {
                    return array('_route' => 'google_login');
                }

            }

        }

        // tunisia_mall_client_affichage
        if ($pathinfo === '/acceuil') {
            return array (  '_controller' => 'TunisiaMall\\ClientBundle\\Controller\\AfficheController::afficherAction',  '_route' => 'tunisia_mall_client_affichage',);
        }

        // tunisia_mall_client_catalogue
        if ($pathinfo === '/catalogue') {
            return array (  '_controller' => 'TunisiaMall\\ClientBundle\\Controller\\AfficheController::catalogueAction',  '_route' => 'tunisia_mall_client_catalogue',);
        }

        // tunisia_mall_client_gallerie
        if ($pathinfo === '/gallerie') {
            return array (  '_controller' => 'TunisiaMall\\ClientBundle\\Controller\\AfficheController::gallerieAction',  '_route' => 'tunisia_mall_client_gallerie',);
        }

        // tunisia_mall_client_fashion
        if ($pathinfo === '/fashion') {
            return array (  '_controller' => 'TunisiaMall\\ClientBundle\\Controller\\AfficheController::fashionAction',  '_route' => 'tunisia_mall_client_fashion',);
        }

        // tunisia_mall_client_boutique
        if ($pathinfo === '/boutique') {
            return array (  '_controller' => 'TunisiaMall\\ClientBundle\\Controller\\AfficheController::boutiqueAction',  '_route' => 'tunisia_mall_client_boutique',);
        }

        // tunisia_mall_client_contact
        if ($pathinfo === '/contact') {
            return array (  '_controller' => 'TunisiaMall\\ClientBundle\\Controller\\AfficheController::contactAction',  '_route' => 'tunisia_mall_client_contact',);
        }

        // tunisia_mall_client_panier
        if ($pathinfo === '/panier') {
            return array (  '_controller' => 'TunisiaMall\\ClientBundle\\Controller\\AfficheController::panierAction',  '_route' => 'tunisia_mall_client_panier',);
        }

        if (0 === strpos($pathinfo, '/Panier')) {
            // tunisia_mall_panier_ajout
            if (0 === strpos($pathinfo, '/Panier/add') && preg_match('#^/Panier/add/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_panier_ajout')), array (  '_controller' => 'TunisiaMall\\ClientBundle\\Controller\\PanierController::ajouterAction',));
            }

            // tunisia_mall_panier_ajout_supp_ajax
            if ($pathinfo === '/Panier/changerQuantiter') {
                return array (  '_controller' => 'TunisiaMall\\ClientBundle\\Controller\\PanierController::changerQuantiterAction',  '_route' => 'tunisia_mall_panier_ajout_supp_ajax',);
            }

            // tunisia_mall_panier_supp
            if (0 === strpos($pathinfo, '/Panier/supprimerligne') && preg_match('#^/Panier/supprimerligne/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_panier_supp')), array (  '_controller' => 'TunisiaMall\\ClientBundle\\Controller\\PanierController::supprimerAction',));
            }

        }

        if (0 === strpos($pathinfo, '/payement')) {
            // payement_carte_fidelite
            if (preg_match('#^/payement/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'payement_carte_fidelite')), array (  '_controller' => 'TunisiaMall\\ClientBundle\\Controller\\AfficheController::payementAction',));
            }

            // payement_carte_fidelite_confirm
            if (preg_match('#^/payement/(?P<id>[^/]++)/confirm$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'payement_carte_fidelite_confirm')), array (  '_controller' => 'TunisiaMall\\ClientBundle\\Controller\\AfficheController::confirmAction',));
            }

        }

        if (0 === strpos($pathinfo, '/ap')) {
            // homepage
            if ($pathinfo === '/app/example') {
                return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
            }

            if (0 === strpos($pathinfo, '/api/threads')) {
                // fos_comment_new_threads
                if (0 === strpos($pathinfo, '/api/threads/new') && preg_match('#^/api/threads/new(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_new_threads;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_new_threads')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::newThreadsAction',  '_format' => 'html',));
                }
                not_fos_comment_new_threads:

                // fos_comment_edit_thread_commentable
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/commentable/edit(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_edit_thread_commentable;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_edit_thread_commentable')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::editThreadCommentableAction',  '_format' => 'html',));
                }
                not_fos_comment_edit_thread_commentable:

                // fos_comment_new_thread_comments
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments/new(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_new_thread_comments;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_new_thread_comments')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::newThreadCommentsAction',  '_format' => 'html',));
                }
                not_fos_comment_new_thread_comments:

                // fos_comment_remove_thread_comment
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/]++)/remove(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_remove_thread_comment;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_remove_thread_comment')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::removeThreadCommentAction',  '_format' => 'html',));
                }
                not_fos_comment_remove_thread_comment:

                // fos_comment_edit_thread_comment
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/]++)/edit(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_edit_thread_comment;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_edit_thread_comment')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::editThreadCommentAction',  '_format' => 'html',));
                }
                not_fos_comment_edit_thread_comment:

                // fos_comment_new_thread_comment_votes
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/]++)/votes/new(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_new_thread_comment_votes;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_new_thread_comment_votes')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::newThreadCommentVotesAction',  '_format' => 'html',));
                }
                not_fos_comment_new_thread_comment_votes:

                // fos_comment_get_thread
                if (preg_match('#^/api/threads/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_get_thread;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_get_thread')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::getThreadAction',  '_format' => 'html',));
                }
                not_fos_comment_get_thread:

                // fos_comment_get_threads
                if (preg_match('#^/api/threads(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_get_threads;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_get_threads')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::getThreadsActions',  '_format' => 'html',));
                }
                not_fos_comment_get_threads:

                // fos_comment_post_threads
                if (preg_match('#^/api/threads(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_comment_post_threads;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_post_threads')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::postThreadsAction',  '_format' => 'html',));
                }
                not_fos_comment_post_threads:

                // fos_comment_patch_thread_commentable
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/commentable(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PATCH') {
                        $allow[] = 'PATCH';
                        goto not_fos_comment_patch_thread_commentable;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_patch_thread_commentable')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::patchThreadCommentableAction',  '_format' => 'html',));
                }
                not_fos_comment_patch_thread_commentable:

                // fos_comment_get_thread_comment
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_get_thread_comment;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_get_thread_comment')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::getThreadCommentAction',  '_format' => 'html',));
                }
                not_fos_comment_get_thread_comment:

                // fos_comment_patch_thread_comment_state
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/]++)/state(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PATCH') {
                        $allow[] = 'PATCH';
                        goto not_fos_comment_patch_thread_comment_state;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_patch_thread_comment_state')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::patchThreadCommentStateAction',  '_format' => 'html',));
                }
                not_fos_comment_patch_thread_comment_state:

                // fos_comment_put_thread_comments
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_fos_comment_put_thread_comments;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_put_thread_comments')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::putThreadCommentsAction',  '_format' => 'html',));
                }
                not_fos_comment_put_thread_comments:

                // fos_comment_get_thread_comments
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_get_thread_comments;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_get_thread_comments')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::getThreadCommentsAction',  '_format' => 'html',));
                }
                not_fos_comment_get_thread_comments:

                // fos_comment_post_thread_comments
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_comment_post_thread_comments;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_post_thread_comments')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::postThreadCommentsAction',  '_format' => 'html',));
                }
                not_fos_comment_post_thread_comments:

                // fos_comment_get_thread_comment_votes
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/]++)/votes(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_get_thread_comment_votes;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_get_thread_comment_votes')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::getThreadCommentVotesAction',  '_format' => 'html',));
                }
                not_fos_comment_get_thread_comment_votes:

                // fos_comment_post_thread_comment_votes
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/]++)/votes(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_comment_post_thread_comment_votes;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_post_thread_comment_votes')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::postThreadCommentVotesAction',  '_format' => 'html',));
                }
                not_fos_comment_post_thread_comment_votes:

            }

        }

        // _welcome
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_welcome');
            }

            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\WelcomeController::indexAction',  '_route' => '_welcome',);
        }

        if (0 === strpos($pathinfo, '/demo')) {
            if (0 === strpos($pathinfo, '/demo/secured')) {
                if (0 === strpos($pathinfo, '/demo/secured/log')) {
                    if (0 === strpos($pathinfo, '/demo/secured/login')) {
                        // _demo_login
                        if ($pathinfo === '/demo/secured/login') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::loginAction',  '_route' => '_demo_login',);
                        }

                        // _demo_security_check
                        if ($pathinfo === '/demo/secured/login_check') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::securityCheckAction',  '_route' => '_demo_security_check',);
                        }

                    }

                    // _demo_logout
                    if ($pathinfo === '/demo/secured/logout') {
                        return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::logoutAction',  '_route' => '_demo_logout',);
                    }

                }

                if (0 === strpos($pathinfo, '/demo/secured/hello')) {
                    // acme_demo_secured_hello
                    if ($pathinfo === '/demo/secured/hello') {
                        return array (  'name' => 'World',  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',  '_route' => 'acme_demo_secured_hello',);
                    }

                    // _demo_secured_hello
                    if (preg_match('#^/demo/secured/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',));
                    }

                    // _demo_secured_hello_admin
                    if (0 === strpos($pathinfo, '/demo/secured/hello/admin') && preg_match('#^/demo/secured/hello/admin/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello_admin')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloadminAction',));
                    }

                }

            }

            // _demo
            if (rtrim($pathinfo, '/') === '/demo') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_demo');
                }

                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::indexAction',  '_route' => '_demo',);
            }

            // _demo_hello
            if (0 === strpos($pathinfo, '/demo/hello') && preg_match('#^/demo/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::helloAction',));
            }

            // _demo_contact
            if ($pathinfo === '/demo/contact') {
                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::contactAction',  '_route' => '_demo_contact',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
