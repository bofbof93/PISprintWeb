<?php

/* TunisiaMallActualiteBundle:Actualite:UpdateActualite.html.twig */
class __TwigTemplate_542b6cffaf8a150cf88c56076f1af41b0c5eacef15cd3ef2fa6c399cfe5dfdf1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ResponsableBundle::Layout.html.twig", "TunisiaMallActualiteBundle:Actualite:UpdateActualite.html.twig", 1);
        $this->blocks = array(
            'TableView' => array($this, 'block_TableView'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ResponsableBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_TableView($context, array $blocks = array())
    {
        // line 3
        echo "
    <div class=\"col-md-6\">
        <!-- Horizontal Form -->
        <div class=\"box box-info\">
            <div class=\"box-header with-border\">
                <h3 class=\"box-title\">Ajouter une produit</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form class=\"form-horizontal\" name=\"form\" method=\"POST\" enctype=\"multipart/form-data\">
                <div class=\"box-body\">

                    <div class=\"form-group\">
                        <label for=\"Titre\" class=\"col-sm-2 control-label\" > Titre : </label>
                        <div class=\"col-sm-10\">
                            <input name=\"titre\" id=\"exampleInputfile\" type=\"text\"class=\"form-control\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Actualite"]) ? $context["Actualite"] : $this->getContext($context, "Actualite")), "Titre", array()), "html", null, true);
        echo "\"/>
                        </div>
                    </div>



                    <div class=\"form-group\">
                        <label for=\"inputPassword3\" class=\"col-sm-2 control-label\">Descreption</label>
                        <div class=\"col-sm-10\">

                            <textarea name=\"description\" rows=\"5\" cols=\"50\" class=\"form-control\" id=\"text1\">";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Actualite"]) ? $context["Actualite"] : $this->getContext($context, "Actualite")), "Description", array()), "html", null, true);
        echo "</textarea>

                        </div>
                    </div>

                </div><!-- /.box-body -->
                <div class=\"box-footer\">
                    <button type=\"submit\" class=\"btn btn-default\">Annuler</button>
                    <input type=\"submit\" value=\"MAJ\" class=\"btn btn-info pull-right\"/>
                </div><!-- /.box-footer -->
            </form>
        </div><!-- /.box -->
    </div>

";
    }

    public function getTemplateName()
    {
        return "TunisiaMallActualiteBundle:Actualite:UpdateActualite.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 27,  47 => 17,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "ResponsableBundle::Layout.html.twig" %}*/
/* {% block TableView %}*/
/* */
/*     <div class="col-md-6">*/
/*         <!-- Horizontal Form -->*/
/*         <div class="box box-info">*/
/*             <div class="box-header with-border">*/
/*                 <h3 class="box-title">Ajouter une produit</h3>*/
/*             </div><!-- /.box-header -->*/
/*             <!-- form start -->*/
/*             <form class="form-horizontal" name="form" method="POST" enctype="multipart/form-data">*/
/*                 <div class="box-body">*/
/* */
/*                     <div class="form-group">*/
/*                         <label for="Titre" class="col-sm-2 control-label" > Titre : </label>*/
/*                         <div class="col-sm-10">*/
/*                             <input name="titre" id="exampleInputfile" type="text"class="form-control" value="{{Actualite.Titre}}"/>*/
/*                         </div>*/
/*                     </div>*/
/* */
/* */
/* */
/*                     <div class="form-group">*/
/*                         <label for="inputPassword3" class="col-sm-2 control-label">Descreption</label>*/
/*                         <div class="col-sm-10">*/
/* */
/*                             <textarea name="description" rows="5" cols="50" class="form-control" id="text1">{{Actualite.Description}}</textarea>*/
/* */
/*                         </div>*/
/*                     </div>*/
/* */
/*                 </div><!-- /.box-body -->*/
/*                 <div class="box-footer">*/
/*                     <button type="submit" class="btn btn-default">Annuler</button>*/
/*                     <input type="submit" value="MAJ" class="btn btn-info pull-right"/>*/
/*                 </div><!-- /.box-footer -->*/
/*             </form>*/
/*         </div><!-- /.box -->*/
/*     </div>*/
/* */
/* {% endblock %}*/
