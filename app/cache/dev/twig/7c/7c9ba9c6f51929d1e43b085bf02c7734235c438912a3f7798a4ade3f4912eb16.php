<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_e9e76d2ca1afc6a23f61c9df7683049f313627435abe3a15da30821bcefceb2a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 6
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 7
            echo "    <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 9
        echo "<body class=\"hold-transition login-page\">
<div class=\"login-box\">
    <div class=\"login-logo\">
         <img src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/client/mt-0295-logo.png"), "html", null, true);
        echo "\" class=\"moto-widget-image-picture\" data-id=\"148\" title=\"\"  alt=\"\" draggable=\"false\">
         
          <a href=\"\"><b>Tunisia</b>Mall</a>
    </div>   
    <div class=\"login-box-body\">
        <p class=\"login-box-msg\">Sign in to start your session</p>
    <form action=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
        echo "\" />
    <div class=\"form-group has-feedback\">
        
        <input type=\"text\" id=\"username\" class=\"form-control\" placeholder=\"username\" name=\"_username\" value=\"";
        // line 22
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\">
        <span class=\"glyphicon glyphicon-envelope form-control-feedback\"></span>
    </div>
    <div class=\"form-group has-feedback\">
        
        <input type=\"password\" id=\"password\" class=\"form-control\" placeholder=\"password\" name=\"_password\" required=\"required\" >
        <span class=\"glyphicon glyphicon-lock form-control-feedback\"></span>
    </div>    
        <div class=\"row\">
            <div class=\"col-xs-8\">
              <div class=\"checkbox icheck\">
        <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
        <label for=\"remember_me\">";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("security.login.remember_me", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
                </div>
            </div>
             <div class=\"col-xs-4\">
        <input type=\"submit\" classe=\"btn btn-primary btn-block btn-flat\" id=\"_submit\" name=\"_submit\" value=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("security.login.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
             </div>
        </div>
              <div class=\"social-auth-links text-center\">
          <p>- OR -</p>
        
        
    
    
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 38,  78 => 34,  63 => 22,  57 => 19,  53 => 18,  44 => 12,  39 => 9,  33 => 7,  31 => 6,  28 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/* {% if error %}*/
/*     <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>*/
/* {% endif %}*/
/* <body class="hold-transition login-page">*/
/* <div class="login-box">*/
/*     <div class="login-logo">*/
/*          <img src="{{asset('images/client/mt-0295-logo.png')}}" class="moto-widget-image-picture" data-id="148" title=""  alt="" draggable="false">*/
/*          */
/*           <a href=""><b>Tunisia</b>Mall</a>*/
/*     </div>   */
/*     <div class="login-box-body">*/
/*         <p class="login-box-msg">Sign in to start your session</p>*/
/*     <form action="{{ path("fos_user_security_check") }}" method="post">*/
/*         <input type="hidden" name="_csrf_token" value="{{ csrf_token }}" />*/
/*     <div class="form-group has-feedback">*/
/*         */
/*         <input type="text" id="username" class="form-control" placeholder="username" name="_username" value="{{ last_username }}" required="required">*/
/*         <span class="glyphicon glyphicon-envelope form-control-feedback"></span>*/
/*     </div>*/
/*     <div class="form-group has-feedback">*/
/*         */
/*         <input type="password" id="password" class="form-control" placeholder="password" name="_password" required="required" >*/
/*         <span class="glyphicon glyphicon-lock form-control-feedback"></span>*/
/*     </div>    */
/*         <div class="row">*/
/*             <div class="col-xs-8">*/
/*               <div class="checkbox icheck">*/
/*         <input type="checkbox" id="remember_me" name="_remember_me" value="on" />*/
/*         <label for="remember_me">{{ 'security.login.remember_me'|trans }}</label>*/
/*                 </div>*/
/*             </div>*/
/*              <div class="col-xs-4">*/
/*         <input type="submit" classe="btn btn-primary btn-block btn-flat" id="_submit" name="_submit" value="{{ 'security.login.submit'|trans }}" />*/
/*              </div>*/
/*         </div>*/
/*               <div class="social-auth-links text-center">*/
/*           <p>- OR -</p>*/
/*         */
/*         */
/*     */
/*     */
/* {% endblock fos_user_content %}*/
/* */
