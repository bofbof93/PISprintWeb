<?php

/* TunisiaMallActualiteBundle:Actualite:Actualites.html.twig */
class __TwigTemplate_985d7aa59e3ed6f7f244e14a2772d747c4118e885eff22c8e9284fe0e5108b49 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("ClientBundle::Layout.html.twig", "TunisiaMallActualiteBundle:Actualite:Actualites.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ClientBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"moto-widget moto-widget-menu moto-preset-default moto-align-center moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto\" data-preset=\"default\" data-widget=\"menu\" data-align=\"{&quot;desktop&quot;:&quot;center&quot;,&quot;tablet&quot;:&quot;&quot;,&quot;mobile-h&quot;:&quot;&quot;,&quot;mobile-v&quot;:&quot;&quot;}\" data-menu-id=\"1\" data-menu-type=\"horizontal\" data-spacing=\"mama\">
        <a href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\" class=\"moto-widget-menu-toggle-btn\"><i class=\"moto-widget-menu-toggle-btn-icon fa fa-bars\"></i></a>
        <ul class=\"moto-widget-menu-list moto-widget-menu-list_horizontal\">
            <li class=\"moto-widget-menu-item\">
                <a href=\"acceuil\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Acceuil</a>
            </li>
            <li class=\"moto-widget-menu-item\">
                <a href=\"ActualitesAcceuil\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-active moto-link\">Actualités</a>
            </li>
            <li class=\"moto-widget-menu-item\">
                <a href=\"catalogue\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Catalogues<span class=\"fa moto-widget-menu-link-arrow\"></span></a>
                <ul class=\"moto-widget-menu-sublist\">
                    <li class=\"moto-widget-menu-item\">
                        <a href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("testimonials/index.html"), "html", null, true);
        echo "\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-2 moto-link\"></a>
                    </li>

                    <li class=\"moto-widget-menu-item\">
                        <a href=\"#\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-2 moto-widget-menu-link-submenu moto-link\">Générale


                    </li>
                    <li class=\"moto-widget-menu-item\">
                        <a href=\"#\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-2 moto-link\">Par enseigne</a>
                    </li>
                </ul>

            </li>
            <li class=\"moto-widget-menu-item\">
                <a href=\"gallerie\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Promotions</a>
            </li>
            <li class=\"moto-widget-menu-item\">
                <a href=\"boutique\"   data-action=\"blog.index\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Nos Boutiques</a>
            </li>
            <li class=\"moto-widget-menu-item\">

                <a href=\"contact\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Contacts</a>
            </li> 
             ";
        // line 41
        if ((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user"))) {
            // line 42
            echo "                                                        <li class=\"moto-widget-menu-item\">
                                                            <a href=\"\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-link\">";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
            echo "</a>
                                                            &nbsp &nbsp &nbsp <a href=\"panier\"><img src=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/client/shopping-cart-of-checkered-design.png"), "html", null, true);
            echo "\"></a>
                                                        </li> 
                                                        ";
        } else {
            // line 47
            echo "                                                         <li class=\"moto-widget-menu-item\">
                                                            <a href=\"";
            // line 48
            echo $this->env->getExtension('routing')->getPath("fos_user_security_login");
            echo "\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-link\">connexion</a>
                                                            &nbsp &nbsp &nbsp <a href=\"panier\"><img src=\"";
            // line 49
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/client/shopping-cart-of-checkered-design.png"), "html", null, true);
            echo "\"></a>
                                                        </li> 
                                                        ";
        }
        // line 52
        echo "        </ul>
    </div>
    <div class=\"row\">

    </div>
</div>
<div class=\"moto-cell col-xs-4\" data-container=\"container\">
    <div class=\"moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto\" data-widget=\"text\" data-preset=\"default\" data-spacing=\"aaaa\">
        <div class=\"moto-widget-text-content moto-widget-text-editable\">
            <p style=\"text-align: right;\" class=\"moto-text_system_3\"><br></p>

        </div>
    </div>



</div>

</div>

</div>

</div>

</div>

</div>

</div>   
";
        // line 81
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["Actualites"]) ? $context["Actualites"] : $this->getContext($context, "Actualites")));
        foreach ($context['_seq'] as $context["_key"] => $context["actualite"]) {
            echo " 
    <br>
<div class=\"row\">
        <div class=\"col-sm-4\"><a href=\"#\" class=\"\"><img src=\"http://placehold.it/1280X720\" class=\"img-responsive\"></a>
        </div>
        <div class=\"col-sm-8\">
          <h3 class=\"title\">";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($context["actualite"], "Titre", array()), "html", null, true);
            echo "</h3>
          <p class=\"text-muted\"><span class=\"glyphicon glyphicon-lock\"></span> ";
            // line 88
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["actualite"], "date", array()), "Y-m-d : h:m"), "html", null, true);
            echo "</p>
          <p>";
            // line 89
            echo twig_escape_filter($this->env, twig_wordwrap_filter($this->env, $this->getAttribute($context["actualite"], "Description", array()), 100), "html", null, true);
            echo "</p>
          
          <p class=\"text-muted\">Presented by <a href=\"#\">Ellen Richey</a></p>
          
        </div>
      </div>
      <hr>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['actualite'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 97
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "TunisiaMallActualiteBundle:Actualite:Actualites.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 97,  154 => 89,  150 => 88,  146 => 87,  135 => 81,  104 => 52,  98 => 49,  94 => 48,  91 => 47,  85 => 44,  81 => 43,  78 => 42,  76 => 41,  49 => 17,  34 => 5,  31 => 4,  28 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "ClientBundle::Layout.html.twig" %}*/
/* {% block contenu %}*/
/*     <div class="moto-widget moto-widget-menu moto-preset-default moto-align-center moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-preset="default" data-widget="menu" data-align="{&quot;desktop&quot;:&quot;center&quot;,&quot;tablet&quot;:&quot;&quot;,&quot;mobile-h&quot;:&quot;&quot;,&quot;mobile-v&quot;:&quot;&quot;}" data-menu-id="1" data-menu-type="horizontal" data-spacing="mama">*/
/*         <a href="{{asset('#')}}" class="moto-widget-menu-toggle-btn"><i class="moto-widget-menu-toggle-btn-icon fa fa-bars"></i></a>*/
/*         <ul class="moto-widget-menu-list moto-widget-menu-list_horizontal">*/
/*             <li class="moto-widget-menu-item">*/
/*                 <a href="acceuil"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Acceuil</a>*/
/*             </li>*/
/*             <li class="moto-widget-menu-item">*/
/*                 <a href="ActualitesAcceuil"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-active moto-link">Actualités</a>*/
/*             </li>*/
/*             <li class="moto-widget-menu-item">*/
/*                 <a href="catalogue"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Catalogues<span class="fa moto-widget-menu-link-arrow"></span></a>*/
/*                 <ul class="moto-widget-menu-sublist">*/
/*                     <li class="moto-widget-menu-item">*/
/*                         <a href="{{asset('testimonials/index.html')}}"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-2 moto-link"></a>*/
/*                     </li>*/
/* */
/*                     <li class="moto-widget-menu-item">*/
/*                         <a href="#"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-2 moto-widget-menu-link-submenu moto-link">Générale*/
/* */
/* */
/*                     </li>*/
/*                     <li class="moto-widget-menu-item">*/
/*                         <a href="#"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-2 moto-link">Par enseigne</a>*/
/*                     </li>*/
/*                 </ul>*/
/* */
/*             </li>*/
/*             <li class="moto-widget-menu-item">*/
/*                 <a href="gallerie"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Promotions</a>*/
/*             </li>*/
/*             <li class="moto-widget-menu-item">*/
/*                 <a href="boutique"   data-action="blog.index" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Nos Boutiques</a>*/
/*             </li>*/
/*             <li class="moto-widget-menu-item">*/
/* */
/*                 <a href="contact"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Contacts</a>*/
/*             </li> */
/*              {% if user %}*/
/*                                                         <li class="moto-widget-menu-item">*/
/*                                                             <a href=""   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link">{{user.username}}</a>*/
/*                                                             &nbsp &nbsp &nbsp <a href="panier"><img src="{{asset('images/client/shopping-cart-of-checkered-design.png')}}"></a>*/
/*                                                         </li> */
/*                                                         {% else %}*/
/*                                                          <li class="moto-widget-menu-item">*/
/*                                                             <a href="{{path('fos_user_security_login')}}"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link">connexion</a>*/
/*                                                             &nbsp &nbsp &nbsp <a href="panier"><img src="{{asset('images/client/shopping-cart-of-checkered-design.png')}}"></a>*/
/*                                                         </li> */
/*                                                         {% endif %}*/
/*         </ul>*/
/*     </div>*/
/*     <div class="row">*/
/* */
/*     </div>*/
/* </div>*/
/* <div class="moto-cell col-xs-4" data-container="container">*/
/*     <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aaaa">*/
/*         <div class="moto-widget-text-content moto-widget-text-editable">*/
/*             <p style="text-align: right;" class="moto-text_system_3"><br></p>*/
/* */
/*         </div>*/
/*     </div>*/
/* */
/* */
/* */
/* </div>*/
/* */
/* </div>*/
/* */
/* </div>*/
/* */
/* </div>*/
/* */
/* </div>*/
/* */
/* </div>*/
/* */
/* </div>   */
/* {% for actualite in Actualites %} */
/*     <br>*/
/* <div class="row">*/
/*         <div class="col-sm-4"><a href="#" class=""><img src="http://placehold.it/1280X720" class="img-responsive"></a>*/
/*         </div>*/
/*         <div class="col-sm-8">*/
/*           <h3 class="title">{{actualite.Titre}}</h3>*/
/*           <p class="text-muted"><span class="glyphicon glyphicon-lock"></span> {{actualite.date| date('Y-m-d : h:m')}}</p>*/
/*           <p>{{actualite.Description|wordwrap(100)}}</p>*/
/*           */
/*           <p class="text-muted">Presented by <a href="#">Ellen Richey</a></p>*/
/*           */
/*         </div>*/
/*       </div>*/
/*       <hr>*/
/* {% endfor %}*/
/* */
/* </div>*/
/* {% endblock %}*/
