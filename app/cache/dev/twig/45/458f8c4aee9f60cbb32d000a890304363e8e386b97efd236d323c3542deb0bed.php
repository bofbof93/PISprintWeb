<?php

/* PromotionProduitBundle:PromotionProduit:ListPromotionsProduit.html.twig */
class __TwigTemplate_3e5f913c69fc4c2a808853ecb54cd077ba02e0d770f4ae95c90d89c2f2e31208 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "PromotionProduitBundle:PromotionProduit:ListPromotionsProduit.html.twig", 1);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 4
        echo "               
            <h1>
            Nos Promotions
            <small>Liste des produits avec promotion</small>
          </h1>
            ";
    }

    // line 13
    public function block_TableView($context, array $blocks = array())
    {
        // line 14
        echo "<div style=\"padding: 10px\">
        <div class=\"TableView\" >
       <div class=\"box box-success \">    
    <div class=\"box-body no-padding\">
              <table class=\"table table-bordered table-hover\" id=\"nosprom\" >
                  <thead>
                  <tr>     
                        <th>
                            Produit
                        </th>
                        <th>
                            Promotion
                        </th>
                        <th>
                            Modification
                        </th>
                        <th>
                            Suppression
                        </th>
                       
                    </tr>
                  </thead>
                  <tbody>
                        ";
        // line 37
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["PromotionProduit"]);
        foreach ($context['_seq'] as $context["_key"] => $context["PromotionProduit"]) {
            // line 38
            echo "                        <tr>      
                                
                        
                         <td >
                           ";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["PromotionProduit"], "Produit", array()), "getLibelle", array(), "method"), "html", null, true);
            echo "
                        </td>
                         <td >
                           ";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["PromotionProduit"], "Promotion", array()), "getTauxdereduction", array(), "method"), "html", null, true);
            echo "
                        </td>
                         <td >
                          <center><a href=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tunisia_mall_promprod_Modif", array("id" => $this->getAttribute($context["PromotionProduit"], "id", array()))), "html", null, true);
            echo "\"><input class=\"modifIcon\" type=\"image\"  src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Modifier.png"), "html", null, true);
            echo " \" value=\"\" href=\"#\" ></center>
                         
                        </td>
                         <td >
                         <center><a href=\"";
            // line 52
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tunisia_mall_promprod_Supp", array("id" => $this->getAttribute($context["PromotionProduit"], "id", array()))), "html", null, true);
            echo "\"><input class=\"modifIcon\" type=\"image\"  src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Supprimer.png"), "html", null, true);
            echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                    </tr>
                    
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['PromotionProduit'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "                    </tbody>
                </table>
            </div></div></div></div>
    ";
    }

    // line 61
    public function block_btn($context, array $blocks = array())
    {
        // line 62
        echo "     <br>
                <div>
                    <form align=\"left\" action=\"";
        // line 64
        echo "Ajouterpromprod";
        echo "\">
                        <center><input type=\"submit\" class=\"btn\" value=\"Ajouter\"/></center>
                    </form>
                    
                </div>   
                    
    ";
    }

    // line 72
    public function block_script($context, array $blocks = array())
    {
        // line 73
        echo "           <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Responsable/tabledata.js"), "html", null, true);
        echo "\"></script>
           <script src=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Responsable/appli.js"), "html", null, true);
        echo "\"></script>
        ";
    }

    public function getTemplateName()
    {
        return "PromotionProduitBundle:PromotionProduit:ListPromotionsProduit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 74,  143 => 73,  140 => 72,  129 => 64,  125 => 62,  122 => 61,  115 => 57,  102 => 52,  93 => 48,  87 => 45,  81 => 42,  75 => 38,  71 => 37,  46 => 14,  43 => 13,  34 => 4,  31 => 3,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* */
/*  {% block HeaderTitle %}*/
/*                */
/*             <h1>*/
/*             Nos Promotions*/
/*             <small>Liste des produits avec promotion</small>*/
/*           </h1>*/
/*             {%endblock %}*/
/* */
/* */
/* */
/* {% block TableView %}*/
/* <div style="padding: 10px">*/
/*         <div class="TableView" >*/
/*        <div class="box box-success ">    */
/*     <div class="box-body no-padding">*/
/*               <table class="table table-bordered table-hover" id="nosprom" >*/
/*                   <thead>*/
/*                   <tr>     */
/*                         <th>*/
/*                             Produit*/
/*                         </th>*/
/*                         <th>*/
/*                             Promotion*/
/*                         </th>*/
/*                         <th>*/
/*                             Modification*/
/*                         </th>*/
/*                         <th>*/
/*                             Suppression*/
/*                         </th>*/
/*                        */
/*                     </tr>*/
/*                   </thead>*/
/*                   <tbody>*/
/*                         {% for PromotionProduit in PromotionProduit %}*/
/*                         <tr>      */
/*                                 */
/*                         */
/*                          <td >*/
/*                            {{PromotionProduit.Produit.getLibelle()}}*/
/*                         </td>*/
/*                          <td >*/
/*                            {{PromotionProduit.Promotion.getTauxdereduction()}}*/
/*                         </td>*/
/*                          <td >*/
/*                           <center><a href="{{path('tunisia_mall_promprod_Modif',{'id':PromotionProduit.id})}}"><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Modifier.png')}} " value="" href="#" ></center>*/
/*                          */
/*                         </td>*/
/*                          <td >*/
/*                          <center><a href="{{path('tunisia_mall_promprod_Supp',{'id':PromotionProduit.id})}}"><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Supprimer.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                     </tr>*/
/*                     */
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/*             </div></div></div></div>*/
/*     {% endblock TableView %}    */
/* {% block btn %}*/
/*      <br>*/
/*                 <div>*/
/*                     <form align="left" action="{{('Ajouterpromprod')}}">*/
/*                         <center><input type="submit" class="btn" value="Ajouter"/></center>*/
/*                     </form>*/
/*                     */
/*                 </div>   */
/*                     */
/*     {% endblock %}*/
/*     */
/*     {% block script %}*/
/*            <script src="{{asset('js/Responsable/tabledata.js')}}"></script>*/
/*            <script src="{{asset('js/Responsable/appli.js')}}"></script>*/
/*         {% endblock %}*/
/*         */
