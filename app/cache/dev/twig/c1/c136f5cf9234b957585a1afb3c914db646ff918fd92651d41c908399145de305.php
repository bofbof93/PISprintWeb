<?php

/* ReservationPackBundle:ReservationPack:ListePack.html.twig */
class __TwigTemplate_af8c4882e3f21cd72d1104d5b2a3c5d5a675ee21ff0bb91907712a64a909c30f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "ReservationPackBundle:ReservationPack:ListePack.html.twig", 2);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 5
        echo "    <style>
        img {
            width: 50px;
            height: 50px;
        }
    </style>

    <h1>
        Gestion des packs publicitaires
        <small>Liste des packs</small>
    </h1>
    <center>
        <form  method=\"POST\">
            <input type=\"text\" name=\"search\">
            <input type=\"submit\" value=\"rechercher\">
        </form>
    </center>
";
    }

    // line 24
    public function block_TableView($context, array $blocks = array())
    {
        // line 25
        echo "    <div style=\"padding: 10px\">
        <div class=\"TableView\" >
            <div class=\"box box-success \">    
                <div  class=\"box-body no-padding\">
                    <table class=\"table table-bordered table-hover\" >

                       <tr>
                <th>Zone</th>
                <th> Libellé</th>
                <th>Prix</th>
                <th>Description</th>
                <th>Réserver</th>
            </tr>
            <tr> ";
        // line 38
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["packs"]) ? $context["packs"] : $this->getContext($context, "packs")));
        foreach ($context['_seq'] as $context["_key"] => $context["pack"]) {
            // line 39
            echo "                <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "Zone", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "libelle", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "prix", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "description", array()), "html", null, true);
            echo "</td>
                <td><center><a href=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Reserver_pack", array("id" => $this->getAttribute($context["pack"], "id", array()))), "html", null, true);
            echo "\"><span class=\"fa fa-2x fa-check\"></span></center></td>
                    </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pack'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "                        </table>
                                    </div>  </div>         
                                    ";
    }

    // line 49
    public function block_btn($context, array $blocks = array())
    {
        echo "  
                                            <br>   
                                            <div>

                                                  <br>   
             <center><div><a href=\"";
        // line 54
        echo $this->env->getExtension('routing')->getPath("ReservationAPayer");
        echo "\"><input type=\"submit\" class=\"btn\" value=\"Vos réservation a payer\"/></div>
        
                                            </div></div></div>
                                        ";
    }

    public function getTemplateName()
    {
        return "ReservationPackBundle:ReservationPack:ListePack.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 54,  108 => 49,  102 => 46,  93 => 43,  89 => 42,  85 => 41,  81 => 40,  76 => 39,  72 => 38,  57 => 25,  54 => 24,  33 => 5,  30 => 4,  11 => 2,);
    }
}
/* */
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* */
/* {% block HeaderTitle %}*/
/*     <style>*/
/*         img {*/
/*             width: 50px;*/
/*             height: 50px;*/
/*         }*/
/*     </style>*/
/* */
/*     <h1>*/
/*         Gestion des packs publicitaires*/
/*         <small>Liste des packs</small>*/
/*     </h1>*/
/*     <center>*/
/*         <form  method="POST">*/
/*             <input type="text" name="search">*/
/*             <input type="submit" value="rechercher">*/
/*         </form>*/
/*     </center>*/
/* {%endblock %}*/
/* */
/* {% block TableView %}*/
/*     <div style="padding: 10px">*/
/*         <div class="TableView" >*/
/*             <div class="box box-success ">    */
/*                 <div  class="box-body no-padding">*/
/*                     <table class="table table-bordered table-hover" >*/
/* */
/*                        <tr>*/
/*                 <th>Zone</th>*/
/*                 <th> Libellé</th>*/
/*                 <th>Prix</th>*/
/*                 <th>Description</th>*/
/*                 <th>Réserver</th>*/
/*             </tr>*/
/*             <tr> {% for pack in packs %}*/
/*                 <td>{{pack.Zone}}</td>*/
/*                 <td>{{pack.libelle}}</td>*/
/*                 <td>{{pack.prix}}</td>*/
/*                 <td>{{pack.description}}</td>*/
/*                 <td><center><a href="{{path('Reserver_pack',{'id':pack.id})}}"><span class="fa fa-2x fa-check"></span></center></td>*/
/*                     </tr>*/
/*                     {%endfor%}*/
/*                         </table>*/
/*                                     </div>  </div>         */
/*                                     {% endblock %} */
/*                                         {%block btn %}  */
/*                                             <br>   */
/*                                             <div>*/
/* */
/*                                                   <br>   */
/*              <center><div><a href="{{path('ReservationAPayer')}}"><input type="submit" class="btn" value="Vos réservation a payer"/></div>*/
/*         */
/*                                             </div></div></div>*/
/*                                         {% endblock %}*/
/* */
/*              */
/*              */
/*              */
/*              */
