<?php

/* TunisiaMallGestionStockBundle:GestionStock:ModifierStock.html.twig */
class __TwigTemplate_43f7d31b4a221b832d1a995eedcb53c672ca6a79861e2dfb86bfd1a8c0475831 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "TunisiaMallGestionStockBundle:GestionStock:ModifierStock.html.twig", 3);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 5
        echo "               
            <h1>
            Modifier
            <small>Stock</small>
          </h1>
            ";
    }

    // line 12
    public function block_TableView($context, array $blocks = array())
    {
        // line 13
        echo "   
 <div class=\"container\">       
    <div class=\"col-md-6\">
              <!-- Horizontal Form -->
              <div class=\"box box-info\">
                <div class=\"box-header with-border\">
                  <h3 class=\"box-title\">Modifier</h3>
                </div>
              <!-- /.box-header -->
              <div class=\"box-body\">
                <!-- form start -->
 <form  method=\"POST\" class=\"form-horizontal\" name=\"form\" onsubmit=\"verif()\">
            <div> ";
        // line 25
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
                ";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
                 
                    <div class=\"form-group\">
                   <label for=\"Quantiteproduit\" class=\"col-sm-2 control-label\">Quantite produit</label>
                    ";
        // line 30
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Quantiteproduit", array()), 'errors');
        echo "
                    <div class=\"col-sm-10\">
                    ";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Quantiteproduit", array()), 'widget');
        echo "
                    </div> 
                 </div>
                    <div class=\"form-group\">
                   <label for=\"DateAjout\" class=\"col-sm-2 control-label\">Date Ajout</label>
                    ";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "DateAjout", array()), 'errors');
        echo "
                    <div class=\"col-sm-10\">
                    ";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "DateAjout", array()), 'widget');
        echo "
                    </div> 
                 </div>
                     <div class=\"form-group\">
                   <label for=\"Enseigne\" class=\"col-sm-2 control-label\">Enseigne</label>
                    ";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Enseigne", array()), 'errors');
        echo "
                    <div class=\"col-sm-10\">
                    ";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Enseigne", array()), 'widget');
        echo "
                    </div></div> 
                    <div class=\"form-group\">
                   <label for=\"Produit\" class=\"col-sm-2 control-label\">Produit</label>
                    ";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Produit", array()), 'errors');
        echo "
                    <div class=\"col-sm-10\">
                    ";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Produit", array()), 'widget');
        echo "
                    </div></div> 
                     
                    
                    
                    
            <div class=\"box-footer\">
            ";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "AjouterStock", array()), 'widget');
        echo "
            </div>
            ";
        // line 61
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
            
            
            </div>
                         </form>
              </div>
</div>
              </div><!-- /.box -->
        </div>

    ";
    }

    public function getTemplateName()
    {
        return "TunisiaMallGestionStockBundle:GestionStock:ModifierStock.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 61,  122 => 59,  112 => 52,  107 => 50,  100 => 46,  95 => 44,  87 => 39,  82 => 37,  74 => 32,  69 => 30,  62 => 26,  58 => 25,  44 => 13,  41 => 12,  32 => 5,  29 => 4,  11 => 3,);
    }
}
/* {# empty Twig template #}*/
/* {# empty Twig template #}*/
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block HeaderTitle %}*/
/*                */
/*             <h1>*/
/*             Modifier*/
/*             <small>Stock</small>*/
/*           </h1>*/
/*             {%endblock %}*/
/* */
/* {% block TableView %}*/
/*    */
/*  <div class="container">       */
/*     <div class="col-md-6">*/
/*               <!-- Horizontal Form -->*/
/*               <div class="box box-info">*/
/*                 <div class="box-header with-border">*/
/*                   <h3 class="box-title">Modifier</h3>*/
/*                 </div>*/
/*               <!-- /.box-header -->*/
/*               <div class="box-body">*/
/*                 <!-- form start -->*/
/*  <form  method="POST" class="form-horizontal" name="form" onsubmit="verif()">*/
/*             <div> {{ form_start(form) }}*/
/*                 {{form_errors(form) }}*/
/*                  */
/*                     <div class="form-group">*/
/*                    <label for="Quantiteproduit" class="col-sm-2 control-label">Quantite produit</label>*/
/*                     {{ form_errors(form.Quantiteproduit) }}*/
/*                     <div class="col-sm-10">*/
/*                     {{ form_widget(form.Quantiteproduit)  }}*/
/*                     </div> */
/*                  </div>*/
/*                     <div class="form-group">*/
/*                    <label for="DateAjout" class="col-sm-2 control-label">Date Ajout</label>*/
/*                     {{ form_errors(form.DateAjout) }}*/
/*                     <div class="col-sm-10">*/
/*                     {{ form_widget(form.DateAjout)  }}*/
/*                     </div> */
/*                  </div>*/
/*                      <div class="form-group">*/
/*                    <label for="Enseigne" class="col-sm-2 control-label">Enseigne</label>*/
/*                     {{ form_errors(form.Enseigne) }}*/
/*                     <div class="col-sm-10">*/
/*                     {{ form_widget(form.Enseigne)  }}*/
/*                     </div></div> */
/*                     <div class="form-group">*/
/*                    <label for="Produit" class="col-sm-2 control-label">Produit</label>*/
/*                     {{ form_errors(form.Produit) }}*/
/*                     <div class="col-sm-10">*/
/*                     {{ form_widget(form.Produit)  }}*/
/*                     </div></div> */
/*                      */
/*                     */
/*                     */
/*                     */
/*             <div class="box-footer">*/
/*             {{form_widget(form.AjouterStock) }}*/
/*             </div>*/
/*             {{ form_end(form)}}*/
/*             */
/*             */
/*             </div>*/
/*                          </form>*/
/*               </div>*/
/* </div>*/
/*               </div><!-- /.box -->*/
/*         </div>*/
/* */
/*     {% endblock %}*/
