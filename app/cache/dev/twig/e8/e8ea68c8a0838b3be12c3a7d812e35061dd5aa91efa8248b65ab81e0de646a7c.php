<?php

/* PackPubBundle:PackPub:AjouterPack.html.twig */
class __TwigTemplate_dc40450a4f9781b5699614bf0528dab8e2166db71b0411c4699730a2fa4fb9fd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "PackPubBundle:PackPub:AjouterPack.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_contenu($context, array $blocks = array())
    {
        // line 3
        echo "    <div class =\"container\">
        <div class=\"col-md-6\">
            <!-- Horizontal Form -->
            <div class=\"box box-info\">
                <div class=\"box-header with-border\">
                    <h3 class=\"box-title\">Ajouter un pack publicitaire</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class=\"form-horizontal\" name=\"form\" onsubmit=\"verif()\" method =\"POST\">
                    <div class=\"box-body\">


                        <div class=\"form-group\"> 
                            <label for=\"zone\" class=\"col-sm-2 control-label\">Zone</label>
                            <div class=\"col-sm-10\">
                                <select name=\"zone\" onChange=\"\">


                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>

                                </select> 
                            </div>  
                        </div>

                        <div class=\"form-group\">
                            <label for=\"text\" class=\"col-sm-2 control-label\">Libelle </label>
                            <div class=\"col-sm-10\">
                                <input name=\"libelle\" type=\"text\" class=\"form-control\" id=\"date1\" placeholder=\"Saisir le libellé\">
                            </div>
                        </div>     



                        <div class=\"form-group\">
                            <label for=\"taux\" class=\"col-sm-2 control-label\">Prix</label>
                            <div class=\"col-sm-10\">
                                <input name=\"prix\"type=\"number\" step=\"0.1\" class=\"form-control\" id=\"inputPassword3\" placeholder=\"Saisir le prix\">
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"inputPassword3\" class=\"col-sm-2 control-label\">Description</label>
                            <div class=\"col-sm-10\">
                                <textarea rows=\"5\" cols=\"50\" class=\"form-control\" id=\"text1\" name=\"description\" placeholder=\"Description\"></textarea>
                            </div>
                        </div>  
                        <div class=\"form-group\"> 
                            <label for=\"etat\" class=\"col-sm-2 control-label\">Etat</label>
                            <div class=\"col-sm-10\">
                                <select name=\"etat\" onChange=\"\">
                                     <option>Non_active</option>
                                    <option>Active</option>
                                   
                                </select> 
                            </div>  
                        </div>

                        <div class=\"box-footer\">
                            <button type=\"reset\" class=\"btn btn-default\" name =\"submit\">Annuler</button>
                            <button type=\"submit\" class=\"btn btn-info pull-right\">Ajouter</button>
                        </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box -->
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "PackPubBundle:PackPub:AjouterPack.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block contenu %}*/
/*     <div class ="container">*/
/*         <div class="col-md-6">*/
/*             <!-- Horizontal Form -->*/
/*             <div class="box box-info">*/
/*                 <div class="box-header with-border">*/
/*                     <h3 class="box-title">Ajouter un pack publicitaire</h3>*/
/*                 </div><!-- /.box-header -->*/
/*                 <!-- form start -->*/
/*                 <form class="form-horizontal" name="form" onsubmit="verif()" method ="POST">*/
/*                     <div class="box-body">*/
/* */
/* */
/*                         <div class="form-group"> */
/*                             <label for="zone" class="col-sm-2 control-label">Zone</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <select name="zone" onChange="">*/
/* */
/* */
/*                                     <option>1</option>*/
/*                                     <option>2</option>*/
/*                                     <option>3</option>*/
/*                                     <option>4</option>*/
/* */
/*                                 </select> */
/*                             </div>  */
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="text" class="col-sm-2 control-label">Libelle </label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="libelle" type="text" class="form-control" id="date1" placeholder="Saisir le libellé">*/
/*                             </div>*/
/*                         </div>     */
/* */
/* */
/* */
/*                         <div class="form-group">*/
/*                             <label for="taux" class="col-sm-2 control-label">Prix</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="prix"type="number" step="0.1" class="form-control" id="inputPassword3" placeholder="Saisir le prix">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="inputPassword3" class="col-sm-2 control-label">Description</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <textarea rows="5" cols="50" class="form-control" id="text1" name="description" placeholder="Description"></textarea>*/
/*                             </div>*/
/*                         </div>  */
/*                         <div class="form-group"> */
/*                             <label for="etat" class="col-sm-2 control-label">Etat</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <select name="etat" onChange="">*/
/*                                      <option>Non_active</option>*/
/*                                     <option>Active</option>*/
/*                                    */
/*                                 </select> */
/*                             </div>  */
/*                         </div>*/
/* */
/*                         <div class="box-footer">*/
/*                             <button type="reset" class="btn btn-default" name ="submit">Annuler</button>*/
/*                             <button type="submit" class="btn btn-info pull-right">Ajouter</button>*/
/*                         </div><!-- /.box-footer -->*/
/*                 </form>*/
/*             </div><!-- /.box -->*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
/* {# empty Twig template #}*/
/* */
