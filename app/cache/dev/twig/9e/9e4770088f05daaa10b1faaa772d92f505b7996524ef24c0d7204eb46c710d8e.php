<?php

/* PackPubBundle:PackPub:ListePack.html.twig */
class __TwigTemplate_d5ae5ec7b65841553fa6a497e80a5c5d14d30efba8683de18fe6e9bf1eaf3755 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 4
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "PackPubBundle:PackPub:ListePack.html.twig", 4);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 7
        echo "    <style>
        img {
            width: 50px;
            height: 50px;
        }
    </style>

    <h1>
        Gestion Des packs publicitairess
        <small>Liste des packs publicitaires</small>
    </h1>
    <center>
        <form  method=\"POST\">
            <input type=\"text\" name=\"search\">
            <input type=\"submit\" value=\"rechercher\">
        </form>
    </center>
";
    }

    // line 26
    public function block_TableView($context, array $blocks = array())
    {
        // line 27
        echo "    <div style=\"padding: 10px\">
        <div class=\"TableView\" >
            <div class=\"box box-success \">    
                <div  class=\"box-body no-padding\">
                    <table class=\"table table-bordered table-hover\" >

                        <tr>
                            <th>Zone</th>
                            <th> Libellé</th>
                            <th>Prix</th>
                            <th>Description</th>
                            <th> Modifier </th>
                            <th> Supprimer </th>
                        </tr>
                        <tr> ";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["packs"]) ? $context["packs"] : $this->getContext($context, "packs")));
        foreach ($context['_seq'] as $context["_key"] => $context["pack"]) {
            // line 42
            echo "                            <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "Zone", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "Libelle", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "Prix", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "Description", array()), "html", null, true);
            echo "</td>
                            <td >
                        <center><a href=\"";
            // line 47
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Modifier_pack", array("id" => $this->getAttribute($context["pack"], "id", array()))), "html", null, true);
            echo "\"><input class=\"modifIcon\" type=\"image\"  src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Modifier.png"), "html", null, true);
            echo " \" value=\"\" href=\"#\" ></center>
                                </td>
                                <td >
                                <center><a href=\"";
            // line 50
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Supprimer_pack", array("id" => $this->getAttribute($context["pack"], "id", array()))), "html", null, true);
            echo "\"><input class=\"suppIcon\" type=\"image\"  src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Supprimer.png"), "html", null, true);
            echo " \" value=\"\"  ></a></center>
                                </td>
                                </tr>

                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pack'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "                                    </table>
                                    </div>  </div>         
                                    ";
    }

    // line 58
    public function block_btn($context, array $blocks = array())
    {
        echo "  
                                            <br>   
                                            <div>
                                                <center>             <a href=\"";
        // line 61
        echo $this->env->getExtension('routing')->getPath("pack_pub_ajouter");
        echo "\"><input type=\"submit\" class=\"btn\" value=\"Ajouter un pack\"/> 

                                                    </center>

                                            </div></div></div>
                                        ";
    }

    public function getTemplateName()
    {
        return "PackPubBundle:PackPub:ListePack.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 61,  122 => 58,  116 => 55,  103 => 50,  95 => 47,  90 => 45,  86 => 44,  82 => 43,  77 => 42,  73 => 41,  57 => 27,  54 => 26,  33 => 7,  30 => 6,  11 => 4,);
    }
}
/* */
/* */
/* {# empty Twig template #}*/
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* */
/* {% block HeaderTitle %}*/
/*     <style>*/
/*         img {*/
/*             width: 50px;*/
/*             height: 50px;*/
/*         }*/
/*     </style>*/
/* */
/*     <h1>*/
/*         Gestion Des packs publicitairess*/
/*         <small>Liste des packs publicitaires</small>*/
/*     </h1>*/
/*     <center>*/
/*         <form  method="POST">*/
/*             <input type="text" name="search">*/
/*             <input type="submit" value="rechercher">*/
/*         </form>*/
/*     </center>*/
/* {%endblock %}*/
/* */
/* {% block TableView %}*/
/*     <div style="padding: 10px">*/
/*         <div class="TableView" >*/
/*             <div class="box box-success ">    */
/*                 <div  class="box-body no-padding">*/
/*                     <table class="table table-bordered table-hover" >*/
/* */
/*                         <tr>*/
/*                             <th>Zone</th>*/
/*                             <th> Libellé</th>*/
/*                             <th>Prix</th>*/
/*                             <th>Description</th>*/
/*                             <th> Modifier </th>*/
/*                             <th> Supprimer </th>*/
/*                         </tr>*/
/*                         <tr> {% for pack in packs %}*/
/*                             <td>{{pack.Zone}}</td>*/
/*                             <td>{{pack.Libelle}}</td>*/
/*                             <td>{{pack.Prix}}</td>*/
/*                             <td>{{pack.Description}}</td>*/
/*                             <td >*/
/*                         <center><a href="{{path('Modifier_pack',{'id':pack.id})}}"><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Modifier.png')}} " value="" href="#" ></center>*/
/*                                 </td>*/
/*                                 <td >*/
/*                                 <center><a href="{{path('Supprimer_pack',{'id':pack.id})}}"><input class="suppIcon" type="image"  src="{{asset('images/Responsable/Supprimer.png')}} " value=""  ></a></center>*/
/*                                 </td>*/
/*                                 </tr>*/
/* */
/*                                 {%endfor%}*/
/*                                     </table>*/
/*                                     </div>  </div>         */
/*                                     {% endblock %} */
/*                                         {%block btn %}  */
/*                                             <br>   */
/*                                             <div>*/
/*                                                 <center>             <a href="{{path('pack_pub_ajouter')}}"><input type="submit" class="btn" value="Ajouter un pack"/> */
/* */
/*                                                     </center>*/
/* */
/*                                             </div></div></div>*/
/*                                         {% endblock %}*/
/* */
