<?php

/* TunisiaMallActualiteBundle:Actualite:AjouterActualite.html.twig */
class __TwigTemplate_2d20805b1c04ab4df22da29c2e2e92879b603e3d9e187be90c1d767dc1f163c8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "TunisiaMallActualiteBundle:Actualite:AjouterActualite.html.twig", 1);
        $this->blocks = array(
            'TableView' => array($this, 'block_TableView'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_TableView($context, array $blocks = array())
    {
        // line 3
        echo "
    <div class=\"col-md-6\">
        <!-- Horizontal Form -->
        <div class=\"box box-info\">
            <div class=\"box-header with-border\">
                <h3 class=\"box-title\">Ajouter une produit</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form class=\"form-horizontal\" name=\"form\" method=\"POST\">
                <div class=\"box-body\">

                <div class=\"form-group\">
                        <label for=\"Titre\" class=\"col-sm-2 control-label\" > Titre : </label>
                        <div class=\"col-sm-10\">
                            <input name=\"titre\" id=\"exampleInputfile\" type=\"text\"class=\"form-control\"/>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <label for=\"Image\" class=\"col-sm-2 control-label\" > Image : </label>
                        <div class=\"col-sm-10\">
                            <input name=\"imgactu\" id=\"exampleInputfile\" type=\"file\"class=\"form-control\"/>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <label for=\"inputPassword3\" class=\"col-sm-2 control-label\">Descreption</label>
                        <div class=\"col-sm-10\">
                            <textarea name=\"description\" rows=\"5\" cols=\"50\" class=\"form-control\" id=\"text1\"></textarea>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class=\"box-footer\">
                    <button type=\"submit\" class=\"btn btn-default\">Annuler</button>
                    <input type=\"submit\" value=\"Ajouter\" class=\"btn btn-info pull-right\"/>
                </div><!-- /.box-footer -->
            </form>
        </div><!-- /.box -->
    </div>

";
    }

    public function getTemplateName()
    {
        return "TunisiaMallActualiteBundle:Actualite:AjouterActualite.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block TableView %}*/
/* */
/*     <div class="col-md-6">*/
/*         <!-- Horizontal Form -->*/
/*         <div class="box box-info">*/
/*             <div class="box-header with-border">*/
/*                 <h3 class="box-title">Ajouter une produit</h3>*/
/*             </div><!-- /.box-header -->*/
/*             <!-- form start -->*/
/*             <form class="form-horizontal" name="form" method="POST">*/
/*                 <div class="box-body">*/
/* */
/*                 <div class="form-group">*/
/*                         <label for="Titre" class="col-sm-2 control-label" > Titre : </label>*/
/*                         <div class="col-sm-10">*/
/*                             <input name="titre" id="exampleInputfile" type="text"class="form-control"/>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="form-group">*/
/*                         <label for="Image" class="col-sm-2 control-label" > Image : </label>*/
/*                         <div class="col-sm-10">*/
/*                             <input name="imgactu" id="exampleInputfile" type="file"class="form-control"/>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="form-group">*/
/*                         <label for="inputPassword3" class="col-sm-2 control-label">Descreption</label>*/
/*                         <div class="col-sm-10">*/
/*                             <textarea name="description" rows="5" cols="50" class="form-control" id="text1"></textarea>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div><!-- /.box-body -->*/
/*                 <div class="box-footer">*/
/*                     <button type="submit" class="btn btn-default">Annuler</button>*/
/*                     <input type="submit" value="Ajouter" class="btn btn-info pull-right"/>*/
/*                 </div><!-- /.box-footer -->*/
/*             </form>*/
/*         </div><!-- /.box -->*/
/*     </div>*/
/* */
/* {% endblock %}*/
