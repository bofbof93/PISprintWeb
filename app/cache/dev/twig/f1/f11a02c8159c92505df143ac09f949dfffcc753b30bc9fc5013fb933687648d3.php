<?php

/* PackPubBundle:PackPub:ModifierPack.html.twig */
class __TwigTemplate_a8af072c850d46588bcdd950cad0e9167a33232487e4e6eced249eaf623c92a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "PackPubBundle:PackPub:ModifierPack.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_contenu($context, array $blocks = array())
    {
        // line 3
        echo "    <div class =\"container\">
        <div class=\"col-md-6\">
            <!-- Horizontal Form -->
            <div class=\"box box-info\">
                <div class=\"box-header with-border\">
                    <h3 class=\"box-title\">Modifier le pack publicitaire</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class=\"form-horizontal\" name=\"form\" onsubmit=\"verif()\" method =\"POST\">
                    <div class=\"box-body\">

                        ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["packs"]) ? $context["packs"] : $this->getContext($context, "packs")));
        foreach ($context['_seq'] as $context["_key"] => $context["pack"]) {
            // line 15
            echo "                            <div class=\"form-group\">
                                <label for=\"taux\" class=\"col-sm-2 control-label\">Zone</label>
                                <div class=\"col-sm-10\">
                                    <input name=\"zone\" type=\"number\" class=\"form-control\" id=\"inputPassword3\" placeholder=\"\" value=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "Zone", array()), "html", null, true);
            echo "\">
                                </div>
                            </div>

                            <div class=\"form-group\">
                                <label for=\"text\" class=\"col-sm-2 control-label\">Libelle </label>
                                <div class=\"col-sm-10\">
                                    <input name=\"libelle\" type=\"text\" class=\"form-control\" id=\"date1\" value=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "Libelle", array()), "html", null, true);
            echo "\">
                                </div>
                            </div>     


                            <div class=\"form-group\">
                                <label for=\"taux\" class=\"col-sm-2 control-label\">Prix</label>
                                <div class=\"col-sm-10\">
                                    <input name=\"prix\" type=\"text\" class=\"form-control\" id=\"inputPassword3\" value=\"";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "Prix", array()), "html", null, true);
            echo "\" placeholder=\"\">
                                </div>
                            </div>



                            <div class=\"form-group\">
                                <label for=\"inputPassword3\" class=\"col-sm-2 control-label\">Description</label>
                                <div class=\"col-sm-10\">
                                    <textarea rows=\"5\" cols=\"50\" class=\"form-control\" id=\"text1\" name=\"description\" placeholder=\"Description\">";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "Description", array()), "html", null, true);
            echo "</textarea>
                                </div>
                            </div>  
                                
                            <div class=\"form-group\"> 
                                <label for=\"etat\" class=\"col-sm-2 control-label\">Etat</label>
                                <div class=\"col-sm-10\">
                                    <select name=\"etat\" onChange=\"\">
                                        <option>Active</option>
                                        <option>Non_active</option>
                                    </select> 
                                </div>  
                            </div>
                        </div>

                        <div class=\"box-footer\">
                            <button type=\"reset\" class=\"btn btn-default\" name =\"submit\">Annuler</button>
                            <button type=\"submit\" class=\"btn btn-info pull-right\">Ajouter</button>
                        </div><!-- /.box-footer -->
                    </form>
                </div><!-- /.box -->
            </div>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pack'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "PackPubBundle:PackPub:ModifierPack.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 42,  74 => 33,  63 => 25,  53 => 18,  48 => 15,  44 => 14,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block contenu %}*/
/*     <div class ="container">*/
/*         <div class="col-md-6">*/
/*             <!-- Horizontal Form -->*/
/*             <div class="box box-info">*/
/*                 <div class="box-header with-border">*/
/*                     <h3 class="box-title">Modifier le pack publicitaire</h3>*/
/*                 </div><!-- /.box-header -->*/
/*                 <!-- form start -->*/
/*                 <form class="form-horizontal" name="form" onsubmit="verif()" method ="POST">*/
/*                     <div class="box-body">*/
/* */
/*                         {% for pack in packs%}*/
/*                             <div class="form-group">*/
/*                                 <label for="taux" class="col-sm-2 control-label">Zone</label>*/
/*                                 <div class="col-sm-10">*/
/*                                     <input name="zone" type="number" class="form-control" id="inputPassword3" placeholder="" value="{{pack.Zone}}">*/
/*                                 </div>*/
/*                             </div>*/
/* */
/*                             <div class="form-group">*/
/*                                 <label for="text" class="col-sm-2 control-label">Libelle </label>*/
/*                                 <div class="col-sm-10">*/
/*                                     <input name="libelle" type="text" class="form-control" id="date1" value="{{pack.Libelle}}">*/
/*                                 </div>*/
/*                             </div>     */
/* */
/* */
/*                             <div class="form-group">*/
/*                                 <label for="taux" class="col-sm-2 control-label">Prix</label>*/
/*                                 <div class="col-sm-10">*/
/*                                     <input name="prix" type="text" class="form-control" id="inputPassword3" value="{{pack.Prix}}" placeholder="">*/
/*                                 </div>*/
/*                             </div>*/
/* */
/* */
/* */
/*                             <div class="form-group">*/
/*                                 <label for="inputPassword3" class="col-sm-2 control-label">Description</label>*/
/*                                 <div class="col-sm-10">*/
/*                                     <textarea rows="5" cols="50" class="form-control" id="text1" name="description" placeholder="Description">{{pack.Description}}</textarea>*/
/*                                 </div>*/
/*                             </div>  */
/*                                 */
/*                             <div class="form-group"> */
/*                                 <label for="etat" class="col-sm-2 control-label">Etat</label>*/
/*                                 <div class="col-sm-10">*/
/*                                     <select name="etat" onChange="">*/
/*                                         <option>Active</option>*/
/*                                         <option>Non_active</option>*/
/*                                     </select> */
/*                                 </div>  */
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="box-footer">*/
/*                             <button type="reset" class="btn btn-default" name ="submit">Annuler</button>*/
/*                             <button type="submit" class="btn btn-info pull-right">Ajouter</button>*/
/*                         </div><!-- /.box-footer -->*/
/*                     </form>*/
/*                 </div><!-- /.box -->*/
/*             </div>*/
/*         </div>*/
/*     {% endfor %}*/
/* {% endblock %}*/
