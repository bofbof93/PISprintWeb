<?php

/* TunisiaMallGestionStockBundle:GestionStock:ListStock.html.twig */
class __TwigTemplate_a1d5fb2d77b1c6b9b5923edce2ed1d1628c3ba86e0670990c37d6e28bc80031b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "TunisiaMallGestionStockBundle:GestionStock:ListStock.html.twig", 2);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 5
        echo "               
            <h1>
            Gestion Des Stock
            <small>Liste Des Stock</small>
          </h1>
            ";
    }

    // line 14
    public function block_TableView($context, array $blocks = array())
    {
        // line 15
        echo "   <div style=\"padding: 10px\">
        <div class=\"TableView\" >
       <div class=\"box box-success \">    
    <div class=\"box-body no-padding\">  
<div class=\"TableView\" >
    <table class=\"table table-bordered table-hover\" id=\"nosprom\">
        <thead>
                    <tr>
                      
                      
                        <th>
                            Quantité Produit
                        </th>
                        <th>
                            Date Ajout
                        </th>
                        <th>
                            Enseigne
                        </th>
                        <th>
                            Produit
                        </th>
                        <th>
                            Modification
                        </th>
                        <th>
                            Suppression
                        </th>
                       
                    </tr>
        </thead>
        <tbody>
                    
                        ";
        // line 48
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["Stock"]);
        foreach ($context['_seq'] as $context["_key"] => $context["Stock"]) {
            // line 49
            echo "                        <tr>      
                                
                       
                        <td>
                            ";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["Stock"], "Quantiteproduit", array()), "html", null, true);
            echo "
                        </td>
                         <td>
                             ";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["Stock"], "DateAjout", array()), "format", array(0 => "Y/m/d"), "method"), "html", null, true);
            echo "
                        </td>
                         <td >
                           ";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["Stock"], "Enseigne", array()), "getLibelle", array(), "method"), "html", null, true);
            echo "
                        </td>
                         <td>
                             ";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["Stock"], "Produit", array()), "getLibelle", array(), "method"), "html", null, true);
            echo "
                        </td>
                         <td >
                          <center><a href=\"";
            // line 65
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tunisia_mall_stock_Modif", array("id" => $this->getAttribute($context["Stock"], "id", array()))), "html", null, true);
            echo "\"><input class=\"modifIcon\" type=\"image\"  src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Modifier.png"), "html", null, true);
            echo " \" value=\"\" href=\"#\" ></center>
                         
                        </td>
                         <td >
                         <center><a href=\"";
            // line 69
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tunisia_mall_stock_Supp", array("id" => $this->getAttribute($context["Stock"], "id", array()))), "html", null, true);
            echo "\"><input class=\"modifIcon\" type=\"image\"  src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Supprimer.png"), "html", null, true);
            echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                    </tr>
                    
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['Stock'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo "                    </tbody>
                </table>
            </div>
                    </div>
                    </div>
                    </div>
                    </div>
    ";
    }

    // line 82
    public function block_btn($context, array $blocks = array())
    {
        // line 83
        echo "     <br>
                <div>
                    <form align=\"left\" action=\"";
        // line 85
        echo "AjouterStock";
        echo "\">
                        <center><input type=\"submit\" class=\"btn\" value=\"Ajouter\"/></center>
                    </form>
                    
                </div>   
                    
    ";
    }

    // line 93
    public function block_script($context, array $blocks = array())
    {
        // line 94
        echo "           <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Responsable/tabledata.js"), "html", null, true);
        echo "\"></script>
           <script src=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Responsable/appli.js"), "html", null, true);
        echo "\"></script>
        ";
    }

    public function getTemplateName()
    {
        return "TunisiaMallGestionStockBundle:GestionStock:ListStock.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 95,  169 => 94,  166 => 93,  155 => 85,  151 => 83,  148 => 82,  137 => 74,  124 => 69,  115 => 65,  109 => 62,  103 => 59,  97 => 56,  91 => 53,  85 => 49,  81 => 48,  46 => 15,  43 => 14,  34 => 5,  31 => 4,  11 => 2,);
    }
}
/* */
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* */
/*  {% block HeaderTitle %}*/
/*                */
/*             <h1>*/
/*             Gestion Des Stock*/
/*             <small>Liste Des Stock</small>*/
/*           </h1>*/
/*             {%endblock %}*/
/* */
/* */
/* */
/* {% block TableView %}*/
/*    <div style="padding: 10px">*/
/*         <div class="TableView" >*/
/*        <div class="box box-success ">    */
/*     <div class="box-body no-padding">  */
/* <div class="TableView" >*/
/*     <table class="table table-bordered table-hover" id="nosprom">*/
/*         <thead>*/
/*                     <tr>*/
/*                       */
/*                       */
/*                         <th>*/
/*                             Quantité Produit*/
/*                         </th>*/
/*                         <th>*/
/*                             Date Ajout*/
/*                         </th>*/
/*                         <th>*/
/*                             Enseigne*/
/*                         </th>*/
/*                         <th>*/
/*                             Produit*/
/*                         </th>*/
/*                         <th>*/
/*                             Modification*/
/*                         </th>*/
/*                         <th>*/
/*                             Suppression*/
/*                         </th>*/
/*                        */
/*                     </tr>*/
/*         </thead>*/
/*         <tbody>*/
/*                     */
/*                         {% for Stock in Stock %}*/
/*                         <tr>      */
/*                                 */
/*                        */
/*                         <td>*/
/*                             {{Stock.Quantiteproduit}}*/
/*                         </td>*/
/*                          <td>*/
/*                              {{Stock.DateAjout.format('Y/m/d')}}*/
/*                         </td>*/
/*                          <td >*/
/*                            {{Stock.Enseigne.getLibelle()}}*/
/*                         </td>*/
/*                          <td>*/
/*                              {{Stock.Produit.getLibelle()}}*/
/*                         </td>*/
/*                          <td >*/
/*                           <center><a href="{{path('tunisia_mall_stock_Modif',{'id':Stock.id})}}"><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Modifier.png')}} " value="" href="#" ></center>*/
/*                          */
/*                         </td>*/
/*                          <td >*/
/*                          <center><a href="{{path('tunisia_mall_stock_Supp',{'id':Stock.id})}}"><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Supprimer.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                     </tr>*/
/*                     */
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/*             </div>*/
/*                     </div>*/
/*                     </div>*/
/*                     </div>*/
/*                     </div>*/
/*     {% endblock TableView %}    */
/* {% block btn %}*/
/*      <br>*/
/*                 <div>*/
/*                     <form align="left" action="{{('AjouterStock')}}">*/
/*                         <center><input type="submit" class="btn" value="Ajouter"/></center>*/
/*                     </form>*/
/*                     */
/*                 </div>   */
/*                     */
/*     {% endblock %}*/
/*     */
/*      {% block script %}*/
/*            <script src="{{asset('js/Responsable/tabledata.js')}}"></script>*/
/*            <script src="{{asset('js/Responsable/appli.js')}}"></script>*/
/*         {% endblock %}*/
/*         */
/*     */
/*         */
