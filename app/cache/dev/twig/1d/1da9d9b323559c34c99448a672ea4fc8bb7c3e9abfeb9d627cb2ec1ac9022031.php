<?php

/* PromotionBundle:Promotion:acceuil.html.twig */
class __TwigTemplate_ff99575ad1174872efb40c427d062c28a7fdb8e9b0c27ed9b64d6e3dd80c2c48 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ClientBundle::Layout.html.twig", "PromotionBundle:Promotion:acceuil.html.twig", 1);
        $this->blocks = array(
            'PromoB' => array($this, 'block_PromoB'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ClientBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_PromoB($context, array $blocks = array())
    {
        // line 4
        echo "
 
    <div class=\"moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-large moto-spacing-right-auto moto-spacing-bottom-large moto-spacing-left-auto\" data-widget=\"spacer\" data-preset=\"default\" data-spacing=\"lala\">
        <div class=\"moto-widget-spacer-block\" style=\"height: 0px;\"></div>
    </div>
    <div class=\"moto-widget moto-widget-row\" data-widget=\"row\">
        <div class=\"container-fluid\">
            <div class=\"row\">
                ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["promotions"]) ? $context["promotions"] : $this->getContext($context, "promotions")));
        foreach ($context['_seq'] as $context["_key"] => $context["promotion"]) {
            // line 13
            echo "                <div class=\"moto-cell col-md-4\" data-container=\"container\">
                    <div data-css-name=\"moto-container_content_56a69c77\" class=\"moto-widget moto-widget-container moto-container_content_56a69c77\" data-widget=\"container\" data-container=\"container\">
                        <div data-grid-type=\"xs\" class=\"moto-widget moto-widget-row row-gutter-0\" data-widget=\"row\">
                            <div class=\"container-fluid\">
                                <div class=\"row\">
                                    <div class=\"moto-cell col-xs-9\" data-container=\"container\">
                                        <div class=\"moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto\" data-widget=\"text\" data-preset=\"default\" data-spacing=\"aaaa\">
                                            <div class=\"moto-widget-text-content moto-widget-text-editable\">
                                                <p class=\"moto-text_289\">
                                                    <a class=\"moto-link\" data-action=\"url\" href=\"Promotion\" target=\"_self\">";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotion"], "Description", array()), "html", null, true);
            echo "</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class=\"moto-cell col-xs-3\" data-container=\"container\">
                                        <div class=\"moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto\" data-widget=\"text\" data-preset=\"default\" data-spacing=\"aaaa\">
                                            <div class=\"moto-widget-text-content moto-widget-text-editable\">
                                                <p style=\"text-align: right;\" class=\"moto-text_system_3\"><br>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>          
                    <div class=\"moto-widget moto-widget-image moto-preset-3 moto-align-center moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto \" data-preset=\"3\" data-spacing=\"aasa\" data-widget=\"image\">
                        <a class=\"moto-widget-image-link moto-link\" href=\"Promotion\"  data-action=\"page\">
                            <img src=\"";
            // line 42
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute($context["promotion"], "image", array())), "html", null, true);
            echo "\" class=\"moto-widget-image-picture\" data-id=\"157\" title=\"\"  alt=\"\" draggable=\"false\">
                        </a>                      
                    </div>
                </div>
                         ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['promotion'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "             
            </div>
            
                </div>                               
            </div>                                        
        </div>


                           
";
    }

    public function getTemplateName()
    {
        return "PromotionBundle:Promotion:acceuil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 47,  79 => 42,  56 => 22,  45 => 13,  41 => 12,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends "ClientBundle::Layout.html.twig" %}*/
/* */
/* {% block PromoB %}*/
/* */
/*  */
/*     <div class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-large moto-spacing-right-auto moto-spacing-bottom-large moto-spacing-left-auto" data-widget="spacer" data-preset="default" data-spacing="lala">*/
/*         <div class="moto-widget-spacer-block" style="height: 0px;"></div>*/
/*     </div>*/
/*     <div class="moto-widget moto-widget-row" data-widget="row">*/
/*         <div class="container-fluid">*/
/*             <div class="row">*/
/*                 {%for promotion in promotions %}*/
/*                 <div class="moto-cell col-md-4" data-container="container">*/
/*                     <div data-css-name="moto-container_content_56a69c77" class="moto-widget moto-widget-container moto-container_content_56a69c77" data-widget="container" data-container="container">*/
/*                         <div data-grid-type="xs" class="moto-widget moto-widget-row row-gutter-0" data-widget="row">*/
/*                             <div class="container-fluid">*/
/*                                 <div class="row">*/
/*                                     <div class="moto-cell col-xs-9" data-container="container">*/
/*                                         <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aaaa">*/
/*                                             <div class="moto-widget-text-content moto-widget-text-editable">*/
/*                                                 <p class="moto-text_289">*/
/*                                                     <a class="moto-link" data-action="url" href="Promotion" target="_self">{{promotion.Description}}</a>*/
/*                                                 </p>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/* */
/*                                     <div class="moto-cell col-xs-3" data-container="container">*/
/*                                         <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aaaa">*/
/*                                             <div class="moto-widget-text-content moto-widget-text-editable">*/
/*                                                 <p style="text-align: right;" class="moto-text_system_3"><br>*/
/*                                                 </p>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>          */
/*                     <div class="moto-widget moto-widget-image moto-preset-3 moto-align-center moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-preset="3" data-spacing="aasa" data-widget="image">*/
/*                         <a class="moto-widget-image-link moto-link" href="Promotion"  data-action="page">*/
/*                             <img src="{{asset(promotion.image)}}" class="moto-widget-image-picture" data-id="157" title=""  alt="" draggable="false">*/
/*                         </a>                      */
/*                     </div>*/
/*                 </div>*/
/*                          {% endfor %}*/
/*              */
/*             </div>*/
/*             */
/*                 </div>                               */
/*             </div>                                        */
/*         </div>*/
/* */
/* */
/*                            */
/* {% endblock %}*/
/* */
