<?php

/* ReservationPackBundle:ReservationPack:ReservationForm.html.twig */
class __TwigTemplate_7ce8cc1f05d42a7299dc1aee573b077cae820ed46272ef0e4d1e2057f81d2630 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "ReservationPackBundle:ReservationPack:ReservationForm.html.twig", 1);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 3
        echo "    <div class =\"container\">
        <div class=\"col-md-6\">
            <!-- Horizontal Form -->
            <div class=\"box box-info\">
                <div class=\"box-header with-border\">
                    <h3 class=\"box-title\">Reserver le pack publicitaire</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class=\"form-horizontal\" name=\"form\" onsubmit=\"verif()\" method =\"POST\">
                    <div class=\"box-body\">

                        ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["Packs"]) ? $context["Packs"] : $this->getContext($context, "Packs")));
        foreach ($context['_seq'] as $context["_key"] => $context["pack"]) {
            // line 15
            echo "                            <label>Zone : </label>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "Zone", array()), "html", null, true);
            echo "<br>

                            <label>Libelle : </label>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "Libelle", array()), "html", null, true);
            echo "<br>

                            <label >Prix : </label>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "Prix", array()), "html", null, true);
            echo "<br>

                            <label>Description : </label>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "Description", array()), "html", null, true);
            echo "<br>

                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pack'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "
                        <div class=\"form-group\">
                            <label for=\"date1\" class=\"col-sm-2 control-label\">Date début</label>
                            <div class=\"col-sm-10\">
                                <input name=\"date1\" type=\"date\" class=\"form-control\" id=\"date1\">
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"date2\" class=\"col-sm-2 control-label\">Date fin </label>
                            <div class=\"col-sm-10\">
                                <input name=\"date2\" type=\"date\" class=\"form-control\" id=\"date2\">
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"image\" class=\"col-sm-2 control-label\">Image</label>
                            <div class=\"col-sm-10\">
                                <input name =\"img\" type=\"file\" class=\"form-control\" id=\"image\" >
                            </div>
                        </div>


                        <div class=\"form-group\">
                            <label for=\"inputPassword3\" class=\"col-sm-2 control-label\">Description</label>
                            <div class=\"col-sm-10\">
                                <textarea rows=\"5\" cols=\"50\" class=\"form-control\" id=\"text1\" name=\"des\"></textarea>
                            </div>
                        </div>  



                    </div><!-- /.box-body -->
                    <div class=\"box-footer\">
                        <button type=\"reset\" class=\"btn btn-default\" name =\"submit\">Annuler</button>
                        <button type=\"submit\" class=\"btn btn-info pull-right\">Réserver</button>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box -->
        </div>

    </div>
";
    }

    public function getTemplateName()
    {
        return "ReservationPackBundle:ReservationPack:ReservationForm.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 24,  64 => 21,  59 => 19,  54 => 17,  48 => 15,  44 => 14,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block HeaderTitle %}*/
/*     <div class ="container">*/
/*         <div class="col-md-6">*/
/*             <!-- Horizontal Form -->*/
/*             <div class="box box-info">*/
/*                 <div class="box-header with-border">*/
/*                     <h3 class="box-title">Reserver le pack publicitaire</h3>*/
/*                 </div><!-- /.box-header -->*/
/*                 <!-- form start -->*/
/*                 <form class="form-horizontal" name="form" onsubmit="verif()" method ="POST">*/
/*                     <div class="box-body">*/
/* */
/*                         {% for pack in Packs %}*/
/*                             <label>Zone : </label>{{pack.Zone}}<br>*/
/* */
/*                             <label>Libelle : </label>{{pack.Libelle}}<br>*/
/* */
/*                             <label >Prix : </label>{{pack.Prix}}<br>*/
/* */
/*                             <label>Description : </label>{{pack.Description}}<br>*/
/* */
/*                         {%endfor%}*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="date1" class="col-sm-2 control-label">Date début</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="date1" type="date" class="form-control" id="date1">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="date2" class="col-sm-2 control-label">Date fin </label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="date2" type="date" class="form-control" id="date2">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="image" class="col-sm-2 control-label">Image</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name ="img" type="file" class="form-control" id="image" >*/
/*                             </div>*/
/*                         </div>*/
/* */
/* */
/*                         <div class="form-group">*/
/*                             <label for="inputPassword3" class="col-sm-2 control-label">Description</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <textarea rows="5" cols="50" class="form-control" id="text1" name="des"></textarea>*/
/*                             </div>*/
/*                         </div>  */
/* */
/* */
/* */
/*                     </div><!-- /.box-body -->*/
/*                     <div class="box-footer">*/
/*                         <button type="reset" class="btn btn-default" name ="submit">Annuler</button>*/
/*                         <button type="submit" class="btn btn-info pull-right">Réserver</button>*/
/*                     </div><!-- /.box-footer -->*/
/*                 </form>*/
/*             </div><!-- /.box -->*/
/*         </div>*/
/* */
/*     </div>*/
/* {% endblock %}*/
/* {# empty Twig template #}*/
/* */
