<?php

/* ResponsableBundle:Default:GestionDesComptes.html.twig */
class __TwigTemplate_ec378c5008921f08f32c76eb09d6d45044f2f0f66cdce352379f3f8b871e9766 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "ResponsableBundle:Default:GestionDesComptes.html.twig", 2);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 5
        echo "               
            <h2>
            Gestion Des Comptes
          </h2>
            ";
    }

    // line 13
    public function block_TableView($context, array $blocks = array())
    {
        // line 14
        echo "    
 <div class=\"box box-success \">    
    <div class=\"box-body no-padding\">
                <table class=\"table table-bordered table-hover\" >
                    <tr>
                        
                        <th>
                            Login
                        </th>
                        <th>
                            email
                        </th>
                        <th>
                            Role
                        </th>
                        <th>
                            Accepter
                        </th> 
                        <th>
                            Refuser
                        </th>
                    </tr>
                    ";
        // line 36
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")));
        foreach ($context['_seq'] as $context["_key"] => $context["u"]) {
            // line 37
            echo "                    <tr>
                        <td >
                          ";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "username", array()), "html", null, true);
            echo " 
                        </td>
                        <td>
                           ";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "email", array()), "html", null, true);
            echo "
                        </td>
                        <td>
                            ";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "flagRole", array()), "html", null, true);
            echo "
                        </td>
                        
                         <td>
                    <center><a href=\"";
            // line 49
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tunisia_mall_Admin_accept_compte", array("id" => $this->getAttribute($context["u"], "id", array()))), "html", null, true);
            echo "\" class=\"fa fa-check\"></a></center>
                        </td>
                         <td>
                          <center><a href=\"#\" class=\"fa fa-close\"></a></center>
                        </td>
                    </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['u'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "                    
                </table>
                    
            </div>
                    </div>
                   
               
 
    ";
    }

    // line 65
    public function block_btn($context, array $blocks = array())
    {
        // line 66
        echo "    
    ";
    }

    public function getTemplateName()
    {
        return "ResponsableBundle:Default:GestionDesComptes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 66,  120 => 65,  108 => 56,  95 => 49,  88 => 45,  82 => 42,  76 => 39,  72 => 37,  68 => 36,  44 => 14,  41 => 13,  33 => 5,  30 => 4,  11 => 2,);
    }
}
/* */
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* */
/*  {% block HeaderTitle %}*/
/*                */
/*             <h2>*/
/*             Gestion Des Comptes*/
/*           </h2>*/
/*             {%endblock %}*/
/* */
/* */
/* */
/* {% block TableView %}*/
/*     */
/*  <div class="box box-success ">    */
/*     <div class="box-body no-padding">*/
/*                 <table class="table table-bordered table-hover" >*/
/*                     <tr>*/
/*                         */
/*                         <th>*/
/*                             Login*/
/*                         </th>*/
/*                         <th>*/
/*                             email*/
/*                         </th>*/
/*                         <th>*/
/*                             Role*/
/*                         </th>*/
/*                         <th>*/
/*                             Accepter*/
/*                         </th> */
/*                         <th>*/
/*                             Refuser*/
/*                         </th>*/
/*                     </tr>*/
/*                     {% for u in users %}*/
/*                     <tr>*/
/*                         <td >*/
/*                           {{ u.username}} */
/*                         </td>*/
/*                         <td>*/
/*                            {{u.email}}*/
/*                         </td>*/
/*                         <td>*/
/*                             {{u.flagRole}}*/
/*                         </td>*/
/*                         */
/*                          <td>*/
/*                     <center><a href="{{path('tunisia_mall_Admin_accept_compte',{'id':u.id})}}" class="fa fa-check"></a></center>*/
/*                         </td>*/
/*                          <td>*/
/*                           <center><a href="#" class="fa fa-close"></a></center>*/
/*                         </td>*/
/*                     </tr>*/
/*                     {% endfor %}*/
/*                     */
/*                 </table>*/
/*                     */
/*             </div>*/
/*                     </div>*/
/*                    */
/*                */
/*  */
/*     {% endblock TableView %}  */
/*     {% block btn %}*/
/*     */
/*     {% endblock %}*/
/* */
