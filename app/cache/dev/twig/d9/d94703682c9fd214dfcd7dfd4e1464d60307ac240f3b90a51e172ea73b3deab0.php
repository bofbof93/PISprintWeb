<?php

/* ReservationPackBundle:ReservationPack:ListeDemandePack.html.twig */
class __TwigTemplate_964f71153452f12b664c1207844b698a0ab388b8a00ab78581529a8fa2ffb2f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "ReservationPackBundle:ReservationPack:ListeDemandePack.html.twig", 2);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 5
        echo "    <style>
        img {
            width: 50px;
            height: 50px;
        }
    </style>

    <h1>
         Gestion Des packs publicitairess
        <small>Liste des demandes de réservation</small>
    </h1>
    <center>
        <form  method=\"POST\">
            <input type=\"text\" name=\"search\">
            <input type=\"submit\" value=\"rechercher\">
        </form>
    </center>
";
    }

    // line 24
    public function block_TableView($context, array $blocks = array())
    {
        // line 25
        echo "    <div style=\"padding: 10px\">
        <div class=\"TableView\" >
            <div class=\"box box-success \">    
                <div  class=\"box-body no-padding\">
                    <table class=\"table table-bordered table-hover\" >
<tr>
                <th>Zone</th>
                <th> Libellé</th>
                <th>Prix</th>
                <th>Date de début </th>
                <th>Date de fin</th>
                <th>Description</th>
                <th>Nom du responsable</th>
                <th>Accepter</th>
                <th>Refuser</th>

            </tr>
            <tr> ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["res"]) ? $context["res"] : $this->getContext($context, "res")));
        foreach ($context['_seq'] as $context["_key"] => $context["re"]) {
            // line 43
            echo "                <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["re"], "PackPublicitaire", array()), "Zone", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["re"], "PackPublicitaire", array()), "Libelle", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["re"], "PackPublicitaire", array()), "Prix", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["re"], "Datedebut", array()), "format", array(0 => "j/m/Y"), "method"), "html", null, true);
            echo "</td>
                <td>";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["re"], "DateFin", array()), "format", array(0 => "j/m/Y"), "method"), "html", null, true);
            echo "</td>
                <td>";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["re"], "Description", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["re"], "User", array()), "username", array()), "html", null, true);
            echo "</td>
                <td>
            <center><a href=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Accepter_Demande", array("id" => $this->getAttribute($context["re"], "id", array()))), "html", null, true);
            echo "\"><span class=\"fa fa-check\"></span></a></center>
                </td>
                <td >
            <center><a href=\"";
            // line 54
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Refuser_Demande", array("id" => $this->getAttribute($context["re"], "id", array()))), "html", null, true);
            echo "\"><span class=\"fa fa-close\"></span></a></center>
                </td>
                </tr>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['re'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "                        </table>
                                    </div>  </div>         
                                    ";
    }

    // line 62
    public function block_btn($context, array $blocks = array())
    {
        echo "  
                                   
                                            </div></div></div>
                                        ";
    }

    public function getTemplateName()
    {
        return "ReservationPackBundle:ReservationPack:ListeDemandePack.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 62,  127 => 59,  116 => 54,  110 => 51,  105 => 49,  101 => 48,  97 => 47,  93 => 46,  89 => 45,  85 => 44,  80 => 43,  76 => 42,  57 => 25,  54 => 24,  33 => 5,  30 => 4,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* */
/* {% block HeaderTitle %}*/
/*     <style>*/
/*         img {*/
/*             width: 50px;*/
/*             height: 50px;*/
/*         }*/
/*     </style>*/
/* */
/*     <h1>*/
/*          Gestion Des packs publicitairess*/
/*         <small>Liste des demandes de réservation</small>*/
/*     </h1>*/
/*     <center>*/
/*         <form  method="POST">*/
/*             <input type="text" name="search">*/
/*             <input type="submit" value="rechercher">*/
/*         </form>*/
/*     </center>*/
/* {%endblock %}*/
/* */
/* {% block TableView %}*/
/*     <div style="padding: 10px">*/
/*         <div class="TableView" >*/
/*             <div class="box box-success ">    */
/*                 <div  class="box-body no-padding">*/
/*                     <table class="table table-bordered table-hover" >*/
/* <tr>*/
/*                 <th>Zone</th>*/
/*                 <th> Libellé</th>*/
/*                 <th>Prix</th>*/
/*                 <th>Date de début </th>*/
/*                 <th>Date de fin</th>*/
/*                 <th>Description</th>*/
/*                 <th>Nom du responsable</th>*/
/*                 <th>Accepter</th>*/
/*                 <th>Refuser</th>*/
/* */
/*             </tr>*/
/*             <tr> {%for re in res%}*/
/*                 <td>{{re.PackPublicitaire.Zone}}</td>*/
/*                 <td>{{re.PackPublicitaire.Libelle}}</td>*/
/*                 <td>{{re.PackPublicitaire.Prix}}</td>*/
/*                 <td>{{re.Datedebut.format('j/m/Y')}}</td>*/
/*                 <td>{{re.DateFin.format('j/m/Y')}}</td>*/
/*                 <td>{{re.Description}}</td>*/
/*                 <td>{{re.User.username}}</td>*/
/*                 <td>*/
/*             <center><a href="{{path('Accepter_Demande',{'id':re.id})}}"><span class="fa fa-check"></span></a></center>*/
/*                 </td>*/
/*                 <td >*/
/*             <center><a href="{{path('Refuser_Demande',{'id':re.id})}}"><span class="fa fa-close"></span></a></center>*/
/*                 </td>*/
/*                 </tr>*/
/* */
/*                     {%endfor%}*/
/*                         </table>*/
/*                                     </div>  </div>         */
/*                                     {% endblock %} */
/*                                         {%block btn %}  */
/*                                    */
/*                                             </div></div></div>*/
/*                                         {% endblock %}*/
/* */
