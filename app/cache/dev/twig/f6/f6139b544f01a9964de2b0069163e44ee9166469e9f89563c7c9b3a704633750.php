<?php

/* TunisiaMallGestionStockBundle:GestionStock:RechercherStock.html.twig */
class __TwigTemplate_47328d666f97a4373ee1f55e64c67e0f35e801d42a4c06fbe645702ef525f21f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "TunisiaMallGestionStockBundle:GestionStock:RechercherStock.html.twig", 2);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 5
        echo "               
            <h1>
            Gestion Des Stocks
            <small>Liste Des Stocks</small>
          </h1>
            ";
    }

    // line 14
    public function block_TableView($context, array $blocks = array())
    {
        // line 15
        echo "   
<div class=\"TableView\" >
    <table border=\"0\">
    <form method=\"POST\">
  <tr>
    <td>Référence:</td>
      <td><input type=\"text\" name=\"Reference\"></td><td><input class=\"btn\" type=\"submit\" value=\"Rechercher\"></td> 
  </tr>
   <tr>
       
\t </tr>
    </form>
</table>
</div>
    <br>
    <br>
       
<div class=\"TableView\" >
    <table >
                    <tr>
                      
                        <td>
                            Id Stock
                        </td>
                        <td >
                            Référence
                        </td>
                        <td>
                            Quantité Produit
                        </td>
                        <td>
                            Date Ajout
                        </td>
                        <td>
                            Enseigne
                        </td>
                        <td>
                            Produit
                        </td>
                        <td>
                            Modification
                        </td>
                        <td>
                            Suppression
                        </td>
                       
                    </tr>
                    <tr>
                        ";
        // line 63
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["Stock"]);
        foreach ($context['_seq'] as $context["_key"] => $context["Stock"]) {
            // line 64
            echo "                              
                                
                        <td >
                           ";
            // line 67
            echo twig_escape_filter($this->env, $this->getAttribute($context["Stock"], "id", array()), "html", null, true);
            echo "
                        </td>
                        <td>
                            ";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute($context["Stock"], "Ref", array()), "html", null, true);
            echo "
                        </td>
                        <td>
                            ";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute($context["Stock"], "Quantiteproduit", array()), "html", null, true);
            echo "
                        </td>
                         <td>
                            ";
            // line 76
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["Stock"], "DateAjout", array()), "format", array(0 => "Y/m/d"), "method"), "html", null, true);
            echo "
                        </td>
                         <td >
                           ";
            // line 79
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["Stock"], "Enseigne", array()), "getId", array(), "method"), "html", null, true);
            echo "
                        </td>
                         <td >
                           ";
            // line 82
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["Stock"], "Produit", array()), "getId", array(), "method"), "html", null, true);
            echo "
                        </td>
                         <td >
                          <center><a href=\"";
            // line 85
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tunisia_mall_stock_Modif", array("id" => $this->getAttribute($context["Stock"], "id", array()))), "html", null, true);
            echo "\"><input class=\"modifIcon\" type=\"image\"  src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Modifier.png"), "html", null, true);
            echo " \" value=\"\" href=\"#\" ></center>
                         
                        </td>
                         <td >
                         <center><a href=\"";
            // line 89
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tunisia_mall_stock_Supp", array("id" => $this->getAttribute($context["Stock"], "id", array()))), "html", null, true);
            echo "\"><input class=\"modifIcon\" type=\"image\"  src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Supprimer.png"), "html", null, true);
            echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                    </tr>
                    
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['Stock'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "                </table>
            </div>
    ";
    }

    // line 97
    public function block_btn($context, array $blocks = array())
    {
        // line 98
        echo "     <br>
                <div>
                    <form align=\"left\" action=\"";
        // line 100
        echo "Ajouter";
        echo "\">
                    <input type=\"submit\" class=\"btn\" value=\"Ajouter Stock\"/>
                    </form>
                    <form align=\"right\" action=\"";
        // line 103
        echo "rechercherStock";
        echo "\">
                    <input type=\"submit\" class=\"btn\" value=\"Chercher Stock\"/>
                    </form>
                </div>  
                    
    ";
    }

    public function getTemplateName()
    {
        return "TunisiaMallGestionStockBundle:GestionStock:RechercherStock.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 103,  175 => 100,  171 => 98,  168 => 97,  162 => 94,  149 => 89,  140 => 85,  134 => 82,  128 => 79,  122 => 76,  116 => 73,  110 => 70,  104 => 67,  99 => 64,  95 => 63,  45 => 15,  42 => 14,  33 => 5,  30 => 4,  11 => 2,);
    }
}
/* */
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* */
/*  {% block HeaderTitle %}*/
/*                */
/*             <h1>*/
/*             Gestion Des Stocks*/
/*             <small>Liste Des Stocks</small>*/
/*           </h1>*/
/*             {%endblock %}*/
/* */
/* */
/* */
/* {% block TableView %}*/
/*    */
/* <div class="TableView" >*/
/*     <table border="0">*/
/*     <form method="POST">*/
/*   <tr>*/
/*     <td>Référence:</td>*/
/*       <td><input type="text" name="Reference"></td><td><input class="btn" type="submit" value="Rechercher"></td> */
/*   </tr>*/
/*    <tr>*/
/*        */
/* 	 </tr>*/
/*     </form>*/
/* </table>*/
/* </div>*/
/*     <br>*/
/*     <br>*/
/*        */
/* <div class="TableView" >*/
/*     <table >*/
/*                     <tr>*/
/*                       */
/*                         <td>*/
/*                             Id Stock*/
/*                         </td>*/
/*                         <td >*/
/*                             Référence*/
/*                         </td>*/
/*                         <td>*/
/*                             Quantité Produit*/
/*                         </td>*/
/*                         <td>*/
/*                             Date Ajout*/
/*                         </td>*/
/*                         <td>*/
/*                             Enseigne*/
/*                         </td>*/
/*                         <td>*/
/*                             Produit*/
/*                         </td>*/
/*                         <td>*/
/*                             Modification*/
/*                         </td>*/
/*                         <td>*/
/*                             Suppression*/
/*                         </td>*/
/*                        */
/*                     </tr>*/
/*                     <tr>*/
/*                         {% for Stock in Stock %}*/
/*                               */
/*                                 */
/*                         <td >*/
/*                            {{Stock.id}}*/
/*                         </td>*/
/*                         <td>*/
/*                             {{Stock.Ref}}*/
/*                         </td>*/
/*                         <td>*/
/*                             {{Stock.Quantiteproduit}}*/
/*                         </td>*/
/*                          <td>*/
/*                             {{Stock.DateAjout.format('Y/m/d')}}*/
/*                         </td>*/
/*                          <td >*/
/*                            {{Stock.Enseigne.getId()}}*/
/*                         </td>*/
/*                          <td >*/
/*                            {{Stock.Produit.getId()}}*/
/*                         </td>*/
/*                          <td >*/
/*                           <center><a href="{{path('tunisia_mall_stock_Modif',{'id':Stock.id})}}"><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Modifier.png')}} " value="" href="#" ></center>*/
/*                          */
/*                         </td>*/
/*                          <td >*/
/*                          <center><a href="{{path('tunisia_mall_stock_Supp',{'id':Stock.id})}}"><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Supprimer.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                     </tr>*/
/*                     */
/*                     {% endfor %}*/
/*                 </table>*/
/*             </div>*/
/*     {% endblock TableView %}    */
/* {% block btn %}*/
/*      <br>*/
/*                 <div>*/
/*                     <form align="left" action="{{('Ajouter')}}">*/
/*                     <input type="submit" class="btn" value="Ajouter Stock"/>*/
/*                     </form>*/
/*                     <form align="right" action="{{('rechercherStock')}}">*/
/*                     <input type="submit" class="btn" value="Chercher Stock"/>*/
/*                     </form>*/
/*                 </div>  */
/*                     */
/*     {% endblock %}*/
