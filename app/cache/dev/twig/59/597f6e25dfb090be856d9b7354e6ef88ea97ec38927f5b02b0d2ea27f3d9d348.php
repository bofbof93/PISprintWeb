<?php

/* PackPubBundle:PackPub:ReserverR.html.twig */
class __TwigTemplate_e9e76a70a09d3dec2c889273b2d3be9762d3aa6e59ad6f2b745dbc9984371a35 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "PackPubBundle:PackPub:ReserverR.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_contenu($context, array $blocks = array())
    {
        // line 3
        echo "    <div class =\"container\">
        <div class=\"col-md-6\">
            <!-- Horizontal Form -->
            <div class=\"box box-info\">
                <div class=\"box-header with-border\">
                    <h3 class=\"box-title\">Ajouter un pack publicitaire</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class=\"form-horizontal\" name=\"form\" onsubmit=\"verif()\" method =\"POST\">
                    <div class=\"box-body\">


                        <div class=\"form-group\">
                            <label for=\"text\" class=\"col-sm-2 control-label\">Libelle </label>
                            <div class=\"col-sm-10\">
                                <input name=\"libelle\" type=\"text\" class=\"form-control\" id=\"date1\">
                            </div>
                        </div>     

                        <div class=\"form-group\">
                            <label for=\"date1\" class=\"col-sm-2 control-label\">Date début </label>
                            <div class=\"col-sm-10\">
                                <input name=\"Datedebut\" type=\"date\" class=\"form-control\" id=\"date1\">
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"date2\" class=\"col-sm-2 control-label\">Date fin </label>
                            <div class=\"col-sm-10\">
                                <input name=\"Datefin\" type=\"date\" class=\"form-control\" id=\"date2\">
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"taux\" class=\"col-sm-2 control-label\">Prix</label>
                            <div class=\"col-sm-10\">
                                <input name=\"prix\" type=\"text\" class=\"form-control\" id=\"inputPassword3\" placeholder=\"\">
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"taux\" class=\"col-sm-2 control-label\">Zone</label>
                            <div class=\"col-sm-10\">
                                <input name=\"zone\" type=\"number\" class=\"form-control\" id=\"inputPassword3\" placeholder=\"\">
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"image\" class=\"col-sm-2 control-label\">Image</label>
                            <div class=\"col-sm-10\">
                                <input name =\"img\" type=\"file\" class=\"form-control\" id=\"image\" >
                            </div>
                        </div>



                        ";
        // line 59
        echo "         

                        <div class=\"col-sm-10\">
                            ";
        // line 63
        echo "                            <select name=\"responsable\" onChange=\"\">


                                <option></option>


                            </select>

                        </div>  



                        <div class=\"col-sm-10\">
                            ";
        // line 77
        echo "                            <select name=\"admin\" onChange=\"\">


                                <option></option>


                            </select>

                        </div> 
                    </div>

            <div class=\"box-footer\">
                <button type=\"submit\" class=\"btn btn-default\" name =\"submit\">Cancel</button>
                <button type=\"submit\" class=\"btn btn-info pull-right\">Sign in</button>
            </div><!-- /.box-footer -->
            </form>
        </div><!-- /.box -->
    </div>
                             </div>

";
    }

    public function getTemplateName()
    {
        return "PackPubBundle:PackPub:ReserverR.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 77,  94 => 63,  89 => 59,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block contenu %}*/
/*     <div class ="container">*/
/*         <div class="col-md-6">*/
/*             <!-- Horizontal Form -->*/
/*             <div class="box box-info">*/
/*                 <div class="box-header with-border">*/
/*                     <h3 class="box-title">Ajouter un pack publicitaire</h3>*/
/*                 </div><!-- /.box-header -->*/
/*                 <!-- form start -->*/
/*                 <form class="form-horizontal" name="form" onsubmit="verif()" method ="POST">*/
/*                     <div class="box-body">*/
/* */
/* */
/*                         <div class="form-group">*/
/*                             <label for="text" class="col-sm-2 control-label">Libelle </label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="libelle" type="text" class="form-control" id="date1">*/
/*                             </div>*/
/*                         </div>     */
/* */
/*                         <div class="form-group">*/
/*                             <label for="date1" class="col-sm-2 control-label">Date début </label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="Datedebut" type="date" class="form-control" id="date1">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="date2" class="col-sm-2 control-label">Date fin </label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="Datefin" type="date" class="form-control" id="date2">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="taux" class="col-sm-2 control-label">Prix</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="prix" type="text" class="form-control" id="inputPassword3" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="taux" class="col-sm-2 control-label">Zone</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="zone" type="number" class="form-control" id="inputPassword3" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="image" class="col-sm-2 control-label">Image</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name ="img" type="file" class="form-control" id="image" >*/
/*                             </div>*/
/*                         </div>*/
/* */
/* */
/* */
/*                         {#  {% for responsable in responsables %} #}         */
/* */
/*                         <div class="col-sm-10">*/
/*                             {#   <input name ="responsable" type="text" class="form-control" id="responsable" value ="{{responsable.id}}">#}*/
/*                             <select name="responsable" onChange="">*/
/* */
/* */
/*                                 <option></option>*/
/* */
/* */
/*                             </select>*/
/* */
/*                         </div>  */
/* */
/* */
/* */
/*                         <div class="col-sm-10">*/
/*                             {#   <input name ="responsable" type="text" class="form-control" id="responsable" value ="{{responsable.id}}">#}*/
/*                             <select name="admin" onChange="">*/
/* */
/* */
/*                                 <option></option>*/
/* */
/* */
/*                             </select>*/
/* */
/*                         </div> */
/*                     </div>*/
/* */
/*             <div class="box-footer">*/
/*                 <button type="submit" class="btn btn-default" name ="submit">Cancel</button>*/
/*                 <button type="submit" class="btn btn-info pull-right">Sign in</button>*/
/*             </div><!-- /.box-footer -->*/
/*             </form>*/
/*         </div><!-- /.box -->*/
/*     </div>*/
/*                              </div>*/
/* */
/* {% endblock %}*/
/* */
