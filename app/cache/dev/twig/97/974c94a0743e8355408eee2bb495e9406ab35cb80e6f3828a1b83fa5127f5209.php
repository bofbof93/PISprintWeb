<?php

/* PromotionProduitBundle:PromotionProduit:AjouterPromotionProduit.html.twig */
class __TwigTemplate_381bc234f7448735d05866b84d691e2efb335b51c4837dcf4e3e97f8995842b1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 4
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "PromotionProduitBundle:PromotionProduit:AjouterPromotionProduit.html.twig", 4);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 6
        echo "
    <h1>
        Ajouter
        <small>Promotion sur Produit</small>
    </h1>
";
    }

    // line 13
    public function block_TableView($context, array $blocks = array())
    {
        // line 14
        echo "
    <div class=\"col-md-6\">
        <!-- Horizontal Form -->
        <div class=\"box box-info\">
            <div class=\"box-header with-border\">
                <h3 class=\"box-title\">Ajouter</h3>

                <form  method=\"POST\" class=\"form-horizontal\" name=\"form\" onsubmit=\"verif()\">
                    <div> ";
        // line 22
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
                        ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
                       
                        
                        <div class=\"form-group\">
                            <label for=\"Produit\" class=\"col-sm-2 control-label\">Produit</label>
                            ";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Produit", array()), 'errors');
        echo "
                            <div class=\"col-sm-10\">
                                ";
        // line 30
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Produit", array()), 'widget');
        echo "
                            </div></div> 
                            <div class=\"form-group\">
                            <label for=\"Enseigne\" class=\"col-sm-2 control-label\">Promotion</label>
                            ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Promotion", array()), 'errors');
        echo "
                            <div class=\"col-sm-10\">
                                ";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Promotion", array()), 'widget');
        echo "
                            </div></div> 




                        <div class=\"box-footer\">
                            ";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Ajouter", array()), 'widget');
        echo "
                        </div>
                        ";
        // line 45
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "


                    </div>
                </form>
            </div>
        </div>
    </div><!-- /.box -->


";
    }

    public function getTemplateName()
    {
        return "PromotionProduitBundle:PromotionProduit:AjouterPromotionProduit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 45,  93 => 43,  83 => 36,  78 => 34,  71 => 30,  66 => 28,  58 => 23,  54 => 22,  44 => 14,  41 => 13,  32 => 6,  29 => 5,  11 => 4,);
    }
}
/* {# empty Twig template #}*/
/* {# empty Twig template #}*/
/* {# empty Twig template #}*/
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block HeaderTitle %}*/
/* */
/*     <h1>*/
/*         Ajouter*/
/*         <small>Promotion sur Produit</small>*/
/*     </h1>*/
/* {%endblock %}*/
/* */
/* {% block TableView %}*/
/* */
/*     <div class="col-md-6">*/
/*         <!-- Horizontal Form -->*/
/*         <div class="box box-info">*/
/*             <div class="box-header with-border">*/
/*                 <h3 class="box-title">Ajouter</h3>*/
/* */
/*                 <form  method="POST" class="form-horizontal" name="form" onsubmit="verif()">*/
/*                     <div> {{ form_start(form) }}*/
/*                         {{form_errors(form) }}*/
/*                        */
/*                         */
/*                         <div class="form-group">*/
/*                             <label for="Produit" class="col-sm-2 control-label">Produit</label>*/
/*                             {{ form_errors(form.Produit) }}*/
/*                             <div class="col-sm-10">*/
/*                                 {{ form_widget(form.Produit)  }}*/
/*                             </div></div> */
/*                             <div class="form-group">*/
/*                             <label for="Enseigne" class="col-sm-2 control-label">Promotion</label>*/
/*                             {{ form_errors(form.Promotion) }}*/
/*                             <div class="col-sm-10">*/
/*                                 {{ form_widget(form.Promotion)  }}*/
/*                             </div></div> */
/* */
/* */
/* */
/* */
/*                         <div class="box-footer">*/
/*                             {{form_widget(form.Ajouter) }}*/
/*                         </div>*/
/*                         {{ form_end(form)}}*/
/* */
/* */
/*                     </div>*/
/*                 </form>*/
/*             </div>*/
/*         </div>*/
/*     </div><!-- /.box -->*/
/* */
/* */
/* {% endblock %}*/
/* */
