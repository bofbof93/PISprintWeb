<?php

/* PromotionBundle:Promotion:AjouterPromotion.html.twig */
class __TwigTemplate_b451f6a5571e7cefc8e08bd488030d5a156df69e19d6318342bc456124fda4cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "PromotionBundle:Promotion:AjouterPromotion.html.twig", 1);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 3
        echo "
    <div class =\"container\">
        <div class=\"col-md-6\">
            <!-- Horizontal Form -->
            <div class=\"box box-info\">
                <div class=\"box-header with-border\">
                    <h3 class=\"box-title\">Ajouter une promotion</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class=\"form-horizontal\" name=\"form\" onsubmit=\"return ajout()\" method =\"POST\">
                    <div class=\"box-body\">
                        <script language=\"javascript\">
                            function ajout()
                            {
                                var d = Date.now();
                                if ((window.document.form.date1.value == \"\") || (window.document.form.date2.value == \"\") || (window.document.form.taux.value == \"\"|| (window.document.form.image.value == \"\") || (window.document.form.description.value == \"\"))
                                {
                                    alert(\"Champ est vider\");
                                    return false;
                                }
                                else if (window.document.form.date1.value == window.document.form.date2.value)
                                {
                                    alert(\"date de début promotion superieur à date de fin \");
                                    return false;
                                }
                                else if (window.document.form.date1.value < d) || (window.document.form.date2.value < d )
                                {
                                    alert(\"la date est passé \");
                                    return false;
                                }
                            }</script> 
                        <div class=\"form-group\">
                            <label for=\"date1\" class=\"col-sm-2 control-label\">Date début </label>
                            <div class=\"col-sm-10\">
                                <input name=\"date1\" type=\"date\" class=\"form-control\" id=\"date1\">
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"date2\" class=\"col-sm-2 control-label\">Date fin </label>
                            <div class=\"col-sm-10\">
                                <input name=\"date2\" type=\"date\" class=\"form-control\" id=\"date2\">
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"taux\" class=\"col-sm-2 control-label\">Taux de réduction</label>
                            <div class=\"col-sm-10\">
                                <input name=\"taux\" type=\"number\" class=\"form-control\" id=\"inputPassword3\" placeholder=\"\">
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"image\" class=\"col-sm-2 control-label\">Image</label>
                            <div class=\"col-sm-10\">
                                <input name =\"img\" type=\"File\" class=\"form-control\" id=\"img\" >
                            </div>
                        </div>


                        <div class=\"form-group\">
                            <label for=\"inputPassword3\" class=\"col-sm-2 control-label\">Description</label>
                            <div class=\"col-sm-10\">
                                <textarea rows=\"5\" cols=\"50\" class=\"form-control\" id=\"text1\" name=\"des\"></textarea>
                            </div>
                        </div>  


                        <div class=\"form-group\"> 
                            <label for=\"etat\" class=\"col-sm-2 control-label\">Etat</label>
                            <div class=\"col-sm-10\">
                                <select name=\"etat\" onChange=\"\">


                                    <option>Active</option>
                                    <option>non active</option>

                                </select> 
                            </div>  
                        </div>


                        <div class=\"form-group\"> 

                            ";
        // line 87
        echo "         


                        </div>





                    </div><!-- /.box-body -->
                    <div class=\"box-footer\">
                        <button type=\"reset\" class=\"btn btn-default\" name =\"submit\">Annuler</button>
                        <button type=\"submit\" class=\"btn btn-info pull-right\">Ajouter</button>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box -->
        </div>

    </div>
";
    }

    public function getTemplateName()
    {
        return "PromotionBundle:Promotion:AjouterPromotion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 87,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block HeaderTitle %}*/
/* */
/*     <div class ="container">*/
/*         <div class="col-md-6">*/
/*             <!-- Horizontal Form -->*/
/*             <div class="box box-info">*/
/*                 <div class="box-header with-border">*/
/*                     <h3 class="box-title">Ajouter une promotion</h3>*/
/*                 </div><!-- /.box-header -->*/
/*                 <!-- form start -->*/
/*                 <form class="form-horizontal" name="form" onsubmit="return ajout()" method ="POST">*/
/*                     <div class="box-body">*/
/*                         <script language="javascript">*/
/*                             function ajout()*/
/*                             {*/
/*                                 var d = Date.now();*/
/*                                 if ((window.document.form.date1.value == "") || (window.document.form.date2.value == "") || (window.document.form.taux.value == ""|| (window.document.form.image.value == "") || (window.document.form.description.value == ""))*/
/*                                 {*/
/*                                     alert("Champ est vider");*/
/*                                     return false;*/
/*                                 }*/
/*                                 else if (window.document.form.date1.value == window.document.form.date2.value)*/
/*                                 {*/
/*                                     alert("date de début promotion superieur à date de fin ");*/
/*                                     return false;*/
/*                                 }*/
/*                                 else if (window.document.form.date1.value < d) || (window.document.form.date2.value < d )*/
/*                                 {*/
/*                                     alert("la date est passé ");*/
/*                                     return false;*/
/*                                 }*/
/*                             }</script> */
/*                         <div class="form-group">*/
/*                             <label for="date1" class="col-sm-2 control-label">Date début </label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="date1" type="date" class="form-control" id="date1">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="date2" class="col-sm-2 control-label">Date fin </label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="date2" type="date" class="form-control" id="date2">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="taux" class="col-sm-2 control-label">Taux de réduction</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="taux" type="number" class="form-control" id="inputPassword3" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="image" class="col-sm-2 control-label">Image</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name ="img" type="File" class="form-control" id="img" >*/
/*                             </div>*/
/*                         </div>*/
/* */
/* */
/*                         <div class="form-group">*/
/*                             <label for="inputPassword3" class="col-sm-2 control-label">Description</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <textarea rows="5" cols="50" class="form-control" id="text1" name="des"></textarea>*/
/*                             </div>*/
/*                         </div>  */
/* */
/* */
/*                         <div class="form-group"> */
/*                             <label for="etat" class="col-sm-2 control-label">Etat</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <select name="etat" onChange="">*/
/* */
/* */
/*                                     <option>Active</option>*/
/*                                     <option>non active</option>*/
/* */
/*                                 </select> */
/*                             </div>  */
/*                         </div>*/
/* */
/* */
/*                         <div class="form-group"> */
/* */
/*                             {#  {% for responsable in responsables %} #}         */
/* */
/* */
/*                         </div>*/
/* */
/* */
/* */
/* */
/* */
/*                     </div><!-- /.box-body -->*/
/*                     <div class="box-footer">*/
/*                         <button type="reset" class="btn btn-default" name ="submit">Annuler</button>*/
/*                         <button type="submit" class="btn btn-info pull-right">Ajouter</button>*/
/*                     </div><!-- /.box-footer -->*/
/*                 </form>*/
/*             </div><!-- /.box -->*/
/*         </div>*/
/* */
/*     </div>*/
/* {% endblock %}*/
/* */
