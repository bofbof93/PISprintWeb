<?php

/* FOSUserBundle:Profile:show_content.html.twig */
class __TwigTemplate_8acc7f17b8d74056f10bf946930c6114793f51b3d8df2f40eb316b050bbfc27a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "

<li class=\"dropdown user user-menu\">
                <!-- Menu Toggle Button -->
                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                  <!-- The user image in the navbar-->
                  <img src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Admin/user2-160x160.jpg"), "html", null, true);
        echo "\" class=\"user-image\" alt=\"User Image\">
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class=\"hidden-xs\"> ";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</span>
                </a>
                <ul class=\"dropdown-menu\">
                  <!-- The user image in the menu -->
                  <li class=\"user-header\">
                    <img src=\"images/Admin/user2-160x160.jpg\" class=\"img-circle\" alt=\"User Image\">
                    <p>
\t\t\t\t\t\t    <p>";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</p>

                      <small>Member since Nov. 2016</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class=\"user-body\">
                    <div class=\"col-xs-4 text-center\">
                      <a href=\"#\">Followers</a>
                    </div>
                    <div class=\"col-xs-4 text-center\">
                      <a href=\"#\">Sales</a>
                    </div>
                    <div class=\"col-xs-4 text-center\">
                      <a href=\"#\">Friends</a>
                    </div>
                  </li>
                  <!-- Menu Footer-->
                  <li class=\"user-footer\">
                    <div class=\"pull-left\">
                      <a href=\"#\" class=\"btn btn-default btn-flat\">Profile</a>
                    </div>
                    <div class=\"pull-right\">
                      <a href=\"#\" class=\"btn btn-default btn-flat\">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 17,  32 => 10,  27 => 8,  19 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* */
/* <li class="dropdown user user-menu">*/
/*                 <!-- Menu Toggle Button -->*/
/*                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">*/
/*                   <!-- The user image in the navbar-->*/
/*                   <img src="{{asset('images/Admin/user2-160x160.jpg')}}" class="user-image" alt="User Image">*/
/*                   <!-- hidden-xs hides the username on small devices so only the image appears. -->*/
/*                   <span class="hidden-xs"> {{ user.username }}</span>*/
/*                 </a>*/
/*                 <ul class="dropdown-menu">*/
/*                   <!-- The user image in the menu -->*/
/*                   <li class="user-header">*/
/*                     <img src="images/Admin/user2-160x160.jpg" class="img-circle" alt="User Image">*/
/*                     <p>*/
/* 						    <p>{{ user.email }}</p>*/
/* */
/*                       <small>Member since Nov. 2016</small>*/
/*                     </p>*/
/*                   </li>*/
/*                   <!-- Menu Body -->*/
/*                   <li class="user-body">*/
/*                     <div class="col-xs-4 text-center">*/
/*                       <a href="#">Followers</a>*/
/*                     </div>*/
/*                     <div class="col-xs-4 text-center">*/
/*                       <a href="#">Sales</a>*/
/*                     </div>*/
/*                     <div class="col-xs-4 text-center">*/
/*                       <a href="#">Friends</a>*/
/*                     </div>*/
/*                   </li>*/
/*                   <!-- Menu Footer-->*/
/*                   <li class="user-footer">*/
/*                     <div class="pull-left">*/
/*                       <a href="#" class="btn btn-default btn-flat">Profile</a>*/
/*                     </div>*/
/*                     <div class="pull-right">*/
/*                       <a href="#" class="btn btn-default btn-flat">Sign out</a>*/
/*                     </div>*/
/*                   </li>*/
/*                 </ul>*/
/*               </li>*/
