<?php

/* PromotionBundle:Promotion:ListeDesPromotion.html.twig */
class __TwigTemplate_86934ab386f0a233bbf98b17c490d0c2f3b8dbafac2ae2f816b52376d201fbf2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "PromotionBundle:Promotion:ListeDesPromotion.html.twig", 2);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 5
        echo "    <style>
        img {
            width: 50px;
            height: 50px;
        }
    </style>

    <h1>
        Gestion Des Promotions
        <small>Liste Des Promotions</small>
    </h1>
    <center>
        <form  method=\"POST\">
            <input type=\"text\" name=\"search\">
            <input type=\"submit\" value=\"rechercher\">
        </form>
    </center>
";
    }

    // line 24
    public function block_TableView($context, array $blocks = array())
    {
        // line 25
        echo "    <div style=\"padding: 10px\">
        <div class=\"TableView\" >
            <div class=\"box box-success \">    
                <div  class=\"box-body no-padding\">
                    <table class=\"table table-bordered table-hover\" >
                        
                        <tr>
                          
                            <th >Date de début</th>
                            <th>Date de fin</th>
                            <th>Taux de réduction </th>
                            <th> Image</th>
                            <th> Description</th>
                            <th>Etat </th>
                            <th>Ajouter produit</th>
                            <th> Modifier </th>
                            <th> Supprimer </th>
                        </tr>
                        <tr> ";
        // line 43
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["promotions"]) ? $context["promotions"] : $this->getContext($context, "promotions")));
        foreach ($context['_seq'] as $context["_key"] => $context["promotion"]) {
            // line 44
            echo "                          
                            <td>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["promotion"], "DateDebut", array()), "format", array(0 => "j/m/Y"), "method"), "html", null, true);
            echo "</td>
                            <td>";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["promotion"], "DateFin", array()), "format", array(0 => "j/m/Y"), "method"), "html", null, true);
            echo "</td>
                            <td>";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotion"], "Tauxdereduction", array()), "html", null, true);
            echo "</td>
                            <td> <img src=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotion"], "Image", array()), "html", null, true);
            echo "\"/></td>
                            
                            
                 
                            <td>";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotion"], "Description", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotion"], "Etat", array()), "html", null, true);
            echo "</td>
                            <td><center><a href=\"";
            // line 54
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Liste_Produit", array("id" => $this->getAttribute($context["promotion"], "id", array()))), "html", null, true);
            echo "\"><span class=\"fa fa-plus\"></span></center> </td>
                            <td><center><a href=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Afficher_Modifier", array("id" => $this->getAttribute($context["promotion"], "id", array()))), "html", null, true);
            echo "\"><span class=\"fa  fa-edit\"></span></center> </td>
                            <td><center><a href=\"";
            // line 56
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Supprimer_promotion", array("id" => $this->getAttribute($context["promotion"], "id", array()))), "html", null, true);
            echo "\"><span class=\"fa  fa-close\"></span></a></center></td>
                        </tr>

                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['promotion'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "                                    </table>
                                    </div>  </div>         
                                    ";
    }

    // line 63
    public function block_btn($context, array $blocks = array())
    {
        echo "  
                                            <br>   
                                            <div>

                                                <center>  <a href=\"";
        // line 67
        echo $this->env->getExtension('routing')->getPath("promotion_ajouter");
        echo "\"><input type=\"submit\" class=\"btn\" value=\"Ajouter une promotion\"/> 

                                                </center>

                                            </div></div></div>
                                        ";
    }

    public function getTemplateName()
    {
        return "PromotionBundle:Promotion:ListeDesPromotion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 67,  135 => 63,  129 => 60,  119 => 56,  115 => 55,  111 => 54,  107 => 53,  103 => 52,  96 => 48,  92 => 47,  88 => 46,  84 => 45,  81 => 44,  77 => 43,  57 => 25,  54 => 24,  33 => 5,  30 => 4,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* */
/* {% block HeaderTitle %}*/
/*     <style>*/
/*         img {*/
/*             width: 50px;*/
/*             height: 50px;*/
/*         }*/
/*     </style>*/
/* */
/*     <h1>*/
/*         Gestion Des Promotions*/
/*         <small>Liste Des Promotions</small>*/
/*     </h1>*/
/*     <center>*/
/*         <form  method="POST">*/
/*             <input type="text" name="search">*/
/*             <input type="submit" value="rechercher">*/
/*         </form>*/
/*     </center>*/
/* {%endblock %}*/
/* */
/* {% block TableView %}*/
/*     <div style="padding: 10px">*/
/*         <div class="TableView" >*/
/*             <div class="box box-success ">    */
/*                 <div  class="box-body no-padding">*/
/*                     <table class="table table-bordered table-hover" >*/
/*                         */
/*                         <tr>*/
/*                           */
/*                             <th >Date de début</th>*/
/*                             <th>Date de fin</th>*/
/*                             <th>Taux de réduction </th>*/
/*                             <th> Image</th>*/
/*                             <th> Description</th>*/
/*                             <th>Etat </th>*/
/*                             <th>Ajouter produit</th>*/
/*                             <th> Modifier </th>*/
/*                             <th> Supprimer </th>*/
/*                         </tr>*/
/*                         <tr> {% for promotion in promotions %}*/
/*                           */
/*                             <td>{{promotion.DateDebut.format('j/m/Y')}}</td>*/
/*                             <td>{{promotion.DateFin.format('j/m/Y')}}</td>*/
/*                             <td>{{promotion.Tauxdereduction}}</td>*/
/*                             <td> <img src="{{promotion.Image}}"/></td>*/
/*                             */
/*                             */
/*                  */
/*                             <td>{{promotion.Description}}</td>*/
/*                             <td>{{promotion.Etat}}</td>*/
/*                             <td><center><a href="{{path('Liste_Produit',{'id':promotion.id})}}"><span class="fa fa-plus"></span></center> </td>*/
/*                             <td><center><a href="{{path('Afficher_Modifier',{'id':promotion.id})}}"><span class="fa  fa-edit"></span></center> </td>*/
/*                             <td><center><a href="{{path('Supprimer_promotion',{'id':promotion.id})}}"><span class="fa  fa-close"></span></a></center></td>*/
/*                         </tr>*/
/* */
/*                                 {%endfor%}*/
/*                                     </table>*/
/*                                     </div>  </div>         */
/*                                     {% endblock %} */
/*                                         {%block btn %}  */
/*                                             <br>   */
/*                                             <div>*/
/* */
/*                                                 <center>  <a href="{{path('promotion_ajouter')}}"><input type="submit" class="btn" value="Ajouter une promotion"/> */
/* */
/*                                                 </center>*/
/* */
/*                                             </div></div></div>*/
/*                                         {% endblock %}*/
/* */
