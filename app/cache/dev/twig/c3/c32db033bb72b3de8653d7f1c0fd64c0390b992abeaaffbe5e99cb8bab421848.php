<?php

/* AdminBundle::Layout.html.twig */
class __TwigTemplate_5ddae1c683bf781d00fb954505ba1a63461982c9cb3ffbd5535692d19553a434 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!DOCTYPE html>

<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <title>Admin</title>

        <link rel=\"SHORTCUT ICON\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/client/faviconb7bb.ico?_build=1454497437"), "html", null, true);
        echo "\" type=\"image/vnd.microsoft.icon')\" />
        <!-- Tell the browser to be responsive to screen width -->
        <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
        <!-- Bootstrap 3.3.5 -->
        <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/Admin/bootstrap.min.css"), "html", null, true);
        echo "\">
        <!-- Font Awesome -->
        <link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"), "html", null, true);
        echo "\">
        <!-- Ionicons -->
        <link rel=\"stylesheet\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"), "html", null, true);
        echo "\">
        <!-- Theme style -->
        <link rel=\"stylesheet\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/Admin/AdminLTE.min.css"), "html", null, true);
        echo "\">
        <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
              page. However, you can choose any other skin. Make sure you
              apply the skin class to the body tag so the changes take effect.
        -->
        
        <link rel=\"stylesheet\" href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/Admin/skins/_all-skins.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/Admin/skins/TableView.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/formulaire/style-formulaire.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\" />
        <script src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/admin/formulaire.js"), "html", null, true);
        echo "\"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src=\"https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js\"></script>
            <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
        <![endif]-->
    </head>
    <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
    <body class=\"hold-transition skin-blue sidebar-mini\">
        <div class=\"wrapper\">

            <!-- Main Header -->
            <header class=\"main-header\">

                <!-- Logo -->
                <a href=\"admin\" class=\"logo\">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class=\"logo-mini\"><b>T</b>MALL</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class=\"logo-lg\"><b>Tunisia</b>Mall</span>
                </a>

                <!-- Header Navbar -->
                <nav class=\"navbar navbar-static-top\" role=\"navigation\">
                    <!-- Sidebar toggle button-->
                    <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
                        <span class=\"sr-only\">Toggle navigation</span>
                    </a>
                    <!-- Navbar Right Menu -->
                    <div class=\"navbar-custom-menu\">
                        <ul class=\"nav navbar-nav\">
                            <!-- Messages: style can be found in dropdown.less-->
                            <li class=\"dropdown messages-menu\">
                                <!-- Menu toggle button -->
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                                    <i class=\"fa fa-envelope-o\"></i>
                                    <span class=\"label label-success\">4</span>
                                </a>
                                <ul class=\"dropdown-menu\">
                                    <li class=\"header\">You have 4 messages</li>
                                    <li>
                                        <!-- inner menu: contains the messages -->
                                        <ul class=\"menu\">
                                            <li><!-- start message -->
                                                <a href=\"#\">
                                                    <div class=\"pull-left\">
                                                        <!-- User Image -->
                                                        <img src=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Admin/user2-160x160.jpg"), "html", null, true);
        echo "\" class=\"img-circle\" alt=\"User Image\">
                                                    </div>
                                                    <!-- Message title and timestamp -->
                                                    <h4>
                                                        Support Team
                                                        <small><i class=\"fa fa-clock-o\"></i> 5 mins</small>
                                                    </h4>
                                                    <!-- The message -->
                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li><!-- end message -->
                                        </ul><!-- /.menu -->
                                    </li>
                                    <li class=\"footer\"><a href=\"#\">See All Messages</a></li>
                                </ul>
                            </li><!-- /.messages-menu -->

                            <!-- Notifications Menu -->
                            <li class=\"dropdown notifications-menu\">
                                <!-- Menu toggle button -->
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                                    <i class=\"fa fa-bell-o\"></i>
                                    <span class=\"label label-warning\">10</span>
                                </a>
                                <ul class=\"dropdown-menu\">
                                    <li class=\"header\">You have 10 notifications</li>
                                    <li>
                                        <!-- Inner Menu: contains the notifications -->
                                        <ul class=\"menu\">
                                            <li><!-- start notification -->
                                                <a href=\"#\">
                                                    <i class=\"fa fa-users text-aqua\"></i> 5 new members joined today
                                                </a>
                                            </li><!-- end notification -->
                                        </ul>
                                    </li>
                                    <li class=\"footer\"><a href=\"#\">View all</a></li>
                                </ul>
                            </li>
                            <!-- Tasks Menu -->
                            <li class=\"dropdown tasks-menu\">
                                <!-- Menu Toggle Button -->
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                                    <i class=\"fa fa-flag-o\"></i>
                                    <span class=\"label label-danger\">9</span>
                                </a>
                                <ul class=\"dropdown-menu\">
                                    <li class=\"header\">You have 9 tasks</li>
                                    <li>
                                        <!-- Inner menu: contains the tasks -->
                                        <ul class=\"menu\">
                                            <li><!-- Task item -->
                                                <a href=\"#\">
                                                    <!-- Task title and progress text -->
                                                    <h3>
                                                        Design some buttons
                                                        <small class=\"pull-right\">20%</small>
                                                    </h3>
                                                    <!-- The progress bar -->
                                                    <div class=\"progress xs\">
                                                        <!-- Change the css width attribute to simulate progress -->
                                                        <div class=\"progress-bar progress-bar-aqua\" style=\"width: 20%\" role=\"progressbar\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\">
                                                            <span class=\"sr-only\">20% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li><!-- end task item -->
                                        </ul>
                                    </li>
                                    <li class=\"footer\">
                                        <a href=\"#\">View all tasks</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- User Account Menu -->
                            <li class=\"dropdown user user-menu\">
                                <!-- Menu Toggle Button -->
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                                    <!-- The user image in the navbar-->
                                    <img src=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Admin/user2-160x160.jpg"), "html", null, true);
        echo "\" class=\"user-image\" alt=\"User Image\">
                                    <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                    <span class=\"hidden-xs\"> ";
        // line 181
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</span>
                                </a>
                                <ul class=\"dropdown-menu\">
                                    <!-- The user image in the menu -->
                                    <li class=\"user-header\">
                                        <img src=\"images/Admin/user2-160x160.jpg\" class=\"img-circle\" alt=\"User Image\">
                                        <p>
                                        <p>";
        // line 188
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</p>

                                        <small>Member since Nov. 2016</small>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <li class=\"user-body\">
                                        <div class=\"col-xs-4 text-center\">
                                            <a href=\"#\">Followers</a>
                                        </div>
                                        <div class=\"col-xs-4 text-center\">
                                            <a href=\"#\">Sales</a>
                                        </div>
                                        <div class=\"col-xs-4 text-center\">
                                            <a href=\"#\">Friends</a>
                                        </div>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class=\"user-footer\">
                                        <div class=\"pull-left\">
                                            <a href=\"profile\" class=\"btn btn-default btn-flat\">Profile</a>
                                        </div>
                                        <div class=\"pull-right\">
                                            <a href=\"";
        // line 211
        echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
        echo "\" class=\"btn btn-default btn-flat\">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href=\"#\" data-toggle=\"control-sidebar\"><i class=\"fa fa-gears\"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class=\"main-sidebar\">

                <!-- sidebar: style can be found in sidebar.less -->
                <section class=\"sidebar\">

                    <!-- Sidebar user panel (optional) -->
                    <div class=\"user-panel\">
                        <div class=\"pull-left image\">
                            <img src=\"";
        // line 233
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Admin/user2-160x160.jpg"), "html", null, true);
        echo "\" class=\"img-circle\" alt=\"User Image\">
                        </div>
                        <div class=\"pull-left info\">
                            ";
        // line 236
        if (($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "flagRole", array()) == 1)) {
            // line 237
            echo "                                <p>Administrateur</p>
                            ";
        } else {
            // line 239
            echo "                                <p>Responsable Enseigne</p>  
                            ";
        }
        // line 241
        echo "                            <!-- Status -->
                            <a href=\"#\"><i class=\"fa fa-circle text-success\"></i>En ligne</a>
                        </div>
                    </div>

                    <!-- search form (Optional) -->
                    <form action=\"#\" method=\"get\" class=\"sidebar-form\">
                        <div class=\"input-group\">
                            <input type=\"text\" name=\"q\" class=\"form-control\" placeholder=\"Search...\">
                            <span class=\"input-group-btn\">
                                <button type=\"submit\" name=\"search\" id=\"search-btn\" class=\"btn btn-success\"><i class=\"fa fa-search\"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->

                    <!-- Sidebar Menu -->
                    <ul class=\"sidebar-menu\">

                        <!-- Optionally, you can add icons to the links -->
                        <li><a href=\"ListActualites\"><i class=\"fa fa-newspaper-o\"></i> <span>Gestion Actualités</span></a></li>
                        <li><a href=\"rechercherEnseigne\"><i class=\"fa fa-book\"></i> <span>Gestion des Catalogues</span></a></li>

                        ";
        // line 264
        if (($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "flagRole", array()) == 1)) {
            // line 265
            echo "                            <li><a href=\"#\"><i class=\"fa fa-users\"></i> <span>Gestion des Comptes</span><i class=\"fa fa-angle-right pull-right\"></i></a><!--dans la page qu'il suit on met <li class=\"active\">-->
                                <ul class=\"treeview-menu\">
                                    <li><a href=\"";
            // line 267
            echo $this->env->getExtension('routing')->getPath("tunisia_mall_responsable_GestionComptes");
            echo "\">Comptes Clients<small class=\"label pull-right bg-yellow\">";
            echo twig_escape_filter($this->env, (isset($context["clientsCount"]) ? $context["clientsCount"] : $this->getContext($context, "clientsCount")), "html", null, true);
            echo "</small></a></li>
                                    <li><a href=\"";
            // line 268
            echo $this->env->getExtension('routing')->getPath("tunisia_mall_responsable_GestionComptesEnseigne");
            echo "\">Comptes d'Enseignes<small class=\"label pull-right bg-red\">";
            echo twig_escape_filter($this->env, (isset($context["responsablesCount"]) ? $context["responsablesCount"] : $this->getContext($context, "responsablesCount")), "html", null, true);
            echo "</small></a></li>
                                </ul>
                            </li>
                             <li><a href=\"afficherPack\"><i class=\"fa fa-bell-o\"></i> <span>Gestion de pack publicitaire</span>     <i class=\"fa fa-angle-right pull-right\"></i></a>
                                <ul class=\"treeview-menu\">
                                    <li><a href=\"ListPack\">Liste des packs publicitaires</a></li>
                                    <li><a href=\"ListeDemandeReservation\">Liste des demandes de reservation</a></li>
                                </ul></a></li><li><a href=\"#\"><i class=\"fa fa-bookmark-o\"></i> <span>Gestion des reclamations</span></a></li>
                            ";
        }
        // line 277
        echo "                        ";
        if (($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "flagRole", array()) == 2)) {
            // line 278
            echo "                            <li><a href=\"Cartes\"><i class=\"fa fa-credit-card\"></i> <span>Gestion des Carte Fidélité</span></a></li>
                              <li><a href=\"rechercherPromotion\"><i class=\"fa fa-file-text-o\"></i> <span>Gestion des Promotions</span></a>
                                <ul class=\"treeview-menu\">
                                    <li><a href=\"rechercherPromotion\">Liste des promotions</a></li>
                                    <li><a href=\"";
            // line 282
            echo $this->env->getExtension('routing')->getPath("promotion_ajouter");
            echo "\">Ajouter une promotion</a></li>
                                </ul> 
                            </li>
                            <li><a href=\"ListeReservationPack\"><i class=\"fa fa-photo\"></i> <span>Gestion des packs publicitaire</span></a>
                            <ul class=\"treeview-menu\">
                                <li><a href=\"";
            // line 287
            echo $this->env->getExtension('routing')->getPath("reservation_pack_pub_Liste");
            echo "\">Liste des pack disponible</a></li>
                                <li><a href=\"";
            // line 288
            echo $this->env->getExtension('routing')->getPath("ReservationAPayer");
            echo "\">Liste des réservations</a></li>
                            </ul>
                            </li>
                            <li><a href=\"rechercherEnseigne\"><i class=\"fa fa-building-o\"></i> <span>Gestion des Enseigne</span></a></li>
                           
                            <li><a href=\"ListProduit\"><i class=\"fa fa-database\"></i> <span>Gestion de Produit</span></a></li>
                            <li><a href=\"ListStock\"><i class=\"fa fa-database\"></i> <span>Gestion de stock</span></a></li>
                            <li><a href=\"#\"><i class=\"fa fa-bookmark-o\"></i> <span>Gestion des suggestions</span></a></li>
                            ";
        }
        // line 297
        echo "                        <li><a href=\"statistique\"><i class=\"fa fa-line-chart\"></i> <span>Statistiques</span></a></li>
                    </ul><!-- /.sidebar-menu -->
                </section>
                <!-- /.sidebar -->
            </aside>


            <!-- Content Wrapper. Contains page content -->

            <div class=\"content-wrapper\">
                ";
        // line 307
        $this->displayBlock('contenu', $context, $blocks);
        // line 321
        echo " 
                ";
        // line 322
        $this->displayBlock('HeaderTitle', $context, $blocks);
        // line 326
        echo "                ";
        $this->displayBlock('TableView', $context, $blocks);
        // line 330
        echo "

                ";
        // line 332
        $this->displayBlock('btn', $context, $blocks);
        // line 335
        echo "                <!-- Main content -->
                <section class=\"content\">

                    <!-- Your Page Content Here -->

                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->

            <!-- Main Footer -->
            <footer class=\"main-footer\">
                <!-- To the right -->
                <div class=\"pull-right hidden-xs\">
                    Anything you want
                </div>
                <!-- Default to the left -->
                <strong>Copyright &copy; 2015 <a href=\"#\">Company</a>.</strong> All rights reserved.
            </footer>

            <!-- Control Sidebar -->
            <aside class=\"control-sidebar control-sidebar-dark\">
                <!-- Create the tabs -->
                <ul class=\"nav nav-tabs nav-justified control-sidebar-tabs\">
                    <li class=\"active\"><a href=\"";
        // line 357
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#control-sidebar-home-tab"), "html", null, true);
        echo "\" data-toggle=\"tab\"><i class=\"fa fa-home\"></i></a></li>
                    <li><a href=\"";
        // line 358
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#control-sidebar-settings-tab"), "html", null, true);
        echo "\" data-toggle=\"tab\"><i class=\"fa fa-gears\"></i></a></li>
                </ul>
                <!-- Tab panes -->
                <div class=\"tab-content\">
                    <!-- Home tab content -->
                    <div class=\"tab-pane active\" id=\"control-sidebar-home-tab\">
                        <h3 class=\"control-sidebar-heading\">Recent Activity</h3>
                        <ul class=\"control-sidebar-menu\">
                            <li>
                                <a href=\"javascript::;\">
                                    <i class=\"menu-icon fa fa-birthday-cake bg-red\"></i>
                                    <div class=\"menu-info\">
                                        <h4 class=\"control-sidebar-subheading\">Langdon's Birthday</h4>
                                        <p>Will be 23 on April 24th</p>
                                    </div>
                                </a>
                            </li>
                        </ul><!-- /.control-sidebar-menu -->

                        <h3 class=\"control-sidebar-heading\">Tasks Progress</h3>
                        <ul class=\"control-sidebar-menu\">
                            <li>
                                <a href=\"javascript::;\">
                                    <h4 class=\"control-sidebar-subheading\">
                                        Custom Template Design
                                        <span class=\"label label-danger pull-right\">70%</span>
                                    </h4>
                                    <div class=\"progress progress-xxs\">
                                        <div class=\"progress-bar progress-bar-danger\" style=\"width: 70%\"></div>
                                    </div>
                                </a>
                            </li>
                        </ul><!-- /.control-sidebar-menu -->

                    </div><!-- /.tab-pane -->
                    <!-- Stats tab content -->
                    <div class=\"tab-pane\" id=\"control-sidebar-stats-tab\">Stats Tab Content</div><!-- /.tab-pane -->
                    <!-- Settings tab content -->
                    <div class=\"tab-pane\" id=\"control-sidebar-settings-tab\">
                        <form method=\"post\">
                            <h3 class=\"control-sidebar-heading\">General Settings</h3>
                            <div class=\"form-group\">
                                <label class=\"control-sidebar-subheading\">
                                    Report panel usage
                                    <input type=\"checkbox\" class=\"pull-right\" checked>
                                </label>
                                <p>
                                    Some information about this general settings option
                                </p>
                            </div><!-- /.form-group -->
                        </form>
                    </div><!-- /.tab-pane -->
                </div>
            </aside><!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class=\"control-sidebar-bg\"></div>
        </div><!-- ./wrapper -->

        <!-- REQUIRED JS SCRIPTS -->

        <!-- jQuery 2.1.4 -->
        <script src=\"";
        // line 420
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Admin/jQuery/jQuery-2.1.4.min.js"), "html", null, true);
        echo "\"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src=\"";
        // line 422
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Admin/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        <!-- AdminLTE App -->
        <script src=\"";
        // line 424
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Admin/app.min.js"), "html", null, true);
        echo "\"></script>

        <!-- Optionally, you can add Slimscroll and FastClick plugins.
             Both of these plugins are recommended to enhance the
             user experience. Slimscroll is required when using the
             fixed layout. -->
    </body>
</html>
";
    }

    // line 307
    public function block_contenu($context, array $blocks = array())
    {
        echo "  
                    <!-- Content Header (Page header) -->
                    <section class=\"content-header\">

                        <h1>


                        </h1>
                        <ol class=\"breadcrumb\">
                            <li><a href=\"#\"><i class=\"fa fa-dashboard\"></i> Level</a></li>
                            <li class=\"active\">Here</li>
                        </ol>

                    </section>
                ";
    }

    // line 322
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 323
        echo "

                ";
    }

    // line 326
    public function block_TableView($context, array $blocks = array())
    {
        // line 327
        echo "
                    <!-- Your Page Content Here -->
                ";
    }

    // line 332
    public function block_btn($context, array $blocks = array())
    {
        // line 333
        echo "
                ";
    }

    public function getTemplateName()
    {
        return "AdminBundle::Layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  584 => 333,  581 => 332,  575 => 327,  572 => 326,  566 => 323,  563 => 322,  543 => 307,  530 => 424,  525 => 422,  520 => 420,  455 => 358,  451 => 357,  427 => 335,  425 => 332,  421 => 330,  418 => 326,  416 => 322,  413 => 321,  411 => 307,  399 => 297,  387 => 288,  383 => 287,  375 => 282,  369 => 278,  366 => 277,  352 => 268,  346 => 267,  342 => 265,  340 => 264,  315 => 241,  311 => 239,  307 => 237,  305 => 236,  299 => 233,  274 => 211,  248 => 188,  238 => 181,  233 => 179,  151 => 100,  81 => 33,  77 => 32,  73 => 31,  69 => 30,  60 => 24,  55 => 22,  50 => 20,  45 => 18,  38 => 14,  23 => 1,);
    }
}
/* */
/* <!DOCTYPE html>*/
/* */
/* <!--*/
/* This is a starter template page. Use this page to start your new project from*/
/* scratch. This page gets rid of all links and provides the needed markup only.*/
/* -->*/
/* <html>*/
/*     <head>*/
/*         <meta charset="utf-8">*/
/*         <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*         <title>Admin</title>*/
/* */
/*         <link rel="SHORTCUT ICON" href="{{asset('images/client/faviconb7bb.ico?_build=1454497437')}}" type="image/vnd.microsoft.icon')" />*/
/*         <!-- Tell the browser to be responsive to screen width -->*/
/*         <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">*/
/*         <!-- Bootstrap 3.3.5 -->*/
/*         <link rel="stylesheet" href="{{asset('css/Admin/bootstrap.min.css')}}">*/
/*         <!-- Font Awesome -->*/
/*         <link rel="stylesheet" href="{{asset('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css')}}">*/
/*         <!-- Ionicons -->*/
/*         <link rel="stylesheet" href="{{asset('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}">*/
/*         <!-- Theme style -->*/
/*         <link rel="stylesheet" href="{{asset('css/Admin/AdminLTE.min.css')}}">*/
/*         <!-- AdminLTE Skins. We have chosen the skin-blue for this starter*/
/*               page. However, you can choose any other skin. Make sure you*/
/*               apply the skin class to the body tag so the changes take effect.*/
/*         -->*/
/*         */
/*         <link rel="stylesheet" href="{{asset('css/Admin/skins/_all-skins.min.css')}}">*/
/*         <link rel="stylesheet" href="{{asset('css/Admin/skins/TableView.css')}}">*/
/*         <link rel="stylesheet" href="{{asset('css/formulaire/style-formulaire.css')}}" type="text/css" media="all" />*/
/*         <script src="{{asset('js/admin/formulaire.js')}}"></script>*/
/*         <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->*/
/*         <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->*/
/*         <!--[if lt IE 9]>*/
/*             <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>*/
/*             <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>*/
/*         <![endif]-->*/
/*     </head>*/
/*     <!--*/
/*     BODY TAG OPTIONS:*/
/*     =================*/
/*     Apply one or more of the following classes to get the*/
/*     desired effect*/
/*     |---------------------------------------------------------|*/
/*     | SKINS         | skin-blue                               |*/
/*     |               | skin-black                              |*/
/*     |               | skin-purple                             |*/
/*     |               | skin-yellow                             |*/
/*     |               | skin-red                                |*/
/*     |               | skin-green                              |*/
/*     |---------------------------------------------------------|*/
/*     |LAYOUT OPTIONS | fixed                                   |*/
/*     |               | layout-boxed                            |*/
/*     |               | layout-top-nav                          |*/
/*     |               | sidebar-collapse                        |*/
/*     |               | sidebar-mini                            |*/
/*     |---------------------------------------------------------|*/
/*     -->*/
/*     <body class="hold-transition skin-blue sidebar-mini">*/
/*         <div class="wrapper">*/
/* */
/*             <!-- Main Header -->*/
/*             <header class="main-header">*/
/* */
/*                 <!-- Logo -->*/
/*                 <a href="admin" class="logo">*/
/*                     <!-- mini logo for sidebar mini 50x50 pixels -->*/
/*                     <span class="logo-mini"><b>T</b>MALL</span>*/
/*                     <!-- logo for regular state and mobile devices -->*/
/*                     <span class="logo-lg"><b>Tunisia</b>Mall</span>*/
/*                 </a>*/
/* */
/*                 <!-- Header Navbar -->*/
/*                 <nav class="navbar navbar-static-top" role="navigation">*/
/*                     <!-- Sidebar toggle button-->*/
/*                     <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">*/
/*                         <span class="sr-only">Toggle navigation</span>*/
/*                     </a>*/
/*                     <!-- Navbar Right Menu -->*/
/*                     <div class="navbar-custom-menu">*/
/*                         <ul class="nav navbar-nav">*/
/*                             <!-- Messages: style can be found in dropdown.less-->*/
/*                             <li class="dropdown messages-menu">*/
/*                                 <!-- Menu toggle button -->*/
/*                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">*/
/*                                     <i class="fa fa-envelope-o"></i>*/
/*                                     <span class="label label-success">4</span>*/
/*                                 </a>*/
/*                                 <ul class="dropdown-menu">*/
/*                                     <li class="header">You have 4 messages</li>*/
/*                                     <li>*/
/*                                         <!-- inner menu: contains the messages -->*/
/*                                         <ul class="menu">*/
/*                                             <li><!-- start message -->*/
/*                                                 <a href="#">*/
/*                                                     <div class="pull-left">*/
/*                                                         <!-- User Image -->*/
/*                                                         <img src="{{asset('images/Admin/user2-160x160.jpg')}}" class="img-circle" alt="User Image">*/
/*                                                     </div>*/
/*                                                     <!-- Message title and timestamp -->*/
/*                                                     <h4>*/
/*                                                         Support Team*/
/*                                                         <small><i class="fa fa-clock-o"></i> 5 mins</small>*/
/*                                                     </h4>*/
/*                                                     <!-- The message -->*/
/*                                                     <p>Why not buy a new awesome theme?</p>*/
/*                                                 </a>*/
/*                                             </li><!-- end message -->*/
/*                                         </ul><!-- /.menu -->*/
/*                                     </li>*/
/*                                     <li class="footer"><a href="#">See All Messages</a></li>*/
/*                                 </ul>*/
/*                             </li><!-- /.messages-menu -->*/
/* */
/*                             <!-- Notifications Menu -->*/
/*                             <li class="dropdown notifications-menu">*/
/*                                 <!-- Menu toggle button -->*/
/*                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">*/
/*                                     <i class="fa fa-bell-o"></i>*/
/*                                     <span class="label label-warning">10</span>*/
/*                                 </a>*/
/*                                 <ul class="dropdown-menu">*/
/*                                     <li class="header">You have 10 notifications</li>*/
/*                                     <li>*/
/*                                         <!-- Inner Menu: contains the notifications -->*/
/*                                         <ul class="menu">*/
/*                                             <li><!-- start notification -->*/
/*                                                 <a href="#">*/
/*                                                     <i class="fa fa-users text-aqua"></i> 5 new members joined today*/
/*                                                 </a>*/
/*                                             </li><!-- end notification -->*/
/*                                         </ul>*/
/*                                     </li>*/
/*                                     <li class="footer"><a href="#">View all</a></li>*/
/*                                 </ul>*/
/*                             </li>*/
/*                             <!-- Tasks Menu -->*/
/*                             <li class="dropdown tasks-menu">*/
/*                                 <!-- Menu Toggle Button -->*/
/*                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">*/
/*                                     <i class="fa fa-flag-o"></i>*/
/*                                     <span class="label label-danger">9</span>*/
/*                                 </a>*/
/*                                 <ul class="dropdown-menu">*/
/*                                     <li class="header">You have 9 tasks</li>*/
/*                                     <li>*/
/*                                         <!-- Inner menu: contains the tasks -->*/
/*                                         <ul class="menu">*/
/*                                             <li><!-- Task item -->*/
/*                                                 <a href="#">*/
/*                                                     <!-- Task title and progress text -->*/
/*                                                     <h3>*/
/*                                                         Design some buttons*/
/*                                                         <small class="pull-right">20%</small>*/
/*                                                     </h3>*/
/*                                                     <!-- The progress bar -->*/
/*                                                     <div class="progress xs">*/
/*                                                         <!-- Change the css width attribute to simulate progress -->*/
/*                                                         <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">*/
/*                                                             <span class="sr-only">20% Complete</span>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 </a>*/
/*                                             </li><!-- end task item -->*/
/*                                         </ul>*/
/*                                     </li>*/
/*                                     <li class="footer">*/
/*                                         <a href="#">View all tasks</a>*/
/*                                     </li>*/
/*                                 </ul>*/
/*                             </li>*/
/*                             <!-- User Account Menu -->*/
/*                             <li class="dropdown user user-menu">*/
/*                                 <!-- Menu Toggle Button -->*/
/*                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">*/
/*                                     <!-- The user image in the navbar-->*/
/*                                     <img src="{{asset('images/Admin/user2-160x160.jpg')}}" class="user-image" alt="User Image">*/
/*                                     <!-- hidden-xs hides the username on small devices so only the image appears. -->*/
/*                                     <span class="hidden-xs"> {{ user.username }}</span>*/
/*                                 </a>*/
/*                                 <ul class="dropdown-menu">*/
/*                                     <!-- The user image in the menu -->*/
/*                                     <li class="user-header">*/
/*                                         <img src="images/Admin/user2-160x160.jpg" class="img-circle" alt="User Image">*/
/*                                         <p>*/
/*                                         <p>{{ user.email }}</p>*/
/* */
/*                                         <small>Member since Nov. 2016</small>*/
/*                                         </p>*/
/*                                     </li>*/
/*                                     <!-- Menu Body -->*/
/*                                     <li class="user-body">*/
/*                                         <div class="col-xs-4 text-center">*/
/*                                             <a href="#">Followers</a>*/
/*                                         </div>*/
/*                                         <div class="col-xs-4 text-center">*/
/*                                             <a href="#">Sales</a>*/
/*                                         </div>*/
/*                                         <div class="col-xs-4 text-center">*/
/*                                             <a href="#">Friends</a>*/
/*                                         </div>*/
/*                                     </li>*/
/*                                     <!-- Menu Footer-->*/
/*                                     <li class="user-footer">*/
/*                                         <div class="pull-left">*/
/*                                             <a href="profile" class="btn btn-default btn-flat">Profile</a>*/
/*                                         </div>*/
/*                                         <div class="pull-right">*/
/*                                             <a href="{{ path('fos_user_security_logout') }}" class="btn btn-default btn-flat">Sign out</a>*/
/*                                         </div>*/
/*                                     </li>*/
/*                                 </ul>*/
/*                             </li>*/
/*                             <!-- Control Sidebar Toggle Button -->*/
/*                             <li>*/
/*                                 <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>*/
/*                             </li>*/
/*                         </ul>*/
/*                     </div>*/
/*                 </nav>*/
/*             </header>*/
/*             <!-- Left side column. contains the logo and sidebar -->*/
/*             <aside class="main-sidebar">*/
/* */
/*                 <!-- sidebar: style can be found in sidebar.less -->*/
/*                 <section class="sidebar">*/
/* */
/*                     <!-- Sidebar user panel (optional) -->*/
/*                     <div class="user-panel">*/
/*                         <div class="pull-left image">*/
/*                             <img src="{{asset('images/Admin/user2-160x160.jpg')}}" class="img-circle" alt="User Image">*/
/*                         </div>*/
/*                         <div class="pull-left info">*/
/*                             {% if user.flagRole==1 %}*/
/*                                 <p>Administrateur</p>*/
/*                             {% else %}*/
/*                                 <p>Responsable Enseigne</p>  */
/*                             {% endif %}*/
/*                             <!-- Status -->*/
/*                             <a href="#"><i class="fa fa-circle text-success"></i>En ligne</a>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <!-- search form (Optional) -->*/
/*                     <form action="#" method="get" class="sidebar-form">*/
/*                         <div class="input-group">*/
/*                             <input type="text" name="q" class="form-control" placeholder="Search...">*/
/*                             <span class="input-group-btn">*/
/*                                 <button type="submit" name="search" id="search-btn" class="btn btn-success"><i class="fa fa-search"></i></button>*/
/*                             </span>*/
/*                         </div>*/
/*                     </form>*/
/*                     <!-- /.search form -->*/
/* */
/*                     <!-- Sidebar Menu -->*/
/*                     <ul class="sidebar-menu">*/
/* */
/*                         <!-- Optionally, you can add icons to the links -->*/
/*                         <li><a href="ListActualites"><i class="fa fa-newspaper-o"></i> <span>Gestion Actualités</span></a></li>*/
/*                         <li><a href="rechercherEnseigne"><i class="fa fa-book"></i> <span>Gestion des Catalogues</span></a></li>*/
/* */
/*                         {%if user.flagRole==1%}*/
/*                             <li><a href="#"><i class="fa fa-users"></i> <span>Gestion des Comptes</span><i class="fa fa-angle-right pull-right"></i></a><!--dans la page qu'il suit on met <li class="active">-->*/
/*                                 <ul class="treeview-menu">*/
/*                                     <li><a href="{{path('tunisia_mall_responsable_GestionComptes')}}">Comptes Clients<small class="label pull-right bg-yellow">{{clientsCount}}</small></a></li>*/
/*                                     <li><a href="{{path('tunisia_mall_responsable_GestionComptesEnseigne')}}">Comptes d'Enseignes<small class="label pull-right bg-red">{{responsablesCount}}</small></a></li>*/
/*                                 </ul>*/
/*                             </li>*/
/*                              <li><a href="afficherPack"><i class="fa fa-bell-o"></i> <span>Gestion de pack publicitaire</span>     <i class="fa fa-angle-right pull-right"></i></a>*/
/*                                 <ul class="treeview-menu">*/
/*                                     <li><a href="ListPack">Liste des packs publicitaires</a></li>*/
/*                                     <li><a href="ListeDemandeReservation">Liste des demandes de reservation</a></li>*/
/*                                 </ul></a></li><li><a href="#"><i class="fa fa-bookmark-o"></i> <span>Gestion des reclamations</span></a></li>*/
/*                             {% endif %}*/
/*                         {%if user.flagRole==2%}*/
/*                             <li><a href="Cartes"><i class="fa fa-credit-card"></i> <span>Gestion des Carte Fidélité</span></a></li>*/
/*                               <li><a href="rechercherPromotion"><i class="fa fa-file-text-o"></i> <span>Gestion des Promotions</span></a>*/
/*                                 <ul class="treeview-menu">*/
/*                                     <li><a href="rechercherPromotion">Liste des promotions</a></li>*/
/*                                     <li><a href="{{path('promotion_ajouter')}}">Ajouter une promotion</a></li>*/
/*                                 </ul> */
/*                             </li>*/
/*                             <li><a href="ListeReservationPack"><i class="fa fa-photo"></i> <span>Gestion des packs publicitaire</span></a>*/
/*                             <ul class="treeview-menu">*/
/*                                 <li><a href="{{path('reservation_pack_pub_Liste')}}">Liste des pack disponible</a></li>*/
/*                                 <li><a href="{{path('ReservationAPayer')}}">Liste des réservations</a></li>*/
/*                             </ul>*/
/*                             </li>*/
/*                             <li><a href="rechercherEnseigne"><i class="fa fa-building-o"></i> <span>Gestion des Enseigne</span></a></li>*/
/*                            */
/*                             <li><a href="ListProduit"><i class="fa fa-database"></i> <span>Gestion de Produit</span></a></li>*/
/*                             <li><a href="ListStock"><i class="fa fa-database"></i> <span>Gestion de stock</span></a></li>*/
/*                             <li><a href="#"><i class="fa fa-bookmark-o"></i> <span>Gestion des suggestions</span></a></li>*/
/*                             {% endif %}*/
/*                         <li><a href="statistique"><i class="fa fa-line-chart"></i> <span>Statistiques</span></a></li>*/
/*                     </ul><!-- /.sidebar-menu -->*/
/*                 </section>*/
/*                 <!-- /.sidebar -->*/
/*             </aside>*/
/* */
/* */
/*             <!-- Content Wrapper. Contains page content -->*/
/* */
/*             <div class="content-wrapper">*/
/*                 {% block contenu %}  */
/*                     <!-- Content Header (Page header) -->*/
/*                     <section class="content-header">*/
/* */
/*                         <h1>*/
/* */
/* */
/*                         </h1>*/
/*                         <ol class="breadcrumb">*/
/*                             <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>*/
/*                             <li class="active">Here</li>*/
/*                         </ol>*/
/* */
/*                     </section>*/
/*                 {% endblock %} */
/*                 {% block HeaderTitle %}*/
/* */
/* */
/*                 {%endblock %}*/
/*                 {% block TableView %}*/
/* */
/*                     <!-- Your Page Content Here -->*/
/*                 {% endblock TableView %}*/
/* */
/* */
/*                 {% block btn %}*/
/* */
/*                 {% endblock %}*/
/*                 <!-- Main content -->*/
/*                 <section class="content">*/
/* */
/*                     <!-- Your Page Content Here -->*/
/* */
/*                 </section><!-- /.content -->*/
/*             </div><!-- /.content-wrapper -->*/
/* */
/*             <!-- Main Footer -->*/
/*             <footer class="main-footer">*/
/*                 <!-- To the right -->*/
/*                 <div class="pull-right hidden-xs">*/
/*                     Anything you want*/
/*                 </div>*/
/*                 <!-- Default to the left -->*/
/*                 <strong>Copyright &copy; 2015 <a href="#">Company</a>.</strong> All rights reserved.*/
/*             </footer>*/
/* */
/*             <!-- Control Sidebar -->*/
/*             <aside class="control-sidebar control-sidebar-dark">*/
/*                 <!-- Create the tabs -->*/
/*                 <ul class="nav nav-tabs nav-justified control-sidebar-tabs">*/
/*                     <li class="active"><a href="{{asset('#control-sidebar-home-tab')}}" data-toggle="tab"><i class="fa fa-home"></i></a></li>*/
/*                     <li><a href="{{asset('#control-sidebar-settings-tab')}}" data-toggle="tab"><i class="fa fa-gears"></i></a></li>*/
/*                 </ul>*/
/*                 <!-- Tab panes -->*/
/*                 <div class="tab-content">*/
/*                     <!-- Home tab content -->*/
/*                     <div class="tab-pane active" id="control-sidebar-home-tab">*/
/*                         <h3 class="control-sidebar-heading">Recent Activity</h3>*/
/*                         <ul class="control-sidebar-menu">*/
/*                             <li>*/
/*                                 <a href="javascript::;">*/
/*                                     <i class="menu-icon fa fa-birthday-cake bg-red"></i>*/
/*                                     <div class="menu-info">*/
/*                                         <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>*/
/*                                         <p>Will be 23 on April 24th</p>*/
/*                                     </div>*/
/*                                 </a>*/
/*                             </li>*/
/*                         </ul><!-- /.control-sidebar-menu -->*/
/* */
/*                         <h3 class="control-sidebar-heading">Tasks Progress</h3>*/
/*                         <ul class="control-sidebar-menu">*/
/*                             <li>*/
/*                                 <a href="javascript::;">*/
/*                                     <h4 class="control-sidebar-subheading">*/
/*                                         Custom Template Design*/
/*                                         <span class="label label-danger pull-right">70%</span>*/
/*                                     </h4>*/
/*                                     <div class="progress progress-xxs">*/
/*                                         <div class="progress-bar progress-bar-danger" style="width: 70%"></div>*/
/*                                     </div>*/
/*                                 </a>*/
/*                             </li>*/
/*                         </ul><!-- /.control-sidebar-menu -->*/
/* */
/*                     </div><!-- /.tab-pane -->*/
/*                     <!-- Stats tab content -->*/
/*                     <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->*/
/*                     <!-- Settings tab content -->*/
/*                     <div class="tab-pane" id="control-sidebar-settings-tab">*/
/*                         <form method="post">*/
/*                             <h3 class="control-sidebar-heading">General Settings</h3>*/
/*                             <div class="form-group">*/
/*                                 <label class="control-sidebar-subheading">*/
/*                                     Report panel usage*/
/*                                     <input type="checkbox" class="pull-right" checked>*/
/*                                 </label>*/
/*                                 <p>*/
/*                                     Some information about this general settings option*/
/*                                 </p>*/
/*                             </div><!-- /.form-group -->*/
/*                         </form>*/
/*                     </div><!-- /.tab-pane -->*/
/*                 </div>*/
/*             </aside><!-- /.control-sidebar -->*/
/*             <!-- Add the sidebar's background. This div must be placed*/
/*                  immediately after the control sidebar -->*/
/*             <div class="control-sidebar-bg"></div>*/
/*         </div><!-- ./wrapper -->*/
/* */
/*         <!-- REQUIRED JS SCRIPTS -->*/
/* */
/*         <!-- jQuery 2.1.4 -->*/
/*         <script src="{{asset('js/Admin/jQuery/jQuery-2.1.4.min.js')}}"></script>*/
/*         <!-- Bootstrap 3.3.5 -->*/
/*         <script src="{{asset('js/Admin/bootstrap.min.js')}}"></script>*/
/*         <!-- AdminLTE App -->*/
/*         <script src="{{asset('js/Admin/app.min.js')}}"></script>*/
/* */
/*         <!-- Optionally, you can add Slimscroll and FastClick plugins.*/
/*              Both of these plugins are recommended to enhance the*/
/*              user experience. Slimscroll is required when using the*/
/*              fixed layout. -->*/
/*     </body>*/
/* </html>*/
/* */
