<?php

/* AdminBundle:Default:formulaire.html.twig */
class __TwigTemplate_89e1b37c8c416b5b0a76cc317ff5f4f732c530eda6598dc4320f7edf466dc520 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "AdminBundle:Default:formulaire.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        // line 4
        echo "   
           <div class=\"col-md-6\">
              <!-- Horizontal Form -->
              <div class=\"box box-info\">
                <div class=\"box-header with-border\">
                  <h3 class=\"box-title\">Ajouter une promotion</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class=\"form-horizontal\" name=\"form\" onsubmit=\"verif()\">
                  <div class=\"box-body\">
                      
                    <div class=\"form-group\">
                      <label for=\"date1\" class=\"col-sm-2 control-label\">Date début </label>
                      <div class=\"col-sm-10\">
                        <input name=\"date1\" type=\"date\" class=\"form-control\" id=\"date1\">
                      </div>
                    </div>
                      
                    <div class=\"form-group\">
                      <label for=\"date2\" class=\"col-sm-2 control-label\">Date fin </label>
                      <div class=\"col-sm-10\">
                        <input name=\"date2\" type=\"date\" class=\"form-control\" id=\"date2\">
                      </div>
                    </div>
                      
                    <div class=\"form-group\">
                      <label for=\"taux\" class=\"col-sm-2 control-label\">Taux de réduction</label>
                      <div class=\"col-sm-10\">
                        <input type=\"password\" class=\"form-control\" id=\"inputPassword3\" placeholder=\"Password\">
                      </div>
                    </div>
                      
                    <div class=\"form-group\">
                      <label for=\"inputPassword3\" class=\"col-sm-2 control-label\">texte</label>
                      <div class=\"col-sm-10\">
                        <textarea rows=\"5\" cols=\"50\" class=\"form-control\" id=\"text1\">Voici mon projet d'option informatique</textarea>
                      </div>
                    </div>
                      
                    <div class=\"form-group\">
                      <div class=\"col-sm-offset-2 col-sm-10\">
                        <div class=\"checkbox\">
                          <label>
                            <input type=\"checkbox\"> Remember me
                          </label>
                        </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->
                  <div class=\"box-footer\">
                    <button type=\"submit\" class=\"btn btn-default\">Cancel</button>
                    <button type=\"submit\" class=\"btn btn-info pull-right\">Sign in</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
           </div>
    ";
    }

    public function getTemplateName()
    {
        return "AdminBundle:Default:formulaire.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block contenu %}*/
/*    */
/*            <div class="col-md-6">*/
/*               <!-- Horizontal Form -->*/
/*               <div class="box box-info">*/
/*                 <div class="box-header with-border">*/
/*                   <h3 class="box-title">Ajouter une promotion</h3>*/
/*                 </div><!-- /.box-header -->*/
/*                 <!-- form start -->*/
/*                 <form class="form-horizontal" name="form" onsubmit="verif()">*/
/*                   <div class="box-body">*/
/*                       */
/*                     <div class="form-group">*/
/*                       <label for="date1" class="col-sm-2 control-label">Date début </label>*/
/*                       <div class="col-sm-10">*/
/*                         <input name="date1" type="date" class="form-control" id="date1">*/
/*                       </div>*/
/*                     </div>*/
/*                       */
/*                     <div class="form-group">*/
/*                       <label for="date2" class="col-sm-2 control-label">Date fin </label>*/
/*                       <div class="col-sm-10">*/
/*                         <input name="date2" type="date" class="form-control" id="date2">*/
/*                       </div>*/
/*                     </div>*/
/*                       */
/*                     <div class="form-group">*/
/*                       <label for="taux" class="col-sm-2 control-label">Taux de réduction</label>*/
/*                       <div class="col-sm-10">*/
/*                         <input type="password" class="form-control" id="inputPassword3" placeholder="Password">*/
/*                       </div>*/
/*                     </div>*/
/*                       */
/*                     <div class="form-group">*/
/*                       <label for="inputPassword3" class="col-sm-2 control-label">texte</label>*/
/*                       <div class="col-sm-10">*/
/*                         <textarea rows="5" cols="50" class="form-control" id="text1">Voici mon projet d'option informatique</textarea>*/
/*                       </div>*/
/*                     </div>*/
/*                       */
/*                     <div class="form-group">*/
/*                       <div class="col-sm-offset-2 col-sm-10">*/
/*                         <div class="checkbox">*/
/*                           <label>*/
/*                             <input type="checkbox"> Remember me*/
/*                           </label>*/
/*                         </div>*/
/*                       </div>*/
/*                     </div>*/
/*                   </div><!-- /.box-body -->*/
/*                   <div class="box-footer">*/
/*                     <button type="submit" class="btn btn-default">Cancel</button>*/
/*                     <button type="submit" class="btn btn-info pull-right">Sign in</button>*/
/*                   </div><!-- /.box-footer -->*/
/*                 </form>*/
/*               </div><!-- /.box -->*/
/*            </div>*/
/*     {% endblock %}*/
