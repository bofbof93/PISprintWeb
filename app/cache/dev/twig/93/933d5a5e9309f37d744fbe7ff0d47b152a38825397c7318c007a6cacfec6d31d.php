<?php

/* ResponsableBundle:Default:GestionDesProduits.html.twig */
class __TwigTemplate_fdc0c65c1bf4386f05504b79a28480fb7b4a3a272265c674a4057e46dbd5bac6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "ResponsableBundle:Default:GestionDesProduits.html.twig", 2);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 5
        echo "               
            <h1>
            Gestion Des Produits
            <small>Liste Des Produits</small>
          </h1>
            ";
    }

    // line 14
    public function block_TableView($context, array $blocks = array())
    {
        // line 15
        echo "<div class=\"TableView\" >
                <table >
                    <tr>
                        <td>
                            Id Produit
                        </td>
                        <td >
                            Référence
                        </td>
                        <td>
                            Libelle
                        </td>
                        <td>
                            Descriptif
                        </td>
                        <td>
                            Prix
                        </td>
                        <td>
                            Categorie
                        </td>
                        <td>
                            Image
                        </td>
                        <td>
                            Note
                        </td>
                        <td>
                            NbPoint
                        </td>
                        <td>
                            Modification
                        </td>
                        <td>
                            Suppression
                        </td>
                    </tr>
                    <tr>
                        <td >
                           
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                         <td>
                            
                        </td>
                         <td >
                           
                        </td>
                         <td >
                           
                        </td>
                         <td >
                           
                        </td>
                         <td >
                           
                        </td>
                         <td >
                           
                        </td>
                         <td >
                          <center><input class=\"modifIcon\" type=\"image\"  src=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Modifier.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                         <td >
                           <center><input class=\"suppIcon\" type=\"image\"  src=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Supprimer.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                         <td>
                            
                        </td>
                        <td >
                            
                        </td>
                        <td >
                            
                        </td>
                        <td >
                            
                        </td>
                        <td >
                            
                        </td>
                        <td >
                            
                        </td>
                         <td >
                          <center><input class=\"modifIcon\" type=\"image\"  src=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Modifier.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                         <td >
                          <center><input class=\"suppIcon\" type=\"image\"  src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Supprimer.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                         <td>
                            
                        </td>
                        <td >
                            
                        </td>
                        <td >
                            
                        </td>
                        <td >
                            
                        </td>
                        <td >
                            
                        </td>
                        <td >
                            
                        </td>
                         <td >
                          <center><input class=\"modifIcon\" type=\"image\"  src=\"";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Modifier.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                         <td >
                           <center><input class=\"suppIcon\" type=\"image\"  src=\"";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Supprimer.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                         <td>
                            
                        </td>
                        <td >
                            
                        </td>
                        <td >
                            
                        </td>
                        <td >
                            
                        </td>
                        <td >
                            
                        </td>
                        <td >
                            
                        </td>
                         <td >
                           <center><input class=\"modifIcon\" type=\"image\"  src=\"";
        // line 186
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Modifier.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                         <td >
                           <center><input class=\"suppIcon\" type=\"image\"  src=\"";
        // line 189
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Supprimer.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                    </tr>
                     <tr>
                        <td >
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                         <td>
                            
                        </td>
                        <td >
                            
                        </td>
                        <td >
                            
                        </td>
                        <td >
                            
                        </td>
                        <td >
                            
                        </td>
                        <td >
                            
                        </td>
                         <td >
                           <center><input class=\"modifIcon\" type=\"image\"  src=\"";
        // line 221
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Modifier.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                         <td >
                           <center><input class=\"suppIcon\" type=\"image\"  src=\"";
        // line 224
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Supprimer.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                    </tr>
                </table>
            </div>
    ";
    }

    // line 230
    public function block_btn($context, array $blocks = array())
    {
        // line 231
        echo "     <br>
                <div>
                    
                    <input type=\"submit\" class=\"btn\" value=\"Ajouter Produit\"/> 
                   
                
                
                </div>
    ";
    }

    public function getTemplateName()
    {
        return "ResponsableBundle:Default:GestionDesProduits.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  296 => 231,  293 => 230,  283 => 224,  277 => 221,  242 => 189,  236 => 186,  201 => 154,  195 => 151,  160 => 119,  154 => 116,  119 => 84,  113 => 81,  45 => 15,  42 => 14,  33 => 5,  30 => 4,  11 => 2,);
    }
}
/* */
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* */
/*  {% block HeaderTitle %}*/
/*                */
/*             <h1>*/
/*             Gestion Des Produits*/
/*             <small>Liste Des Produits</small>*/
/*           </h1>*/
/*             {%endblock %}*/
/* */
/* */
/* */
/* {% block TableView %}*/
/* <div class="TableView" >*/
/*                 <table >*/
/*                     <tr>*/
/*                         <td>*/
/*                             Id Produit*/
/*                         </td>*/
/*                         <td >*/
/*                             Référence*/
/*                         </td>*/
/*                         <td>*/
/*                             Libelle*/
/*                         </td>*/
/*                         <td>*/
/*                             Descriptif*/
/*                         </td>*/
/*                         <td>*/
/*                             Prix*/
/*                         </td>*/
/*                         <td>*/
/*                             Categorie*/
/*                         </td>*/
/*                         <td>*/
/*                             Image*/
/*                         </td>*/
/*                         <td>*/
/*                             Note*/
/*                         </td>*/
/*                         <td>*/
/*                             NbPoint*/
/*                         </td>*/
/*                         <td>*/
/*                             Modification*/
/*                         </td>*/
/*                         <td>*/
/*                             Suppression*/
/*                         </td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <td >*/
/*                            */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                          <td>*/
/*                             */
/*                         </td>*/
/*                          <td >*/
/*                            */
/*                         </td>*/
/*                          <td >*/
/*                            */
/*                         </td>*/
/*                          <td >*/
/*                            */
/*                         </td>*/
/*                          <td >*/
/*                            */
/*                         </td>*/
/*                          <td >*/
/*                            */
/*                         </td>*/
/*                          <td >*/
/*                           <center><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Modifier.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                          <td >*/
/*                            <center><input class="suppIcon" type="image"  src="{{asset('images/Responsable/Supprimer.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                          <td>*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                          <td >*/
/*                           <center><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Modifier.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                          <td >*/
/*                           <center><input class="suppIcon" type="image"  src="{{asset('images/Responsable/Supprimer.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                          <td>*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                          <td >*/
/*                           <center><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Modifier.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                          <td >*/
/*                            <center><input class="suppIcon" type="image"  src="{{asset('images/Responsable/Supprimer.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                          <td>*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                          <td >*/
/*                            <center><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Modifier.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                          <td >*/
/*                            <center><input class="suppIcon" type="image"  src="{{asset('images/Responsable/Supprimer.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                     </tr>*/
/*                      <tr>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                          <td>*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                          <td >*/
/*                            <center><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Modifier.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                          <td >*/
/*                            <center><input class="suppIcon" type="image"  src="{{asset('images/Responsable/Supprimer.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                     </tr>*/
/*                 </table>*/
/*             </div>*/
/*     {% endblock TableView %}    */
/* {% block btn %}*/
/*      <br>*/
/*                 <div>*/
/*                     */
/*                     <input type="submit" class="btn" value="Ajouter Produit"/> */
/*                    */
/*                 */
/*                 */
/*                 </div>*/
/*     {% endblock %}*/
/* */
