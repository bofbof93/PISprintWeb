<?php

/* ReservationPackBundle:ReservationPack:ReservationAPayer.html.twig */
class __TwigTemplate_0b1b5c3442752247d612c48445687de1fcaa0095b96dae6ef97c2a2bb3ebeb7b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "ReservationPackBundle:ReservationPack:ReservationAPayer.html.twig", 2);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 5
        echo "    <style>
        img {
            width: 50px;
            height: 50px;
        }
    </style>

    <h1>
         Gestion Des packs publicitairess
        <small>Liste des réservation à payer</small>
    </h1>
    <center>
        <form  method=\"POST\">
            <input type=\"text\" name=\"search\">
            <input type=\"submit\" value=\"rechercher\">
        </form>
    </center>
";
    }

    // line 24
    public function block_TableView($context, array $blocks = array())
    {
        // line 25
        echo "    <div style=\"padding: 10px\">
        <div class=\"TableView\" >
            <div class=\"box box-success \">    
                <div  class=\"box-body no-padding\">
                    <table class=\"table table-bordered table-hover\" >
<tr>
                <th>Réservation</th>
                <th>Payer</th>
                
            </tr>
            <tr> ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["res"]) ? $context["res"] : $this->getContext($context, "res")));
        foreach ($context['_seq'] as $context["_key"] => $context["re"]) {
            // line 36
            echo "                <td>Votre réservation pour la zone ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["re"], "PackPublicitaire", array()), "Zone", array()), "html", null, true);
            echo " du ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["re"], "Datedebut", array()), "format", array(0 => "j/m/Y"), "method"), "html", null, true);
            echo " au ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["re"], "DateFin", array()), "format", array(0 => "j/m/Y"), "method"), "html", null, true);
            echo " est accepté vous devez payez </td>
               
                <td>
            <center><a href=\"";
            // line 39
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Payer", array("id" => $this->getAttribute($context["re"], "id", array()))), "html", null, true);
            echo "\"><span class=\"fa fa-2x fa-credit-card\"></span></center>
                </td>
               
                </tr>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['re'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "                        </table>
                                    </div>  </div>         
                                    ";
    }

    // line 48
    public function block_btn($context, array $blocks = array())
    {
        echo "  
                                   
                                            </div></div></div>
                                        ";
    }

    public function getTemplateName()
    {
        return "ReservationPackBundle:ReservationPack:ReservationAPayer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 48,  96 => 45,  84 => 39,  73 => 36,  69 => 35,  57 => 25,  54 => 24,  33 => 5,  30 => 4,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* */
/* {% block HeaderTitle %}*/
/*     <style>*/
/*         img {*/
/*             width: 50px;*/
/*             height: 50px;*/
/*         }*/
/*     </style>*/
/* */
/*     <h1>*/
/*          Gestion Des packs publicitairess*/
/*         <small>Liste des réservation à payer</small>*/
/*     </h1>*/
/*     <center>*/
/*         <form  method="POST">*/
/*             <input type="text" name="search">*/
/*             <input type="submit" value="rechercher">*/
/*         </form>*/
/*     </center>*/
/* {%endblock %}*/
/* */
/* {% block TableView %}*/
/*     <div style="padding: 10px">*/
/*         <div class="TableView" >*/
/*             <div class="box box-success ">    */
/*                 <div  class="box-body no-padding">*/
/*                     <table class="table table-bordered table-hover" >*/
/* <tr>*/
/*                 <th>Réservation</th>*/
/*                 <th>Payer</th>*/
/*                 */
/*             </tr>*/
/*             <tr> {%for re in res%}*/
/*                 <td>Votre réservation pour la zone {{re.PackPublicitaire.Zone}} du {{re.Datedebut.format('j/m/Y')}} au {{re.DateFin.format('j/m/Y')}} est accepté vous devez payez </td>*/
/*                */
/*                 <td>*/
/*             <center><a href="{{path('Payer',{'id':re.id})}}"><span class="fa fa-2x fa-credit-card"></span></center>*/
/*                 </td>*/
/*                */
/*                 </tr>*/
/* */
/*                     {%endfor%}*/
/*                         </table>*/
/*                                     </div>  </div>         */
/*                                     {% endblock %} */
/*                                         {%block btn %}  */
/*                                    */
/*                                             </div></div></div>*/
/*                                         {% endblock %}*/
/* */
