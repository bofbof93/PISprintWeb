<?php

/* PromotionBundle:Promotion:Promotion.html.twig */
class __TwigTemplate_a2045cffcb09d3d1d9b0af8d282732237809b0fc98f9c23de1dd9ccc30565498 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ClientBundle::Layout.html.twig", "PromotionBundle:Promotion:Promotion.html.twig", 1);
        $this->blocks = array(
            'PromoA' => array($this, 'block_PromoA'),
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ClientBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_PromoA($context, array $blocks = array())
    {
        // line 4
        echo "
    <header id=\"section-header\" class=\"header moto-section\" data-widget=\"section\" data-container=\"section\">
        <div class=\"moto-widget moto-widget-container\" data-widget=\"container\" data-container=\"container\" data-css-name=\"moto-container_header_56ab411036030\">
            <img src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/client/promo.gif"), "html", null, true);
        echo "\"style=\" width: 100%; height: 500px;\"/></div>

        <header>

        ";
    }

    // line 12
    public function block_contenu($context, array $blocks = array())
    {
        echo " 
            <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js\"></script>


            <div class=\"moto-widget moto-widget-menu moto-preset-default moto-align-center moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto\" data-preset=\"default\" data-widget=\"menu\" data-align=\"{&quot;desktop&quot;:&quot;center&quot;,&quot;tablet&quot;:&quot;&quot;,&quot;mobile-h&quot;:&quot;&quot;,&quot;mobile-v&quot;:&quot;&quot;}\" data-menu-id=\"1\" data-menu-type=\"horizontal\" data-spacing=\"mama\">
                <a href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\" class=\"moto-widget-menu-toggle-btn\"><i class=\"moto-widget-menu-toggle-btn-icon fa fa-bars\"></i></a>
                <ul class=\"moto-widget-menu-list moto-widget-menu-list_horizontal\">
                    <li class=\"moto-widget-menu-item\">
                        <a href=\"acceuil\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Acceuil</a>
                    </li><li class=\"moto-widget-menu-item\">
                        <a href=\"catalogue\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Catalogues<span class=\"fa moto-widget-menu-link-arrow\"></span></a>
                        <ul class=\"moto-widget-menu-sublist\">
                            <li class=\"moto-widget-menu-item\">
                                <a href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("testimonials/index.html"), "html", null, true);
        echo "\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-2 moto-link\"></a>
                            </li>

                            <li class=\"moto-widget-menu-item\">
                                <a href=\"#\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-2 moto-widget-menu-link-submenu moto-link\">Générale<!--<span class=\"fa moto-widget-menu-link-arrow\"></span></a>
                                    <!--<ul class=\"moto-widget-menu-sublist\">
                                        <li class=\"moto-widget-menu-item\">
                        <a href=\"#\"   data-action=\"blog.index\" class=\"moto-widget-menu-link moto-widget-menu-link-level-3 moto-link\">gallery1</a>
                        </li>
                                        <li class=\"moto-widget-menu-item\">
                        <a href=\"#}\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-3 moto-link\">About us</a>
                        </li>
                                </ul>-->

                            </li>
                            <li class=\"moto-widget-menu-item\">
                                <a href=\"#\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-2 moto-link\">Par enseigne</a>
                            </li>
                        </ul>

                    </li>
                    <li class=\"moto-widget-menu-item\">
                        <a href=\"gallerie\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Galleries</a>
                    </li>
                    <li class=\"moto-widget-menu-item\">
                        <a href=\"fashion\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Fashion</a>
                    </li>
                    <li class=\"moto-widget-menu-item\">
                        <a href=\"boutique\"   data-action=\"blog.index\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-active moto-link\">Nos Boutiques</a>
                    </li><li class=\"moto-widget-menu-item\">

                        <a href=\"contact\"   data-action=\"page\" class=\"moto-wisdget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Contacts</a>
                    </li>   
                </ul>
            </div>
        </header>




      <section id=\"section-content\" class=\"content page-5 moto-section\" data-widget=\"section\" data-container=\"section\">
            <div class=\"moto-widget moto-widget-row row-fixed\" data-widget=\"row\">
  
                <div class=\"moto-widget moto-widget-row\" data-widget=\"row\">
                    <div class=\"container-fluid\">
                        <div class=\"row\">
                            
               ";
        // line 72
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["promotions"]) ? $context["promotions"] : $this->getContext($context, "promotions")));
        foreach ($context['_seq'] as $context["_key"] => $context["promotion"]) {
            // line 73
            echo "                           <!---*******************--->      
                            
                            <div class=\"moto-cell col-sm-6\" data-container=\"container\">
                                <div class=\"moto-widget moto-widget-image moto-preset-5 moto-align-center moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto \" data-preset=\"5\" data-spacing=\"aaaa\" data-widget=\"image\">


                                    <a class=\"moto-widget-image-link moto-link\" href=\"../mt-demo/58300/58352/mt-content/uploads/2016/01/mt-0295-gallery-img2-1.jpg\"   data-action=\"lightbox\">
                                        <img  src=";
            // line 80
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotion"], "Image", array()), "html", null, true);
            echo " class=\"moto-widget-image-picture\" data-id=\"168\" title=\"\"  alt=\"\" draggable=\"false\">
                                    </a>
                                </div>
                                <div data-css-name=\"moto-container_content_56a8800e\" class=\"moto-widget moto-widget-container moto-container_content_56a8800e\" data-widget=\"container\" data-container=\"container\">
                                    <div data-animation=\"fadeIn\" class=\"moto-widget moto-widget-text moto-preset-default        wow moto-spacing-top-large moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto fadeIn\" data-widget=\"text\" data-preset=\"default\" data-spacing=\"lama\">
                                        <div class=\"moto-widget-text-content moto-widget-text-editable\"><p class=\"moto-text_system_6\">
                                                <a class=\"moto-link\" data-action=\"url\" href=\"#\" target=\"_self\">Du ";
            // line 86
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["promotion"], "DateDebut", array()), "format", array(0 => "j/m/Y"), "method"), "html", null, true);
            echo " au  ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["promotion"], "DateFin", array()), "format", array(0 => "j/m/Y"), "method"), "html", null, true);
            echo "</a>
                                                <br>
                                            </p> 
                                        </div>
                                   </div>
                                    
                                    <div data-animation=\"fadeIn\" class=\"moto-widget moto-widget-text moto-preset-default        wow moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto fadeIn\" data-widget=\"text\" data-preset=\"default\" data-spacing=\"aama\">
                                        <div class=\"moto-widget-text-content moto-widget-text-editable\">
                                            <p class=\"moto-text_normal\">";
            // line 94
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotion"], "Description", array()), "html", null, true);
            echo ".<br></p>
                                        </div>
                                    </div>
                                    <div class=\"moto-widget moto-widget-button moto-preset-2 moto-align-left moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto wow fadeIn\" data-widget=\"button\" data-preset=\"2\" data-spacing=\"sama\">


                                        <a href=\"../more/index.html\"   data-action=\"page\" class=\"moto-widget-button-link moto-size-large moto-link\"
                                           data-anchor=\"\"
                                           data-size=\"large\">
                                            <span class=\"fa moto-widget-theme-icon\"></span> 
                                            <span class=\"moto-widget-button-label\">more</span></a>
                                    </div>

                                </div>
                            </div>
                                    
                                                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['promotion'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 111
        echo "                  
                                    
               <!---*******************--->         
                                    
                                    
                                    
                              
                                    
                                    
                            
                                    
                            
                            </div>             
                       </div>             
                    </div>                      
                </div>
           
        </section>   
        <center>  ";
        // line 129
        $this->loadTemplate("FOSCommentBundle:Thread:async.html.twig", "PromotionBundle:Promotion:Promotion.html.twig", 129)->display(array_merge($context, array("id" => "foo")));
        echo "</div></center>
    ";
    }

    public function getTemplateName()
    {
        return "PromotionBundle:Promotion:Promotion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 129,  174 => 111,  151 => 94,  138 => 86,  129 => 80,  120 => 73,  116 => 72,  66 => 25,  55 => 17,  46 => 12,  37 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }
}
/* {% extends "ClientBundle::Layout.html.twig" %}*/
/* */
/* {% block PromoA %}*/
/* */
/*     <header id="section-header" class="header moto-section" data-widget="section" data-container="section">*/
/*         <div class="moto-widget moto-widget-container" data-widget="container" data-container="container" data-css-name="moto-container_header_56ab411036030">*/
/*             <img src="{{asset('images/client/promo.gif')}}"style=" width: 100%; height: 500px;"/></div>*/
/* */
/*         <header>*/
/* */
/*         {% endblock %}*/
/*         {% block contenu %} */
/*             <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>*/
/* */
/* */
/*             <div class="moto-widget moto-widget-menu moto-preset-default moto-align-center moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-preset="default" data-widget="menu" data-align="{&quot;desktop&quot;:&quot;center&quot;,&quot;tablet&quot;:&quot;&quot;,&quot;mobile-h&quot;:&quot;&quot;,&quot;mobile-v&quot;:&quot;&quot;}" data-menu-id="1" data-menu-type="horizontal" data-spacing="mama">*/
/*                 <a href="{{asset('#')}}" class="moto-widget-menu-toggle-btn"><i class="moto-widget-menu-toggle-btn-icon fa fa-bars"></i></a>*/
/*                 <ul class="moto-widget-menu-list moto-widget-menu-list_horizontal">*/
/*                     <li class="moto-widget-menu-item">*/
/*                         <a href="acceuil"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Acceuil</a>*/
/*                     </li><li class="moto-widget-menu-item">*/
/*                         <a href="catalogue"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Catalogues<span class="fa moto-widget-menu-link-arrow"></span></a>*/
/*                         <ul class="moto-widget-menu-sublist">*/
/*                             <li class="moto-widget-menu-item">*/
/*                                 <a href="{{asset('testimonials/index.html')}}"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-2 moto-link"></a>*/
/*                             </li>*/
/* */
/*                             <li class="moto-widget-menu-item">*/
/*                                 <a href="#"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-2 moto-widget-menu-link-submenu moto-link">Générale<!--<span class="fa moto-widget-menu-link-arrow"></span></a>*/
/*                                     <!--<ul class="moto-widget-menu-sublist">*/
/*                                         <li class="moto-widget-menu-item">*/
/*                         <a href="#"   data-action="blog.index" class="moto-widget-menu-link moto-widget-menu-link-level-3 moto-link">gallery1</a>*/
/*                         </li>*/
/*                                         <li class="moto-widget-menu-item">*/
/*                         <a href="#}"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-3 moto-link">About us</a>*/
/*                         </li>*/
/*                                 </ul>-->*/
/* */
/*                             </li>*/
/*                             <li class="moto-widget-menu-item">*/
/*                                 <a href="#"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-2 moto-link">Par enseigne</a>*/
/*                             </li>*/
/*                         </ul>*/
/* */
/*                     </li>*/
/*                     <li class="moto-widget-menu-item">*/
/*                         <a href="gallerie"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Galleries</a>*/
/*                     </li>*/
/*                     <li class="moto-widget-menu-item">*/
/*                         <a href="fashion"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Fashion</a>*/
/*                     </li>*/
/*                     <li class="moto-widget-menu-item">*/
/*                         <a href="boutique"   data-action="blog.index" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-active moto-link">Nos Boutiques</a>*/
/*                     </li><li class="moto-widget-menu-item">*/
/* */
/*                         <a href="contact"   data-action="page" class="moto-wisdget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Contacts</a>*/
/*                     </li>   */
/*                 </ul>*/
/*             </div>*/
/*         </header>*/
/* */
/* */
/* */
/* */
/*       <section id="section-content" class="content page-5 moto-section" data-widget="section" data-container="section">*/
/*             <div class="moto-widget moto-widget-row row-fixed" data-widget="row">*/
/*   */
/*                 <div class="moto-widget moto-widget-row" data-widget="row">*/
/*                     <div class="container-fluid">*/
/*                         <div class="row">*/
/*                             */
/*                {% for promotion in promotions %}*/
/*                            <!---*******************--->      */
/*                             */
/*                             <div class="moto-cell col-sm-6" data-container="container">*/
/*                                 <div class="moto-widget moto-widget-image moto-preset-5 moto-align-center moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-preset="5" data-spacing="aaaa" data-widget="image">*/
/* */
/* */
/*                                     <a class="moto-widget-image-link moto-link" href="../mt-demo/58300/58352/mt-content/uploads/2016/01/mt-0295-gallery-img2-1.jpg"   data-action="lightbox">*/
/*                                         <img  src={{promotion.Image}} class="moto-widget-image-picture" data-id="168" title=""  alt="" draggable="false">*/
/*                                     </a>*/
/*                                 </div>*/
/*                                 <div data-css-name="moto-container_content_56a8800e" class="moto-widget moto-widget-container moto-container_content_56a8800e" data-widget="container" data-container="container">*/
/*                                     <div data-animation="fadeIn" class="moto-widget moto-widget-text moto-preset-default        wow moto-spacing-top-large moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto fadeIn" data-widget="text" data-preset="default" data-spacing="lama">*/
/*                                         <div class="moto-widget-text-content moto-widget-text-editable"><p class="moto-text_system_6">*/
/*                                                 <a class="moto-link" data-action="url" href="#" target="_self">Du {{promotion.DateDebut.format('j/m/Y')}} au  {{promotion.DateFin.format('j/m/Y')}}</a>*/
/*                                                 <br>*/
/*                                             </p> */
/*                                         </div>*/
/*                                    </div>*/
/*                                     */
/*                                     <div data-animation="fadeIn" class="moto-widget moto-widget-text moto-preset-default        wow moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto fadeIn" data-widget="text" data-preset="default" data-spacing="aama">*/
/*                                         <div class="moto-widget-text-content moto-widget-text-editable">*/
/*                                             <p class="moto-text_normal">{{promotion.Description}}.<br></p>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                     <div class="moto-widget moto-widget-button moto-preset-2 moto-align-left moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto wow fadeIn" data-widget="button" data-preset="2" data-spacing="sama">*/
/* */
/* */
/*                                         <a href="../more/index.html"   data-action="page" class="moto-widget-button-link moto-size-large moto-link"*/
/*                                            data-anchor=""*/
/*                                            data-size="large">*/
/*                                             <span class="fa moto-widget-theme-icon"></span> */
/*                                             <span class="moto-widget-button-label">more</span></a>*/
/*                                     </div>*/
/* */
/*                                 </div>*/
/*                             </div>*/
/*                                     */
/*                                                   {%endfor%}*/
/*                   */
/*                                     */
/*                <!---*******************--->         */
/*                                     */
/*                                     */
/*                                     */
/*                               */
/*                                     */
/*                                     */
/*                             */
/*                                     */
/*                             */
/*                             </div>             */
/*                        </div>             */
/*                     </div>                      */
/*                 </div>*/
/*            */
/*         </section>   */
/*         <center>  {% include 'FOSCommentBundle:Thread:async.html.twig' with {'id': 'foo'} %}</div></center>*/
/*     {% endblock %}*/
