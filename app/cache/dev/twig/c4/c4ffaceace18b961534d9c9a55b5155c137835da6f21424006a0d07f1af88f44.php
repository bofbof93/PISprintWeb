<?php

/* PackPubBundle:PackPub:ModifierA.html.twig */
class __TwigTemplate_90c4087e9ce6c8938c7a935fe8b9c3e7c7c20537928ba2ca61be053fc146a60c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "PackPubBundle:PackPub:ModifierA.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_contenu($context, array $blocks = array())
    {
        // line 3
        echo "    <div class =\"container\">
        <div class=\"col-md-6\">
            <!-- Horizontal Form -->
            <div class=\"box box-info\">
                <div class=\"box-header with-border\">
                    <h3 class=\"box-title\">Ajouter un pack publicitaire</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class=\"form-horizontal\" name=\"form\" onsubmit=\"verif()\" method =\"POST\">
                    <div class=\"box-body\">

";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["packs"]) ? $context["packs"] : $this->getContext($context, "packs")));
        foreach ($context['_seq'] as $context["_key"] => $context["pack"]) {
            // line 15
            echo "                        <div class=\"form-group\">
                            <label for=\"text\" class=\"col-sm-2 control-label\">Libelle </label>
                            <div class=\"col-sm-10\">
                                <input name=\"libelle\" type=\"text\" class=\"form-control\" id=\"date1\" value=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "Libelle", array()), "html", null, true);
            echo "\">
                            </div>
                        </div>     

                        <div class=\"form-group\">
                            <label for=\"date1\" class=\"col-sm-2 control-label\">Date début </label>
                            <div class=\"col-sm-10\">
                                <input name=\"Datedebut\" type=\"date\" class=\"form-control\" id=\"date1\"value=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "Datedebut", array()), "html", null, true);
            echo "\">
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"date2\" class=\"col-sm-2 control-label\">Date fin </label>
                            <div class=\"col-sm-10\">
                                <input name=\"Datefin\" type=\"date\" class=\"form-control\" id=\"date2\">
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"taux\" class=\"col-sm-2 control-label\">Prix</label>
                            <div class=\"col-sm-10\">
                                <input name=\"prix\" type=\"text\" class=\"form-control\" id=\"inputPassword3\" placeholder=\"\">
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"taux\" class=\"col-sm-2 control-label\">Zone</label>
                            <div class=\"col-sm-10\">
                                <input name=\"zone\" type=\"number\" class=\"form-control\" id=\"inputPassword3\" placeholder=\"\">
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"image\" class=\"col-sm-2 control-label\">Image</label>
                            <div class=\"col-sm-10\">
                                <input name =\"img\" type=\"file\" class=\"form-control\" id=\"image\" >
                            </div>
                        </div>



                        ";
            // line 59
            echo "         

                        <div class=\"col-sm-10\">
                            ";
            // line 63
            echo "                            <select name=\"responsable\" onChange=\"\">


                                <option></option>


                            </select>

                        </div>  



                        <div class=\"col-sm-10\">
                            ";
            // line 77
            echo "                            <select name=\"admin\" onChange=\"\">


                                <option></option>


                            </select>

                        </div> 
                    </div>

            <div class=\"box-footer\">
                <button type=\"submit\" class=\"btn btn-default\" name =\"submit\">Cancel</button>
                <button type=\"submit\" class=\"btn btn-info pull-right\">Sign in</button>
            </div><!-- /.box-footer -->
            </form>
        </div><!-- /.box -->
    </div>
                             </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pack'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "PackPubBundle:PackPub:ModifierA.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 77,  105 => 63,  100 => 59,  63 => 25,  53 => 18,  48 => 15,  44 => 14,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block contenu %}*/
/*     <div class ="container">*/
/*         <div class="col-md-6">*/
/*             <!-- Horizontal Form -->*/
/*             <div class="box box-info">*/
/*                 <div class="box-header with-border">*/
/*                     <h3 class="box-title">Ajouter un pack publicitaire</h3>*/
/*                 </div><!-- /.box-header -->*/
/*                 <!-- form start -->*/
/*                 <form class="form-horizontal" name="form" onsubmit="verif()" method ="POST">*/
/*                     <div class="box-body">*/
/* */
/* {% for pack in packs%}*/
/*                         <div class="form-group">*/
/*                             <label for="text" class="col-sm-2 control-label">Libelle </label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="libelle" type="text" class="form-control" id="date1" value="{{pack.Libelle}}">*/
/*                             </div>*/
/*                         </div>     */
/* */
/*                         <div class="form-group">*/
/*                             <label for="date1" class="col-sm-2 control-label">Date début </label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="Datedebut" type="date" class="form-control" id="date1"value="{{pack.Datedebut}}">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="date2" class="col-sm-2 control-label">Date fin </label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="Datefin" type="date" class="form-control" id="date2">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="taux" class="col-sm-2 control-label">Prix</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="prix" type="text" class="form-control" id="inputPassword3" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="taux" class="col-sm-2 control-label">Zone</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="zone" type="number" class="form-control" id="inputPassword3" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="image" class="col-sm-2 control-label">Image</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name ="img" type="file" class="form-control" id="image" >*/
/*                             </div>*/
/*                         </div>*/
/* */
/* */
/* */
/*                         {#  {% for responsable in responsables %} #}         */
/* */
/*                         <div class="col-sm-10">*/
/*                             {#   <input name ="responsable" type="text" class="form-control" id="responsable" value ="{{responsable.id}}">#}*/
/*                             <select name="responsable" onChange="">*/
/* */
/* */
/*                                 <option></option>*/
/* */
/* */
/*                             </select>*/
/* */
/*                         </div>  */
/* */
/* */
/* */
/*                         <div class="col-sm-10">*/
/*                             {#   <input name ="responsable" type="text" class="form-control" id="responsable" value ="{{responsable.id}}">#}*/
/*                             <select name="admin" onChange="">*/
/* */
/* */
/*                                 <option></option>*/
/* */
/* */
/*                             </select>*/
/* */
/*                         </div> */
/*                     </div>*/
/* */
/*             <div class="box-footer">*/
/*                 <button type="submit" class="btn btn-default" name ="submit">Cancel</button>*/
/*                 <button type="submit" class="btn btn-info pull-right">Sign in</button>*/
/*             </div><!-- /.box-footer -->*/
/*             </form>*/
/*         </div><!-- /.box -->*/
/*     </div>*/
/*                              </div>*/
/* {% endfor %}*/
/* {% endblock %}*/
/* */
