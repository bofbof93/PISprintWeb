<?php

/* ReservationPackBundle:ReservationPack:confirm.html.twig */
class __TwigTemplate_0fb7e1a0f2ec33f4b59ee12d18913de010d00df4c57c144adb380eff1c192bff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "ReservationPackBundle:ReservationPack:confirm.html.twig", 2);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 4
        echo "    <div class =\"container\">
        <div class=\"col-md-6\">
            <!-- Horizontal Form -->
            <div class=\"box box-info\">
                <div class=\"box-header with-border\">
                    <h3 class=\"box-title\">Paiement du reservation</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class=\"form-horizontal\" name=\"form\" onsubmit=\" return verif()\" method =\"POST\">
                    <div class=\"box-body\">
                    
                            <script language=\"javascript\">
                                function verif()
                                {
                                    if ((window.document.form.password.value == \"\") || (window.document.form.num.value == \"\"))
                                    {
                                    alert(\"saisir votre mot de passe et votre numéro de carte bancaire\");
                                            return false;
                                    }
                                   
                                    
                                }</script> 
";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 27
            echo "
<div class=\"alert alert-danger\">
    ";
            // line 29
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 33
            echo "
<div class=\"alert alert-success\">
    ";
            // line 35
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
</div>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "                        <div class=\"form-group\">
                            <label for=\"date1\" class=\"col-sm-2 control-label\">Numéro de carte bancaire</label>
                            <div class=\"col-sm-10\">
                                <input name=\"num\" type=\"text\" class=\"form-control\" id=\"date1\">
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"date2\" class=\"col-sm-2 control-label\">mot de passe </label>
                            <div class=\"col-sm-10\">
                                <input name=\"password\" type=\"password\" class=\"form-control\" id=\"date2\">
                            </div>
                        </div>

                    </div><!-- /.box-body -->
                    <div class=\"box-footer\">
                        <button type=\"reset\" class=\"btn btn-default\" name =\"submit\">Annuler</button>
                        <button type=\"submit\" class=\"btn btn-info pull-right\">Payer</button>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box -->
        </div>

    </div>
";
    }

    public function getTemplateName()
    {
        return "ReservationPackBundle:ReservationPack:confirm.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 39,  80 => 35,  76 => 33,  72 => 32,  63 => 29,  59 => 27,  55 => 26,  31 => 4,  28 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block HeaderTitle %}*/
/*     <div class ="container">*/
/*         <div class="col-md-6">*/
/*             <!-- Horizontal Form -->*/
/*             <div class="box box-info">*/
/*                 <div class="box-header with-border">*/
/*                     <h3 class="box-title">Paiement du reservation</h3>*/
/*                 </div><!-- /.box-header -->*/
/*                 <!-- form start -->*/
/*                 <form class="form-horizontal" name="form" onsubmit=" return verif()" method ="POST">*/
/*                     <div class="box-body">*/
/*                     */
/*                             <script language="javascript">*/
/*                                 function verif()*/
/*                                 {*/
/*                                     if ((window.document.form.password.value == "") || (window.document.form.num.value == ""))*/
/*                                     {*/
/*                                     alert("saisir votre mot de passe et votre numéro de carte bancaire");*/
/*                                             return false;*/
/*                                     }*/
/*                                    */
/*                                     */
/*                                 }</script> */
/* {% for flashMessage in app.session.flashbag.get('error') %}*/
/* */
/* <div class="alert alert-danger">*/
/*     {{ flashMessage }}*/
/* </div>*/
/* {% endfor %}*/
/* {% for flashMessage in app.session.flashbag.get('notice') %}*/
/* */
/* <div class="alert alert-success">*/
/*     {{ flashMessage }}*/
/* </div>*/
/* */
/* {% endfor %}*/
/*                         <div class="form-group">*/
/*                             <label for="date1" class="col-sm-2 control-label">Numéro de carte bancaire</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="num" type="text" class="form-control" id="date1">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="date2" class="col-sm-2 control-label">mot de passe </label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="password" type="password" class="form-control" id="date2">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                     </div><!-- /.box-body -->*/
/*                     <div class="box-footer">*/
/*                         <button type="reset" class="btn btn-default" name ="submit">Annuler</button>*/
/*                         <button type="submit" class="btn btn-info pull-right">Payer</button>*/
/*                     </div><!-- /.box-footer -->*/
/*                 </form>*/
/*             </div><!-- /.box -->*/
/*         </div>*/
/* */
/*     </div>*/
/* {% endblock %}*/
