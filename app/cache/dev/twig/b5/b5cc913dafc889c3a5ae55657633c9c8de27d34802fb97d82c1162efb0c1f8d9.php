<?php

/* AdminBundle:Default:AjouterProduit.html.twig */
class __TwigTemplate_0114a1cf4e18b85050a05e44447dda497f3195f52297bc6fb5e7f47818b86136 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "AdminBundle:Default:AjouterProduit.html.twig", 3);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_contenu($context, array $blocks = array())
    {
        // line 5
        echo "   
           <div class=\"col-md-6\">
              <!-- Horizontal Form -->
              <div class=\"box box-info\">
                <div class=\"box-header with-border\">
                  <h3 class=\"box-title\">Ajouter une produit</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class=\"form-horizontal\" name=\"form\" onsubmit=\"verif()\">
                  <div class=\"box-body\">
                      
                    <div class=\"form-group\">
                      <label for=\"date1\" class=\"col-sm-2 control-label\">Référence</label>
                      <div class=\"col-sm-10\">
                        <input name=\"date1\" type=\"text\" class=\"form-control\" id=\"ref\">
                      </div>
                    </div>
                      
                    <div class=\"form-group\">
                      <label for=\"date2\" class=\"col-sm-2 control-label\">Libellé </label>
                      <div class=\"col-sm-10\">
                        <input name=\"date2\" type=\"text\" class=\"form-control\" id=\"libelle\">
                      </div>
                    </div>
                      
                    <div class=\"form-group\">
                      <label for=\"inputPassword3\" class=\"col-sm-2 control-label\">Descreptif</label>
                      <div class=\"col-sm-10\">
                        <textarea rows=\"5\" cols=\"50\" class=\"form-control\" id=\"text1\"></textarea>
                      </div>
                    </div>
  
                    <div class=\"form-group\">
                      <label for=\"date2\" class=\"col-sm-2 control-label\">Prix</label>
                      <div class=\"col-sm-10\">
                        <input name=\"date2\" type=\"text\" class=\"form-control\" id=\"libelle\">
                      </div>
                    </div>
                      
                    <div class=\"form-group\">
                      <label for=\"date2\" class=\"col-sm-2 control-label\">Catégorie</label>
                      <div class=\"col-sm-10\">
                        <input name=\"date2\" type=\"text\" class=\"form-control\" id=\"libelle\">
                      </div>
                    </div>
                      
            
                      

                      
                  </div><!-- /.box-body -->
                  <div class=\"box-footer\">
                    <button type=\"submit\" class=\"btn btn-default\">Cancel</button>
                    <button type=\"submit\" class=\"btn btn-info pull-right\">Sign in</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
           </div>
    
    ";
    }

    public function getTemplateName()
    {
        return "AdminBundle:Default:AjouterProduit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 5,  28 => 4,  11 => 3,);
    }
}
/* {# empty Twig template #}*/
/* {# empty Twig template #}*/
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block contenu %}*/
/*    */
/*            <div class="col-md-6">*/
/*               <!-- Horizontal Form -->*/
/*               <div class="box box-info">*/
/*                 <div class="box-header with-border">*/
/*                   <h3 class="box-title">Ajouter une produit</h3>*/
/*                 </div><!-- /.box-header -->*/
/*                 <!-- form start -->*/
/*                 <form class="form-horizontal" name="form" onsubmit="verif()">*/
/*                   <div class="box-body">*/
/*                       */
/*                     <div class="form-group">*/
/*                       <label for="date1" class="col-sm-2 control-label">Référence</label>*/
/*                       <div class="col-sm-10">*/
/*                         <input name="date1" type="text" class="form-control" id="ref">*/
/*                       </div>*/
/*                     </div>*/
/*                       */
/*                     <div class="form-group">*/
/*                       <label for="date2" class="col-sm-2 control-label">Libellé </label>*/
/*                       <div class="col-sm-10">*/
/*                         <input name="date2" type="text" class="form-control" id="libelle">*/
/*                       </div>*/
/*                     </div>*/
/*                       */
/*                     <div class="form-group">*/
/*                       <label for="inputPassword3" class="col-sm-2 control-label">Descreptif</label>*/
/*                       <div class="col-sm-10">*/
/*                         <textarea rows="5" cols="50" class="form-control" id="text1"></textarea>*/
/*                       </div>*/
/*                     </div>*/
/*   */
/*                     <div class="form-group">*/
/*                       <label for="date2" class="col-sm-2 control-label">Prix</label>*/
/*                       <div class="col-sm-10">*/
/*                         <input name="date2" type="text" class="form-control" id="libelle">*/
/*                       </div>*/
/*                     </div>*/
/*                       */
/*                     <div class="form-group">*/
/*                       <label for="date2" class="col-sm-2 control-label">Catégorie</label>*/
/*                       <div class="col-sm-10">*/
/*                         <input name="date2" type="text" class="form-control" id="libelle">*/
/*                       </div>*/
/*                     </div>*/
/*                       */
/*             */
/*                       */
/* */
/*                       */
/*                   </div><!-- /.box-body -->*/
/*                   <div class="box-footer">*/
/*                     <button type="submit" class="btn btn-default">Cancel</button>*/
/*                     <button type="submit" class="btn btn-info pull-right">Sign in</button>*/
/*                   </div><!-- /.box-footer -->*/
/*                 </form>*/
/*               </div><!-- /.box -->*/
/*            </div>*/
/*     */
/*     {% endblock %}*/
