<?php

/* TunisiaMallCarteBundle:Carte:index.html.twig */
class __TwigTemplate_2a06eefa77857314635d9ae3b5b0ab641f38412bd8596a2a791231d48dd835cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "TunisiaMallCarteBundle:Carte:index.html.twig", 2);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 5
        echo "               
            <h1>
            Cartes fidélité
            <small>Liste Des Cartes</small>
          </h1>
            ";
    }

    // line 14
    public function block_TableView($context, array $blocks = array())
    {
        // line 15
        echo "
        <div class=\"row\">
    </div>
                <div class=\"row\">
                    
                    ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cartes"]) ? $context["cartes"] : $this->getContext($context, "cartes")));
        foreach ($context['_seq'] as $context["_key"] => $context["carte"]) {
            // line 21
            echo "                    <div class=\"col-md-4\">
                        <div class=\"small-box bg-aqua-active\">
                            <div class=\"inner\">
                                <h4>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["carte"], "numero", array()), "html", null, true);
            echo "</h4>
                                    <p>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["carte"], "user", array()), "email", array()), "html", null, true);
            echo "</p>
                            </div>
                            <div class=\"icon\">
                                <i class=\"fa fa-credit-card\"></i>
                            </div>
                            <a href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tunisia_mall_responsable_Carte_afficher", array("id" => $this->getAttribute($context["carte"], "id", array()))), "html", null, true);
            echo "\" class=\"small-box-footer\">
                                Choose <i class=\"fa fa-arrow-circle-right\"></i>
                            </a>
                        </div>
                    </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['carte'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "                </div>    
                
    ";
    }

    // line 40
    public function block_btn($context, array $blocks = array())
    {
        // line 41
        echo "     <br>
                <div>
                    
                    <a href=\"";
        // line 44
        echo $this->env->getExtension('routing')->getPath("tunisia_mall_responsable_Carte_ajout");
        echo "\"><input type=\"button\" class=\"btn btn-success\" value=\"Ajouter Carte\"></a>
                
                
                </div>
    ";
    }

    public function getTemplateName()
    {
        return "TunisiaMallCarteBundle:Carte:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 44,  94 => 41,  91 => 40,  85 => 36,  73 => 30,  65 => 25,  61 => 24,  56 => 21,  52 => 20,  45 => 15,  42 => 14,  33 => 5,  30 => 4,  11 => 2,);
    }
}
/* */
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* */
/*  {% block HeaderTitle %}*/
/*                */
/*             <h1>*/
/*             Cartes fidélité*/
/*             <small>Liste Des Cartes</small>*/
/*           </h1>*/
/*             {%endblock %}*/
/* */
/* */
/* */
/* {% block TableView %}*/
/* */
/*         <div class="row">*/
/*     </div>*/
/*                 <div class="row">*/
/*                     */
/*                     {%for carte in cartes %}*/
/*                     <div class="col-md-4">*/
/*                         <div class="small-box bg-aqua-active">*/
/*                             <div class="inner">*/
/*                                 <h4>{{ carte.numero }}</h4>*/
/*                                     <p>{{ carte.user.email}}</p>*/
/*                             </div>*/
/*                             <div class="icon">*/
/*                                 <i class="fa fa-credit-card"></i>*/
/*                             </div>*/
/*                             <a href="{{path('tunisia_mall_responsable_Carte_afficher',{'id': carte.id})}}" class="small-box-footer">*/
/*                                 Choose <i class="fa fa-arrow-circle-right"></i>*/
/*                             </a>*/
/*                         </div>*/
/*                     </div>*/
/*                         {% endfor %}*/
/*                 </div>    */
/*                 */
/*     {% endblock TableView %}    */
/* */
/* {% block btn %}*/
/*      <br>*/
/*                 <div>*/
/*                     */
/*                     <a href="{{path('tunisia_mall_responsable_Carte_ajout')}}"><input type="button" class="btn btn-success" value="Ajouter Carte"></a>*/
/*                 */
/*                 */
/*                 </div>*/
/*     {% endblock %}*/
