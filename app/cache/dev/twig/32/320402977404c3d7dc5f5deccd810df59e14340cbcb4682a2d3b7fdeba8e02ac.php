<?php

/* TunisiaMallProduitBundle:Produit:AjouterProduit.html.twig */
class __TwigTemplate_3106283690c73da02b8ae56a02a384fdafc0d0aa8a440eeddc7e2f77946a11ca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "TunisiaMallProduitBundle:Produit:AjouterProduit.html.twig", 1);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 3
        echo "
    <h1>
        Ajouter
        <small>Des Produits</small>
    </h1>
";
    }

    // line 9
    public function block_TableView($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"col-md-6\">
        <!-- Horizontal Form -->
        <div class=\"box box-info\">
            <div class=\"box-header with-border\">
                <h3 class=\"box-title\">Ajouter une produit</h3>
            </div>
            <!-- /.box-header -->
            <div class=\"box-body\">
                <!-- form start -->
                <form  method=\"POST\" class=\"form-horizontal\" name=\"form\" onsubmit=\"verif()\">
                    <div> ";
        // line 20
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
                        ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
                        <div class=\"form-group\">
                            <label for=\"Reference\" class=\"col-sm-2 control-label\">Reference</label>
                            ";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Reference", array()), 'errors');
        echo "
                            <div class=\"col-sm-10\">
                                ";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Reference", array()), 'widget');
        echo "
                            </div> 
                        </div>
                        <div class=\"form-group\">
                            <label for=\"Libelle\" class=\"col-sm-2 control-label\">Libelle</label>
                            ";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Libelle", array()), 'errors');
        echo "
                            <div class=\"col-sm-10\">
                                ";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Libelle", array()), 'widget');
        echo "
                            </div> 
                        </div>
                        <div class=\"form-group\">
                            <label for=\"Descriptif\" class=\"col-sm-2 control-label\">Descriptif</label>
                            ";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Descriptif", array()), 'errors');
        echo "
                            <div class=\"col-sm-10\">
                                ";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Descriptif", array()), 'widget');
        echo "
                            </div> 
                        </div>
                        <div class=\"form-group\">
                            <label for=\"Prix\" class=\"col-sm-2 control-label\">Prix</label>
                            ";
        // line 45
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Prix", array()), 'errors');
        echo "
                            <div class=\"col-sm-10\">
                                ";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Prix", array()), 'widget');
        echo "
                            </div> 
                        </div>
                        <div class=\"form-group\">
                            <label for=\"Categorie\" class=\"col-sm-2 control-label\">Categorie</label>
                            ";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Categorie", array()), 'errors');
        echo "
                            <div class=\"col-sm-10\">
                                ";
        // line 54
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Categorie", array()), 'widget');
        echo "
                            </div> 
                        </div>
                        <div class=\"form-group\">
                            <label for=\"Image\" class=\"col-sm-2 control-label\">Image</label>
                            ";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Image", array()), 'errors');
        echo "
                            <div class=\"col-sm-10\">
                                ";
        // line 61
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Image", array()), 'widget');
        echo "
                            </div> 
                        </div>

                        <div class=\"form-group\">
                            <label for=\"NbPoint\" class=\"col-sm-2 control-label\">NbPoint</label>
                            ";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "NbPoint", array()), 'errors');
        echo "
                            <div class=\"col-sm-10\">
                                ";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "NbPoint", array()), 'widget');
        echo "
                            </div></div> 

                        <div class=\"form-group\">
                            <label for=\"Panier\" class=\"col-sm-2 control-label\">Panier</label>
                            ";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Panier", array()), 'errors');
        echo "
                            <div class=\"col-sm-10\">
                                ";
        // line 76
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Panier", array()), 'widget');
        echo "
                            </div> </div>




                        <div class=\"box-footer\">
                            ";
        // line 83
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "AjouterProduit", array()), 'widget');
        echo "
                        </div>
                        ";
        // line 85
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "


                    </div>
                </form>
            </div>
        </div>
    </div><!-- /.box -->


";
    }

    public function getTemplateName()
    {
        return "TunisiaMallProduitBundle:Produit:AjouterProduit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 85,  173 => 83,  163 => 76,  158 => 74,  150 => 69,  145 => 67,  136 => 61,  131 => 59,  123 => 54,  118 => 52,  110 => 47,  105 => 45,  97 => 40,  92 => 38,  84 => 33,  79 => 31,  71 => 26,  66 => 24,  60 => 21,  56 => 20,  44 => 10,  41 => 9,  32 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block HeaderTitle %}*/
/* */
/*     <h1>*/
/*         Ajouter*/
/*         <small>Des Produits</small>*/
/*     </h1>*/
/* {%endblock %}*/
/*  {% block TableView %}*/
/*     <div class="col-md-6">*/
/*         <!-- Horizontal Form -->*/
/*         <div class="box box-info">*/
/*             <div class="box-header with-border">*/
/*                 <h3 class="box-title">Ajouter une produit</h3>*/
/*             </div>*/
/*             <!-- /.box-header -->*/
/*             <div class="box-body">*/
/*                 <!-- form start -->*/
/*                 <form  method="POST" class="form-horizontal" name="form" onsubmit="verif()">*/
/*                     <div> {{ form_start(form) }}*/
/*                         {{form_errors(form) }}*/
/*                         <div class="form-group">*/
/*                             <label for="Reference" class="col-sm-2 control-label">Reference</label>*/
/*                             {{ form_errors(form.Reference) }}*/
/*                             <div class="col-sm-10">*/
/*                                 {{ form_widget(form.Reference)  }}*/
/*                             </div> */
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="Libelle" class="col-sm-2 control-label">Libelle</label>*/
/*                             {{ form_errors(form.Libelle) }}*/
/*                             <div class="col-sm-10">*/
/*                                 {{ form_widget(form.Libelle)  }}*/
/*                             </div> */
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="Descriptif" class="col-sm-2 control-label">Descriptif</label>*/
/*                             {{ form_errors(form.Descriptif) }}*/
/*                             <div class="col-sm-10">*/
/*                                 {{ form_widget(form.Descriptif)  }}*/
/*                             </div> */
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="Prix" class="col-sm-2 control-label">Prix</label>*/
/*                             {{ form_errors(form.Prix) }}*/
/*                             <div class="col-sm-10">*/
/*                                 {{ form_widget(form.Prix)  }}*/
/*                             </div> */
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="Categorie" class="col-sm-2 control-label">Categorie</label>*/
/*                             {{ form_errors(form.Categorie) }}*/
/*                             <div class="col-sm-10">*/
/*                                 {{ form_widget(form.Categorie)  }}*/
/*                             </div> */
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="Image" class="col-sm-2 control-label">Image</label>*/
/*                             {{ form_errors(form.Image) }}*/
/*                             <div class="col-sm-10">*/
/*                                 {{ form_widget(form.Image)  }}*/
/*                             </div> */
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="NbPoint" class="col-sm-2 control-label">NbPoint</label>*/
/*                             {{ form_errors(form.NbPoint) }}*/
/*                             <div class="col-sm-10">*/
/*                                 {{ form_widget(form.NbPoint)  }}*/
/*                             </div></div> */
/* */
/*                         <div class="form-group">*/
/*                             <label for="Panier" class="col-sm-2 control-label">Panier</label>*/
/*                             {{ form_errors(form.Panier) }}*/
/*                             <div class="col-sm-10">*/
/*                                 {{ form_widget(form.Panier)  }}*/
/*                             </div> </div>*/
/* */
/* */
/* */
/* */
/*                         <div class="box-footer">*/
/*                             {{form_widget(form.AjouterProduit) }}*/
/*                         </div>*/
/*                         {{ form_end(form)}}*/
/* */
/* */
/*                     </div>*/
/*                 </form>*/
/*             </div>*/
/*         </div>*/
/*     </div><!-- /.box -->*/
/* */
/* */
/* {% endblock %}*/
