<?php

/* TunisiaMallProduitBundle:Produit:RechercherProduit.html.twig */
class __TwigTemplate_6f81f8632504219597bb11153a8bad82d29e7b9de645bb412ec026ce7a30a2c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "TunisiaMallProduitBundle:Produit:RechercherProduit.html.twig", 1);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 4
        echo "               
            <h1>
            Gestion Des Produits
            <small>Liste Des Produits</small>
          </h1>
            ";
    }

    // line 13
    public function block_TableView($context, array $blocks = array())
    {
        // line 14
        echo "   
<div class=\"TableView\" >
    <table border=\"0\">
    <form method=\"POST\">
  <tr>
    <td>Référence:</td>
      <td><input type=\"text\" name=\"Reference\"></td><td><input class=\"btn\" type=\"submit\" value=\"Rechercher\"></td> 
  </tr>
   <tr>
       
\t </tr>
    </form>
</table>   
    <br>
    <br>
    <table >
                    <tr>
                      
                        <td>
                            Id Produit
                        </td>
                        <td >
                            Référence
                        </td>
                        <td>
                            Libelle
                        </td>
                        <td>
                            Descriptif
                        </td>
                        <td>
                            Prix
                        </td>
                        <td>
                            Categorie
                        </td>
                        <td>
                            Image
                        </td>
                        <td>
                            Note
                        </td>
                        <td>
                            NbPoint
                        </td>
                        <td>
                            Enseigne
                        </td>
                        <td>
                            Promotion
                        </td>
                        <td>
                            Catalogue
                        </td>
                        <td>
                            Panier
                        </td>
                        <td>
                            Modification
                        </td>
                        <td>
                            Suppression
                        </td>
                       
                    </tr>
                    <tr>
                        ";
        // line 80
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["Produit"]);
        foreach ($context['_seq'] as $context["_key"] => $context["Produit"]) {
            // line 81
            echo "                              
                                
                        <td >
                           ";
            // line 84
            echo twig_escape_filter($this->env, $this->getAttribute($context["Produit"], "id", array()), "html", null, true);
            echo "
                        </td>
                        <td>
                            ";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($context["Produit"], "Reference", array()), "html", null, true);
            echo "
                        </td>
                        <td>
                            ";
            // line 90
            echo twig_escape_filter($this->env, $this->getAttribute($context["Produit"], "Libelle", array()), "html", null, true);
            echo "
                        </td>
                         <td>
                            ";
            // line 93
            echo twig_escape_filter($this->env, $this->getAttribute($context["Produit"], "Descriptif", array()), "html", null, true);
            echo "
                        </td>
                         <td >
                           ";
            // line 96
            echo twig_escape_filter($this->env, $this->getAttribute($context["Produit"], "Prix", array()), "html", null, true);
            echo "
                        </td>
                         <td >
                           ";
            // line 99
            echo twig_escape_filter($this->env, $this->getAttribute($context["Produit"], "Categorie", array()), "html", null, true);
            echo "
                        </td>
                         <td >
                           ";
            // line 102
            echo twig_escape_filter($this->env, $this->getAttribute($context["Produit"], "Image", array()), "html", null, true);
            echo "
                        </td>
                         <td >
                           ";
            // line 105
            echo twig_escape_filter($this->env, $this->getAttribute($context["Produit"], "Note", array()), "html", null, true);
            echo "
                        </td>
                         <td >
                           ";
            // line 108
            echo twig_escape_filter($this->env, $this->getAttribute($context["Produit"], "NbPoint", array()), "html", null, true);
            echo "
                        </td>
                         <td >
                           ";
            // line 111
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["Produit"], "Enseigne", array()), "getId", array(), "method"), "html", null, true);
            echo "
                        </td>
                         <td >
                           ";
            // line 114
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["Produit"], "Promotion", array()), "getId", array(), "method"), "html", null, true);
            echo "
                        </td>
                         <td >
                           ";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["Produit"], "Catalogue", array()), "getId", array(), "method"), "html", null, true);
            echo "
                        </td>
                         <td >
                           ";
            // line 120
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["Produit"], "Panier", array()), "getId", array(), "method"), "html", null, true);
            echo "
                        </td>
                         <td >
                          <center><input class=\"modifIcon\" type=\"image\"  src=\"";
            // line 123
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Modifier.png"), "html", null, true);
            echo " \" value=\"\" href=\"#\" ></center>
                         
                        </td>
                         <td >
                         <center><a href=\"";
            // line 127
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tunisia_mall_produit_Supp", array("id" => $this->getAttribute($context["Produit"], "id", array()))), "html", null, true);
            echo "\"><input class=\"modifIcon\" type=\"image\"  src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Supprimer.png"), "html", null, true);
            echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                    </tr>
                    
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['Produit'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 132
        echo "                </table>
            </div>
    ";
    }

    // line 135
    public function block_btn($context, array $blocks = array())
    {
        // line 136
        echo "     <br>
                <div>
                    <form action=\"";
        // line 138
        echo "AjouterProduit";
        echo "\">
                    <input type=\"submit\" class=\"btn\" value=\"Ajouter Produit\"/>
                    </form>
                    
    ";
    }

    public function getTemplateName()
    {
        return "TunisiaMallProduitBundle:Produit:RechercherProduit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  233 => 138,  229 => 136,  226 => 135,  220 => 132,  207 => 127,  200 => 123,  194 => 120,  188 => 117,  182 => 114,  176 => 111,  170 => 108,  164 => 105,  158 => 102,  152 => 99,  146 => 96,  140 => 93,  134 => 90,  128 => 87,  122 => 84,  117 => 81,  113 => 80,  45 => 14,  42 => 13,  33 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* */
/*  {% block HeaderTitle %}*/
/*                */
/*             <h1>*/
/*             Gestion Des Produits*/
/*             <small>Liste Des Produits</small>*/
/*           </h1>*/
/*             {%endblock %}*/
/* */
/* */
/* */
/* {% block TableView %}*/
/*    */
/* <div class="TableView" >*/
/*     <table border="0">*/
/*     <form method="POST">*/
/*   <tr>*/
/*     <td>Référence:</td>*/
/*       <td><input type="text" name="Reference"></td><td><input class="btn" type="submit" value="Rechercher"></td> */
/*   </tr>*/
/*    <tr>*/
/*        */
/* 	 </tr>*/
/*     </form>*/
/* </table>   */
/*     <br>*/
/*     <br>*/
/*     <table >*/
/*                     <tr>*/
/*                       */
/*                         <td>*/
/*                             Id Produit*/
/*                         </td>*/
/*                         <td >*/
/*                             Référence*/
/*                         </td>*/
/*                         <td>*/
/*                             Libelle*/
/*                         </td>*/
/*                         <td>*/
/*                             Descriptif*/
/*                         </td>*/
/*                         <td>*/
/*                             Prix*/
/*                         </td>*/
/*                         <td>*/
/*                             Categorie*/
/*                         </td>*/
/*                         <td>*/
/*                             Image*/
/*                         </td>*/
/*                         <td>*/
/*                             Note*/
/*                         </td>*/
/*                         <td>*/
/*                             NbPoint*/
/*                         </td>*/
/*                         <td>*/
/*                             Enseigne*/
/*                         </td>*/
/*                         <td>*/
/*                             Promotion*/
/*                         </td>*/
/*                         <td>*/
/*                             Catalogue*/
/*                         </td>*/
/*                         <td>*/
/*                             Panier*/
/*                         </td>*/
/*                         <td>*/
/*                             Modification*/
/*                         </td>*/
/*                         <td>*/
/*                             Suppression*/
/*                         </td>*/
/*                        */
/*                     </tr>*/
/*                     <tr>*/
/*                         {% for Produit in Produit %}*/
/*                               */
/*                                 */
/*                         <td >*/
/*                            {{Produit.id}}*/
/*                         </td>*/
/*                         <td>*/
/*                             {{Produit.Reference}}*/
/*                         </td>*/
/*                         <td>*/
/*                             {{Produit.Libelle}}*/
/*                         </td>*/
/*                          <td>*/
/*                             {{Produit.Descriptif}}*/
/*                         </td>*/
/*                          <td >*/
/*                            {{Produit.Prix}}*/
/*                         </td>*/
/*                          <td >*/
/*                            {{Produit.Categorie}}*/
/*                         </td>*/
/*                          <td >*/
/*                            {{Produit.Image}}*/
/*                         </td>*/
/*                          <td >*/
/*                            {{Produit.Note}}*/
/*                         </td>*/
/*                          <td >*/
/*                            {{Produit.NbPoint}}*/
/*                         </td>*/
/*                          <td >*/
/*                            {{Produit.Enseigne.getId()}}*/
/*                         </td>*/
/*                          <td >*/
/*                            {{Produit.Promotion.getId()}}*/
/*                         </td>*/
/*                          <td >*/
/*                            {{Produit.Catalogue.getId()}}*/
/*                         </td>*/
/*                          <td >*/
/*                            {{Produit.Panier.getId()}}*/
/*                         </td>*/
/*                          <td >*/
/*                           <center><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Modifier.png')}} " value="" href="#" ></center>*/
/*                          */
/*                         </td>*/
/*                          <td >*/
/*                          <center><a href="{{path('tunisia_mall_produit_Supp',{'id':Produit.id})}}"><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Supprimer.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                     </tr>*/
/*                     */
/*                     {% endfor %}*/
/*                 </table>*/
/*             </div>*/
/*     {% endblock TableView %}    */
/* {% block btn %}*/
/*      <br>*/
/*                 <div>*/
/*                     <form action="{{('AjouterProduit')}}">*/
/*                     <input type="submit" class="btn" value="Ajouter Produit"/>*/
/*                     </form>*/
/*                     */
/*     {% endblock %}*/
/*     */
/*         */
