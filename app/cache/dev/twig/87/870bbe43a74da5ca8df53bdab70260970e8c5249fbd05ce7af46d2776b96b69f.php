<?php

/* ResponsableBundle::Layout.html.twig */
class __TwigTemplate_23fc1afe6b89f86b81d21369234ebe7735e759f57cd16a89534ba458036bfae5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
  <head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <title>Responsable d'Enseigne</title>
    <link rel=\"SHORTCUT ICON\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/client/faviconb7bb.ico?_build=1454497437"), "html", null, true);
        echo "\" type=\"image/vnd.microsoft.icon')\" />
        <link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/Responsable/skins/TableView.css"), "html", null, true);
        echo "\">
    <link rel=\"SHORTCUT ICON\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/faviconb7bb.ico?_build=1454497437"), "html", null, true);
        echo "\" type=\"image/vnd.microsoft.icon')\" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
    <!-- Bootstrap 3.3.5 -->
    <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/Responsable/bootstrap.min.css"), "html", null, true);
        echo "\">
    
    <!-- Font Awesome -->
    <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"), "html", null, true);
        echo "\">
    <!-- Ionicons -->
    <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"), "html", null, true);
        echo "\">
    <!-- Theme style -->
    <link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/Responsable/AdminLTE.min.css"), "html", null, true);
        echo "\">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel=\"stylesheet\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/Responsable/skins/_all-skins.min.css"), "html", null, true);
        echo "\">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js\"></script>
        <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class=\"hold-transition skin-red sidebar-mini\">
    <div class=\"wrapper\">

      <!-- Main Header -->
      <header class=\"main-header\">

        <!-- Logo -->
        <a href=\"";
        // line 61
        echo "Responsable";
        echo "\" class=\"logo\">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class=\"logo-mini\"><b>T</b>MALL</span>
          <!-- logo for regular state and mobile devices -->
          <span class=\"logo-lg\"><b>Tunisia</b>Mall</span>
        </a>

        <!-- Header Navbar -->
        <nav class=\"navbar navbar-static-top\" role=\"navigation\">
          <!-- Sidebar toggle button-->
          
          <!-- Navbar Right Menu -->
          <div class=\"navbar-custom-menu\">
            <ul class=\"nav navbar-nav\">
              <!-- Messages: style can be found in dropdown.less-->
              <li class=\"dropdown messages-menu\">
                <!-- Menu toggle button -->
                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                  <i class=\"fa fa-envelope-o\"></i>
                  <span class=\"label label-success\">4</span>
                </a>
                <ul class=\"dropdown-menu\">
                  <li class=\"header\">You have 4 messages</li>
                  <li>
                    <!-- inner menu: contains the messages -->
                    <ul class=\"menu\">
                      <li><!-- start message -->
                        <a href=\"#\">
                          <div class=\"pull-left\">
                            <!-- User Image -->
                            <img src=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/user2-160x160.jpg"), "html", null, true);
        echo "\" class=\"img-circle\" alt=\"User Image\">
                          </div>
                          <!-- Message title and timestamp -->
                          <h4>
                            Support Team
                            <small><i class=\"fa fa-clock-o\"></i> 5 mins</small>
                          </h4>
                          <!-- The message -->
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li><!-- end message -->
                    </ul><!-- /.menu -->
                  </li>
                  <li class=\"footer\"><a href=\"#\">See All Messages</a></li>
                </ul>
              </li><!-- /.messages-menu -->

              <!-- Notifications Menu -->
              <li class=\"dropdown notifications-menu\">
                <!-- Menu toggle button -->
                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                  <i class=\"fa fa-bell-o\"></i>
                  <span class=\"label label-warning\">10</span>
                </a>
                <ul class=\"dropdown-menu\">
                  <li class=\"header\">You have 10 notifications</li>
                  <li>
                    <!-- Inner Menu: contains the notifications -->
                    <ul class=\"menu\">
                      <li><!-- start notification -->
                        <a href=\"#\">
                          <i class=\"fa fa-users text-aqua\"></i> 5 new members joined today
                        </a>
                      </li><!-- end notification -->
                    </ul>
                  </li>
                  <li class=\"footer\"><a href=\"#\">View all</a></li>
                </ul>
              </li>
              <!-- Tasks Menu -->
              <li class=\"dropdown tasks-menu\">
                <!-- Menu Toggle Button -->
                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                  <i class=\"fa fa-flag-o\"></i>
                  <span class=\"label label-danger\">9</span>
                </a>
                <ul class=\"dropdown-menu\">
                  <li class=\"header\">You have 9 tasks</li>
                  <li>
                    <!-- Inner menu: contains the tasks -->
                    <ul class=\"menu\">
                      <li><!-- Task item -->
                        <a href=\"#\">
                          <!-- Task title and progress text -->
                          <h3>
                            Design some buttons
                            <small class=\"pull-right\">20%</small>
                          </h3>
                          <!-- The progress bar -->
                          <div class=\"progress xs\">
                            <!-- Change the css width attribute to simulate progress -->
                            <div class=\"progress-bar progress-bar-aqua\" style=\"width: 20%\" role=\"progressbar\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\">
                              <span class=\"sr-only\">20% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                    </ul>
                  </li>
                  <li class=\"footer\">
                    <a href=\"#\">View all tasks</a>
                  </li>
                </ul>
              </li>
              <!-- User Account Menu -->
              <li class=\"dropdown user user-menu\">
                <!-- Menu Toggle Button -->
                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                  <!-- The user image in the navbar-->
                  <img src=\"";
        // line 170
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/user2-160x160.png"), "html", null, true);
        echo "\" class=\"user-image\" alt=\"User Image\">
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class=\"hidden-xs\">Responsable d'Enseigne</span>
                </a>
                <ul class=\"dropdown-menu\">
                  <!-- The user image in the menu -->
                  <li class=\"user-header\">
                    <img src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/user2-160x160.jpg"), "html", null, true);
        echo "\" class=\"img-circle\" alt=\"User Image\">
                    <p>
\t\t\t\t\t\tDevPremuim- Web Developer
                      <small>Member since Nov. 2016</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class=\"user-body\">
                    <div class=\"col-xs-4 text-center\">
                      <a href=\"#\">Followers</a>
                    </div>
                    <div class=\"col-xs-4 text-center\">
                      <a href=\"#\">Sales</a>
                    </div>
                    <div class=\"col-xs-4 text-center\">
                      <a href=\"#\">Friends</a>
                    </div>
                  </li>
                  <!-- Menu Footer-->
                  <li class=\"user-footer\">
                    <div class=\"pull-left\">
                      <a href=\"#\" class=\"btn btn-default btn-flat\">Profile</a>
                    </div>
                    <div class=\"pull-right\">
                      <a href=\"#\" class=\"btn btn-default btn-flat btn-success\">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class=\"main-sidebar\">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class=\"sidebar\">

          <!-- Sidebar user panel (optional) -->
          <div class=\"user-panel\">
            <div class=\"pull-left image\">
              <img src=\"";
        // line 221
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/user2-160x160.png"), "html", null, true);
        echo "\" class=\"img-circle\" alt=\"User Image\">
            </div>
            <div class=\"pull-left info\">
              <p>Responsable d'Enseigne</p>
              <!-- Status -->
              <a href=\"#\"><i class=\"fa fa-circle text-success\"></i>En ligne</a>
            </div>
          </div>

          <!-- search form (Optional) -->
          <form action=\"#\" method=\"get\" class=\"sidebar-form\">
            <div class=\"input-group\">
              <input type=\"text\" name=\"q\" class=\"form-control\" placeholder=\"Search...\">
              <span class=\"input-group-btn\">
                <button type=\"submit\" name=\"search\" id=\"search-btn\" class=\"btn btn-flat\"><i class=\"fa fa-search\"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->

          <!-- Sidebar Menu -->
          <ul class=\"sidebar-menu\">
            <li class=\"header\">LISTE</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href=\"";
        // line 245
        echo "Comptes";
        echo "\"><i class=\"fa fa-link\"></i> <span>Gestion des comptes</span> <small class=\"label pull-right bg-red\">";
        echo twig_escape_filter($this->env, (isset($context["clientsCount"]) ? $context["clientsCount"] : $this->getContext($context, "clientsCount")), "html", null, true);
        echo "</small></a></li>
            <li><a href=\"";
        // line 246
        echo "Produits";
        echo "\"><i class=\"fa fa-link\"></i> <span>Gestion des produits</span></a></li>
            <li><a href=\"";
        // line 247
        echo "Stock";
        echo "\"><i class=\"fa fa-link\"></i> <span>Gestion du Stock</span></a></li>
            <li><a href=\"";
        // line 248
        echo "Enseignes";
        echo "\"><i class=\"fa fa-link\"></i> <span>Gestion des enseignes</span></a></li>
\t    <li><a href=\"";
        // line 249
        echo "Pub";
        echo "\"><i class=\"fa fa-link\"></i> <span>Gestion des packs  publicitaire</span></a></li>
            <li><a href=\"#\"><i class=\"fa fa-link\"></i> <span>Gestion des promotion</span><i class=\"fa fa-angle-left pull-right\"></i></a>
               <ul class=\"treeview-menu\">
                <li><a href=\"#\">Liste des promotions</a></li>
\t        <li><a href=\"AjouterPromotion\">Ajouter des promotions</a></li>
                <li><a href=\"#\">Modifier des promotions</a></li>
              </ul>
            </li>
\t    <li><a href=\"";
        // line 257
        echo "Catalogues";
        echo "\"><i class=\"fa fa-link\"></i> <span>Gestion des catalogues</span></a></li>
            <li><a href=\"";
        // line 258
        echo "Stat";
        echo "\"><i class=\"fa fa-link\"></i> <span>Statistiques</span></a></li>
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>
            

      <!-- Content Wrapper. Contains page content -->
      <div class=\"content-wrapper\">
        <!-- Content Header (Page header) -->
        <section class=\"content-header\">
            ";
        // line 269
        $this->displayBlock('HeaderTitle', $context, $blocks);
        // line 273
        echo "        
        </section>

        <!-- Main content -->
        <section class=\"content\">
     ";
        // line 278
        $this->displayBlock('TableView', $context, $blocks);
        // line 282
        echo "          
          
          ";
        // line 284
        $this->displayBlock('btn', $context, $blocks);
        // line 287
        echo "        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <footer class=\"main-footer\">
        <!-- To the right -->
        
        <!-- Default to the left -->
        <strong>Copyright &copy; 2015 <a href=\"#\">Tunisia Mall</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      <!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class=\"control-sidebar-bg\"></div>
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src=\"js/Responsable/jQuery/jQuery-2.1.4.min.js\"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src=\"js/Responsable/bootstrap.min.js\"></script>
    <!-- AdminLTE App -->
    <script src=\"js/Responsable/app.min.js\"></script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->
  </body>
</html>
";
        // line 322
        echo "
";
    }

    // line 269
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 270
        echo "               
        
            ";
    }

    // line 278
    public function block_TableView($context, array $blocks = array())
    {
        // line 279
        echo "         
          <!-- Your Page Content Here -->
          ";
    }

    // line 284
    public function block_btn($context, array $blocks = array())
    {
        // line 285
        echo "    
    ";
    }

    public function getTemplateName()
    {
        return "ResponsableBundle::Layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  432 => 285,  429 => 284,  423 => 279,  420 => 278,  414 => 270,  411 => 269,  406 => 322,  371 => 287,  369 => 284,  365 => 282,  363 => 278,  356 => 273,  354 => 269,  340 => 258,  336 => 257,  325 => 249,  321 => 248,  317 => 247,  313 => 246,  307 => 245,  280 => 221,  233 => 177,  223 => 170,  141 => 91,  108 => 61,  69 => 25,  61 => 20,  56 => 18,  51 => 16,  45 => 13,  38 => 9,  34 => 8,  30 => 7,  22 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*   <head>*/
/*     <meta charset="utf-8">*/
/*     <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*     <title>Responsable d'Enseigne</title>*/
/*     <link rel="SHORTCUT ICON" href="{{asset('images/client/faviconb7bb.ico?_build=1454497437')}}" type="image/vnd.microsoft.icon')" />*/
/*         <link rel="stylesheet" href="{{asset('css/Responsable/skins/TableView.css')}}">*/
/*     <link rel="SHORTCUT ICON" href="{{asset('images/Responsable/faviconb7bb.ico?_build=1454497437')}}" type="image/vnd.microsoft.icon')" />*/
/*     <!-- Tell the browser to be responsive to screen width -->*/
/*     <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">*/
/*     <!-- Bootstrap 3.3.5 -->*/
/*     <link rel="stylesheet" href="{{asset('css/Responsable/bootstrap.min.css')}}">*/
/*     */
/*     <!-- Font Awesome -->*/
/*     <link rel="stylesheet" href="{{asset('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css')}}">*/
/*     <!-- Ionicons -->*/
/*     <link rel="stylesheet" href="{{asset('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}">*/
/*     <!-- Theme style -->*/
/*     <link rel="stylesheet" href="{{asset('css/Responsable/AdminLTE.min.css')}}">*/
/*     <!-- AdminLTE Skins. We have chosen the skin-blue for this starter*/
/*           page. However, you can choose any other skin. Make sure you*/
/*           apply the skin class to the body tag so the changes take effect.*/
/*     -->*/
/*     <link rel="stylesheet" href="{{asset('css/Responsable/skins/_all-skins.min.css')}}">*/
/* */
/*     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->*/
/*     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->*/
/*     <!--[if lt IE 9]>*/
/*         <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>*/
/*         <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>*/
/*     <![endif]-->*/
/*   </head>*/
/*   <!--*/
/*   BODY TAG OPTIONS:*/
/*   =================*/
/*   Apply one or more of the following classes to get the*/
/*   desired effect*/
/*   |---------------------------------------------------------|*/
/*   | SKINS         | skin-blue                               |*/
/*   |               | skin-black                              |*/
/*   |               | skin-purple                             |*/
/*   |               | skin-yellow                             |*/
/*   |               | skin-red                                |*/
/*   |               | skin-green                              |*/
/*   |---------------------------------------------------------|*/
/*   |LAYOUT OPTIONS | fixed                                   |*/
/*   |               | layout-boxed                            |*/
/*   |               | layout-top-nav                          |*/
/*   |               | sidebar-collapse                        |*/
/*   |               | sidebar-mini                            |*/
/*   |---------------------------------------------------------|*/
/*   -->*/
/*   <body class="hold-transition skin-red sidebar-mini">*/
/*     <div class="wrapper">*/
/* */
/*       <!-- Main Header -->*/
/*       <header class="main-header">*/
/* */
/*         <!-- Logo -->*/
/*         <a href="{{'Responsable'}}" class="logo">*/
/*           <!-- mini logo for sidebar mini 50x50 pixels -->*/
/*           <span class="logo-mini"><b>T</b>MALL</span>*/
/*           <!-- logo for regular state and mobile devices -->*/
/*           <span class="logo-lg"><b>Tunisia</b>Mall</span>*/
/*         </a>*/
/* */
/*         <!-- Header Navbar -->*/
/*         <nav class="navbar navbar-static-top" role="navigation">*/
/*           <!-- Sidebar toggle button-->*/
/*           */
/*           <!-- Navbar Right Menu -->*/
/*           <div class="navbar-custom-menu">*/
/*             <ul class="nav navbar-nav">*/
/*               <!-- Messages: style can be found in dropdown.less-->*/
/*               <li class="dropdown messages-menu">*/
/*                 <!-- Menu toggle button -->*/
/*                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">*/
/*                   <i class="fa fa-envelope-o"></i>*/
/*                   <span class="label label-success">4</span>*/
/*                 </a>*/
/*                 <ul class="dropdown-menu">*/
/*                   <li class="header">You have 4 messages</li>*/
/*                   <li>*/
/*                     <!-- inner menu: contains the messages -->*/
/*                     <ul class="menu">*/
/*                       <li><!-- start message -->*/
/*                         <a href="#">*/
/*                           <div class="pull-left">*/
/*                             <!-- User Image -->*/
/*                             <img src="{{asset('images/Responsable/user2-160x160.jpg')}}" class="img-circle" alt="User Image">*/
/*                           </div>*/
/*                           <!-- Message title and timestamp -->*/
/*                           <h4>*/
/*                             Support Team*/
/*                             <small><i class="fa fa-clock-o"></i> 5 mins</small>*/
/*                           </h4>*/
/*                           <!-- The message -->*/
/*                           <p>Why not buy a new awesome theme?</p>*/
/*                         </a>*/
/*                       </li><!-- end message -->*/
/*                     </ul><!-- /.menu -->*/
/*                   </li>*/
/*                   <li class="footer"><a href="#">See All Messages</a></li>*/
/*                 </ul>*/
/*               </li><!-- /.messages-menu -->*/
/* */
/*               <!-- Notifications Menu -->*/
/*               <li class="dropdown notifications-menu">*/
/*                 <!-- Menu toggle button -->*/
/*                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">*/
/*                   <i class="fa fa-bell-o"></i>*/
/*                   <span class="label label-warning">10</span>*/
/*                 </a>*/
/*                 <ul class="dropdown-menu">*/
/*                   <li class="header">You have 10 notifications</li>*/
/*                   <li>*/
/*                     <!-- Inner Menu: contains the notifications -->*/
/*                     <ul class="menu">*/
/*                       <li><!-- start notification -->*/
/*                         <a href="#">*/
/*                           <i class="fa fa-users text-aqua"></i> 5 new members joined today*/
/*                         </a>*/
/*                       </li><!-- end notification -->*/
/*                     </ul>*/
/*                   </li>*/
/*                   <li class="footer"><a href="#">View all</a></li>*/
/*                 </ul>*/
/*               </li>*/
/*               <!-- Tasks Menu -->*/
/*               <li class="dropdown tasks-menu">*/
/*                 <!-- Menu Toggle Button -->*/
/*                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">*/
/*                   <i class="fa fa-flag-o"></i>*/
/*                   <span class="label label-danger">9</span>*/
/*                 </a>*/
/*                 <ul class="dropdown-menu">*/
/*                   <li class="header">You have 9 tasks</li>*/
/*                   <li>*/
/*                     <!-- Inner menu: contains the tasks -->*/
/*                     <ul class="menu">*/
/*                       <li><!-- Task item -->*/
/*                         <a href="#">*/
/*                           <!-- Task title and progress text -->*/
/*                           <h3>*/
/*                             Design some buttons*/
/*                             <small class="pull-right">20%</small>*/
/*                           </h3>*/
/*                           <!-- The progress bar -->*/
/*                           <div class="progress xs">*/
/*                             <!-- Change the css width attribute to simulate progress -->*/
/*                             <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">*/
/*                               <span class="sr-only">20% Complete</span>*/
/*                             </div>*/
/*                           </div>*/
/*                         </a>*/
/*                       </li><!-- end task item -->*/
/*                     </ul>*/
/*                   </li>*/
/*                   <li class="footer">*/
/*                     <a href="#">View all tasks</a>*/
/*                   </li>*/
/*                 </ul>*/
/*               </li>*/
/*               <!-- User Account Menu -->*/
/*               <li class="dropdown user user-menu">*/
/*                 <!-- Menu Toggle Button -->*/
/*                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">*/
/*                   <!-- The user image in the navbar-->*/
/*                   <img src="{{asset('images/Responsable/user2-160x160.png')}}" class="user-image" alt="User Image">*/
/*                   <!-- hidden-xs hides the username on small devices so only the image appears. -->*/
/*                   <span class="hidden-xs">Responsable d'Enseigne</span>*/
/*                 </a>*/
/*                 <ul class="dropdown-menu">*/
/*                   <!-- The user image in the menu -->*/
/*                   <li class="user-header">*/
/*                     <img src="{{asset('images/Responsable/user2-160x160.jpg')}}" class="img-circle" alt="User Image">*/
/*                     <p>*/
/* 						DevPremuim- Web Developer*/
/*                       <small>Member since Nov. 2016</small>*/
/*                     </p>*/
/*                   </li>*/
/*                   <!-- Menu Body -->*/
/*                   <li class="user-body">*/
/*                     <div class="col-xs-4 text-center">*/
/*                       <a href="#">Followers</a>*/
/*                     </div>*/
/*                     <div class="col-xs-4 text-center">*/
/*                       <a href="#">Sales</a>*/
/*                     </div>*/
/*                     <div class="col-xs-4 text-center">*/
/*                       <a href="#">Friends</a>*/
/*                     </div>*/
/*                   </li>*/
/*                   <!-- Menu Footer-->*/
/*                   <li class="user-footer">*/
/*                     <div class="pull-left">*/
/*                       <a href="#" class="btn btn-default btn-flat">Profile</a>*/
/*                     </div>*/
/*                     <div class="pull-right">*/
/*                       <a href="#" class="btn btn-default btn-flat btn-success">Sign out</a>*/
/*                     </div>*/
/*                   </li>*/
/*                 </ul>*/
/*               </li>*/
/*               <!-- Control Sidebar Toggle Button -->*/
/*               */
/*             </ul>*/
/*           </div>*/
/*         </nav>*/
/*       </header>*/
/*       <!-- Left side column. contains the logo and sidebar -->*/
/*       <aside class="main-sidebar">*/
/* */
/*         <!-- sidebar: style can be found in sidebar.less -->*/
/*         <section class="sidebar">*/
/* */
/*           <!-- Sidebar user panel (optional) -->*/
/*           <div class="user-panel">*/
/*             <div class="pull-left image">*/
/*               <img src="{{asset('images/Responsable/user2-160x160.png')}}" class="img-circle" alt="User Image">*/
/*             </div>*/
/*             <div class="pull-left info">*/
/*               <p>Responsable d'Enseigne</p>*/
/*               <!-- Status -->*/
/*               <a href="#"><i class="fa fa-circle text-success"></i>En ligne</a>*/
/*             </div>*/
/*           </div>*/
/* */
/*           <!-- search form (Optional) -->*/
/*           <form action="#" method="get" class="sidebar-form">*/
/*             <div class="input-group">*/
/*               <input type="text" name="q" class="form-control" placeholder="Search...">*/
/*               <span class="input-group-btn">*/
/*                 <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>*/
/*               </span>*/
/*             </div>*/
/*           </form>*/
/*           <!-- /.search form -->*/
/* */
/*           <!-- Sidebar Menu -->*/
/*           <ul class="sidebar-menu">*/
/*             <li class="header">LISTE</li>*/
/*             <!-- Optionally, you can add icons to the links -->*/
/*             <li><a href="{{('Comptes')}}"><i class="fa fa-link"></i> <span>Gestion des comptes</span> <small class="label pull-right bg-red">{{clientsCount}}</small></a></li>*/
/*             <li><a href="{{('Produits')}}"><i class="fa fa-link"></i> <span>Gestion des produits</span></a></li>*/
/*             <li><a href="{{('Stock')}}"><i class="fa fa-link"></i> <span>Gestion du Stock</span></a></li>*/
/*             <li><a href="{{('Enseignes')}}"><i class="fa fa-link"></i> <span>Gestion des enseignes</span></a></li>*/
/* 	    <li><a href="{{('Pub')}}"><i class="fa fa-link"></i> <span>Gestion des packs  publicitaire</span></a></li>*/
/*             <li><a href="#"><i class="fa fa-link"></i> <span>Gestion des promotion</span><i class="fa fa-angle-left pull-right"></i></a>*/
/*                <ul class="treeview-menu">*/
/*                 <li><a href="#">Liste des promotions</a></li>*/
/* 	        <li><a href="AjouterPromotion">Ajouter des promotions</a></li>*/
/*                 <li><a href="#">Modifier des promotions</a></li>*/
/*               </ul>*/
/*             </li>*/
/* 	    <li><a href="{{('Catalogues')}}"><i class="fa fa-link"></i> <span>Gestion des catalogues</span></a></li>*/
/*             <li><a href="{{('Stat')}}"><i class="fa fa-link"></i> <span>Statistiques</span></a></li>*/
/*           </ul><!-- /.sidebar-menu -->*/
/*         </section>*/
/*         <!-- /.sidebar -->*/
/*       </aside>*/
/*             */
/* */
/*       <!-- Content Wrapper. Contains page content -->*/
/*       <div class="content-wrapper">*/
/*         <!-- Content Header (Page header) -->*/
/*         <section class="content-header">*/
/*             {% block HeaderTitle %}*/
/*                */
/*         */
/*             {%endblock %}*/
/*         */
/*         </section>*/
/* */
/*         <!-- Main content -->*/
/*         <section class="content">*/
/*      {% block TableView %}*/
/*          */
/*           <!-- Your Page Content Here -->*/
/*           {% endblock TableView %}*/
/*           */
/*           */
/*           {% block btn %}*/
/*     */
/*     {% endblock %}*/
/*         </section><!-- /.content -->*/
/*       </div><!-- /.content-wrapper -->*/
/* */
/*       <!-- Main Footer -->*/
/*       <footer class="main-footer">*/
/*         <!-- To the right -->*/
/*         */
/*         <!-- Default to the left -->*/
/*         <strong>Copyright &copy; 2015 <a href="#">Tunisia Mall</a>.</strong> All rights reserved.*/
/*       </footer>*/
/* */
/*       <!-- Control Sidebar -->*/
/*       <!-- /.control-sidebar -->*/
/*       <!-- Add the sidebar's background. This div must be placed*/
/*            immediately after the control sidebar -->*/
/*       <div class="control-sidebar-bg"></div>*/
/*     </div><!-- ./wrapper -->*/
/* */
/*     <!-- REQUIRED JS SCRIPTS -->*/
/* */
/*     <!-- jQuery 2.1.4 -->*/
/*     <script src="js/Responsable/jQuery/jQuery-2.1.4.min.js"></script>*/
/*     <!-- Bootstrap 3.3.5 -->*/
/*     <script src="js/Responsable/bootstrap.min.js"></script>*/
/*     <!-- AdminLTE App -->*/
/*     <script src="js/Responsable/app.min.js"></script>*/
/* */
/*     <!-- Optionally, you can add Slimscroll and FastClick plugins.*/
/*          Both of these plugins are recommended to enhance the*/
/*          user experience. Slimscroll is required when using the*/
/*          fixed layout. -->*/
/*   </body>*/
/* </html>*/
/* {# empty Twig template #}*/
/* {# empty Twig template #}*/
/* */
/* */
