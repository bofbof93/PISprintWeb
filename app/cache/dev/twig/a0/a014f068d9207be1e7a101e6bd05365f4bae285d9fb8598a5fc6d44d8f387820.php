<?php

/* ClientBundle:Default:payement.html.twig */
class __TwigTemplate_e7ffef3240f32b5ce7e4c66e8ecdb94aa24b9cd63642df13e59680b224b34c50 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("ClientBundle::Layout.html.twig", "ClientBundle:Default:payement.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ClientBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"moto-widget moto-widget-menu moto-preset-default moto-align-center moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto\" data-preset=\"default\" data-widget=\"menu\" data-align=\"{&quot;desktop&quot;:&quot;center&quot;,&quot;tablet&quot;:&quot;&quot;,&quot;mobile-h&quot;:&quot;&quot;,&quot;mobile-v&quot;:&quot;&quot;}\" data-menu-id=\"1\" data-menu-type=\"horizontal\" data-spacing=\"mama\">
        <a href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\" class=\"moto-widget-menu-toggle-btn\"><i class=\"moto-widget-menu-toggle-btn-icon fa fa-bars\"></i></a>
    <ul class=\"moto-widget-menu-list moto-widget-menu-list_horizontal\">
        <li class=\"moto-widget-menu-item\">
    <a href=\"acceuil\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Acceuil</a>
    </li><li class=\"moto-widget-menu-item\">
    <a href=\"catalogue\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Catalogues<span class=\"fa moto-widget-menu-link-arrow\"></span></a>
                <ul class=\"moto-widget-menu-sublist\">
                    <li class=\"moto-widget-menu-item\">
    <a href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("testimonials/index.html"), "html", null, true);
        echo "\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-2 moto-link\"></a>
    </li>
    
                    <li class=\"moto-widget-menu-item\">
    <a href=\"#\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-2 moto-widget-menu-link-submenu moto-link\">Générale<!--<span class=\"fa moto-widget-menu-link-arrow\"></span></a>
                <!--<ul class=\"moto-widget-menu-sublist\">
                    <li class=\"moto-widget-menu-item\">
    <a href=\"#\"   data-action=\"blog.index\" class=\"moto-widget-menu-link moto-widget-menu-link-level-3 moto-link\">gallery1</a>
    </li>
                    <li class=\"moto-widget-menu-item\">
    <a href=\"#}\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-3 moto-link\">About us</a>
    </li>
            </ul>-->

    </li>
                    <li class=\"moto-widget-menu-item\">
    <a href=\"#\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-2 moto-link\">Par enseigne</a>
    </li>
            </ul>

    </li>
    <li class=\"moto-widget-menu-item\">
    <a href=\"gallerie\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-active moto-link\">Galleries</a>
    </li>
     <li class=\"moto-widget-menu-item\">
    <a href=\"fashion\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Actualité</a>
    </li>
    <li class=\"moto-widget-menu-item\">
    <a href=\"boutique\"   data-action=\"blog.index\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Nos Boutiques</a>
    </li><li class=\"moto-widget-menu-item\">

    <a href=\"contact\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Contacts</a>
    </li>  
    ";
        // line 46
        if (($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "getFlagRole", array(), "method") == 3)) {
            // line 47
            echo "                                                        <li class=\"moto-widget-menu-item\">
                                                            <a href=\"\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-link\">";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
            echo "</a>
                                                            &nbsp &nbsp &nbsp <a href=\"panier\"><img src=\"";
            // line 49
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/client/shopping-cart-of-checkered-design.png"), "html", null, true);
            echo "\"></a>
                                                        </li> 
                                                        ";
        } else {
            // line 52
            echo "                                                         <li class=\"moto-widget-menu-item\">
                                                            <a href=\"";
            // line 53
            echo $this->env->getExtension('routing')->getPath("fos_user_security_login");
            echo "\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-link\">connexion</a>
                                                            &nbsp &nbsp &nbsp <a href=\"panier\"><img src=\"";
            // line 54
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/client/shopping-cart-of-checkered-design.png"), "html", null, true);
            echo "\"></a>
                                                        </li> 
                                                        ";
        }
        // line 56
        echo "</ul>
    </div><div class=\"moto-widget moto-widget-spacer moto-preset-default                      moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto\" data-widget=\"spacer\" data-preset=\"default\" data-spacing=\"sasa\">
    <div class=\"moto-widget-spacer-block\" style=\"height: 2px;\"></div>
</div></div></div></div></div></div></div>        </header>
  <section id=\"section-content\" class=\"content page-1 moto-section\" data-widget=\"section\" data-container=\"section\">
                   
                                    <div class=\"row\">
                                        <div class=\"col-md-4\">
                                        </div>  
                                        <div class=\"col-md-4\">
                                            <table class=\"table table-bordered\">
                                        
                                        <tr>
                                      
                                        </tr>    
                                        ";
        // line 71
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
        echo "
                                            </table>
                                        </div>
                                            <a href=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("payement_carte_fidelite_confirm", array("id" => $this->getAttribute((isset($context["panier"]) ? $context["panier"] : $this->getContext($context, "panier")), "id", array()))), "html", null, true);
        echo "\">confirmer</a>
                                    </div>
                          
    </section>
    ";
    }

    public function getTemplateName()
    {
        return "ClientBundle:Default:payement.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 74,  126 => 71,  109 => 56,  103 => 54,  99 => 53,  96 => 52,  90 => 49,  86 => 48,  83 => 47,  81 => 46,  45 => 13,  34 => 5,  31 => 4,  28 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "ClientBundle::Layout.html.twig" %}*/
/* {% block contenu %}*/
/*     <div class="moto-widget moto-widget-menu moto-preset-default moto-align-center moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-preset="default" data-widget="menu" data-align="{&quot;desktop&quot;:&quot;center&quot;,&quot;tablet&quot;:&quot;&quot;,&quot;mobile-h&quot;:&quot;&quot;,&quot;mobile-v&quot;:&quot;&quot;}" data-menu-id="1" data-menu-type="horizontal" data-spacing="mama">*/
/*         <a href="{{asset('#')}}" class="moto-widget-menu-toggle-btn"><i class="moto-widget-menu-toggle-btn-icon fa fa-bars"></i></a>*/
/*     <ul class="moto-widget-menu-list moto-widget-menu-list_horizontal">*/
/*         <li class="moto-widget-menu-item">*/
/*     <a href="acceuil"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Acceuil</a>*/
/*     </li><li class="moto-widget-menu-item">*/
/*     <a href="catalogue"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Catalogues<span class="fa moto-widget-menu-link-arrow"></span></a>*/
/*                 <ul class="moto-widget-menu-sublist">*/
/*                     <li class="moto-widget-menu-item">*/
/*     <a href="{{asset('testimonials/index.html')}}"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-2 moto-link"></a>*/
/*     </li>*/
/*     */
/*                     <li class="moto-widget-menu-item">*/
/*     <a href="#"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-2 moto-widget-menu-link-submenu moto-link">Générale<!--<span class="fa moto-widget-menu-link-arrow"></span></a>*/
/*                 <!--<ul class="moto-widget-menu-sublist">*/
/*                     <li class="moto-widget-menu-item">*/
/*     <a href="#"   data-action="blog.index" class="moto-widget-menu-link moto-widget-menu-link-level-3 moto-link">gallery1</a>*/
/*     </li>*/
/*                     <li class="moto-widget-menu-item">*/
/*     <a href="#}"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-3 moto-link">About us</a>*/
/*     </li>*/
/*             </ul>-->*/
/* */
/*     </li>*/
/*                     <li class="moto-widget-menu-item">*/
/*     <a href="#"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-2 moto-link">Par enseigne</a>*/
/*     </li>*/
/*             </ul>*/
/* */
/*     </li>*/
/*     <li class="moto-widget-menu-item">*/
/*     <a href="gallerie"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-active moto-link">Galleries</a>*/
/*     </li>*/
/*      <li class="moto-widget-menu-item">*/
/*     <a href="fashion"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Actualité</a>*/
/*     </li>*/
/*     <li class="moto-widget-menu-item">*/
/*     <a href="boutique"   data-action="blog.index" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Nos Boutiques</a>*/
/*     </li><li class="moto-widget-menu-item">*/
/* */
/*     <a href="contact"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Contacts</a>*/
/*     </li>  */
/*     {% if user.getFlagRole()==3 %}*/
/*                                                         <li class="moto-widget-menu-item">*/
/*                                                             <a href=""   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link">{{user.username}}</a>*/
/*                                                             &nbsp &nbsp &nbsp <a href="panier"><img src="{{asset('images/client/shopping-cart-of-checkered-design.png')}}"></a>*/
/*                                                         </li> */
/*                                                         {% else %}*/
/*                                                          <li class="moto-widget-menu-item">*/
/*                                                             <a href="{{path('fos_user_security_login')}}"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link">connexion</a>*/
/*                                                             &nbsp &nbsp &nbsp <a href="panier"><img src="{{asset('images/client/shopping-cart-of-checkered-design.png')}}"></a>*/
/*                                                         </li> */
/*                                                         {% endif %}</ul>*/
/*     </div><div class="moto-widget moto-widget-spacer moto-preset-default                      moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="spacer" data-preset="default" data-spacing="sasa">*/
/*     <div class="moto-widget-spacer-block" style="height: 2px;"></div>*/
/* </div></div></div></div></div></div></div>        </header>*/
/*   <section id="section-content" class="content page-1 moto-section" data-widget="section" data-container="section">*/
/*                    */
/*                                     <div class="row">*/
/*                                         <div class="col-md-4">*/
/*                                         </div>  */
/*                                         <div class="col-md-4">*/
/*                                             <table class="table table-bordered">*/
/*                                         */
/*                                         <tr>*/
/*                                       */
/*                                         </tr>    */
/*                                         {{message}}*/
/*                                             </table>*/
/*                                         </div>*/
/*                                             <a href="{{path('payement_carte_fidelite_confirm',{'id':panier.id})}}">confirmer</a>*/
/*                                     </div>*/
/*                           */
/*     </section>*/
/*     {% endblock %}     */
