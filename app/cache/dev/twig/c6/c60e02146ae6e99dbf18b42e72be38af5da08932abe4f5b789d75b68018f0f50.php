<?php

/* ClientBundle:Default:panier.html.twig */
class __TwigTemplate_cd6dab38a89e356abb7934a0a5e4d8425022ae78ca6d9cafbb0a4afd63672b1a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("ClientBundle::Layout.html.twig", "ClientBundle:Default:panier.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ClientBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_contenu($context, array $blocks = array())
    {
        // line 6
        echo "                    <div class=\"moto-widget moto-widget-menu moto-preset-default moto-align-center moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto\" data-preset=\"default\" data-widget=\"menu\" data-align=\"{&quot;desktop&quot;:&quot;center&quot;,&quot;tablet&quot;:&quot;&quot;,&quot;mobile-h&quot;:&quot;&quot;,&quot;mobile-v&quot;:&quot;&quot;}\" data-menu-id=\"1\" data-menu-type=\"horizontal\" data-spacing=\"mama\">
        <a href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\" class=\"moto-widget-menu-toggle-btn\"><i class=\"moto-widget-menu-toggle-btn-icon fa fa-bars\"></i></a>
    <ul class=\"moto-widget-menu-list moto-widget-menu-list_horizontal\">
        <li class=\"moto-widget-menu-item\">
    <a href=\"acceuil\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Acceuil</a>
    </li><li class=\"moto-widget-menu-item\">
    <a href=\"catalogue\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Catalogues<span class=\"fa moto-widget-menu-link-arrow\"></span></a>
                <ul class=\"moto-widget-menu-sublist\">
                    <li class=\"moto-widget-menu-item\">
    <a href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("testimonials/index.html"), "html", null, true);
        echo "\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-2 moto-link\"></a>
    </li>
    
                    <li class=\"moto-widget-menu-item\">
    <a href=\"#\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-2 moto-widget-menu-link-submenu moto-link\">Générale<!--<span class=\"fa moto-widget-menu-link-arrow\"></span></a>
                <!--<ul class=\"moto-widget-menu-sublist\">
                    <li class=\"moto-widget-menu-item\">
    <a href=\"#\"   data-action=\"blog.index\" class=\"moto-widget-menu-link moto-widget-menu-link-level-3 moto-link\">gallery1</a>
    </li>
                    <li class=\"moto-widget-menu-item\">
    <a href=\"#}\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-3 moto-link\">About us</a>
    </li>
            </ul>-->

    </li>
                    <li class=\"moto-widget-menu-item\">
    <a href=\"#\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-2 moto-link\">Par enseigne</a>
    </li>
            </ul>

    </li>
    <li class=\"moto-widget-menu-item\">
    <a href=\"gallerie\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Galleries</a>
    </li>
     <li class=\"moto-widget-menu-item\">
    <a href=\"fashion\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-active moto-link\">Fashion</a>
    </li>
    <li class=\"moto-widget-menu-item\">
    <a href=\"boutique\"   data-action=\"blog.index\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Nos Boutiques</a>
    </li><li class=\"moto-widget-menu-item\">

    <a href=\"contact\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Contacts</a>
    </li>    
    
    
                                                        ";
        // line 50
        if ((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user"))) {
            // line 51
            echo "                                                            <li class=\"moto-widget-menu-item\">
                                                                
                                                                <a href=\"\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-link\">";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
            echo "</a>
                                                                &nbsp &nbsp &nbsp 
                                                                <div class=\"icon\">
                                                                    <a href=\"";
            // line 56
            echo $this->env->getExtension('routing')->getPath("tunisia_mall_client_panier");
            echo "\"> <span class=\"fa fa-5x fa-shopping-cart\" href=\"panier\"></span></a>
                                                                </div>    
                                                            </li> 
                                                        ";
        } else {
            // line 60
            echo "                                                            <li class=\"moto-widget-menu-item\">
                                                                <a href=\"";
            // line 61
            echo $this->env->getExtension('routing')->getPath("fos_user_security_login");
            echo "\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-link\">connexion</a>
                                                               
                                                            </li> 
                                                        ";
        }
        // line 65
        echo "    </ul>
    </div><div class=\"moto-widget moto-widget-spacer moto-preset-default                      moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto\" data-widget=\"spacer\" data-preset=\"default\" data-spacing=\"sasa\">
    <div class=\"moto-widget-spacer-block\" style=\"height: 2px;\"></div>
</div></div></div></div></div></div></div>        </header>
  <section id=\"section_panier\" style=\"min-height: 1200px;\" >
      <center><h1 id=\"title\">Recupltatif de commande </h1></center>
      
<script src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Responsable/jQuery/jQuery-2.1.4.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

                        <center>  <table  id=\"table_panier\">
                            
                           
                            <tr id=\"tr_panier\">
                               
                                <th id=\"th_panier\"></th>
                                <th id=\"th_panier\">Produit</th>
                                <th id=\"th_panier\">prix</th>
                                <th id=\"th_panier\">Quantite</th>
                                <th id=\"th_panier\">Total</th>
                                <th id=\"th_panier\">Acheter</th>
                                <th id=\"th_panier\">Supprimer</th>
                            </tr>
                            ";
        // line 87
        $context["somme"] = 0;
        // line 88
        echo "                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["panier"]) ? $context["panier"] : $this->getContext($context, "panier")), "achats", array()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["ligne"]) {
            // line 89
            echo "                            ";
            $context["somme"] = ((isset($context["somme"]) ? $context["somme"] : $this->getContext($context, "somme")) + $this->getAttribute($context["ligne"], "prixtotal", array()));
            // line 90
            echo "                            <tr id=\"tr_panier\">
                                <td id=\"td_panier\"><img src=\"";
            // line 91
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(twig_join_filter(array(0 => "images/panier/", 1 => $this->getAttribute($this->getAttribute($context["ligne"], "produit", array()), "Image", array())))), "html", null, true);
            echo "\" height=\"30\" width=\"50\"  alt=''/></td>
                                <td id=\"td_panier\">";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["ligne"], "produit", array()), "Libelle", array()), "html", null, true);
            echo "</td>
                                <td id=\"td_panier\">
                                    <span class=\"prixunitaire";
            // line 94
            echo twig_escape_filter($this->env, $this->getAttribute($context["ligne"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["ligne"], "prix", array()), "html", null, true);
            echo "</span>
                                    
                                </td>
                            
                            
                                <td id=\"td_panier\" style=\"text-align: center;\" >
                                    
                                    <span class=\"quantiteligne\" >
                                   <center><span class=\"quantiteligne\" id=\"";
            // line 102
            echo twig_escape_filter($this->env, $this->getAttribute($context["ligne"], "id", array()), "html", null, true);
            echo "\">
                                   
                                    <span class=\"quantmoin\"><a>
                                         <img src=\"";
            // line 105
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/panier/left.png"), "html", null, true);
            echo "\">
                                     </a></span>
                                     <span class=\"quantactuel\">";
            // line 107
            echo twig_escape_filter($this->env, $this->getAttribute($context["ligne"], "quantite", array()), "html", null, true);
            echo "</span>
                                        <span class=\"quantplus\"><a>
                                         <img src=\"";
            // line 109
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/panier/right.png"), "html", null, true);
            echo "\">
                                     </a></span>
                                    </span></center>
                                </td>
                                <td id=\"td_panier\">
                                    <span class=\"prixtotal";
            // line 114
            echo twig_escape_filter($this->env, $this->getAttribute($context["ligne"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["ligne"], "prixtotal", array()), "html", null, true);
            echo "</span>
                                </td>
                                <td id=\"td_panier\">
                                     <a href=\"#\">
                                         <img src=\"";
            // line 118
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/panier/acheter.png"), "html", null, true);
            echo "\">
                                     </a>
                                </td>
                                <td id=\"td_panier\">
                                    <a href=\"";
            // line 122
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tunisia_mall_panier_supp", array("id" => $this->getAttribute($context["ligne"], "id", array()))), "html", null, true);
            echo "\">
                                        <img src=\"";
            // line 123
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/panier/delete2.png"), "html", null, true);
            echo "\">
                                    </a>
                                </td>
                            </tr>
                            ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 128
            echo "                                Panier vide
                         ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ligne'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 130
        echo "                         
                         <tr id=\"tr_panier\">
                             <td colspan=\"6\" style=\"text-align: right\"><h3>Total:&nbsp; </h3></td>
                             <td id=\"sommepanier\" style=\"font-size: 30px\"> ";
        // line 133
        echo twig_escape_filter($this->env, (isset($context["somme"]) ? $context["somme"] : $this->getContext($context, "somme")), "html", null, true);
        echo " </td>
                         </tr>
                        </table>
                        </center>
                    </section>

                
 
            <script type=\"text/javascript\">
                \$('document').ready(function(){
                    \$(\".quantmoin\").click(function(){
                        id = \$(this).parent('span.quantiteligne').attr('id');
                        quantite = \$(this).parent('span.quantiteligne').children('.quantactuel').html();
                        if(quantite == 1)
                        {
                            alert('Quantite minimale est 1');
                        }
                        else
                        {
                            new_quantite = quantite - 1;
                            //alert(\"new_quantite: \"+new_quantite);
                            \$(this).parent('span.quantiteligne').children('.quantactuel').html(new_quantite);
                            prixunitaire = \$(\".prixunitaire\"+id).html();
                            
                            //alert(\"prixunitaire: \"+prixunitaire);
                            var somme = parseInt(new_quantite)*parseInt(prixunitaire);
                            //alert(\"somme: \"+somme);
                            \$(\".prixtotal\"+id).html(somme);
                            
                            \$(\"#sommepanier\").html(parseInt(\$(\"#sommepanier\").html()) - parseInt(prixunitaire));
                            
                            \$.ajax({
                                type: \"POST\",
                                url: \"";
        // line 166
        echo $this->env->getExtension('routing')->getPath("tunisia_mall_panier_ajout_supp_ajax");
        echo "\",
                                data: {id:id,new_quantite:new_quantite,somme:somme},
                                success: function(reponse)
                                {
                                    //alert(\"reponse\");
                                },

                                error: function(XMLHttpRequest, textStatus, errorThrown)
                                {
                                    alert('Error : ' + errorThrown);
                                }
                            });
                        }
                    });
                    
                    
                    
                    \$(\".quantplus\").click(function(){
                        id = \$(this).parent('span.quantiteligne').attr('id');
                        quantite = \$(this).parent('span.quantiteligne').children('.quantactuel').html();
                        
                            new_quantite = parseInt(quantite) + 1;
                            \$(this).parent('span.quantiteligne').children('.quantactuel').html(new_quantite);
                            prixunitaire = \$(\".prixunitaire\"+id).html();
                            
                            var somme = parseInt(new_quantite)*parseInt(prixunitaire);
                            \$(\".prixtotal\"+id).html(somme);
                            
                            sommepanier = parseInt(\$(\"#sommepanier\").html()) + parseInt(prixunitaire);
                            \$(\"#sommepanier\").html(parseInt(\$(\"#sommepanier\").html()) + parseInt(prixunitaire));
                            \$.ajax({
                                type: \"POST\",
                                url: \"";
        // line 198
        echo $this->env->getExtension('routing')->getPath("tunisia_mall_panier_ajout_supp_ajax");
        echo "\",
                                data: {id:id,new_quantite:new_quantite,somme:somme,sommepanier:sommepanier},
                                success: function(reponse)
                                {
                                    //alert(\"reponse\");
                                },

                                error: function(XMLHttpRequest, textStatus, errorThrown)
                                {
                                    alert('Error : ' + errorThrown);
                                }
                            });
                    });
                })
            </script>
                
";
    }

    public function getTemplateName()
    {
        return "ClientBundle:Default:panier.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  312 => 198,  277 => 166,  241 => 133,  236 => 130,  229 => 128,  219 => 123,  215 => 122,  208 => 118,  199 => 114,  191 => 109,  186 => 107,  181 => 105,  175 => 102,  162 => 94,  157 => 92,  153 => 91,  150 => 90,  147 => 89,  141 => 88,  139 => 87,  121 => 72,  112 => 65,  105 => 61,  102 => 60,  95 => 56,  89 => 53,  85 => 51,  83 => 50,  45 => 15,  34 => 7,  31 => 6,  28 => 5,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "ClientBundle::Layout.html.twig" %}*/
/* */
/*         */
/* {% block contenu %}*/
/*                     <div class="moto-widget moto-widget-menu moto-preset-default moto-align-center moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-preset="default" data-widget="menu" data-align="{&quot;desktop&quot;:&quot;center&quot;,&quot;tablet&quot;:&quot;&quot;,&quot;mobile-h&quot;:&quot;&quot;,&quot;mobile-v&quot;:&quot;&quot;}" data-menu-id="1" data-menu-type="horizontal" data-spacing="mama">*/
/*         <a href="{{asset('#')}}" class="moto-widget-menu-toggle-btn"><i class="moto-widget-menu-toggle-btn-icon fa fa-bars"></i></a>*/
/*     <ul class="moto-widget-menu-list moto-widget-menu-list_horizontal">*/
/*         <li class="moto-widget-menu-item">*/
/*     <a href="acceuil"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Acceuil</a>*/
/*     </li><li class="moto-widget-menu-item">*/
/*     <a href="catalogue"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Catalogues<span class="fa moto-widget-menu-link-arrow"></span></a>*/
/*                 <ul class="moto-widget-menu-sublist">*/
/*                     <li class="moto-widget-menu-item">*/
/*     <a href="{{asset('testimonials/index.html')}}"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-2 moto-link"></a>*/
/*     </li>*/
/*     */
/*                     <li class="moto-widget-menu-item">*/
/*     <a href="#"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-2 moto-widget-menu-link-submenu moto-link">Générale<!--<span class="fa moto-widget-menu-link-arrow"></span></a>*/
/*                 <!--<ul class="moto-widget-menu-sublist">*/
/*                     <li class="moto-widget-menu-item">*/
/*     <a href="#"   data-action="blog.index" class="moto-widget-menu-link moto-widget-menu-link-level-3 moto-link">gallery1</a>*/
/*     </li>*/
/*                     <li class="moto-widget-menu-item">*/
/*     <a href="#}"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-3 moto-link">About us</a>*/
/*     </li>*/
/*             </ul>-->*/
/* */
/*     </li>*/
/*                     <li class="moto-widget-menu-item">*/
/*     <a href="#"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-2 moto-link">Par enseigne</a>*/
/*     </li>*/
/*             </ul>*/
/* */
/*     </li>*/
/*     <li class="moto-widget-menu-item">*/
/*     <a href="gallerie"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Galleries</a>*/
/*     </li>*/
/*      <li class="moto-widget-menu-item">*/
/*     <a href="fashion"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-active moto-link">Fashion</a>*/
/*     </li>*/
/*     <li class="moto-widget-menu-item">*/
/*     <a href="boutique"   data-action="blog.index" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Nos Boutiques</a>*/
/*     </li><li class="moto-widget-menu-item">*/
/* */
/*     <a href="contact"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Contacts</a>*/
/*     </li>    */
/*     */
/*     */
/*                                                         {% if user %}*/
/*                                                             <li class="moto-widget-menu-item">*/
/*                                                                 */
/*                                                                 <a href=""   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link">{{user.username}}</a>*/
/*                                                                 &nbsp &nbsp &nbsp */
/*                                                                 <div class="icon">*/
/*                                                                     <a href="{{path('tunisia_mall_client_panier')}}"> <span class="fa fa-5x fa-shopping-cart" href="panier"></span></a>*/
/*                                                                 </div>    */
/*                                                             </li> */
/*                                                         {% else %}*/
/*                                                             <li class="moto-widget-menu-item">*/
/*                                                                 <a href="{{path('fos_user_security_login')}}"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link">connexion</a>*/
/*                                                                */
/*                                                             </li> */
/*                                                         {% endif %}*/
/*     </ul>*/
/*     </div><div class="moto-widget moto-widget-spacer moto-preset-default                      moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="spacer" data-preset="default" data-spacing="sasa">*/
/*     <div class="moto-widget-spacer-block" style="height: 2px;"></div>*/
/* </div></div></div></div></div></div></div>        </header>*/
/*   <section id="section_panier" style="min-height: 1200px;" >*/
/*       <center><h1 id="title">Recupltatif de commande </h1></center>*/
/*       */
/* <script src="{{asset('js/Responsable/jQuery/jQuery-2.1.4.min.js')}}" type="text/javascript"></script>*/
/* */
/*                         <center>  <table  id="table_panier">*/
/*                             */
/*                            */
/*                             <tr id="tr_panier">*/
/*                                */
/*                                 <th id="th_panier"></th>*/
/*                                 <th id="th_panier">Produit</th>*/
/*                                 <th id="th_panier">prix</th>*/
/*                                 <th id="th_panier">Quantite</th>*/
/*                                 <th id="th_panier">Total</th>*/
/*                                 <th id="th_panier">Acheter</th>*/
/*                                 <th id="th_panier">Supprimer</th>*/
/*                             </tr>*/
/*                             {% set somme = 0 %}*/
/*                         {% for ligne in panier.achats %}*/
/*                             {% set somme = somme + ligne.prixtotal %}*/
/*                             <tr id="tr_panier">*/
/*                                 <td id="td_panier"><img src="{{ asset(['images/panier/',ligne.produit.Image]|join )}}" height="30" width="50"  alt=''/></td>*/
/*                                 <td id="td_panier">{{ ligne.produit.Libelle }}</td>*/
/*                                 <td id="td_panier">*/
/*                                     <span class="prixunitaire{{ligne.id}}">{{ ligne.prix }}</span>*/
/*                                     */
/*                                 </td>*/
/*                             */
/*                             */
/*                                 <td id="td_panier" style="text-align: center;" >*/
/*                                     */
/*                                     <span class="quantiteligne" >*/
/*                                    <center><span class="quantiteligne" id="{{ligne.id}}">*/
/*                                    */
/*                                     <span class="quantmoin"><a>*/
/*                                          <img src="{{asset('images/panier/left.png')}}">*/
/*                                      </a></span>*/
/*                                      <span class="quantactuel">{{ ligne.quantite }}</span>*/
/*                                         <span class="quantplus"><a>*/
/*                                          <img src="{{asset('images/panier/right.png')}}">*/
/*                                      </a></span>*/
/*                                     </span></center>*/
/*                                 </td>*/
/*                                 <td id="td_panier">*/
/*                                     <span class="prixtotal{{ligne.id}}">{{ ligne.prixtotal }}</span>*/
/*                                 </td>*/
/*                                 <td id="td_panier">*/
/*                                      <a href="#">*/
/*                                          <img src="{{asset('images/panier/acheter.png')}}">*/
/*                                      </a>*/
/*                                 </td>*/
/*                                 <td id="td_panier">*/
/*                                     <a href="{{path("tunisia_mall_panier_supp",{"id" :ligne.id})}}">*/
/*                                         <img src="{{asset('images/panier/delete2.png')}}">*/
/*                                     </a>*/
/*                                 </td>*/
/*                             </tr>*/
/*                             {% else %}*/
/*                                 Panier vide*/
/*                          {% endfor %}*/
/*                          */
/*                          <tr id="tr_panier">*/
/*                              <td colspan="6" style="text-align: right"><h3>Total:&nbsp; </h3></td>*/
/*                              <td id="sommepanier" style="font-size: 30px"> {{somme}} </td>*/
/*                          </tr>*/
/*                         </table>*/
/*                         </center>*/
/*                     </section>*/
/* */
/*                 */
/*  */
/*             <script type="text/javascript">*/
/*                 $('document').ready(function(){*/
/*                     $(".quantmoin").click(function(){*/
/*                         id = $(this).parent('span.quantiteligne').attr('id');*/
/*                         quantite = $(this).parent('span.quantiteligne').children('.quantactuel').html();*/
/*                         if(quantite == 1)*/
/*                         {*/
/*                             alert('Quantite minimale est 1');*/
/*                         }*/
/*                         else*/
/*                         {*/
/*                             new_quantite = quantite - 1;*/
/*                             //alert("new_quantite: "+new_quantite);*/
/*                             $(this).parent('span.quantiteligne').children('.quantactuel').html(new_quantite);*/
/*                             prixunitaire = $(".prixunitaire"+id).html();*/
/*                             */
/*                             //alert("prixunitaire: "+prixunitaire);*/
/*                             var somme = parseInt(new_quantite)*parseInt(prixunitaire);*/
/*                             //alert("somme: "+somme);*/
/*                             $(".prixtotal"+id).html(somme);*/
/*                             */
/*                             $("#sommepanier").html(parseInt($("#sommepanier").html()) - parseInt(prixunitaire));*/
/*                             */
/*                             $.ajax({*/
/*                                 type: "POST",*/
/*                                 url: "{{ path('tunisia_mall_panier_ajout_supp_ajax') }}",*/
/*                                 data: {id:id,new_quantite:new_quantite,somme:somme},*/
/*                                 success: function(reponse)*/
/*                                 {*/
/*                                     //alert("reponse");*/
/*                                 },*/
/* */
/*                                 error: function(XMLHttpRequest, textStatus, errorThrown)*/
/*                                 {*/
/*                                     alert('Error : ' + errorThrown);*/
/*                                 }*/
/*                             });*/
/*                         }*/
/*                     });*/
/*                     */
/*                     */
/*                     */
/*                     $(".quantplus").click(function(){*/
/*                         id = $(this).parent('span.quantiteligne').attr('id');*/
/*                         quantite = $(this).parent('span.quantiteligne').children('.quantactuel').html();*/
/*                         */
/*                             new_quantite = parseInt(quantite) + 1;*/
/*                             $(this).parent('span.quantiteligne').children('.quantactuel').html(new_quantite);*/
/*                             prixunitaire = $(".prixunitaire"+id).html();*/
/*                             */
/*                             var somme = parseInt(new_quantite)*parseInt(prixunitaire);*/
/*                             $(".prixtotal"+id).html(somme);*/
/*                             */
/*                             sommepanier = parseInt($("#sommepanier").html()) + parseInt(prixunitaire);*/
/*                             $("#sommepanier").html(parseInt($("#sommepanier").html()) + parseInt(prixunitaire));*/
/*                             $.ajax({*/
/*                                 type: "POST",*/
/*                                 url: "{{ path('tunisia_mall_panier_ajout_supp_ajax') }}",*/
/*                                 data: {id:id,new_quantite:new_quantite,somme:somme,sommepanier:sommepanier},*/
/*                                 success: function(reponse)*/
/*                                 {*/
/*                                     //alert("reponse");*/
/*                                 },*/
/* */
/*                                 error: function(XMLHttpRequest, textStatus, errorThrown)*/
/*                                 {*/
/*                                     alert('Error : ' + errorThrown);*/
/*                                 }*/
/*                             });*/
/*                     });*/
/*                 })*/
/*             </script>*/
/*                 */
/* {% endblock %}{# empty Twig template #}*/
/* */
/* */
