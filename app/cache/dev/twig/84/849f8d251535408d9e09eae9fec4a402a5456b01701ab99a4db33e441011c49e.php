<?php

/* ReservationPackBundle:ReservationPack:Paiement.html.twig */
class __TwigTemplate_c67e620b1cb1050b69261593de23caca3f4c9b62d9fbc7b37a94618ef7fb8c4c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "ReservationPackBundle:ReservationPack:Paiement.html.twig", 3);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 5
        echo "
    <div class =\"container\">
        <div class=\"col-md-6\">
            <!-- Horizontal Form -->
            <div class=\"box box-info\">
                <div class=\"box-header with-border\">
                    <h3 class=\"box-title\">Paiement du reservation</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class=\"form-horizontal\" name=\"form\" onsubmit=\" return verif()\" method =\"POST\">
                    <div class=\"box-body\">
              
                        ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["res"]) ? $context["res"] : $this->getContext($context, "res")));
        foreach ($context['_seq'] as $context["_key"] => $context["re"]) {
            // line 18
            echo "                            <label>Zone : </label>";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["re"], "PackPublicitaire", array()), "Zone", array()), "html", null, true);
            echo "<br><br>

                            <label>Libelle : </label>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["re"], "PackPublicitaire", array()), "Libelle", array()), "html", null, true);
            echo "<br><br>

                            <label >Prix : </label>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["re"], "PackPublicitaire", array()), "Prix", array()), "html", null, true);
            echo "<br><br>

                            <label>Date de début de réservation : </label>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["re"], "Datedebut", array()), "format", array(0 => "j/m/Y"), "method"), "html", null, true);
            echo "<br><br>

                            <label>Date de fin de réservation : </label>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["re"], "DateFin", array()), "format", array(0 => "j/m/Y"), "method"), "html", null, true);
            echo "<br><br>
                       



                    </div><!-- /.box-body -->
                    <div class=\"box-footer\">
                        <button type=\"submit\" class=\"btn btn-default\" name =\"submit\">Cancel</button>
                    <a href=\"";
            // line 34
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Confirm_carte", array("id" => $this->getAttribute($context["re"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-info pull-right\">Payer</a>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box -->
        </div>

    </div> ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['re'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "ReservationPackBundle:ReservationPack:Paiement.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 34,  70 => 26,  65 => 24,  60 => 22,  55 => 20,  49 => 18,  45 => 17,  31 => 5,  28 => 4,  11 => 3,);
    }
}
/* */
/* */
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block HeaderTitle %}*/
/* */
/*     <div class ="container">*/
/*         <div class="col-md-6">*/
/*             <!-- Horizontal Form -->*/
/*             <div class="box box-info">*/
/*                 <div class="box-header with-border">*/
/*                     <h3 class="box-title">Paiement du reservation</h3>*/
/*                 </div><!-- /.box-header -->*/
/*                 <!-- form start -->*/
/*                 <form class="form-horizontal" name="form" onsubmit=" return verif()" method ="POST">*/
/*                     <div class="box-body">*/
/*               */
/*                         {% for re in res %}*/
/*                             <label>Zone : </label>{{re.PackPublicitaire.Zone}}<br><br>*/
/* */
/*                             <label>Libelle : </label>{{re.PackPublicitaire.Libelle}}<br><br>*/
/* */
/*                             <label >Prix : </label>{{re.PackPublicitaire.Prix}}<br><br>*/
/* */
/*                             <label>Date de début de réservation : </label>{{re.Datedebut.format('j/m/Y')}}<br><br>*/
/* */
/*                             <label>Date de fin de réservation : </label>{{re.DateFin.format('j/m/Y')}}<br><br>*/
/*                        */
/* */
/* */
/* */
/*                     </div><!-- /.box-body -->*/
/*                     <div class="box-footer">*/
/*                         <button type="submit" class="btn btn-default" name ="submit">Cancel</button>*/
/*                     <a href="{{path('Confirm_carte',{'id':re.id})}}" class="btn btn-info pull-right">Payer</a>*/
/*                     </div><!-- /.box-footer -->*/
/*                 </form>*/
/*             </div><!-- /.box -->*/
/*         </div>*/
/* */
/*     </div> {%endfor%}*/
/* {% endblock %}*/
/* {# empty Twig template #}*/
/* */
