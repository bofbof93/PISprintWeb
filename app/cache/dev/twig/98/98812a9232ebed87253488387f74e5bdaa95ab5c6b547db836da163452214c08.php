<?php

/* ResponsableBundle:Default:GestionDuStock.html.twig */
class __TwigTemplate_92cdf65bfbe1496102dd2d1c5b39998f9c97478c5d261eab7d79a013fec2a228 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "ResponsableBundle:Default:GestionDuStock.html.twig", 2);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 5
        echo "               
            <h1>
            Gestion Du Stock
            <small>Liste Des Stocks</small>
          </h1>
            ";
    }

    // line 15
    public function block_TableView($context, array $blocks = array())
    {
        // line 16
        echo "<div class=\"TableView\" >
                <table >
                    <tr>
                        <td>
                            Id Stock
                        </td>
                        <td >
                            Référence
                        </td>
                        <td >
                            Nom Enseigne
                        </td>
                        <td>
                            Nom Fournisseur
                        </td>  
                        <td>
                            Quantite
                        </td>
                        <td>
                            Date d'entrée
                        </td> 
                        <td>
                            Modification
                        </td>
                        <td>
                            Suppression
                        </td>
                    </tr>
                    <tr>
                        <td >
                           
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                         <td>
                            <center><input class=\"modifIcon\" type=\"image\"  src=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Modifier.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                         <td>
                            <center><input class=\"suppIcon\" type=\"image\"  src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Supprimer.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                       <td>
                            <center><input class=\"modifIcon\" type=\"image\"  src=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Modifier.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                        <td>
                            <center><input class=\"suppIcon\" type=\"image\"  src=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Supprimer.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                   </tr>
                    <tr>
                        <td >
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                       <td>
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            <center><input class=\"modifIcon\" type=\"image\"  src=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Modifier.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                        <td>
                            <center><input class=\"suppIcon\" type=\"image\"  src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Supprimer.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                         <td>
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            <center><input class=\"modifIcon\" type=\"image\"  src=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Modifier.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                        <td>
                             <center><input class=\"suppIcon\" type=\"image\"  src=\"";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Supprimer.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                    </tr>
                     <tr>
                        <td >
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            <center><input class=\"modifIcon\" type=\"image\"  src=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Modifier.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                        <td>
                            <center><input class=\"suppIcon\" type=\"image\"  src=\"";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Supprimer.png"), "html", null, true);
        echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                    </tr>
                </table>
            </div>
    ";
    }

    // line 177
    public function block_btn($context, array $blocks = array())
    {
        // line 178
        echo "     <br>
                <div>
                    
                    <input type=\"submit\" class=\"btn\" value=\"Ajouter Stock\"/> 
                   
                
                
                </div>
    ";
    }

    public function getTemplateName()
    {
        return "ResponsableBundle:Default:GestionDuStock.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  242 => 178,  239 => 177,  229 => 171,  223 => 168,  197 => 145,  191 => 142,  165 => 119,  159 => 116,  133 => 93,  127 => 90,  101 => 67,  95 => 64,  45 => 16,  42 => 15,  33 => 5,  30 => 4,  11 => 2,);
    }
}
/* */
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* */
/*  {% block HeaderTitle %}*/
/*                */
/*             <h1>*/
/*             Gestion Du Stock*/
/*             <small>Liste Des Stocks</small>*/
/*           </h1>*/
/*             {%endblock %}*/
/*            */
/* */
/* */
/* */
/* {% block TableView %}*/
/* <div class="TableView" >*/
/*                 <table >*/
/*                     <tr>*/
/*                         <td>*/
/*                             Id Stock*/
/*                         </td>*/
/*                         <td >*/
/*                             Référence*/
/*                         </td>*/
/*                         <td >*/
/*                             Nom Enseigne*/
/*                         </td>*/
/*                         <td>*/
/*                             Nom Fournisseur*/
/*                         </td>  */
/*                         <td>*/
/*                             Quantite*/
/*                         </td>*/
/*                         <td>*/
/*                             Date d'entrée*/
/*                         </td> */
/*                         <td>*/
/*                             Modification*/
/*                         </td>*/
/*                         <td>*/
/*                             Suppression*/
/*                         </td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <td >*/
/*                            */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                          <td>*/
/*                             <center><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Modifier.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                          <td>*/
/*                             <center><input class="suppIcon" type="image"  src="{{asset('images/Responsable/Supprimer.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                        <td>*/
/*                             <center><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Modifier.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                         <td>*/
/*                             <center><input class="suppIcon" type="image"  src="{{asset('images/Responsable/Supprimer.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                    </tr>*/
/*                     <tr>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                        <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             <center><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Modifier.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                         <td>*/
/*                             <center><input class="suppIcon" type="image"  src="{{asset('images/Responsable/Supprimer.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                          <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             <center><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Modifier.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                         <td>*/
/*                              <center><input class="suppIcon" type="image"  src="{{asset('images/Responsable/Supprimer.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                     </tr>*/
/*                      <tr>*/
/*                         <td >*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             */
/*                         </td>*/
/*                         <td>*/
/*                             <center><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Modifier.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                         <td>*/
/*                             <center><input class="suppIcon" type="image"  src="{{asset('images/Responsable/Supprimer.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                     </tr>*/
/*                 </table>*/
/*             </div>*/
/*     {% endblock TableView %}    */
/*  {% block btn %}*/
/*      <br>*/
/*                 <div>*/
/*                     */
/*                     <input type="submit" class="btn" value="Ajouter Stock"/> */
/*                    */
/*                 */
/*                 */
/*                 </div>*/
/*     {% endblock %}*/
