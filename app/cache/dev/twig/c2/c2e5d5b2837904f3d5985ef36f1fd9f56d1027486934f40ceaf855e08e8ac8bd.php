<?php

/* HWIOAuthBundle:Connect:login.html.twig */
class __TwigTemplate_8a783e0205088e37baf30d69ab1aa4ce19f41345b9f351563725fd5c51f91f2f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'hwi_oauth_content' => array($this, 'block_hwi_oauth_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("FOSUserBundle:Security:login"));
        echo " 

";
        // line 4
        $this->displayBlock('hwi_oauth_content', $context, $blocks);
        // line 32
        echo "
";
    }

    // line 4
    public function block_hwi_oauth_content($context, array $blocks = array())
    {
        // line 5
        echo "    ";
        if ((array_key_exists("error", $context) && (isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")))) {
            // line 6
            echo "        <span>";
            echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "html", null, true);
            echo "</span>
    ";
        }
        // line 8
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('hwi_oauth')->getResourceOwners());
        foreach ($context['_seq'] as $context["_key"] => $context["owner"]) {
            // line 9
            echo "          ";
            if (($this->env->getExtension('translator')->trans($context["owner"], array(), "HWIOAuthBundle") == "facebook")) {
                // line 10
                echo "              <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('hwi_oauth')->getLoginUrl($context["owner"]), "html", null, true);
                echo "\" class=\"btn btn-block btn-social btn-facebook btn-flat\"><i class=\"fa fa-facebook\"></i> Sign in using Facebook</a>

       
         ";
            }
            // line 14
            echo "           ";
            if (($this->env->getExtension('translator')->trans($context["owner"], array(), "HWIOAuthBundle") == "google")) {
                // line 15
                echo "            <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('hwi_oauth')->getLoginUrl($context["owner"]), "html", null, true);
                echo "\" class=\"btn btn-block btn-social btn-google btn-flat\"><i class=\"fa fa-google-plus\"></i> Sign in using Google+</a>
         
         
         ";
            }
            // line 19
            echo "       
  
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['owner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "      </div><!-- /.social-auth-links -->

        <a href=\"";
        // line 24
        echo $this->env->getExtension('routing')->getPath("fos_user_change_password");
        echo "\">I forgot my password</a><br>
        <a href=\"http://localhost/TunisiaMall/web/app_dev.php/registration\" class=\"text-center\">Register a new membership</a>

         </form>
    </div>
</div>
</body>
";
    }

    public function getTemplateName()
    {
        return "HWIOAuthBundle:Connect:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 24,  82 => 22,  74 => 19,  66 => 15,  63 => 14,  55 => 10,  52 => 9,  47 => 8,  41 => 6,  38 => 5,  35 => 4,  30 => 32,  28 => 4,  23 => 2,  20 => 1,);
    }
}
/* */
/* {{ render(controller('FOSUserBundle:Security:login')) }} */
/* */
/* {% block hwi_oauth_content %}*/
/*     {% if error is defined and error %}*/
/*         <span>{{ error }}</span>*/
/*     {% endif %}*/
/*     {% for owner in hwi_oauth_resource_owners() %}*/
/*           {% if owner|trans({}, 'HWIOAuthBundle')=="facebook" %}*/
/*               <a href="{{ hwi_oauth_login_url(owner) }}" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>*/
/* */
/*        */
/*          {% endif %}*/
/*            {% if owner|trans({}, 'HWIOAuthBundle')=="google" %}*/
/*             <a href="{{ hwi_oauth_login_url(owner) }}" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>*/
/*          */
/*          */
/*          {% endif %}*/
/*        */
/*   */
/*     {% endfor %}*/
/*       </div><!-- /.social-auth-links -->*/
/* */
/*         <a href="{{path('fos_user_change_password')}}">I forgot my password</a><br>*/
/*         <a href="http://localhost/TunisiaMall/web/app_dev.php/registration" class="text-center">Register a new membership</a>*/
/* */
/*          </form>*/
/*     </div>*/
/* </div>*/
/* </body>*/
/* {% endblock hwi_oauth_content %}*/
/* */
/* */
