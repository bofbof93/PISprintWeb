<?php

/* ClientBundle:Default:gallerie.html.twig */
class __TwigTemplate_3cf7fff8e52b2b25bd983238194dc3e19692a18ffffb659277e71ca5466d1802 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("ClientBundle::Layout.html.twig", "ClientBundle:Default:gallerie.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ClientBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_contenu($context, array $blocks = array())
    {
        // line 6
        echo "                    <div class=\"moto-widget moto-widget-menu moto-preset-default moto-align-center moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto\" data-preset=\"default\" data-widget=\"menu\" data-align=\"{&quot;desktop&quot;:&quot;center&quot;,&quot;tablet&quot;:&quot;&quot;,&quot;mobile-h&quot;:&quot;&quot;,&quot;mobile-v&quot;:&quot;&quot;}\" data-menu-id=\"1\" data-menu-type=\"horizontal\" data-spacing=\"mama\">
        <a href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("#"), "html", null, true);
        echo "\" class=\"moto-widget-menu-toggle-btn\"><i class=\"moto-widget-menu-toggle-btn-icon fa fa-bars\"></i></a>
    <ul class=\"moto-widget-menu-list moto-widget-menu-list_horizontal\">
        <li class=\"moto-widget-menu-item\">
    <a href=\"acceuil\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Acceuil</a>
    </li><li class=\"moto-widget-menu-item\">
    <a href=\"catalogue\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Catalogues<span class=\"fa moto-widget-menu-link-arrow\"></span></a>
                <ul class=\"moto-widget-menu-sublist\">
                    <li class=\"moto-widget-menu-item\">
    <a href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("testimonials/index.html"), "html", null, true);
        echo "\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-2 moto-link\"></a>
    </li>
    
                    <li class=\"moto-widget-menu-item\">
    <a href=\"#\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-2 moto-widget-menu-link-submenu moto-link\">Générale<!--<span class=\"fa moto-widget-menu-link-arrow\"></span></a>
                <!--<ul class=\"moto-widget-menu-sublist\">
                    <li class=\"moto-widget-menu-item\">
    <a href=\"#\"   data-action=\"blog.index\" class=\"moto-widget-menu-link moto-widget-menu-link-level-3 moto-link\">gallery1</a>
    </li>
                    <li class=\"moto-widget-menu-item\">
    <a href=\"#}\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-3 moto-link\">About us</a>
    </li>
            </ul>-->

    </li>
                    <li class=\"moto-widget-menu-item\">
    <a href=\"#\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-2 moto-link\">Par enseigne</a>
    </li>
            </ul>

    </li>
    <li class=\"moto-widget-menu-item\">
    <a href=\"gallerie\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-active moto-link\">Galleries</a>
    </li>
     <li class=\"moto-widget-menu-item\">
    <a href=\"fashion\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Actualité</a>
    </li>
    <li class=\"moto-widget-menu-item\">
    <a href=\"boutique\"   data-action=\"blog.index\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Nos Boutiques</a>
    </li><li class=\"moto-widget-menu-item\">

    <a href=\"contact\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link\">Contacts</a>
    </li>  
    ";
        // line 48
        if (($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "getFlagRole", array(), "method") == 3)) {
            // line 49
            echo "                                                        <li class=\"moto-widget-menu-item\">
                                                            <a href=\"\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-link\">";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
            echo "</a>
                                                            &nbsp &nbsp &nbsp <a href=\"panier\"><img src=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/client/shopping-cart-of-checkered-design.png"), "html", null, true);
            echo "\"></a>
                                                        </li> 
                                                        ";
        } else {
            // line 54
            echo "                                                         <li class=\"moto-widget-menu-item\">
                                                            <a href=\"";
            // line 55
            echo $this->env->getExtension('routing')->getPath("fos_user_security_login");
            echo "\"   data-action=\"page\" class=\"moto-widget-menu-link moto-widget-menu-link-level-1 moto-link\">connexion</a>
                                                            &nbsp &nbsp &nbsp <a href=\"panier\"><img src=\"";
            // line 56
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/client/shopping-cart-of-checkered-design.png"), "html", null, true);
            echo "\"></a>
                                                        </li> 
                                                        ";
        }
        // line 58
        echo "</ul>
    </div><div class=\"moto-widget moto-widget-spacer moto-preset-default                      moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto\" data-widget=\"spacer\" data-preset=\"default\" data-spacing=\"sasa\">
    <div class=\"moto-widget-spacer-block\" style=\"height: 2px;\"></div>
</div></div></div></div></div></div></div>        </header>
";
    }

    public function getTemplateName()
    {
        return "ClientBundle:Default:gallerie.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 58,  103 => 56,  99 => 55,  96 => 54,  90 => 51,  86 => 50,  83 => 49,  81 => 48,  45 => 15,  34 => 7,  31 => 6,  28 => 5,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "ClientBundle::Layout.html.twig" %}*/
/* */
/*         */
/* {% block contenu %}*/
/*                     <div class="moto-widget moto-widget-menu moto-preset-default moto-align-center moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-preset="default" data-widget="menu" data-align="{&quot;desktop&quot;:&quot;center&quot;,&quot;tablet&quot;:&quot;&quot;,&quot;mobile-h&quot;:&quot;&quot;,&quot;mobile-v&quot;:&quot;&quot;}" data-menu-id="1" data-menu-type="horizontal" data-spacing="mama">*/
/*         <a href="{{asset('#')}}" class="moto-widget-menu-toggle-btn"><i class="moto-widget-menu-toggle-btn-icon fa fa-bars"></i></a>*/
/*     <ul class="moto-widget-menu-list moto-widget-menu-list_horizontal">*/
/*         <li class="moto-widget-menu-item">*/
/*     <a href="acceuil"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Acceuil</a>*/
/*     </li><li class="moto-widget-menu-item">*/
/*     <a href="catalogue"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Catalogues<span class="fa moto-widget-menu-link-arrow"></span></a>*/
/*                 <ul class="moto-widget-menu-sublist">*/
/*                     <li class="moto-widget-menu-item">*/
/*     <a href="{{asset('testimonials/index.html')}}"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-2 moto-link"></a>*/
/*     </li>*/
/*     */
/*                     <li class="moto-widget-menu-item">*/
/*     <a href="#"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-2 moto-widget-menu-link-submenu moto-link">Générale<!--<span class="fa moto-widget-menu-link-arrow"></span></a>*/
/*                 <!--<ul class="moto-widget-menu-sublist">*/
/*                     <li class="moto-widget-menu-item">*/
/*     <a href="#"   data-action="blog.index" class="moto-widget-menu-link moto-widget-menu-link-level-3 moto-link">gallery1</a>*/
/*     </li>*/
/*                     <li class="moto-widget-menu-item">*/
/*     <a href="#}"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-3 moto-link">About us</a>*/
/*     </li>*/
/*             </ul>-->*/
/* */
/*     </li>*/
/*                     <li class="moto-widget-menu-item">*/
/*     <a href="#"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-2 moto-link">Par enseigne</a>*/
/*     </li>*/
/*             </ul>*/
/* */
/*     </li>*/
/*     <li class="moto-widget-menu-item">*/
/*     <a href="gallerie"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-active moto-link">Galleries</a>*/
/*     </li>*/
/*      <li class="moto-widget-menu-item">*/
/*     <a href="fashion"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Actualité</a>*/
/*     </li>*/
/*     <li class="moto-widget-menu-item">*/
/*     <a href="boutique"   data-action="blog.index" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Nos Boutiques</a>*/
/*     </li><li class="moto-widget-menu-item">*/
/* */
/*     <a href="contact"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-submenu moto-link">Contacts</a>*/
/*     </li>  */
/*     {% if user.getFlagRole()==3 %}*/
/*                                                         <li class="moto-widget-menu-item">*/
/*                                                             <a href=""   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link">{{user.username}}</a>*/
/*                                                             &nbsp &nbsp &nbsp <a href="panier"><img src="{{asset('images/client/shopping-cart-of-checkered-design.png')}}"></a>*/
/*                                                         </li> */
/*                                                         {% else %}*/
/*                                                          <li class="moto-widget-menu-item">*/
/*                                                             <a href="{{path('fos_user_security_login')}}"   data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link">connexion</a>*/
/*                                                             &nbsp &nbsp &nbsp <a href="panier"><img src="{{asset('images/client/shopping-cart-of-checkered-design.png')}}"></a>*/
/*                                                         </li> */
/*                                                         {% endif %}</ul>*/
/*     </div><div class="moto-widget moto-widget-spacer moto-preset-default                      moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="spacer" data-preset="default" data-spacing="sasa">*/
/*     <div class="moto-widget-spacer-block" style="height: 2px;"></div>*/
/* </div></div></div></div></div></div></div>        </header>*/
/* {% endblock %}*/
