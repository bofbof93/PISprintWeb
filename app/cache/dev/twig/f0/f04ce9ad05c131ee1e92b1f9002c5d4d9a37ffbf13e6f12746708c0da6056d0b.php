<?php

/* ResponsableBundle:Default:Acceuil.html.twig */
class __TwigTemplate_83047df8f6c615e9d7fc0431d692a5148cec0213d59cba89106bee52249c6513 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "ResponsableBundle:Default:Acceuil.html.twig", 1);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 3
        echo "               
            <h1>
            Bienvenue
            <small>Responsable</small>
          </h1>
            ";
    }

    public function getTemplateName()
    {
        return "ResponsableBundle:Default:Acceuil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block HeaderTitle %}*/
/*                */
/*             <h1>*/
/*             Bienvenue*/
/*             <small>Responsable</small>*/
/*           </h1>*/
/*             {%endblock %}*/
