<?php

/* TunisiaMallStatBundle:Default:index.html.twig */
class __TwigTemplate_47700f19286643a087168d5d3b473fde6d16cea74360003dd0b28514191582a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "TunisiaMallStatBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'TableView' => array($this, 'block_TableView'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_TableView($context, array $blocks = array())
    {
        // line 3
        echo "    <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js\"

    type=\"text/javascript\"></script>

    <script src=\"//code.highcharts.com/4.0.1/highcharts.js\"></script>

    <script src=\"//code.highcharts.com/4.0.1/modules/exporting.js\"></script>

    <script type=\"text/javascript\">

        ";
        // line 13
        echo $this->env->getExtension('highcharts_extension')->chart((isset($context["chart"]) ? $context["chart"] : $this->getContext($context, "chart")));
        echo "

    </script>

    <div id=\"linechart\" style=\" width: 400px; height: 400px; margin: 0 auto\"></div>

";
    }

    public function getTemplateName()
    {
        return "TunisiaMallStatBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 13,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block TableView %}*/
/*     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"*/
/* */
/*     type="text/javascript"></script>*/
/* */
/*     <script src="//code.highcharts.com/4.0.1/highcharts.js"></script>*/
/* */
/*     <script src="//code.highcharts.com/4.0.1/modules/exporting.js"></script>*/
/* */
/*     <script type="text/javascript">*/
/* */
/*         {{ chart(chart) }}*/
/* */
/*     </script>*/
/* */
/*     <div id="linechart" style=" width: 400px; height: 400px; margin: 0 auto"></div>*/
/* */
/* {% endblock TableView %}*/
