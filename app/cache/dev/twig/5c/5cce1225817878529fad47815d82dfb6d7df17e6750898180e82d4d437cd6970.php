<?php

/* FOSUserBundle:Registration:register_content.html.twig */
class __TwigTemplate_a3f3f421a2ef1ac294003c929bb1a2945d383360dfc9b640a49e0042ad6970b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "  <body class=\"hold-transition register-page\">
<div class=\"register-box\">
      <div class=\"register-logo\">
          
          <img src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/client/mt-0295-logo.png"), "html", null, true);
        echo "\" class=\"moto-widget-image-picture\" data-id=\"148\" title=\"\"  alt=\"\" draggable=\"false\">
         
        <span><b>Tunisia</b>Mall</span>
        
            
      </div>
    ";
        // line 12
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("method" => "post", "action" => $this->env->getExtension('routing')->getPath("fos_user_registration_register"), "attr" => array("class" => "fos_user_registration_register")));
        echo "

    <div class=\"register-box-body\">
         



       <table class=\"table table-hover\">
           <tr>
       
               <td>email : </td>
               <td> ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget');
        echo "</td>
           </tr>
           
            <tr>
             
           <td>Username: </td>
               <td> ";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'widget');
        echo "</td>
          
           </tr>
           <tr></tr>
           <tr>
            
           <td>Password: </td>
               <td> ";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'widget');
        echo "</td>
          
           </tr>
            <tr>
            
           <td>Confirm: </td>
               <td> ";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'widget');
        echo "</td>
          
           </tr>
           <tr>
           
            <tr>
            <td>Role: </td>
               <td> ";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "flagRole", array()), 'widget');
        echo "</td>
          
           </tr>
           
          
    </table>


            <input type=\"submit\" value=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" class=\"btn btn-primary btn-block btn-flat\"/> 
         
       <div class=\"social-auth-links text-center\">
          <p>- OR -</p>
          <a href=\"#\" class=\"btn btn-block btn-social btn-facebook btn-flat\"><i class=\"fa fa-facebook\"></i> Sign up using Facebook</a>
          <a href=\"#\" class=\"btn btn-block btn-social btn-google btn-flat\"><i class=\"fa fa-google-plus\"></i> Sign up using Google+</a>
        </div>

        <a href=\"http://localhost/TunisiaMall/web/app_dev.php/login\" class=\"text-center\">I already have a membership</a>
      </div>
            ";
        // line 67
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
        
        

       
          
        </div>
  </body><!-- /.form-box -->




  

";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 67,  97 => 57,  86 => 49,  76 => 42,  67 => 36,  57 => 29,  48 => 23,  34 => 12,  25 => 6,  19 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/*   <body class="hold-transition register-page">*/
/* <div class="register-box">*/
/*       <div class="register-logo">*/
/*           */
/*           <img src="{{asset('images/client/mt-0295-logo.png')}}" class="moto-widget-image-picture" data-id="148" title=""  alt="" draggable="false">*/
/*          */
/*         <span><b>Tunisia</b>Mall</span>*/
/*         */
/*             */
/*       </div>*/
/*     {{ form_start(form, {'method': 'post', 'action': path('fos_user_registration_register'), 'attr': {'class': 'fos_user_registration_register'}}) }}*/
/* */
/*     <div class="register-box-body">*/
/*          */
/* */
/* */
/* */
/*        <table class="table table-hover">*/
/*            <tr>*/
/*        */
/*                <td>email : </td>*/
/*                <td> {{form_widget(form.email)}}</td>*/
/*            </tr>*/
/*            */
/*             <tr>*/
/*              */
/*            <td>Username: </td>*/
/*                <td> {{form_widget(form.username)}}</td>*/
/*           */
/*            </tr>*/
/*            <tr></tr>*/
/*            <tr>*/
/*             */
/*            <td>Password: </td>*/
/*                <td> {{form_widget(form.plainPassword.first)}}</td>*/
/*           */
/*            </tr>*/
/*             <tr>*/
/*             */
/*            <td>Confirm: </td>*/
/*                <td> {{form_widget(form.plainPassword.second)}}</td>*/
/*           */
/*            </tr>*/
/*            <tr>*/
/*            */
/*             <tr>*/
/*             <td>Role: </td>*/
/*                <td> {{form_widget(form.flagRole)}}</td>*/
/*           */
/*            </tr>*/
/*            */
/*           */
/*     </table>*/
/* */
/* */
/*             <input type="submit" value="{{ 'registration.submit'|trans }}" class="btn btn-primary btn-block btn-flat"/> */
/*          */
/*        <div class="social-auth-links text-center">*/
/*           <p>- OR -</p>*/
/*           <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using Facebook</a>*/
/*           <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using Google+</a>*/
/*         </div>*/
/* */
/*         <a href="http://localhost/TunisiaMall/web/app_dev.php/login" class="text-center">I already have a membership</a>*/
/*       </div>*/
/*             {{ form_end(form) }}*/
/*         */
/*         */
/* */
/*        */
/*           */
/*         </div>*/
/*   </body><!-- /.form-box -->*/
/* */
/* */
/* */
/* */
/*   */
/* */
/* */
