<?php

/* TunisiaMallEnseigneBundle:Enseigne:ListeDesEnseigne.html.twig */
class __TwigTemplate_c6606f049ee1b395ac1b25dca3fae5296bd85957f405dea24530b17edcaf476e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "TunisiaMallEnseigneBundle:Enseigne:ListeDesEnseigne.html.twig", 2);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 5
        echo "               
            <h1>
            Gestion Des Enseignes
            </br>
            </h1></br>
     
         <form   method=\"POST\">
        <input type=\"text\" name=\"search\">
        <input type=\"submit\" value=\"rechercher\">
         </form>
    
           <h1>
            <center>   <small>Liste Des Enseignes</small></center>
          </h1>
     
     
            ";
    }

    // line 23
    public function block_TableView($context, array $blocks = array())
    {
        // line 24
        echo "    <div style=\"padding: 10px\">
        <div class=\"TableView\" >
       <div class=\"box box-success \">    
    <div class=\"box-body no-padding\">
              <table class=\"table table-bordered table-hover\" >
                    <tr>
                        
                        <th ><center> Libelle</center> </th>
                        <th ><center> Description </center> </th>
                        <th><center> Image</center> </th>
                        <th><center> Catégorie</center>  </th>
                        <th> <center> Etage</center> </th>
                        <th> <center> Heure d'ouverture</center> </th>
                        <th> <center> Heure de fermeture</center> </th>
                        <th> <center> Jour ouvert</center> </th>
                        <th> <center> Site web</center> </th>
                        
                     <th> <center> Modifier</center>  </th>
                     <th> <center> Supprimer</center>  </th>
                    </tr>
                    <tr> ";
        // line 44
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["Enseignes"]) ? $context["Enseignes"] : $this->getContext($context, "Enseignes")));
        foreach ($context['_seq'] as $context["_key"] => $context["Enseigne"]) {
            // line 45
            echo "                      
                       
                        <td>
                           <center> ";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["Enseigne"], "Libelle", array()), "html", null, true);
            echo "</td></center>
                            <td>
                           <center> ";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["Enseigne"], "Description", array()), "html", null, true);
            echo "</td></center>
                         <td >
";
            // line 53
            echo "                         <td>
                          <center> ";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($context["Enseigne"], "Categorie", array()), "html", null, true);
            echo " <center></td>
                       <td >
                           <center>";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($context["Enseigne"], "Etage", array()), "html", null, true);
            echo "<center></td>
                                   <td >
                           <center>";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($context["Enseigne"], "TimeOuverture", array()), "html", null, true);
            echo "<center></td>
                      <td >
                           <center>";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($context["Enseigne"], "TimeFermeture", array()), "html", null, true);
            echo "<center></td>
                                     <td >
                           <center>";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute($context["Enseigne"], "NBJour", array()), "html", null, true);
            echo "<center></td>
           
                          <td >
                           <center>";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute($context["Enseigne"], "URL", array()), "html", null, true);
            echo "<center></td>
                    
           
                         <td >
                         
                          <center><a href=\"";
            // line 70
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Enseigne_Modifier", array("id" => $this->getAttribute($context["Enseigne"], "id", array()))), "html", null, true);
            echo "\"><input class=\"modifIcon\" type=\"image\"  src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Modifier.png"), "html", null, true);
            echo " \" value=\"\" ></center>
                        </td>
                         <td >
                         <center><a href=\"";
            // line 73
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Supprimer_Enseigne", array("id" => $this->getAttribute($context["Enseigne"], "id", array()))), "html", null, true);
            echo "\"><input class=\"suppIcon\" type=\"image\"  src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Supprimer.png"), "html", null, true);
            echo " \" value=\"\"  ></a></center>
                        </td>
                    </tr>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['Enseigne'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 78
        echo "                                    </table>
            </div>           
                    </div>
                    </di>
                    </div>
                    </div>
    ";
    }

    // line 86
    public function block_btn($context, array $blocks = array())
    {
        echo "  
    <br>   
                <div>
                    
                    <center>  <a href=\"";
        // line 90
        echo $this->env->getExtension('routing')->getPath("enseigne_ajouter");
        echo "\"><input type=\"submit\" class=\"btn\" value=\"Ajouter une nouvelle enseigne\"/> 
                   <center>
                
              ";
        // line 94
        echo "                </div>
    ";
    }

    public function getTemplateName()
    {
        return "TunisiaMallEnseigneBundle:Enseigne:ListeDesEnseigne.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  179 => 94,  173 => 90,  165 => 86,  155 => 78,  142 => 73,  134 => 70,  126 => 65,  120 => 62,  115 => 60,  110 => 58,  105 => 56,  100 => 54,  97 => 53,  92 => 50,  87 => 48,  82 => 45,  78 => 44,  56 => 24,  53 => 23,  33 => 5,  30 => 4,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* */
/*  {% block HeaderTitle %}*/
/*                */
/*             <h1>*/
/*             Gestion Des Enseignes*/
/*             </br>*/
/*             </h1></br>*/
/*      */
/*          <form   method="POST">*/
/*         <input type="text" name="search">*/
/*         <input type="submit" value="rechercher">*/
/*          </form>*/
/*     */
/*            <h1>*/
/*             <center>   <small>Liste Des Enseignes</small></center>*/
/*           </h1>*/
/*      */
/*      */
/*             {%endblock %}*/
/* */
/* {% block TableView %}*/
/*     <div style="padding: 10px">*/
/*         <div class="TableView" >*/
/*        <div class="box box-success ">    */
/*     <div class="box-body no-padding">*/
/*               <table class="table table-bordered table-hover" >*/
/*                     <tr>*/
/*                         */
/*                         <th ><center> Libelle</center> </th>*/
/*                         <th ><center> Description </center> </th>*/
/*                         <th><center> Image</center> </th>*/
/*                         <th><center> Catégorie</center>  </th>*/
/*                         <th> <center> Etage</center> </th>*/
/*                         <th> <center> Heure d'ouverture</center> </th>*/
/*                         <th> <center> Heure de fermeture</center> </th>*/
/*                         <th> <center> Jour ouvert</center> </th>*/
/*                         <th> <center> Site web</center> </th>*/
/*                         */
/*                      <th> <center> Modifier</center>  </th>*/
/*                      <th> <center> Supprimer</center>  </th>*/
/*                     </tr>*/
/*                     <tr> {% for Enseigne in Enseignes %}*/
/*                       */
/*                        */
/*                         <td>*/
/*                            <center> {{Enseigne.Libelle}}</td></center>*/
/*                             <td>*/
/*                            <center> {{Enseigne.Description}}</td></center>*/
/*                          <td >*/
/* {#                            <center> <img src="{{path("image_route", {'id':Enseigne.id })}}"/></center>#}*/
/*                          <td>*/
/*                           <center> {{Enseigne.Categorie}} <center></td>*/
/*                        <td >*/
/*                            <center>{{Enseigne.Etage}}<center></td>*/
/*                                    <td >*/
/*                            <center>{{Enseigne.TimeOuverture}}<center></td>*/
/*                       <td >*/
/*                            <center>{{Enseigne.TimeFermeture}}<center></td>*/
/*                                      <td >*/
/*                            <center>{{Enseigne.NBJour}}<center></td>*/
/*            */
/*                           <td >*/
/*                            <center>{{Enseigne.URL}}<center></td>*/
/*                     */
/*            */
/*                          <td >*/
/*                          */
/*                           <center><a href="{{path('Enseigne_Modifier',{'id':Enseigne.id})}}"><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Modifier.png')}} " value="" ></center>*/
/*                         </td>*/
/*                          <td >*/
/*                          <center><a href="{{path('Supprimer_Enseigne',{'id':Enseigne.id})}}"><input class="suppIcon" type="image"  src="{{asset('images/Responsable/Supprimer.png')}} " value=""  ></a></center>*/
/*                         </td>*/
/*                     </tr>*/
/* */
/*                     {%endfor%}*/
/*                                     </table>*/
/*             </div>           */
/*                     </div>*/
/*                     </di>*/
/*                     </div>*/
/*                     </div>*/
/*     {% endblock TableView %} */
/*           */
/*         {%block btn %}  */
/*     <br>   */
/*                 <div>*/
/*                     */
/*                     <center>  <a href="{{path('enseigne_ajouter')}}"><input type="submit" class="btn" value="Ajouter une nouvelle enseigne"/> */
/*                    <center>*/
/*                 */
/*               {#  <div>{{ socialButtons() }}</div>#}*/
/*                 </div>*/
/*     {% endblock %}*/
/* */
