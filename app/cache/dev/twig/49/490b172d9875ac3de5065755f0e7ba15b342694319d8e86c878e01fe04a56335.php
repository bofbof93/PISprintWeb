<?php

/* PromotionBundle:Promotion:ModifierPromotion.html.twig */
class __TwigTemplate_4b18fc69e9014ef8bdb0bf5b1a7038f679d3f35aa1b8b9bd47d1ddd716d9c049 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "PromotionBundle:Promotion:ModifierPromotion.html.twig", 1);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 3
        echo "   <div class =\"container\">
           <div class=\"col-md-6\">
              <!-- Horizontal Form -->
              <div class=\"box box-info\">
                <div class=\"box-header with-border\">
                  <h3 class=\"box-title\">Modifier une promotion</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                
                <form class=\"form-horizontal\" name=\"form\" onsubmit=\"verif()\" method =\"POST\">
                  <div class=\"box-body\">
                      ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["promotions"]) ? $context["promotions"] : $this->getContext($context, "promotions")));
        foreach ($context['_seq'] as $context["_key"] => $context["promotion"]) {
            // line 15
            echo "                    <div class=\"form-group\">
                      <label for=\"date1\" class=\"col-sm-2 control-label\" >Date début </label>
                      <div class=\"col-sm-10\">
                        <input name=\"date1\" type=\"date\" class=\"form-control\" id=\"date1\" value=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["promotion"], "dateDebut", array()), "format", array(0 => "j/m/Y"), "method"), "html", null, true);
            echo "\">
                      </div>
                    </div>
                      
                    <div class=\"form-group\">
                      <label for=\"date2\" class=\"col-sm-2 control-label\">Date fin </label>
                      <div class=\"col-sm-10\">
                        <input name=\"date2\" type=\"date\" class=\"form-control\" id=\"date2\"   value=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["promotion"], "dateFin", array()), "format", array(0 => "j/m/Y"), "method"), "html", null, true);
            echo "\">
                      </div>
                    </div>
                      
                    <div class=\"form-group\">
                      <label for=\"taux\" class=\"col-sm-2 control-label\">Taux de réduction</label>
                      <div class=\"col-sm-10\">
                        <input name=\"taux\" type=\"number\" class=\"form-control\" id=\"inputPassword3\" value=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotion"], "Tauxdereduction", array()), "html", null, true);
            echo "\">
                      </div>
                    </div>
                      
                   <div class=\"form-group\">
                      <label for=\"image\" class=\"col-sm-2 control-label\">Image</label>
                      <div class=\"col-sm-10\">
                        <input name =\"img\" type=\"file\" class=\"form-control\" id=\"image\" value=\"";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotion"], "Image", array()), "html", null, true);
            echo "\">
                      </div>
                    </div>
                      
                      
                    <div class=\"form-group\">
                      <label for=\"inputPassword3\" class=\"col-sm-2 control-label\">Description</label>
                      <div class=\"col-sm-10\">
                        <textarea rows=\"5\" cols=\"50\" class=\"form-control\" id=\"text1\" name=\"des\" >";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotion"], "Description", array()), "html", null, true);
            echo "</textarea>
                      </div>
                    </div>  
                      
                      
                   <div class=\"form-group\"> 
                     <label for=\"etat\" class=\"col-sm-2 control-label\">Etat</label>
                       <div class=\"col-sm-10\">
                           <select name=\"etat\" onChange=\"\" value=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotion"], "Etat", array()), "html", null, true);
            echo "\">
                                       

                           <option>Active</option>
                            <option>non active</option>
                          
                         
                           </select> 
                       </div>  
                   </div>
    
                           ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['promotion'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "                      
                      
                  </div><!-- /.box-body -->
                  <div class=\"box-footer\">
                    <button type=\"submit\" class=\"btn btn-default\" name =\"submit\">Cancel</button>
                    <button type=\"submit\" class=\"btn btn-info pull-right\">Sign in</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
           </div>
   </div>
    ";
    }

    public function getTemplateName()
    {
        return "PromotionBundle:Promotion:ModifierPromotion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 67,  105 => 55,  94 => 47,  83 => 39,  73 => 32,  63 => 25,  53 => 18,  48 => 15,  44 => 14,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block HeaderTitle %}*/
/*    <div class ="container">*/
/*            <div class="col-md-6">*/
/*               <!-- Horizontal Form -->*/
/*               <div class="box box-info">*/
/*                 <div class="box-header with-border">*/
/*                   <h3 class="box-title">Modifier une promotion</h3>*/
/*                 </div><!-- /.box-header -->*/
/*                 <!-- form start -->*/
/*                 */
/*                 <form class="form-horizontal" name="form" onsubmit="verif()" method ="POST">*/
/*                   <div class="box-body">*/
/*                       {% for promotion in promotions %}*/
/*                     <div class="form-group">*/
/*                       <label for="date1" class="col-sm-2 control-label" >Date début </label>*/
/*                       <div class="col-sm-10">*/
/*                         <input name="date1" type="date" class="form-control" id="date1" value="{{promotion.dateDebut.format('j/m/Y')}}">*/
/*                       </div>*/
/*                     </div>*/
/*                       */
/*                     <div class="form-group">*/
/*                       <label for="date2" class="col-sm-2 control-label">Date fin </label>*/
/*                       <div class="col-sm-10">*/
/*                         <input name="date2" type="date" class="form-control" id="date2"   value="{{promotion.dateFin.format('j/m/Y')}}">*/
/*                       </div>*/
/*                     </div>*/
/*                       */
/*                     <div class="form-group">*/
/*                       <label for="taux" class="col-sm-2 control-label">Taux de réduction</label>*/
/*                       <div class="col-sm-10">*/
/*                         <input name="taux" type="number" class="form-control" id="inputPassword3" value="{{promotion.Tauxdereduction}}">*/
/*                       </div>*/
/*                     </div>*/
/*                       */
/*                    <div class="form-group">*/
/*                       <label for="image" class="col-sm-2 control-label">Image</label>*/
/*                       <div class="col-sm-10">*/
/*                         <input name ="img" type="file" class="form-control" id="image" value="{{promotion.Image}}">*/
/*                       </div>*/
/*                     </div>*/
/*                       */
/*                       */
/*                     <div class="form-group">*/
/*                       <label for="inputPassword3" class="col-sm-2 control-label">Description</label>*/
/*                       <div class="col-sm-10">*/
/*                         <textarea rows="5" cols="50" class="form-control" id="text1" name="des" >{{promotion.Description}}</textarea>*/
/*                       </div>*/
/*                     </div>  */
/*                       */
/*                       */
/*                    <div class="form-group"> */
/*                      <label for="etat" class="col-sm-2 control-label">Etat</label>*/
/*                        <div class="col-sm-10">*/
/*                            <select name="etat" onChange="" value="{{promotion.Etat}}">*/
/*                                        */
/* */
/*                            <option>Active</option>*/
/*                             <option>non active</option>*/
/*                           */
/*                          */
/*                            </select> */
/*                        </div>  */
/*                    </div>*/
/*     */
/*                            {% endfor %}*/
/*                       */
/*                       */
/*                   </div><!-- /.box-body -->*/
/*                   <div class="box-footer">*/
/*                     <button type="submit" class="btn btn-default" name ="submit">Cancel</button>*/
/*                     <button type="submit" class="btn btn-info pull-right">Sign in</button>*/
/*                   </div><!-- /.box-footer -->*/
/*                 </form>*/
/*               </div><!-- /.box -->*/
/*            </div>*/
/*    </div>*/
/*     {% endblock %}*/
/* {# empty Twig template #}*/
/* */
