<?php

/* TunisiaMallGestionStockBundle:GestionStock:AjouterStock.html.twig */
class __TwigTemplate_e4b9cc6fa8859a460fbf2018a75638936b4391dd6b9accf0f1776bdc4c52cd7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "TunisiaMallGestionStockBundle:GestionStock:AjouterStock.html.twig", 3);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 5
        echo "
    <h1>
        Ajouter
        <small>Stock</small>
    </h1>
";
    }

    // line 12
    public function block_TableView($context, array $blocks = array())
    {
        // line 13
        echo "
    <div class=\"col-md-6\">
        <!-- Horizontal Form -->
        <div class=\"box box-info\">
            <div class=\"box-header with-border\">
                <h3 class=\"box-title\">Ajouter une produit</h3>

                <form  method=\"POST\" class=\"form-horizontal\" name=\"form\" onsubmit=\"verif()\">
                    <div> ";
        // line 21
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
                        ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
                       
                        <div class=\"form-group\">
                            <label for=\"Quantiteproduit\" class=\"col-sm-2 control-label\">Quantite produit</label>
                            ";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Quantiteproduit", array()), 'errors');
        echo "
                            <div class=\"col-sm-10\">
                                ";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Quantiteproduit", array()), 'widget');
        echo "
                            </div> 
                        </div>
                        <div class=\"form-group\">
                            <label for=\"DateAjout\" class=\"col-sm-2 control-label\">Date Ajout</label>
                            ";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "DateAjout", array()), 'errors');
        echo "
                            <div class=\"col-sm-10\">
                                ";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "DateAjout", array()), 'widget');
        echo "
                            </div> 
                        </div>
                        <div class=\"form-group\">
                            <label for=\"Enseigne\" class=\"col-sm-2 control-label\">Enseigne</label>
                            ";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Enseigne", array()), 'errors');
        echo "
                            <div class=\"col-sm-10\">
                                ";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Enseigne", array()), 'widget');
        echo "
                            </div></div> 
                        <div class=\"form-group\">
                            <label for=\"Produit\" class=\"col-sm-2 control-label\">Produit</label>
                            ";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Produit", array()), 'errors');
        echo "
                            <div class=\"col-sm-10\">
                                ";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Produit", array()), 'widget');
        echo "
                            </div></div> 




                        <div class=\"box-footer\">
                            ";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "AjouterStock", array()), 'widget');
        echo "
                        </div>
                        ";
        // line 57
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "


                    </div>
                </form>
            </div>
        </div>
    </div><!-- /.box -->


";
    }

    public function getTemplateName()
    {
        return "TunisiaMallGestionStockBundle:GestionStock:AjouterStock.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 57,  118 => 55,  108 => 48,  103 => 46,  96 => 42,  91 => 40,  83 => 35,  78 => 33,  70 => 28,  65 => 26,  58 => 22,  54 => 21,  44 => 13,  41 => 12,  32 => 5,  29 => 4,  11 => 3,);
    }
}
/* {# empty Twig template #}*/
/* {# empty Twig template #}*/
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block HeaderTitle %}*/
/* */
/*     <h1>*/
/*         Ajouter*/
/*         <small>Stock</small>*/
/*     </h1>*/
/* {%endblock %}*/
/* */
/* {% block TableView %}*/
/* */
/*     <div class="col-md-6">*/
/*         <!-- Horizontal Form -->*/
/*         <div class="box box-info">*/
/*             <div class="box-header with-border">*/
/*                 <h3 class="box-title">Ajouter une produit</h3>*/
/* */
/*                 <form  method="POST" class="form-horizontal" name="form" onsubmit="verif()">*/
/*                     <div> {{ form_start(form) }}*/
/*                         {{form_errors(form) }}*/
/*                        */
/*                         <div class="form-group">*/
/*                             <label for="Quantiteproduit" class="col-sm-2 control-label">Quantite produit</label>*/
/*                             {{ form_errors(form.Quantiteproduit) }}*/
/*                             <div class="col-sm-10">*/
/*                                 {{ form_widget(form.Quantiteproduit)  }}*/
/*                             </div> */
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="DateAjout" class="col-sm-2 control-label">Date Ajout</label>*/
/*                             {{ form_errors(form.DateAjout) }}*/
/*                             <div class="col-sm-10">*/
/*                                 {{ form_widget(form.DateAjout)  }}*/
/*                             </div> */
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="Enseigne" class="col-sm-2 control-label">Enseigne</label>*/
/*                             {{ form_errors(form.Enseigne) }}*/
/*                             <div class="col-sm-10">*/
/*                                 {{ form_widget(form.Enseigne)  }}*/
/*                             </div></div> */
/*                         <div class="form-group">*/
/*                             <label for="Produit" class="col-sm-2 control-label">Produit</label>*/
/*                             {{ form_errors(form.Produit) }}*/
/*                             <div class="col-sm-10">*/
/*                                 {{ form_widget(form.Produit)  }}*/
/*                             </div></div> */
/* */
/* */
/* */
/* */
/*                         <div class="box-footer">*/
/*                             {{form_widget(form.AjouterStock) }}*/
/*                         </div>*/
/*                         {{ form_end(form)}}*/
/* */
/* */
/*                     </div>*/
/*                 </form>*/
/*             </div>*/
/*         </div>*/
/*     </div><!-- /.box -->*/
/* */
/* */
/* {% endblock %}*/
