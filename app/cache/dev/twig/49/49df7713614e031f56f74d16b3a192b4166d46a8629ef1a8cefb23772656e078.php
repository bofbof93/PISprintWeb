<?php

/* TunisiaMallEnseigneBundle:Enseigne:ModifierEnseigne.html.twig */
class __TwigTemplate_e8353adace8c9623d16450f55cf43c26f89d7dae108284f85d35bf14061d9c9e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "TunisiaMallEnseigneBundle:Enseigne:ModifierEnseigne.html.twig", 1);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 3
        echo "  <div  class =\"container\">  
        <div class=\"col-md-6\">
            <!-- Horizontal Form -->
            <div class=\"box box-info\">
                <div class=\"box-header with-border\">
                    <center>  <h3 class=\"box-title\">Modifier votre enseigne</h3></center>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class=\"form-horizontal\" name=\"form\" onsubmit=\"verif()\" method =\"POST\">
                    <div class=\"box-body\">


                        <div class=\"form-group\">
                            <label for=\"Libelle\" class=\"col-sm-2 control-label\">Libelle</label>
                            <div class=\"col-sm-10\">
                                <input name=\"Libelle\" type=\"text\" class=\"form-control\" id=\"Libelle\" placeholder=\"\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Enseigne"]) ? $context["Enseigne"] : $this->getContext($context, "Enseigne")), "Libelle", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                           <div class=\"form-group\">
                            <label for=\"Description\" class=\"col-sm-2 control-label\">Description</label>
                            <div class=\"col-sm-10\">
                                <input name=\"Description\" type=\"text\" class=\"form-control\" id=\"Description\" placeholder=\"\" value=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Enseigne"]) ? $context["Enseigne"] : $this->getContext($context, "Enseigne")), "Description", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"Etage\" class=\"col-sm-2 control-label\">Etage</label>
                            <div class=\"col-sm-10\">
                                <input name =\"Etage\" type=\"number\" class=\"form-control\" id=\"Etage\" value=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Enseigne"]) ? $context["Enseigne"] : $this->getContext($context, "Enseigne")), "Etage", array()), "html", null, true);
        echo "\" >
                            </div>
                        </div>

                        <div class=\"form-group\"> 
                            <label for=\"Catégorie\" class=\"col-sm-2 control-label\">Catégorie</label>
                            <div class=\"col-sm-10\">
                                <select name=\"Categorie\">
                                    
                                    <option value=\"Fashion\">Fashion</option>
                                    <option value=\"Gastronomie\">Gastronomie</option>
                                    <option value=\"Beauté\">Beauté</option>
                                    <option value=\"Bijoux & Accessoires\">Bijoux & Accessoires</option>
                                    <option value=\"Chaussures\">Chaussures</option>
                                    <option value=\"Culture\">Culture</option>
                                    <option value=\"Loisirs & Jeux\">Loisirs & Jeux</option>
                                    <option value=\"Maison & Décoration\">Maison & Décoration</option>
                                    <option value=\"Restaurations\">Restaurations</option>
                                    <option value=\"Services\">Services</option>
                                    <option value=\"Electronique & Hi-tech\">Electronique & Hi-tech</option>
                                    <option value=\"Parfum & Bien Etre\">Parfum & Bien Etre</option>
                                    <option value=\"Sport\">Sport</option>

                                </select> 
                            </div>  
                        </div>
                        <div class=\"form-group\">
                            <label for=\"TimeOuverture\" class=\"col-sm-2 control-label\">Heure d'ouverture</label>
                            <div class=\"col-sm-10\">
                                <input name=\"TimeOuverture\" type=\"text\" class=\"form-control\" id=\"TimeOuverture\" placeholder=\"\" value=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Enseigne"]) ? $context["Enseigne"] : $this->getContext($context, "Enseigne")), "TimeOuverture", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                            <div class=\"form-group\">
                            <label for=\"TimeFermeture\" class=\"col-sm-2 control-label\">Heure de fermeture</label>
                            <div class=\"col-sm-10\">
                                <input name=\"TimeFermeture\" type=\"text\" class=\"form-control\" id=\"TimeFermeture\" placeholder=\"\" value=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Enseigne"]) ? $context["Enseigne"] : $this->getContext($context, "Enseigne")), "TimeFermeture", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                         <div class=\"form-group\">
                            <label for=\"URL\" class=\"col-sm-2 control-label\">Site Web</label>
                            <div class=\"col-sm-10\">
                                <input name=\"URL\" type=\"text\" class=\"form-control\" id=\"URL\" placeholder=\"\" value=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Enseigne"]) ? $context["Enseigne"] : $this->getContext($context, "Enseigne")), "TimeFermeture", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"image\" class=\"col-sm-2 control-label\">Image</label>
                            <div class=\"col-sm-10\">
                                <input name =\"img\" type=\"file\" class=\"form-control\" id=\"image\" >
                            </div>
                        </div>
                        
                        <div class=\"form-group\">
                            <label for=\"NBJour\" class=\"col-sm-2 control-label\">Nombre de jours ouverts</label>
                            <div class=\"col-sm-10\">
                                <input name =\"NBJour\" type=\"number\" class=\"form-control\" id=\"NBJour\" value=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Enseigne"]) ? $context["Enseigne"] : $this->getContext($context, "Enseigne")), "NBjour", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>

                        <div class=\"form-group\"> 

                            ";
        // line 91
        echo "         

                            <div class=\"col-sm-10\">
                                ";
        // line 95
        echo "
                            </div>  
                        </div>





                    </div><!-- /.box-body -->
                    <div class=\"box-footer\">
                        <button type=\"submit\" class=\"btn btn-default\" name =\"submit\">Annuler</button>
                        <button type=\"submit\" class=\"btn btn-info pull-right\">Modifier</button>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box -->
        </div>
    </div > 
";
    }

    public function getTemplateName()
    {
        return "TunisiaMallEnseigneBundle:Enseigne:ModifierEnseigne.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 95,  142 => 91,  133 => 85,  117 => 72,  108 => 66,  99 => 60,  67 => 31,  57 => 24,  48 => 18,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block HeaderTitle %}*/
/*   <div  class ="container">  */
/*         <div class="col-md-6">*/
/*             <!-- Horizontal Form -->*/
/*             <div class="box box-info">*/
/*                 <div class="box-header with-border">*/
/*                     <center>  <h3 class="box-title">Modifier votre enseigne</h3></center>*/
/*                 </div><!-- /.box-header -->*/
/*                 <!-- form start -->*/
/*                 <form class="form-horizontal" name="form" onsubmit="verif()" method ="POST">*/
/*                     <div class="box-body">*/
/* */
/* */
/*                         <div class="form-group">*/
/*                             <label for="Libelle" class="col-sm-2 control-label">Libelle</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="Libelle" type="text" class="form-control" id="Libelle" placeholder="" value="{{Enseigne.Libelle}}">*/
/*                             </div>*/
/*                         </div>*/
/*                            <div class="form-group">*/
/*                             <label for="Description" class="col-sm-2 control-label">Description</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="Description" type="text" class="form-control" id="Description" placeholder="" value="{{Enseigne.Description}}">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="Etage" class="col-sm-2 control-label">Etage</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name ="Etage" type="number" class="form-control" id="Etage" value="{{Enseigne.Etage}}" >*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group"> */
/*                             <label for="Catégorie" class="col-sm-2 control-label">Catégorie</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <select name="Categorie">*/
/*                                     */
/*                                     <option value="Fashion">Fashion</option>*/
/*                                     <option value="Gastronomie">Gastronomie</option>*/
/*                                     <option value="Beauté">Beauté</option>*/
/*                                     <option value="Bijoux & Accessoires">Bijoux & Accessoires</option>*/
/*                                     <option value="Chaussures">Chaussures</option>*/
/*                                     <option value="Culture">Culture</option>*/
/*                                     <option value="Loisirs & Jeux">Loisirs & Jeux</option>*/
/*                                     <option value="Maison & Décoration">Maison & Décoration</option>*/
/*                                     <option value="Restaurations">Restaurations</option>*/
/*                                     <option value="Services">Services</option>*/
/*                                     <option value="Electronique & Hi-tech">Electronique & Hi-tech</option>*/
/*                                     <option value="Parfum & Bien Etre">Parfum & Bien Etre</option>*/
/*                                     <option value="Sport">Sport</option>*/
/* */
/*                                 </select> */
/*                             </div>  */
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="TimeOuverture" class="col-sm-2 control-label">Heure d'ouverture</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="TimeOuverture" type="text" class="form-control" id="TimeOuverture" placeholder="" value="{{Enseigne.TimeOuverture}}">*/
/*                             </div>*/
/*                         </div>*/
/*                             <div class="form-group">*/
/*                             <label for="TimeFermeture" class="col-sm-2 control-label">Heure de fermeture</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="TimeFermeture" type="text" class="form-control" id="TimeFermeture" placeholder="" value="{{Enseigne.TimeFermeture}}">*/
/*                             </div>*/
/*                         </div>*/
/*                          <div class="form-group">*/
/*                             <label for="URL" class="col-sm-2 control-label">Site Web</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="URL" type="text" class="form-control" id="URL" placeholder="" value="{{Enseigne.TimeFermeture}}">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="image" class="col-sm-2 control-label">Image</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name ="img" type="file" class="form-control" id="image" >*/
/*                             </div>*/
/*                         </div>*/
/*                         */
/*                         <div class="form-group">*/
/*                             <label for="NBJour" class="col-sm-2 control-label">Nombre de jours ouverts</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name ="NBJour" type="number" class="form-control" id="NBJour" value="{{Enseigne.NBjour}}">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group"> */
/* */
/*                             {#  {% for responsable in responsables %} #}         */
/* */
/*                             <div class="col-sm-10">*/
/*                                 {#   <input name ="responsable" type="text" class="form-control" id="responsable" value ="{{responsable.id}}">#}*/
/* */
/*                             </div>  */
/*                         </div>*/
/* */
/* */
/* */
/* */
/* */
/*                     </div><!-- /.box-body -->*/
/*                     <div class="box-footer">*/
/*                         <button type="submit" class="btn btn-default" name ="submit">Annuler</button>*/
/*                         <button type="submit" class="btn btn-info pull-right">Modifier</button>*/
/*                     </div><!-- /.box-footer -->*/
/*                 </form>*/
/*             </div><!-- /.box -->*/
/*         </div>*/
/*     </div > */
/* {% endblock %}*/
/* */
