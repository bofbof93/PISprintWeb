<?php

/* TunisiaMallProduitBundle:Produit:ListProduit.html.twig */
class __TwigTemplate_8aee859d586e832294d8bb965412fb82a523a1dd64316a15282bf8c524b4954c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "TunisiaMallProduitBundle:Produit:ListProduit.html.twig", 1);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 4
        echo "               
            <h1>
            Gestion Des Produits
            <small>Liste Des Produits</small>
          </h1>
            ";
    }

    // line 13
    public function block_TableView($context, array $blocks = array())
    {
        // line 14
        echo " <div style=\"padding: 10px\">
        <div class=\"TableView\" >
       <div class=\"box box-success \">    
    <div class=\"box-body no-padding\">
              <table class=\"table table-bordered table-hover\" id=\"nosprom\" >
        <thead>
                    <tr>
                       
                        <th>
                            Référence
                        </th>
                        <th>
                            Libelle
                        </th>
                      
                        <th>
                            Prix
                        </th>
                        <th>
                            Categorie
                        </th>
                        <th>
                            Image
                        </th>
                        <th>
                            NbPoint
                        </th>
                        <th>
                            Modification
                        </th>
                        <th>
                            Suppression
                        </th>
                       
                    </tr>
                   
        </thead>
        
        <tbody>            
                        ";
        // line 53
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["Produit"]);
        foreach ($context['_seq'] as $context["x"] => $context["Produit"]) {
            // line 54
            echo "        <tr>                      
                                
                       
                        <td>
                            ";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($context["Produit"], "Reference", array()), "html", null, true);
            echo "
                        </td>
                        <td>
                            ";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($context["Produit"], "Libelle", array()), "html", null, true);
            echo "
                        </td>
                       
                         <td >
                           ";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute($context["Produit"], "Prix", array()), "html", null, true);
            echo "
                        </td>
                         <td >
                           ";
            // line 68
            echo twig_escape_filter($this->env, $this->getAttribute($context["Produit"], "Categorie", array()), "html", null, true);
            echo "
                        </td>
                         <td >
                        <center> <img style=\" margin-left: -50px ; margin-right: -50px ; width: 150px ;height: 80px;\" src=\"data:image/png;base64,";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), $context["x"], array(), "array"), "html", null, true);
            echo "\" /> </center>
                        </td>
                         <td >
                           ";
            // line 74
            echo twig_escape_filter($this->env, $this->getAttribute($context["Produit"], "NbPoint", array()), "html", null, true);
            echo "
                        </td>
                         
                         <td >
                          <center><a href=\"";
            // line 78
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tunisia_mall_produit_Modif", array("id" => $this->getAttribute($context["Produit"], "id", array()))), "html", null, true);
            echo "\"><input class=\"modifIcon\" type=\"image\"  src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Modifier.png"), "html", null, true);
            echo " \" value=\"\" href=\"#\" ></center>
                         
                        </td>
                         <td >
                         <center><a href=\"";
            // line 82
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tunisia_mall_produit_Supp", array("id" => $this->getAttribute($context["Produit"], "id", array()))), "html", null, true);
            echo "\"><input class=\"modifIcon\" type=\"image\"  src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Supprimer.png"), "html", null, true);
            echo " \" value=\"\" href=\"#\" ></center>
                        </td>
                    </tr>
                    
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['x'], $context['Produit'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo "                    </tbody>
                    
                </table>
    </div></div></div></div>
                
    ";
    }

    // line 93
    public function block_btn($context, array $blocks = array())
    {
        // line 94
        echo "   
                <div>
                    <form align=\"left\" action=\"";
        // line 96
        echo "AjouterProduit";
        echo "\">
                        <center><input type=\"submit\" class=\"btn\" value=\"Ajouter Produit\"/></center>
                    </form>
                   
                    
                </div>
    ";
    }

    // line 104
    public function block_script($context, array $blocks = array())
    {
        // line 105
        echo "           <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Responsable/tabledata.js"), "html", null, true);
        echo "\"></script>
           <script src=\"";
        // line 106
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Responsable/appli.js"), "html", null, true);
        echo "\"></script>
        ";
    }

    public function getTemplateName()
    {
        return "TunisiaMallProduitBundle:Produit:ListProduit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 106,  187 => 105,  184 => 104,  173 => 96,  169 => 94,  166 => 93,  157 => 87,  144 => 82,  135 => 78,  128 => 74,  122 => 71,  116 => 68,  110 => 65,  103 => 61,  97 => 58,  91 => 54,  87 => 53,  46 => 14,  43 => 13,  34 => 4,  31 => 3,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* */
/*  {% block HeaderTitle %}*/
/*                */
/*             <h1>*/
/*             Gestion Des Produits*/
/*             <small>Liste Des Produits</small>*/
/*           </h1>*/
/*             {%endblock %}*/
/* */
/* */
/* */
/* {% block TableView %}*/
/*  <div style="padding: 10px">*/
/*         <div class="TableView" >*/
/*        <div class="box box-success ">    */
/*     <div class="box-body no-padding">*/
/*               <table class="table table-bordered table-hover" id="nosprom" >*/
/*         <thead>*/
/*                     <tr>*/
/*                        */
/*                         <th>*/
/*                             Référence*/
/*                         </th>*/
/*                         <th>*/
/*                             Libelle*/
/*                         </th>*/
/*                       */
/*                         <th>*/
/*                             Prix*/
/*                         </th>*/
/*                         <th>*/
/*                             Categorie*/
/*                         </th>*/
/*                         <th>*/
/*                             Image*/
/*                         </th>*/
/*                         <th>*/
/*                             NbPoint*/
/*                         </th>*/
/*                         <th>*/
/*                             Modification*/
/*                         </th>*/
/*                         <th>*/
/*                             Suppression*/
/*                         </th>*/
/*                        */
/*                     </tr>*/
/*                    */
/*         </thead>*/
/*         */
/*         <tbody>            */
/*                         {% for x,Produit in Produit %}*/
/*         <tr>                      */
/*                                 */
/*                        */
/*                         <td>*/
/*                             {{Produit.Reference}}*/
/*                         </td>*/
/*                         <td>*/
/*                             {{Produit.Libelle}}*/
/*                         </td>*/
/*                        */
/*                          <td >*/
/*                            {{Produit.Prix}}*/
/*                         </td>*/
/*                          <td >*/
/*                            {{Produit.Categorie}}*/
/*                         </td>*/
/*                          <td >*/
/*                         <center> <img style=" margin-left: -50px ; margin-right: -50px ; width: 150px ;height: 80px;" src="data:image/png;base64,{{image[x]}}" /> </center>*/
/*                         </td>*/
/*                          <td >*/
/*                            {{Produit.NbPoint}}*/
/*                         </td>*/
/*                          */
/*                          <td >*/
/*                           <center><a href="{{path('tunisia_mall_produit_Modif',{'id':Produit.id})}}"><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Modifier.png')}} " value="" href="#" ></center>*/
/*                          */
/*                         </td>*/
/*                          <td >*/
/*                          <center><a href="{{path('tunisia_mall_produit_Supp',{'id':Produit.id})}}"><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Supprimer.png')}} " value="" href="#" ></center>*/
/*                         </td>*/
/*                     </tr>*/
/*                     */
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                     */
/*                 </table>*/
/*     </div></div></div></div>*/
/*                 */
/*     {% endblock TableView %}    */
/* {% block btn %}*/
/*    */
/*                 <div>*/
/*                     <form align="left" action="{{('AjouterProduit')}}">*/
/*                         <center><input type="submit" class="btn" value="Ajouter Produit"/></center>*/
/*                     </form>*/
/*                    */
/*                     */
/*                 </div>*/
/*     {% endblock %}*/
/*     */
/*      {% block script %}*/
/*            <script src="{{asset('js/Responsable/tabledata.js')}}"></script>*/
/*            <script src="{{asset('js/Responsable/appli.js')}}"></script>*/
/*         {% endblock %}*/
/*             */
