<?php

/* TunisiaMallProduitBundle:Produit:ModifierProduit.html.twig */
class __TwigTemplate_7dba77884449011debb0bd337abffbac912cbc9b7f55365ebeb7a4884d57ffd5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "TunisiaMallProduitBundle:Produit:ModifierProduit.html.twig", 1);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 3
        echo "               
            <h1>
            Modifier
            <small>Des Produits</small>
          </h1>
            ";
    }

    // line 10
    public function block_contenu($context, array $blocks = array())
    {
        // line 11
        echo "   
 <div class=\"container\">       
    <div class=\"col-md-6\">
              <!-- Horizontal Form -->
              <div class=\"box box-info\">
                <div class=\"box-header with-border\">
                  <h3 class=\"box-title\">Modifier une produit</h3>
                </div>
              <!-- /.box-header -->
              <div class=\"box-body\">
                <!-- form start -->
 <form  method=\"POST\" class=\"form-horizontal\" name=\"form\" onsubmit=\"verif()\">
            <div> ";
        // line 23
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
                ";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
                 <div class=\"form-group\">
                   <label for=\"Reference\" class=\"col-sm-2 control-label\">Reference</label>
                    ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Reference", array()), 'errors');
        echo "
                    <div class=\"col-sm-10\">
                    ";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Reference", array()), 'widget');
        echo "
                    </div> 
                 </div>
                    <div class=\"form-group\">
                   <label for=\"Libelle\" class=\"col-sm-2 control-label\">Libelle</label>
                    ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Libelle", array()), 'errors');
        echo "
                    <div class=\"col-sm-10\">
                    ";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Libelle", array()), 'widget');
        echo "
                    </div> 
                 </div>
                    <div class=\"form-group\">
                   <label for=\"Descriptif\" class=\"col-sm-2 control-label\">Descriptif</label>
                    ";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Descriptif", array()), 'errors');
        echo "
                    <div class=\"col-sm-10\">
                    ";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Descriptif", array()), 'widget');
        echo "
                    </div> 
                 </div>
                    <div class=\"form-group\">
                   <label for=\"Prix\" class=\"col-sm-2 control-label\">Prix</label>
                    ";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Prix", array()), 'errors');
        echo "
                    <div class=\"col-sm-10\">
                    ";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Prix", array()), 'widget');
        echo "
                    </div> 
                 </div>
                    <div class=\"form-group\">
                   <label for=\"Categorie\" class=\"col-sm-2 control-label\">Categorie</label>
                    ";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Categorie", array()), 'errors');
        echo "
                    <div class=\"col-sm-10\">
                    ";
        // line 57
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Categorie", array()), 'widget');
        echo "
                    </div> 
                 </div>
                    <div class=\"form-group\">
                   <label for=\"Image\" class=\"col-sm-2 control-label\">Image</label>
                    ";
        // line 62
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Image", array()), 'errors');
        echo "
                    <div class=\"col-sm-10\">
                    ";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Image", array()), 'widget');
        echo "
                    </div> 
                 </div>
                    <div class=\"form-group\">
                   <label for=\"Note\" class=\"col-sm-2 control-label\">Note</label>
                    ";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Note", array()), 'errors');
        echo "
                    <div class=\"col-sm-10\">
                    ";
        // line 71
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Note", array()), 'widget');
        echo "
                    </div> 
                 </div>
                     <div class=\"form-group\">
                   <label for=\"NbPoint\" class=\"col-sm-2 control-label\">NbPoint</label>
                    ";
        // line 76
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "NbPoint", array()), 'errors');
        echo "
                    <div class=\"col-sm-10\">
                    ";
        // line 78
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "NbPoint", array()), 'widget');
        echo "
                    </div></div> 
                     <div class=\"form-group\">
                   <label for=\"Enseigne\" class=\"col-sm-2 control-label\">Enseigne</label>
                    ";
        // line 82
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Enseigne", array()), 'errors');
        echo "
                    <div class=\"col-sm-10\">
                    ";
        // line 84
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Enseigne", array()), 'widget');
        echo "
                    </div> </div>
                     <div class=\"form-group\">
                   <label for=\"Promotion\" class=\"col-sm-2 control-label\">Promotion</label>
                    ";
        // line 88
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Promotion", array()), 'errors');
        echo "
                    <div class=\"col-sm-10\">
                    ";
        // line 90
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Promotion", array()), 'widget');
        echo "
                    </div></div> 
                    <div class=\"form-group\">
                   <label for=\"Catalogue\" class=\"col-sm-2 control-label\">Catalogue</label>
                    ";
        // line 94
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Catalogue", array()), 'errors');
        echo "
                    <div class=\"col-sm-10\">
                    ";
        // line 96
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Catalogue", array()), 'widget');
        echo "
                    </div></div> 
                     <div class=\"form-group\">
                   <label for=\"Panier\" class=\"col-sm-2 control-label\">Panier</label>
                    ";
        // line 100
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Panier", array()), 'errors');
        echo "
                    <div class=\"col-sm-10\">
                    ";
        // line 102
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Panier", array()), 'widget');
        echo "
                    </div> </div>
                   
                    
                    
                    
            <div class=\"box-footer\">
            ";
        // line 109
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "AjouterProduit", array()), 'widget');
        echo "
            </div>
            ";
        // line 111
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
            
            
            </div>
                         </form>
              </div>
</div>
              </div><!-- /.box -->
        </div>

    ";
    }

    public function getTemplateName()
    {
        return "TunisiaMallProduitBundle:Produit:ModifierProduit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  227 => 111,  222 => 109,  212 => 102,  207 => 100,  200 => 96,  195 => 94,  188 => 90,  183 => 88,  176 => 84,  171 => 82,  164 => 78,  159 => 76,  151 => 71,  146 => 69,  138 => 64,  133 => 62,  125 => 57,  120 => 55,  112 => 50,  107 => 48,  99 => 43,  94 => 41,  86 => 36,  81 => 34,  73 => 29,  68 => 27,  62 => 24,  58 => 23,  44 => 11,  41 => 10,  32 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block HeaderTitle %}*/
/*                */
/*             <h1>*/
/*             Modifier*/
/*             <small>Des Produits</small>*/
/*           </h1>*/
/*             {%endblock %}*/
/* */
/* {% block contenu %}*/
/*    */
/*  <div class="container">       */
/*     <div class="col-md-6">*/
/*               <!-- Horizontal Form -->*/
/*               <div class="box box-info">*/
/*                 <div class="box-header with-border">*/
/*                   <h3 class="box-title">Modifier une produit</h3>*/
/*                 </div>*/
/*               <!-- /.box-header -->*/
/*               <div class="box-body">*/
/*                 <!-- form start -->*/
/*  <form  method="POST" class="form-horizontal" name="form" onsubmit="verif()">*/
/*             <div> {{ form_start(form) }}*/
/*                 {{form_errors(form) }}*/
/*                  <div class="form-group">*/
/*                    <label for="Reference" class="col-sm-2 control-label">Reference</label>*/
/*                     {{ form_errors(form.Reference) }}*/
/*                     <div class="col-sm-10">*/
/*                     {{ form_widget(form.Reference)  }}*/
/*                     </div> */
/*                  </div>*/
/*                     <div class="form-group">*/
/*                    <label for="Libelle" class="col-sm-2 control-label">Libelle</label>*/
/*                     {{ form_errors(form.Libelle) }}*/
/*                     <div class="col-sm-10">*/
/*                     {{ form_widget(form.Libelle)  }}*/
/*                     </div> */
/*                  </div>*/
/*                     <div class="form-group">*/
/*                    <label for="Descriptif" class="col-sm-2 control-label">Descriptif</label>*/
/*                     {{ form_errors(form.Descriptif) }}*/
/*                     <div class="col-sm-10">*/
/*                     {{ form_widget(form.Descriptif)  }}*/
/*                     </div> */
/*                  </div>*/
/*                     <div class="form-group">*/
/*                    <label for="Prix" class="col-sm-2 control-label">Prix</label>*/
/*                     {{ form_errors(form.Prix) }}*/
/*                     <div class="col-sm-10">*/
/*                     {{ form_widget(form.Prix)  }}*/
/*                     </div> */
/*                  </div>*/
/*                     <div class="form-group">*/
/*                    <label for="Categorie" class="col-sm-2 control-label">Categorie</label>*/
/*                     {{ form_errors(form.Categorie) }}*/
/*                     <div class="col-sm-10">*/
/*                     {{ form_widget(form.Categorie)  }}*/
/*                     </div> */
/*                  </div>*/
/*                     <div class="form-group">*/
/*                    <label for="Image" class="col-sm-2 control-label">Image</label>*/
/*                     {{ form_errors(form.Image) }}*/
/*                     <div class="col-sm-10">*/
/*                     {{ form_widget(form.Image)  }}*/
/*                     </div> */
/*                  </div>*/
/*                     <div class="form-group">*/
/*                    <label for="Note" class="col-sm-2 control-label">Note</label>*/
/*                     {{ form_errors(form.Note) }}*/
/*                     <div class="col-sm-10">*/
/*                     {{ form_widget(form.Note)  }}*/
/*                     </div> */
/*                  </div>*/
/*                      <div class="form-group">*/
/*                    <label for="NbPoint" class="col-sm-2 control-label">NbPoint</label>*/
/*                     {{ form_errors(form.NbPoint) }}*/
/*                     <div class="col-sm-10">*/
/*                     {{ form_widget(form.NbPoint)  }}*/
/*                     </div></div> */
/*                      <div class="form-group">*/
/*                    <label for="Enseigne" class="col-sm-2 control-label">Enseigne</label>*/
/*                     {{ form_errors(form.Enseigne) }}*/
/*                     <div class="col-sm-10">*/
/*                     {{ form_widget(form.Enseigne)  }}*/
/*                     </div> </div>*/
/*                      <div class="form-group">*/
/*                    <label for="Promotion" class="col-sm-2 control-label">Promotion</label>*/
/*                     {{ form_errors(form.Promotion) }}*/
/*                     <div class="col-sm-10">*/
/*                     {{ form_widget(form.Promotion)  }}*/
/*                     </div></div> */
/*                     <div class="form-group">*/
/*                    <label for="Catalogue" class="col-sm-2 control-label">Catalogue</label>*/
/*                     {{ form_errors(form.Catalogue) }}*/
/*                     <div class="col-sm-10">*/
/*                     {{ form_widget(form.Catalogue)  }}*/
/*                     </div></div> */
/*                      <div class="form-group">*/
/*                    <label for="Panier" class="col-sm-2 control-label">Panier</label>*/
/*                     {{ form_errors(form.Panier) }}*/
/*                     <div class="col-sm-10">*/
/*                     {{ form_widget(form.Panier)  }}*/
/*                     </div> </div>*/
/*                    */
/*                     */
/*                     */
/*                     */
/*             <div class="box-footer">*/
/*             {{form_widget(form.AjouterProduit) }}*/
/*             </div>*/
/*             {{ form_end(form)}}*/
/*             */
/*             */
/*             </div>*/
/*                          </form>*/
/*               </div>*/
/* </div>*/
/*               </div><!-- /.box -->*/
/*         </div>*/
/* */
/*     {% endblock %}*/
