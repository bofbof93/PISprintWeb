<?php

/* TunisiaMallEnseigneBundle:Enseigne:AjouterEnseigne.html.twig */
class __TwigTemplate_092888c6419d319803ea8eb5d7027812ee0b97dbfb206c84d1e6c182f4d688a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "TunisiaMallEnseigneBundle:Enseigne:AjouterEnseigne.html.twig", 1);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 4
        echo " 
    <div  class =\"container\">  
 
        <div class=\"col-md-6\">
            <!-- Horizontal Form -->
            <div class=\"box box-info\">
                <div class=\"box-header with-border\">
                    <center>  <h3 class=\"box-title\">Ajouter une enseigne</h3></center>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class=\"form-horizontal\" name=\"form\" onsubmit=\"verif()\" method =\"POST\">
                    <div class=\"box-body\">


                        <div class=\"form-group\">
                            <label for=\"Libelle\" class=\"col-sm-2 control-label\">Libelle</label>
                            <div class=\"col-sm-10\">
                                <input name=\"Libelle\" type=\"text\" class=\"form-control\" id=\"Libelle\" placeholder=\"\">
                            </div>
                        </div>
                           <div class=\"form-group\">
                            <label for=\"Description\" class=\"col-sm-2 control-label\">Description</label>
                            <div class=\"col-sm-10\">
                                <input name=\"Description\" type=\"text\" class=\"form-control\" id=\"Description\" placeholder=\"\">
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"Etage\" class=\"col-sm-2 control-label\">Etage</label>
                            <div class=\"col-sm-10\">
                                <input name =\"Etage\" type=\"number\" class=\"form-control\" id=\"Etage\" >
                            </div>
                        </div>

                        <div class=\"form-group\"> 
                            <label for=\"Catégorie\" class=\"col-sm-2 control-label\">Catégorie</label>
                            <div class=\"col-sm-10\">
                                <select name=\"Categorie\">
                                    
                                    <option value=\"Fashion\">Fashion</option>
                                    <option value=\"Gastronomie\">Gastronomie</option>
                                    <option value=\"Beauté\">Beauté</option>
                                    <option value=\"Bijoux & Accessoires\">Bijoux & Accessoires</option>
                                    <option value=\"Chaussures\">Chaussures</option>
                                    <option value=\"Culture\">Culture</option>
                                    <option value=\"Loisirs & Jeux\">Loisirs & Jeux</option>
                                    <option value=\"Maison & Décoration\">Maison & Décoration</option>
                                    <option value=\"Restaurations\">Restaurations</option>
                                    <option value=\"Services\">Services</option>
                                    <option value=\"Electronique & Hi-tech\">Electronique & Hi-tech</option>
                                    <option value=\"Parfum & Bien Etre\">Parfum & Bien Etre</option>
                                    <option value=\"Sport\">Sport</option>

                                </select> 
                            </div>  
                        </div>
                        <div class=\"form-group\">
                            <label for=\"TimeOuverture\" class=\"col-sm-2 control-label\">Heure d'ouverture</label>
                            <div class=\"col-sm-10\">
                                <input name=\"TimeOuverture\" type=\"text\" class=\"form-control\" id=\"TimeOuverture\" placeholder=\"\">
                            </div>
                        </div>
                            <div class=\"form-group\">
                            <label for=\"TimeFermeture\" class=\"col-sm-2 control-label\">Heure de fermeture</label>
                            <div class=\"col-sm-10\">
                                <input name=\"TimeFermeture\" type=\"text\" class=\"form-control\" id=\"TimeFermeture\" placeholder=\"\">
                            </div>
                        </div>
                         <div class=\"form-group\">
                            <label for=\"URL\" class=\"col-sm-2 control-label\">Site Web</label>
                            <div class=\"col-sm-10\">
                                <input name=\"URL\" type=\"text\" class=\"form-control\" id=\"URL\" placeholder=\"\">
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"image\" class=\"col-sm-2 control-label\">Image</label>
                            <div class=\"col-sm-10\">
                                <input name =\"imaEns\" type=\"file\" class=\"form-control\" id=\"image\" >
                            </div>
                        </div>
                        
                        <div class=\"form-group\">
                            <label for=\"NBJour\" class=\"col-sm-2 control-label\">Nombre de jours ouverts</label>
                            <div class=\"col-sm-10\">
                                <input name =\"NBJour\" type=\"number\" class=\"form-control\" id=\"NBJour\" >
                            </div>
                        </div>

                        <div class=\"form-group\"> 

                            ";
        // line 94
        echo "         

                            <div class=\"col-sm-10\">
                                ";
        // line 98
        echo "
                            </div>  
                        </div>





                    </div><!-- /.box-body -->
                    <div class=\"box-footer\">
                        <button type=\"submit\" class=\"btn btn-default\" name =\"submit\">Annuler</button>
                        <button type=\"submit\" class=\"btn btn-info pull-right\">Ajouter enseigne</button>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box -->
        </div>
                              
    </div >
     
";
    }

    public function getTemplateName()
    {
        return "TunisiaMallEnseigneBundle:Enseigne:AjouterEnseigne.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 98,  123 => 94,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/*   */
/* {% block HeaderTitle %}*/
/*  */
/*     <div  class ="container">  */
/*  */
/*         <div class="col-md-6">*/
/*             <!-- Horizontal Form -->*/
/*             <div class="box box-info">*/
/*                 <div class="box-header with-border">*/
/*                     <center>  <h3 class="box-title">Ajouter une enseigne</h3></center>*/
/*                 </div><!-- /.box-header -->*/
/*                 <!-- form start -->*/
/*                 <form class="form-horizontal" name="form" onsubmit="verif()" method ="POST">*/
/*                     <div class="box-body">*/
/* */
/* */
/*                         <div class="form-group">*/
/*                             <label for="Libelle" class="col-sm-2 control-label">Libelle</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="Libelle" type="text" class="form-control" id="Libelle" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/*                            <div class="form-group">*/
/*                             <label for="Description" class="col-sm-2 control-label">Description</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="Description" type="text" class="form-control" id="Description" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="Etage" class="col-sm-2 control-label">Etage</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name ="Etage" type="number" class="form-control" id="Etage" >*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group"> */
/*                             <label for="Catégorie" class="col-sm-2 control-label">Catégorie</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <select name="Categorie">*/
/*                                     */
/*                                     <option value="Fashion">Fashion</option>*/
/*                                     <option value="Gastronomie">Gastronomie</option>*/
/*                                     <option value="Beauté">Beauté</option>*/
/*                                     <option value="Bijoux & Accessoires">Bijoux & Accessoires</option>*/
/*                                     <option value="Chaussures">Chaussures</option>*/
/*                                     <option value="Culture">Culture</option>*/
/*                                     <option value="Loisirs & Jeux">Loisirs & Jeux</option>*/
/*                                     <option value="Maison & Décoration">Maison & Décoration</option>*/
/*                                     <option value="Restaurations">Restaurations</option>*/
/*                                     <option value="Services">Services</option>*/
/*                                     <option value="Electronique & Hi-tech">Electronique & Hi-tech</option>*/
/*                                     <option value="Parfum & Bien Etre">Parfum & Bien Etre</option>*/
/*                                     <option value="Sport">Sport</option>*/
/* */
/*                                 </select> */
/*                             </div>  */
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="TimeOuverture" class="col-sm-2 control-label">Heure d'ouverture</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="TimeOuverture" type="text" class="form-control" id="TimeOuverture" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/*                             <div class="form-group">*/
/*                             <label for="TimeFermeture" class="col-sm-2 control-label">Heure de fermeture</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="TimeFermeture" type="text" class="form-control" id="TimeFermeture" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/*                          <div class="form-group">*/
/*                             <label for="URL" class="col-sm-2 control-label">Site Web</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name="URL" type="text" class="form-control" id="URL" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label for="image" class="col-sm-2 control-label">Image</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name ="imaEns" type="file" class="form-control" id="image" >*/
/*                             </div>*/
/*                         </div>*/
/*                         */
/*                         <div class="form-group">*/
/*                             <label for="NBJour" class="col-sm-2 control-label">Nombre de jours ouverts</label>*/
/*                             <div class="col-sm-10">*/
/*                                 <input name ="NBJour" type="number" class="form-control" id="NBJour" >*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="form-group"> */
/* */
/*                             {#  {% for responsable in responsables %} #}         */
/* */
/*                             <div class="col-sm-10">*/
/*                                 {#   <input name ="responsable" type="text" class="form-control" id="responsable" value ="{{responsable.id}}">#}*/
/* */
/*                             </div>  */
/*                         </div>*/
/* */
/* */
/* */
/* */
/* */
/*                     </div><!-- /.box-body -->*/
/*                     <div class="box-footer">*/
/*                         <button type="submit" class="btn btn-default" name ="submit">Annuler</button>*/
/*                         <button type="submit" class="btn btn-info pull-right">Ajouter enseigne</button>*/
/*                     </div><!-- /.box-footer -->*/
/*                 </form>*/
/*             </div><!-- /.box -->*/
/*         </div>*/
/*                               */
/*     </div >*/
/*      */
/* {% endblock %}*/
/* */
