<?php

/* TunisiaMallCarteBundle:Carte:ajout.html.twig */
class __TwigTemplate_e9d25d35731bef1735fc9bad7972624e6f72ae42ace4809d3f2931b6150ab17a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "TunisiaMallCarteBundle:Carte:ajout.html.twig", 2);
        $this->blocks = array(
            'TableView' => array($this, 'block_TableView'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_TableView($context, array $blocks = array())
    {
        // line 4
        echo "
    <div class=\"col-md-6\">
        <!-- Horizontal Form -->
        <div class=\"box box-info\">
            <div class=\"box-header with-border\">
                <h3 class=\"box-title\">Ajouter Carte</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form class=\"form-horizontal\" name=\"form\" method=\"POST\">
                <div class=\"box-body\">

                <div class=\"form-group\">
                        <label for=\"Titre\" class=\"col-sm-2 control-label\" > Numero </label>
                        <div class=\"col-sm-10\">
                            <input name=\"numero\" id=\"exampleInputfile\" type=\"text\"class=\"form-control\"/>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <label for=\"Image\" class=\"col-sm-2 control-label\" > Nbre point </label>
                        <div class=\"col-sm-10\">
                            <input name=\"nbrepoint\" id=\"exampleInputfile\" type=\"text\"class=\"form-control\"/>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <label for=\"user\" class=\"col-sm-2 control-label\">User</label>
                        <div class=\"col-sm-10\">
                             <select name=\"user\">
                                 ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")));
        foreach ($context['_seq'] as $context["_key"] => $context["u"]) {
            // line 34
            echo "                                    <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "username", array()), "html", null, true);
            echo "</option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['u'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "                                </select> 
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class=\"box-footer\">
                    <button type=\"submit\" class=\"btn btn-default\">Annuler</button>
                    <input type=\"submit\" value=\"Ajouter\" class=\"btn btn-info pull-right\"/>
                </div><!-- /.box-footer -->
            </form>
        </div><!-- /.box -->
    </div>

";
    }

    public function getTemplateName()
    {
        return "TunisiaMallCarteBundle:Carte:ajout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 36,  66 => 34,  62 => 33,  31 => 4,  28 => 3,  11 => 2,);
    }
}
/*     {# empty Twig template #}*/
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block TableView %}*/
/* */
/*     <div class="col-md-6">*/
/*         <!-- Horizontal Form -->*/
/*         <div class="box box-info">*/
/*             <div class="box-header with-border">*/
/*                 <h3 class="box-title">Ajouter Carte</h3>*/
/*             </div><!-- /.box-header -->*/
/*             <!-- form start -->*/
/*             <form class="form-horizontal" name="form" method="POST">*/
/*                 <div class="box-body">*/
/* */
/*                 <div class="form-group">*/
/*                         <label for="Titre" class="col-sm-2 control-label" > Numero </label>*/
/*                         <div class="col-sm-10">*/
/*                             <input name="numero" id="exampleInputfile" type="text"class="form-control"/>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="form-group">*/
/*                         <label for="Image" class="col-sm-2 control-label" > Nbre point </label>*/
/*                         <div class="col-sm-10">*/
/*                             <input name="nbrepoint" id="exampleInputfile" type="text"class="form-control"/>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="form-group">*/
/*                         <label for="user" class="col-sm-2 control-label">User</label>*/
/*                         <div class="col-sm-10">*/
/*                              <select name="user">*/
/*                                  {% for u in users%}*/
/*                                     <option value="{{u.id}}">{{u.username}}</option>*/
/*                                     {%endfor%}*/
/*                                 </select> */
/*                         </div>*/
/*                     </div>*/
/*                 </div><!-- /.box-body -->*/
/*                 <div class="box-footer">*/
/*                     <button type="submit" class="btn btn-default">Annuler</button>*/
/*                     <input type="submit" value="Ajouter" class="btn btn-info pull-right"/>*/
/*                 </div><!-- /.box-footer -->*/
/*             </form>*/
/*         </div><!-- /.box -->*/
/*     </div>*/
/* */
/* {% endblock %}*/
