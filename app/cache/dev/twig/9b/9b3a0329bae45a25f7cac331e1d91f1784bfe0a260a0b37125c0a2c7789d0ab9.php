<?php

/* ResponsableBundle:Default:AjouterPromotion.html.twig */
class __TwigTemplate_3d48d33121f82f673cfec476c82ad24cc74d31e8e9369ddc9053431fa0e084e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "ResponsableBundle:Default:AjouterPromotion.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        // line 4
        echo "   
           <div class=\"col-md-6\">
              <!-- Horizontal Form -->
              <div class=\"box box-info\">
                <div class=\"box-header with-border\">
                  <h3 class=\"box-title\">Ajouter une promotion</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class=\"form-horizontal\" name=\"form\" onsubmit=\"verif()\">
                  <div class=\"box-body\">
                      
                    <div class=\"form-group\">
                      <label for=\"date1\" class=\"col-sm-2 control-label\">Date début </label>
                      <div class=\"col-sm-10\">
                        <input name=\"date1\" type=\"date\" class=\"form-control\" id=\"date1\">
                      </div>
                    </div>
                      
                    <div class=\"form-group\">
                      <label for=\"date2\" class=\"col-sm-2 control-label\">Date fin </label>
                      <div class=\"col-sm-10\">
                        <input name=\"date2\" type=\"date\" class=\"form-control\" id=\"date2\">
                      </div>
                    </div>
                      
                    <div class=\"form-group\">
                      <label for=\"taux\" class=\"col-sm-2 control-label\">Taux de réduction</label>
                      <div class=\"col-sm-10\">
                        <input type=\"number\" class=\"form-control\" id=\"inputPassword3\" placeholder=\"\">
                      </div>
                    </div>
                      
                   <div class=\"form-group\">
                      <label for=\"image\" class=\"col-sm-2 control-label\">Image</label>
                      <div class=\"col-sm-10\">
                        <input type=\"file\" class=\"form-control\" id=\"image\" >
                      </div>
                    </div>
 
                  </div><!-- /.box-body -->
                  <div class=\"box-footer\">
                    <button type=\"submit\" class=\"btn btn-default\">Cancel</button>
                    <button type=\"submit\" class=\"btn btn-info pull-right\">Sign in</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
           </div>
    ";
    }

    public function getTemplateName()
    {
        return "ResponsableBundle:Default:AjouterPromotion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block contenu %}*/
/*    */
/*            <div class="col-md-6">*/
/*               <!-- Horizontal Form -->*/
/*               <div class="box box-info">*/
/*                 <div class="box-header with-border">*/
/*                   <h3 class="box-title">Ajouter une promotion</h3>*/
/*                 </div><!-- /.box-header -->*/
/*                 <!-- form start -->*/
/*                 <form class="form-horizontal" name="form" onsubmit="verif()">*/
/*                   <div class="box-body">*/
/*                       */
/*                     <div class="form-group">*/
/*                       <label for="date1" class="col-sm-2 control-label">Date début </label>*/
/*                       <div class="col-sm-10">*/
/*                         <input name="date1" type="date" class="form-control" id="date1">*/
/*                       </div>*/
/*                     </div>*/
/*                       */
/*                     <div class="form-group">*/
/*                       <label for="date2" class="col-sm-2 control-label">Date fin </label>*/
/*                       <div class="col-sm-10">*/
/*                         <input name="date2" type="date" class="form-control" id="date2">*/
/*                       </div>*/
/*                     </div>*/
/*                       */
/*                     <div class="form-group">*/
/*                       <label for="taux" class="col-sm-2 control-label">Taux de réduction</label>*/
/*                       <div class="col-sm-10">*/
/*                         <input type="number" class="form-control" id="inputPassword3" placeholder="">*/
/*                       </div>*/
/*                     </div>*/
/*                       */
/*                    <div class="form-group">*/
/*                       <label for="image" class="col-sm-2 control-label">Image</label>*/
/*                       <div class="col-sm-10">*/
/*                         <input type="file" class="form-control" id="image" >*/
/*                       </div>*/
/*                     </div>*/
/*  */
/*                   </div><!-- /.box-body -->*/
/*                   <div class="box-footer">*/
/*                     <button type="submit" class="btn btn-default">Cancel</button>*/
/*                     <button type="submit" class="btn btn-info pull-right">Sign in</button>*/
/*                   </div><!-- /.box-footer -->*/
/*                 </form>*/
/*               </div><!-- /.box -->*/
/*            </div>*/
/*     {% endblock %}*/
/* */
