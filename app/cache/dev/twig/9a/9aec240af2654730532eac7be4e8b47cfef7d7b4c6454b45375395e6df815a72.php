<?php

/* TunisiaMallActualiteBundle:Actualite:ListActualite.html.twig */
class __TwigTemplate_c57d8223a05398c1fc879e4d1d8c3d79cb13062776f5d25f7ed51586b06febb4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "TunisiaMallActualiteBundle:Actualite:ListActualite.html.twig", 1);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 3
        echo "               
            <h1>
           Liste des Actualites
          </h1>
            ";
    }

    // line 9
    public function block_TableView($context, array $blocks = array())
    {
        // line 10
        echo "<div class=\"TableView\" >
                <table >
                    <tr>
                        <td>
                            Id
                        </td>
                         <td>
                            Titre
                        </td>
                        <td >
                           Date
                        </td>
                        <td>
                           Image
                        </td>
                        <td>
                            Description
                        </td>
                        <td>
                            Modifier
                        </td>
                        <td>
                            Supprimer
                        </td>
                    ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["Actualites"]) ? $context["Actualites"] : $this->getContext($context, "Actualites")));
        foreach ($context['_seq'] as $context["_key"] => $context["actualite"]) {
            echo "    
                    <tr>
                        <td >
                       ";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["actualite"], "id", array()), "html", null, true);
            echo "
                        </td>
                        <td>
                            ";
            // line 40
            echo twig_escape_filter($this->env, twig_wordwrap_filter($this->env, $this->getAttribute($context["actualite"], "Titre", array()), 100), "html", null, true);
            echo "
                        </td>
                        
                        <td>
                            ";
            // line 44
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["actualite"], "date", array()), "Y-m-d:h:m"), "html", null, true);
            echo "
                        </td>
                        <td>
                        <img src=\"data:image/jpg;base64,base64_decode(";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["actualite"], "Image", array()), "html", null, true);
            echo ")\"/>
                        </td>
                        <td>
                           ";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["actualite"], "Description", array()), "html", null, true);
            echo "
                        </td>
                         <td >
                          
                        
                         <a href=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tunisia_mall_actualite_MAJ", array("id" => $this->getAttribute($context["actualite"], "id", array()))), "html", null, true);
            echo "\"> <center><input class=\"modifIcon\" type=\"image\"  src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Modifier.png"), "html", null, true);
            echo " \" value=\"\" href=\"\" ></center></a>
                        </td>
                         <td >
                         <a href=\"";
            // line 58
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tunisia_mall_actualite_Supprimer", array("id" => $this->getAttribute($context["actualite"], "id", array()))), "html", null, true);
            echo "\"><center><input class=\"suppIcon\" type=\"image\"  src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/Supprimer.png"), "html", null, true);
            echo " \" value=\"\"  ></center></a>
                        </td>
                    </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['actualite'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "                    
                </table>
            </div>
    ";
    }

    // line 66
    public function block_btn($context, array $blocks = array())
    {
        // line 67
        echo "         <br>
        <div>

            <a href='";
        // line 70
        echo "AjouterActualite";
        echo "'> <input type=\"submit\" class=\"btn\" value=\"Ajouter Actualité\"/> </a>



        </div>

    ";
    }

    public function getTemplateName()
    {
        return "TunisiaMallActualiteBundle:Actualite:ListActualite.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 70,  141 => 67,  138 => 66,  131 => 62,  119 => 58,  111 => 55,  103 => 50,  97 => 47,  91 => 44,  84 => 40,  78 => 37,  70 => 34,  44 => 10,  41 => 9,  33 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block HeaderTitle %}*/
/*                */
/*             <h1>*/
/*            Liste des Actualites*/
/*           </h1>*/
/*             {%endblock %}*/
/* */
/* {% block TableView %}*/
/* <div class="TableView" >*/
/*                 <table >*/
/*                     <tr>*/
/*                         <td>*/
/*                             Id*/
/*                         </td>*/
/*                          <td>*/
/*                             Titre*/
/*                         </td>*/
/*                         <td >*/
/*                            Date*/
/*                         </td>*/
/*                         <td>*/
/*                            Image*/
/*                         </td>*/
/*                         <td>*/
/*                             Description*/
/*                         </td>*/
/*                         <td>*/
/*                             Modifier*/
/*                         </td>*/
/*                         <td>*/
/*                             Supprimer*/
/*                         </td>*/
/*                     {% for actualite in Actualites %}    */
/*                     <tr>*/
/*                         <td >*/
/*                        {{ actualite.id}}*/
/*                         </td>*/
/*                         <td>*/
/*                             {{ actualite.Titre|wordwrap(100)}}*/
/*                         </td>*/
/*                         */
/*                         <td>*/
/*                             {{actualite.date| date('Y-m-d:h:m')}}*/
/*                         </td>*/
/*                         <td>*/
/*                         <img src="data:image/jpg;base64,base64_decode({{ actualite.Image}})"/>*/
/*                         </td>*/
/*                         <td>*/
/*                            {{actualite.Description}}*/
/*                         </td>*/
/*                          <td >*/
/*                           */
/*                         */
/*                          <a href="{{ path('tunisia_mall_actualite_MAJ', { 'id': actualite.id }) }}"> <center><input class="modifIcon" type="image"  src="{{asset('images/Responsable/Modifier.png')}} " value="" href="" ></center></a>*/
/*                         </td>*/
/*                          <td >*/
/*                          <a href="{{ path('tunisia_mall_actualite_Supprimer', { 'id': actualite.id }) }}"><center><input class="suppIcon" type="image"  src="{{asset('images/Responsable/Supprimer.png')}} " value=""  ></center></a>*/
/*                         </td>*/
/*                     </tr>*/
/*                     {% endfor %}*/
/*                     */
/*                 </table>*/
/*             </div>*/
/*     {% endblock TableView %}*/
/*     {% block btn %}*/
/*          <br>*/
/*         <div>*/
/* */
/*             <a href='{{('AjouterActualite')}}'> <input type="submit" class="btn" value="Ajouter Actualité"/> </a>*/
/* */
/* */
/* */
/*         </div>*/
/* */
/*     {% endblock %}*/
/* {# empty Twig template #}*/
/* */
