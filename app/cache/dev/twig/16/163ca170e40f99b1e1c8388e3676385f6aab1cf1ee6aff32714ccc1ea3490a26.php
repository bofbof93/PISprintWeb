<?php

/* AdminBundle:Default:reclamation.html.twig */
class __TwigTemplate_7b91f167d4efec7b34d3cab43aa061e6b14f8381991118dd7ed8a28b9cb89bac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "AdminBundle:Default:reclamation.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        // line 4
        echo "   
           <div class=\"col-md-6\">
              <!-- Horizontal Form -->
              <div class=\"box box-info\">
                <div class=\"box-header with-border\">
                  <h3 class=\"box-title\">Ajouter une promotion</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class=\"form-horizontal\">
                  <div class=\"box-body\">
                      
                    <div class=\"form-group\">
                      <label for=\"inputEmail3\" class=\"col-sm-2 control-label\">Email</label>
                      <div class=\"col-sm-10\">
                        <input type=\"email\" class=\"form-control\" id=\"inputEmail3\" placeholder=\"Email\">
                      </div>
                    </div>
                      
                    <div class=\"form-group\">
                      <label for=\"inputPassword3\" class=\"col-sm-2 control-label\">Password</label>
                      <div class=\"col-sm-10\">
                        <input type=\"password\" class=\"form-control\" id=\"inputPassword3\" placeholder=\"Password\">
                      </div>
                    </div>
                      
                    <div class=\"form-group\">
                      <label for=\"inputPassword3\" class=\"col-sm-2 control-label\">texte</label>
                      <div class=\"col-sm-10\">
                        <textarea rows=\"5\" cols=\"50\" class=\"form-control\" id=\"text1\">Voici mon projet d'option informatique</textarea>
                      </div>
                    </div>
                      
                    <div class=\"form-group\">
                      <div class=\"col-sm-offset-2 col-sm-10\">
                        <div class=\"checkbox\">
                          <label>
                            <input type=\"checkbox\"> Remember me
                          </label>
                        </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->
                  <div class=\"box-footer\">
                    <button type=\"submit\" class=\"btn btn-default\">Cancel</button>
                    <button type=\"submit\" class=\"btn btn-info pull-right\">Sign in</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
           </div>
    ";
    }

    public function getTemplateName()
    {
        return "AdminBundle:Default:reclamation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* {% block contenu %}*/
/*    */
/*            <div class="col-md-6">*/
/*               <!-- Horizontal Form -->*/
/*               <div class="box box-info">*/
/*                 <div class="box-header with-border">*/
/*                   <h3 class="box-title">Ajouter une promotion</h3>*/
/*                 </div><!-- /.box-header -->*/
/*                 <!-- form start -->*/
/*                 <form class="form-horizontal">*/
/*                   <div class="box-body">*/
/*                       */
/*                     <div class="form-group">*/
/*                       <label for="inputEmail3" class="col-sm-2 control-label">Email</label>*/
/*                       <div class="col-sm-10">*/
/*                         <input type="email" class="form-control" id="inputEmail3" placeholder="Email">*/
/*                       </div>*/
/*                     </div>*/
/*                       */
/*                     <div class="form-group">*/
/*                       <label for="inputPassword3" class="col-sm-2 control-label">Password</label>*/
/*                       <div class="col-sm-10">*/
/*                         <input type="password" class="form-control" id="inputPassword3" placeholder="Password">*/
/*                       </div>*/
/*                     </div>*/
/*                       */
/*                     <div class="form-group">*/
/*                       <label for="inputPassword3" class="col-sm-2 control-label">texte</label>*/
/*                       <div class="col-sm-10">*/
/*                         <textarea rows="5" cols="50" class="form-control" id="text1">Voici mon projet d'option informatique</textarea>*/
/*                       </div>*/
/*                     </div>*/
/*                       */
/*                     <div class="form-group">*/
/*                       <div class="col-sm-offset-2 col-sm-10">*/
/*                         <div class="checkbox">*/
/*                           <label>*/
/*                             <input type="checkbox"> Remember me*/
/*                           </label>*/
/*                         </div>*/
/*                       </div>*/
/*                     </div>*/
/*                   </div><!-- /.box-body -->*/
/*                   <div class="box-footer">*/
/*                     <button type="submit" class="btn btn-default">Cancel</button>*/
/*                     <button type="submit" class="btn btn-info pull-right">Sign in</button>*/
/*                   </div><!-- /.box-footer -->*/
/*                 </form>*/
/*               </div><!-- /.box -->*/
/*            </div>*/
/*     {% endblock %}*/
