<?php

/* PromotionBundle:Promotion:ListeProduitPromotion.html.twig */
class __TwigTemplate_9350fcf75b3545ad5f79d9f64eaed2bc25c98ffb35fe394dcf5fbf5df8f675cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "PromotionBundle:Promotion:ListeProduitPromotion.html.twig", 2);
        $this->blocks = array(
            'HeaderTitle' => array($this, 'block_HeaderTitle'),
            'TableView' => array($this, 'block_TableView'),
            'btn' => array($this, 'block_btn'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_HeaderTitle($context, array $blocks = array())
    {
        // line 5
        echo "    <style>
        img {
            width: 50px;
            height: 50px;
        }
    </style>

    <h1>
        Gestion Des Promotions
        <small>Liste Des Promotions</small>
    </h1>
    <center>
        <form  method=\"POST\">
            <input type=\"text\" name=\"search\">
            <input type=\"submit\" value=\"rechercher\">
        </form>
    </center>
";
    }

    // line 24
    public function block_TableView($context, array $blocks = array())
    {
        // line 25
        echo "    <div style=\"padding: 10px\">
        <div class=\"TableView\" >
            <div class=\"box box-success \">    
                <div  class=\"box-body no-padding\">
                    <table class=\"table table-bordered table-hover\" >
                        
                        <tr>
                          
                            <th>Référence</th>
                            <th>Libelle</th>
                            <th>Description</th>
                            <th>Categorie</th>
                            <th>Image</th>
                            <th>Ajouter/Annuler</th>
                         
                        </tr>
                        <tr> ";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["produits"]) ? $context["produits"] : $this->getContext($context, "produits")));
        foreach ($context['_seq'] as $context["_key"] => $context["produit"]) {
            // line 42
            echo "                          
                            <td>";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($context["produit"], "Reference", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($context["produit"], "Libelle", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["produit"], "Descriptif", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($context["produit"], "Categorie", array()), "html", null, true);
            echo "</td>
                            <td> <img src=\"";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["produit"], "Image", array()), "html", null, true);
            echo "\"/></td>

                            <td><center><a href=\"";
            // line 49
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Accepter_Produit", array("id" => $this->getAttribute($context["produit"], "id", array()), "idPromotion" => (isset($context["idPromotion"]) ? $context["idPromotion"] : $this->getContext($context, "idPromotion")))), "html", null, true);
            echo "\"><input class=\"modifIcon\" type=\"image\"  src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/Responsable/accepter.png"), "html", null, true);
            echo " \" value=\"\" href=\"#\" ></center> </td>
                 
                        </tr>

                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['produit'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "                                    </table>
                                    </div>  </div>         
                                    ";
    }

    // line 57
    public function block_btn($context, array $blocks = array())
    {
        echo "  
                                            <br>   
                                            <div>

                                                <center>  <a href=\"";
        // line 61
        echo $this->env->getExtension('routing')->getPath("promotion_ajouter");
        echo "\"><input type=\"submit\" class=\"btn\" value=\"Ajouter une promotion\"/> 

                                                </center>

                                            </div></div></div>
                                        ";
    }

    public function getTemplateName()
    {
        return "PromotionBundle:Promotion:ListeProduitPromotion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 61,  122 => 57,  116 => 54,  103 => 49,  98 => 47,  94 => 46,  90 => 45,  86 => 44,  82 => 43,  79 => 42,  75 => 41,  57 => 25,  54 => 24,  33 => 5,  30 => 4,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/* */
/* {% block HeaderTitle %}*/
/*     <style>*/
/*         img {*/
/*             width: 50px;*/
/*             height: 50px;*/
/*         }*/
/*     </style>*/
/* */
/*     <h1>*/
/*         Gestion Des Promotions*/
/*         <small>Liste Des Promotions</small>*/
/*     </h1>*/
/*     <center>*/
/*         <form  method="POST">*/
/*             <input type="text" name="search">*/
/*             <input type="submit" value="rechercher">*/
/*         </form>*/
/*     </center>*/
/* {%endblock %}*/
/* */
/* {% block TableView %}*/
/*     <div style="padding: 10px">*/
/*         <div class="TableView" >*/
/*             <div class="box box-success ">    */
/*                 <div  class="box-body no-padding">*/
/*                     <table class="table table-bordered table-hover" >*/
/*                         */
/*                         <tr>*/
/*                           */
/*                             <th>Référence</th>*/
/*                             <th>Libelle</th>*/
/*                             <th>Description</th>*/
/*                             <th>Categorie</th>*/
/*                             <th>Image</th>*/
/*                             <th>Ajouter/Annuler</th>*/
/*                          */
/*                         </tr>*/
/*                         <tr> {% for produit in produits %}*/
/*                           */
/*                             <td>{{produit.Reference}}</td>*/
/*                             <td>{{produit.Libelle}}</td>*/
/*                             <td>{{produit.Descriptif}}</td>*/
/*                             <td>{{produit.Categorie}}</td>*/
/*                             <td> <img src="{{produit.Image}}"/></td>*/
/* */
/*                             <td><center><a href="{{path('Accepter_Produit',{'id':produit.id,'idPromotion':idPromotion})}}"><input class="modifIcon" type="image"  src="{{asset('images/Responsable/accepter.png')}} " value="" href="#" ></center> </td>*/
/*                  */
/*                         </tr>*/
/* */
/*                                 {%endfor%}*/
/*                                     </table>*/
/*                                     </div>  </div>         */
/*                                     {% endblock %} */
/*                                         {%block btn %}  */
/*                                             <br>   */
/*                                             <div>*/
/* */
/*                                                 <center>  <a href="{{path('promotion_ajouter')}}"><input type="submit" class="btn" value="Ajouter une promotion"/> */
/* */
/*                                                 </center>*/
/* */
/*                                             </div></div></div>*/
/*                                         {% endblock %}*/
/* {# empty Twig template #}*/
/* */
