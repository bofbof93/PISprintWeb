<?php

/* ResponsableBundle:Default:Statistiques.html.twig */
class __TwigTemplate_e9131d479e0d45a9083499744e2ed5fbecc04f0adb6af41ca7ed0602f5af6cc6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::Layout.html.twig", "ResponsableBundle:Default:Statistiques.html.twig", 1);
        $this->blocks = array(
            'Content' => array($this, 'block_Content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::Layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_Content($context, array $blocks = array())
    {
        // line 3
        echo "               
            <h1>
            Statistiques
            <small></small>
          </h1>
            ";
    }

    public function getTemplateName()
    {
        return "ResponsableBundle:Default:Statistiques.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "AdminBundle::Layout.html.twig" %}*/
/*  {% block Content %}*/
/*                */
/*             <h1>*/
/*             Statistiques*/
/*             <small></small>*/
/*           </h1>*/
/*             {%endblock %}*/
/* */
