<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // tunisia_mall_actualite_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_actualite_homepage')), array (  '_controller' => 'TunisiaMall\\ActualiteBundle\\Controller\\DefaultController::indexAction',));
        }

        // tunisia_mall_actualite_Ajouter
        if ($pathinfo === '/AjouterActualite') {
            return array (  '_controller' => 'TunisiaMall\\ActualiteBundle\\Controller\\ActualiteController::AjouterAction',  '_route' => 'tunisia_mall_actualite_Ajouter',);
        }

        // tunisia_mall_actualite_List
        if ($pathinfo === '/ListActualites') {
            return array (  '_controller' => 'TunisiaMall\\ActualiteBundle\\Controller\\ActualiteController::ListeActualitesAction',  '_route' => 'tunisia_mall_actualite_List',);
        }

        // tunisia_mall_actualite_Supprimer
        if (0 === strpos($pathinfo, '/SupprimerActualites') && preg_match('#^/SupprimerActualites(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_actualite_Supprimer')), array (  '_controller' => 'TunisiaMall\\ActualiteBundle\\Controller\\ActualiteController::SupprimeAction',));
        }

        // tunisia_mall_actualite_MAJ
        if (0 === strpos($pathinfo, '/MAJ') && preg_match('#^/MAJ(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_actualite_MAJ')), array (  '_controller' => 'TunisiaMall\\ActualiteBundle\\Controller\\ActualiteController::MAJAction',));
        }

        // tunisia_mall_actualite_List_Client
        if ($pathinfo === '/Actualites') {
            return array (  '_controller' => 'TunisiaMall\\ActualiteBundle\\Controller\\ActualiteController::ListeActualitesClientAction',  '_route' => 'tunisia_mall_actualite_List_Client',);
        }

        if (0 === strpos($pathinfo, '/hello')) {
            // users_users_homepage
            if (preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'users_users_homepage')), array (  '_controller' => 'Users\\UsersBundle\\Controller\\DefaultController::indexAction',));
            }

            // tunisia_mall_responsable_homepage
            if (preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisia_mall_responsable_homepage')), array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\DefaultController::indexAction',));
            }

        }

        // tunisia_mall_responsable_affichage
        if ($pathinfo === '/Responsable') {
            return array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\ResponsableController::afficherAction',  '_route' => 'tunisia_mall_responsable_affichage',);
        }

        // tunisia_mall_responsable_GestionComptes
        if ($pathinfo === '/Comptes') {
            return array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\ResponsableController::GestionDesComptesAction',  '_route' => 'tunisia_mall_responsable_GestionComptes',);
        }

        // tunisia_mall_responsable_GestionProduits
        if ($pathinfo === '/Produits') {
            return array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\ResponsableController::GestionDesProduitsAction',  '_route' => 'tunisia_mall_responsable_GestionProduits',);
        }

        // tunisia_mall_responsable_GestionStock
        if ($pathinfo === '/Stock') {
            return array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\ResponsableController::GestionDuStockAction',  '_route' => 'tunisia_mall_responsable_GestionStock',);
        }

        // tunisia_mall_responsable_GestionEnseignes
        if ($pathinfo === '/Enseignes') {
            return array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\ResponsableController::GestionDesEnseignesAction',  '_route' => 'tunisia_mall_responsable_GestionEnseignes',);
        }

        // tunisia_mall_responsable_GestionPub
        if ($pathinfo === '/Pub') {
            return array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\ResponsableController::GestionDesPackPublicitaireAction',  '_route' => 'tunisia_mall_responsable_GestionPub',);
        }

        // tunisia_mall_responsable_GestionCatalogues
        if ($pathinfo === '/Catalogues') {
            return array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\ResponsableController::GestionDesCataloguesAction',  '_route' => 'tunisia_mall_responsable_GestionCatalogues',);
        }

        // tunisia_mall_responsable_Statistique
        if ($pathinfo === '/Stat') {
            return array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\ResponsableController::StatistiqueAction',  '_route' => 'tunisia_mall_responsable_Statistique',);
        }

        // tunisia_mall_Admin_affichage
        if ($pathinfo === '/admin') {
            return array (  '_controller' => 'TunisiaMall\\AdminBundle\\Controller\\AfficheController::afficherAction',  '_route' => 'tunisia_mall_Admin_affichage',);
        }

        // tunisia_mall_Admin_AjouterProduit
        if ($pathinfo === '/AjouterProduit') {
            return array (  '_controller' => 'TunisiaMall\\ResponsableBundle\\Controller\\ResponsableController::AjouterProduitAction',  '_route' => 'tunisia_mall_Admin_AjouterProduit',);
        }

        // tunisia_mall_Admin_stock
        if ($pathinfo === '/formulaire') {
            return array (  '_controller' => 'TunisiaMall\\AdminBundle\\Controller\\AfficheController::formulaireAction',  '_route' => 'tunisia_mall_Admin_stock',);
        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }
                not_fos_user_security_login:

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_security_logout;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }
            not_fos_user_security_logout:

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_profile_edit;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }
            not_fos_user_profile_edit:

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/registration')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/registration') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_registration_register;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                }
                not_fos_user_registration_register:

                if (0 === strpos($pathinfo, '/registration/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/registration/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/registration/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/registration/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/registration/confirmed') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                // fos_user_resetting_request
                if ($pathinfo === '/resetting/request') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ($pathinfo === '/resetting/send-email') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ($pathinfo === '/resetting/check-email') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
                }
                not_fos_user_resetting_reset:

            }

        }

        // fos_user_change_password
        if ($pathinfo === '/change/change-password') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }

            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        // tunisia_mall_client_affichage
        if ($pathinfo === '/acceuil') {
            return array (  '_controller' => 'TunisiaMall\\ClientBundle\\Controller\\AfficheController::afficherAction',  '_route' => 'tunisia_mall_client_affichage',);
        }

        // tunisia_mall_client_catalogue
        if ($pathinfo === '/catalogue') {
            return array (  '_controller' => 'TunisiaMall\\ClientBundle\\Controller\\AfficheController::catalogueAction',  '_route' => 'tunisia_mall_client_catalogue',);
        }

        // tunisia_mall_client_gallerie
        if ($pathinfo === '/gallerie') {
            return array (  '_controller' => 'TunisiaMall\\ClientBundle\\Controller\\AfficheController::gallerieAction',  '_route' => 'tunisia_mall_client_gallerie',);
        }

        // tunisia_mall_client_fashion
        if ($pathinfo === '/fashion') {
            return array (  '_controller' => 'TunisiaMall\\ClientBundle\\Controller\\AfficheController::fashionAction',  '_route' => 'tunisia_mall_client_fashion',);
        }

        // tunisia_mall_client_boutique
        if ($pathinfo === '/boutique') {
            return array (  '_controller' => 'TunisiaMall\\ClientBundle\\Controller\\AfficheController::boutiqueAction',  '_route' => 'tunisia_mall_client_boutique',);
        }

        // tunisia_mall_client_contact
        if ($pathinfo === '/contact') {
            return array (  '_controller' => 'TunisiaMall\\ClientBundle\\Controller\\AfficheController::contactAction',  '_route' => 'tunisia_mall_client_contact',);
        }

        // homepage
        if ($pathinfo === '/app/example') {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
